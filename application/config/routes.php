<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain 6dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'HomeController';
$route['aboutus'] = 'HomeController/aboutUs';
$route['aboutus-2'] = 'HomeController/aboutUs_2';
$route['android'] = 'HomeController/android';
$route['bike'] = 'HomeController/bike';
$route['bills'] = 'HomeController/bills';
$route['cabs'] = 'HomeController/cabs';
$route['career'] = 'HomeController/career';
$route['contact-us'] = 'HomeController/contactUs';
$route['express-deliverydriver-tab'] = 'HomeController/expressDeliveryDriver';
$route['food-deliverydriver-tab'] = 'HomeController/foodDeliveryDriver';
$route['godriver-tab'] = 'HomeController/goDriver';
$route['franchise'] = 'HomeController/franchise';
$route['franchise-sample'] = 'HomeController/franchiseSample';
$route['goads'] = 'HomeController/goAds';
$route['goauto'] = 'HomeController/goAuto';
$route['gobeauty'] = 'HomeController/goBeauty';
$route['gocashback'] = 'HomeController/goCashBack';
$route['godemand'] = 'HomeController/goDemand';
$route['gofood'] = 'HomeController/goFood';
$route['gokiosk'] = 'HomeController/GoKiosk';
$route['goloan'] = 'HomeController/GoLoan';
$route['gomart'] = 'HomeController/GoMart';
$route['gomedical'] = 'HomeController/GoMedical';
$route['gopack'] = 'HomeController/GoPack';
$route['gopay'] = 'HomeController/GoPay';
$route['gopoints'] = 'HomeController/GoPoints';
$route['goupi'] = 'HomeController/GoUpi';
$route['grocery'] = 'HomeController/Grocery';
$route['help'] = 'HomeController/Help';
$route['jobs'] = 'HomeController/Jobs';
$route['login'] = 'HomeController/Login';
$route['login-2'] = 'HomeController/Login_2';
$route['money'] = 'HomeController/money';
$route['press-meet'] = 'HomeController/pressMeet';
$route['pricing'] = 'HomeController/pricing';
$route['privacy-policy'] = 'HomeController/privacyPolicy';
$route['promotor'] = 'HomeController/promotor';
$route['safety'] = 'HomeController/safety';
$route['send'] = 'HomeController/send';
$route['signup'] = 'HomeController/signup';
$route['tab'] = 'HomeController/tab';
$route['tracking'] = 'HomeController/tracking';



$route['api'] = 'ApiController/index';
$route['api/signup'] = 'ApiController/signup';
$route['api/login'] = 'ApiController/login';
$route['api/forgotpassword'] = 'ApiController/forgotPassword';
$route['api/reset-password'] = 'ApiController/resetPassword';
$route['api/forgotTokenActivate/(:any)'] = 'ApiController/forgotTokenActivate/$1';
$route['api/userPasswordReset/(:any)'] = 'ApiController/userPasswordReset/$1';
$route['api/userPasswordChange'] = 'ApiController/userPasswordChange';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin'] = 'AdminController/index';
$route['admin/login'] = 'AdminController/login';
$route['admin/dashboard'] = 'AdminController/dashboard';

$route['admin/update-status'] = 'AdminController/updateStatus';

$route['admin/set-cost/(:any)'] = 'AdminController/setCost/$1';
$route['admin/change-commission-status/(:num)/(:any)/(:num)/(:num)'] = 'AdminController/changeCommissionStatus/$1/$2/$3/$4';
$route['admin/view-commission/(:any)'] = 'AdminController/viewCommission/$1';
$route['admin/change-peakhour-status/(:num)/(:any)/(:num)'] = 'AdminController/changePeakHourStatus/$1/$2/$3';
$route['admin/commissions/(:any)'] = 'AdminController/commissions/$1';
$route['admin/update-peak-hours'] = 'AdminController/updatePeakHours';   
$route['admin/delete-peak-hours'] = 'AdminController/deletePeakHours';
$route['admin/response'] = 'AdminController/response';
$route['admin/payment'] = 'AdminController/payment';
$route['admin/notify'] = 'AdminController/notify';
$route['admin/request'] = 'AdminController/request';
$route['admin/topup/(:any)/(:num)'] = 'AdminController/topup/$1/$2'; 
$route['admin/alltransactions/(:num)'] = 'AdminController/allTransactions/$1';   
$route['admin/collections/(:any)'] = 'AdminController/driverCollections/$1'; 
$route['admin/collectamount'] = 'AdminController/collectAmount';   
$route['admin/withdraw-requests/(:any)'] = 'AdminController/withdrawRequests/$1'; 
$route['admin/change-withdraw-status'] = 'AdminController/changeWithdrawStatus';
$route['admin/zones'] = 'AdminController/zones';
$route['admin/add-zone'] = 'AdminController/addZone';
$route['admin/edit-zone/(:any)'] = 'AdminController/editZone/$1';
$route['admin/delete-zone/(:any)'] = 'AdminController/deleteZone/$1';
$route['admin/delete-user/(:any)'] = 'AdminController/deleteUser/$1';
$route['admin/view-user/(:any)'] = 'AdminController/viewUser/$1';
$route['admin/view-driver/(:any)'] = 'AdminController/viewDriver/$1';
$route['admin/rides'] = 'AdminController/rides';
$route['admin/users'] = 'AdminController/Users'; 
$route['admin/get-zone-drivers'] = 'AdminController/GetZoneDrivers';
$route['admin/view-ride-details/(:any)'] = 'AdminController/ViewRideDetails/$1';
$route['admin/manual-transactions/(:any)'] = 'AdminController/manualTransactions/$1';
$route['admin/transaction-details/(:any)'] = 'AdminController/transactionDetails/$1';
$route['admin/change-transaction-status'] = 'AdminController/changeTransactionStatus';

$route['admin/logout'] = 'AdminController/logout';

$route['api/test'] = 'ApiController/test';
$route['api/verify-otp'] = 'ApiController/verifyOtp';
$route['api/get-wallet-balance'] = 'ApiController/getWalletBalance';
$route['api/get-profile-details'] = 'ApiController/getProfileDetails';
$route['api/upload-kyc-documents'] = 'ApiController/uploadKycDocuments';
$route['api/rating'] = 'ApiController/updateRating';
$route['api/calculate-ride-fare'] = 'ApiController/calculateRideFare';
$route['api/update-driver-location'] = 'ApiController/updateDriverLocation';
$route['api/change-vehicle-details'] = 'ApiController/changeVehicleDetails';
$route['api/change-account-details'] = 'ApiController/changeAccountDetails';
$route['api/get-rc-details'] = 'ApiController/getRC';
$route['api/nearby-drivers'] = 'ApiController/getDriversNearBy';
$route['api/nearby-drivers-zone'] = 'ApiController/getDriversNearByZone';
$route['api/confirm-ride-order'] = 'ApiController/confirmRideOrder';
$route['api/accept-ride-order'] = 'ApiController/acceptRideOrder';
$route['api/start-ride-order'] = 'ApiController/startRideOrder';
$route['api/complete-ride-order'] = 'ApiController/completeRideOrder';
$route['api/get-countries'] = 'ApiController/getCountries';
$route['api/get-driver-location'] = 'ApiController/getDriverLocation';
$route['api/send-withdraw-request'] = 'ApiController/withdrawRequest';
$route['api/provider-details'] = 'ApiController/providerDetails';
$route['api/provider-categories'] = 'ApiController/providerCategories';
$route['api/list-of-banks'] = 'ApiController/getListOfBanks';
$route['api/send-payment-details'] = 'ApiController/sendPaymentDetails';
$route['api/get-ride-status'] = 'ApiController/getRideStatus';

$route['registration'] = 'RegistrationController/index';
$route['registration/gocab'] = 'RegistrationController/gocab';
$route['registration/save'] = 'RegistrationController/saveRegistrationDetails';
$route['registration/thank-you'] = 'RegistrationController/thankyou';
$route['registration/goride'] = 'RegistrationController/goride';
$route['registration/goauto'] = 'RegistrationController/goauto';
$route['registration/gofood'] = 'RegistrationController/gofood';
$route['registration/gofood-deliverypartner'] = 'RegistrationController/gofoodDeliveryPartner';
$route['registration/gofood-save'] = 'RegistrationController/saveRestaurantDetails';
$route['registration/gobox'] = 'RegistrationController/gobox';
$route['registration/gosend'] = 'RegistrationController/gosend';
$route['registration/gocargo'] = 'RegistrationController/gocargo';

$route['admin/listdrivers/gocab'] = 'AdminController/listDriverGocab';
$route['admin/listdrivers/details'] = 'AdminController/listDriverDetails';
$route['admin/listdrivers/update'] = 'AdminController/updateDriverDetails';
$route['admin/listdrivers/delete'] = 'AdminController/deleteDriverDetails';
$route['admin/listdrivers/verify'] = 'AdminController/verifyStatus';
$route['admin/listdrivers/goride'] = 'AdminController/listDriverGoride';
$route['admin/listdrivers/goauto'] = 'AdminController/listDriverGoauto';
$route['admin/listdrivers/gofood-deliverypartner'] = 'AdminController/listDriverGofoodDeliveryPartner';
$route['admin/listrestaurants/gofood'] = 'AdminController/listRestaurantGofood';
$route['admin/listrestaurants/details'] = 'AdminController/listRestaurantDetails';
$route['admin/listrestaurants/update'] = 'AdminController/updateRestaurantDetails';
$route['admin/listrestaurants/delete'] = 'AdminController/deleteRestaurantDetails';
$route['admin/listrestaurants/verify'] = 'AdminController/verifyRestaurantStatus';

$route['admin/listdrivers/gobox'] = 'AdminController/listDriverGobox';
$route['admin/listdrivers/gosend'] = 'AdminController/listDriverGosend';
$route['admin/listdrivers/gocargo'] = 'AdminController/listDriverGocargo';

$route['admin/add-driver/gocab'] = 'AdminController/addDriverGocab';
$route['admin/add-driver/goride'] = 'AdminController/addDriverGoride';
$route['admin/add-driver/goauto'] = 'AdminController/addDriverGoauto';
$route['admin/add-driver/gofood-deliverypartner'] = 'AdminController/addDriverGofoodDeliveryPartner';
$route['admin/add-driver/gobox'] = 'AdminController/addDriverGobox';
$route['admin/add-driver/gosend'] = 'AdminController/addDriverGosend';
$route['admin/add-driver/gocargo'] = 'AdminController/addDriverGocargo';
$route['admin/add-driver/save'] = 'AdminController/saveDriverDetails';
$route['admin/add-restaurant/save'] = 'AdminController/saveRestaurantDetails';
$route['admin/add-driver/thank-you'] = 'AdminController/thankyou';
$route['admin/add-restaurant/gofood'] = 'AdminController/addRestaurantGofood';

$route['admin/approve-driver/gocab'] = 'AdminController/approveGocabDriver';
$route['admin/approve-driver/goride'] = 'AdminController/approveGorideDriver';
$route['admin/approve-driver/goauto'] = 'AdminController/approveGoautoDriver';
$route['admin/approve-driver/gofood-deliverypartner'] = 'AdminController/approveGofoodDeliveryPartner';
$route['admin/approve-restaurant/gofood'] = 'AdminController/approveGofoodRestaurant';
$route['api/update-driver-status'] = 'ApiController/updateDriverStatus';
$route['admin/approve-driver/gobox'] = 'AdminController/approveGoboxDriver';
$route['admin/approve-driver/gosend'] = 'AdminController/approveGosendDriver';
$route['admin/approve-driver/gocargo'] = 'AdminController/approveGocargoDriver';
$route['api/get-driver-status'] = 'ApiController/getDriverStatus';
$route['api/update-profile-details'] = 'ApiController/updateProfileDetails';
$route['api/change-password'] = 'ApiController/changePassword';
$route['api/get-promocode'] = 'ApiController/getPromocode';
$route['api/update-registration-id'] = 'ApiController/updateRegistrationId';
$route['api/get-country-list'] = 'ApiController/getCountryList';
$route['api/generate-order'] = 'ApiController/generateOrder';
$route['api/get-ride-driver-details'] = 'ApiController/getRideDriverDetails';
$route['api/get-ride-customer-details'] = 'ApiController/getRideCustomerDetails';
$route['api/phone-number-exists'] = 'ApiController/isPhonenumberExists';

$route['api/get-kyc-documents'] = 'ApiController/getKycDocuments';
$route['api/get-vehicle-documents'] = 'ApiController/getVehicleDocuments';
$route['api/get-gocab-list'] = 'ApiController/getGoCabList';
$route['api/search-by-keyword'] = 'ApiController/searchByKeywordProvider';
$route['api/get-providers-nearby'] = 'ApiController/getProvidersNearBy';
$route['api/ride-history'] = 'ApiController/getRideHistory';
$route['api/wallet-transaction-history'] = 'ApiController/getWalletTransactionHistory';
$route['api/withdraw-request'] = 'ApiController/getWithdrawRequest';
