<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require FCPATH . 'vendor/autoload.php';

require_once('ApiServiceController.php');

class ApiController extends ApiServiceController {

    public function __construct() {
		parent::__construct();
        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']);
    }
    
    public function test_get() {
        $tokenData = 'Hello World!';
        
        $dummy_user = [
            'UserId' => '1'
        ];


        // // Create a token
        $token = AUTHORIZATION::generateToken(['UserId' => $dummy_user['UserId']]);
        // // Set HTTP status code
        $status = parent::HTTP_OK;
        // // Prepare the response
        $response = ['status' => $status, 'token' => $token];
        // REST_Controller provide this method to send responses
        $this->response($response, $status);
    }

    private function verifyRequest() {
        // Get all the headers
        $headers = $this->input->request_headers();
        // Extract the token
        $token = (isset($headers['X-Authorization'])) ? $headers['X-Authorization'] : null;
        // Use try-catch
        // JWT library throws exception if the token is not valid
        try {
            // Validate the token
            // Successfull validation will return the decoded user data else returns false
            $data = AUTHORIZATION::validateToken($token);
            if ($data === false) {
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                $this->response($response, $status);
                exit();
            } else {
                return $data;
            }
        } catch (Exception $e) {
            // Token is invalid
            // Send the unathorized access message
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
            exit();
        }
    }

    public function getWalletBalance_get() {

        $response = array(
            "status" => true,
            "walletBalance" => 0
        );

        $decoded = $this->verifyRequest();

        $response['walletBalance'] = $this->_getUserWalletBalance($decoded->UserId)[0]->WalletBalance;

        $this->response($response, parent::HTTP_OK);

    }

    public function getProfileDetails_get() {

        $response = array(
            "status" => true,
            "profileDetails" => array()
        );

        $decoded = $this->verifyRequest();

        $response['profileDetails'] = $this->_getProfileDetails($decoded->UserId)[0];

        $this->response($response, parent::HTTP_OK);

    }

    public function updateProfileDetails_post() {
        $response = array(
            "status" => false,
            "message" => "Something Goes Wrong"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($postData->profilePicture) {
            $base64Decoded = base64_decode($postData->profilePicture);
            $profilePicture = "uploads/dp/" . $decoded->UserId . '-DB-' . time() . ".png";
            file_put_contents($profilePicture, $base64Decoded);
        }

        $data = array(
            'FirstName' => $postData->firstName,
            'LastName' => $postData->lastName,
            'Email' => $postData->email,
            'DOB' => $postData->dob,
            'PlaceOfBirth' => $postData->placeOfBirth
        );

        if($postData->profilePicture) {
            $data['DisplayPicture'] = $profilePicture;
        }

        if($this->_updateProfileDetails($decoded->UserId, $data)) {
            $response = array(
                "status" => true,
                "message" => "Profile Details Updated"
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function changePassword_post() {
        $response = array(
            "status" => false,
            "message" => "Something Goes Wrong"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($postData->password != $postData->retypePassword) {
            $response = array(
                "status" => false,
                "message" => "Password mismatch!"
            );
        } else {
            $data = array(
                'Password' => password_hash($postData->password, PASSWORD_DEFAULT)
            );
    
            if($this->_updateProfileDetails($decoded->UserId, $data)) {
                $response = array(
                    "status" => true,
                    "message" => "Password updated"
                );
            }

        }

        $this->response($response, parent::HTTP_OK);
    }

    public function updateRegistrationId_post() {
        $response = array(
            "status" => false,
            "message" => "Something Goes Wrong"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        $data = array(
            'RegistrationId' => $postData->registrationId
        );

        if($this->_updateRegistrationId($decoded->UserId, $data)) {
            $response = array(
                "status" => true,
                "message" => "Registration Id Updated"
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function getDriverStatus_get() {

        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();


        if($this->_getDriverStatus($decoded->UserId)) {
            $response = array(
                "status" => true,
                "driverStatus" => $this->_getDriverStatus($decoded->UserId)['0']->Available
            );
        }
        
        $this->response($response, parent::HTTP_OK);

    }

    public function updateDriverStatus_post() {

        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        try {

            if($this->_updateDriverStatus($decoded->UserId, $postData->driverStatus)) {
                $response = array(
                    "status" => true,
                    "message" => "Driver status updated"
                );
            }
            
        } catch (Exception $ex) {
            $response = array(
                "status" => false,
                "message" => "Something goes wrong"
            );
        }

        $this->response($response, parent::HTTP_OK);

    }

    public function uploadKycDocuments_post() {
        $response = array(
            "status" => false,
            "message" => "Something Goes Wrong"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        $base64Decoded = base64_decode($postData->aadharDocumentFront);
        $aadharDocumentNameFront = "uploads/kyc/" . $decoded->UserId . '-AadharFront-' . time() . ".png";
        file_put_contents($aadharDocumentNameFront, $base64Decoded);

        $base64Decoded = base64_decode($postData->aadharDocumentBack);
        $aadharDocumentNameBack = "uploads/kyc/" . $decoded->UserId . '-AadharBack-' . time() . ".png";
        file_put_contents($aadharDocumentNameBack, $base64Decoded);

        $base64Decoded = base64_decode($postData->panDocument);
        $panDocumentName = "uploads/kyc/" . $decoded->UserId . '-Pan-' . time() . ".png";
        file_put_contents($panDocumentName, $base64Decoded);

        $data = array(
            'aadharNo' => $postData->aadharNo,
            'aadharDocumentFront' => $aadharDocumentNameFront,
            'aadharDocumentBack' => $aadharDocumentNameBack,
            'panNo' => $postData->panNo,
            'panDocument' => $panDocumentName
        );

        if($this->_updateKycDocuments($decoded->UserId, $data)) {
            $response = array(
                "status" => true,
                "message" => "KYC Updated"
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function getKycDocuments_get() {
        $decoded = $this->verifyRequest();

        $response = array(
            'status' => true,
            'kycDocuments' => $this->_getKycDocuments($decoded->UserId)
        );

        $this->response($response, parent::HTTP_OK);
    }

    public function getVehicleDocuments_get() {
        $decoded = $this->verifyRequest();

        $response = array(
            'status' => true,
            'vehicleDocuments' => $this->_getVehicleDocuments($decoded->UserId)
        );

        $this->response($response, parent::HTTP_OK);
    }

    public function updateRating_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Ride ID"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($this->_updateRating($decoded->UserId, $postData->txnId, $postData->ratedBy, $postData->rating , $postData->feedback)) {
            $response = array(
                "status" => true,
                "message" => "Rating Updated"
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function calculateRideFare_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        $consolidatedFare = $this->_calculateRideFare($postData->kilometer, $postData->time, $postData->pincode, $postData->vehicleType);

        if($consolidatedFare) {
            $response = array(
                "status" => true,
                "fareDetails" => $consolidatedFare
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function updateDriverLocation_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($postData->latitude != '' && $postData->longitude != '') {
            $this->_updateDriverLocation($decoded->UserId, $postData->latitude, $postData->longitude);
            $response = array(
                "status" => true,
                "message" => 'Location Updated'
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function getDriversNearBy_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($postData->latitude != '' && $postData->longitude != '') {
            $response = array(
                "status" => true,
                "drivers" => $this->_getDriversNearBy($postData->latitude, $postData->longitude, $postData->vehicleType)
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function getDriversNearByZone_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $regions = $this->_getPincodes($this->input->post('id'));
        $output = array();
        if($regions) {
             $pincodes = array();
             foreach($regions as $reg) {
                    $pincodes[] = $reg->Pincode;
             }
            $drivers = $this->_getDrivers($pincodes);
           
            foreach ($drivers as $row) {
                $location = $this->_getDriversNearByZone($row->UserId);
                $driverDetails = array();
                $driverDetails['location'] = $location; 
                $driverDetails['Name'] = $row->FirstName;
                $driverDetails['DisplayPicture'] = $row->DisplayPicture;
                $driverDetails['JobCategory_JobCategoryId'] = $row->JobCategory_JobCategoryId;
                $driverDetails['UserId'] = $row->UserId;
                $output[] = $driverDetails;
            }
            
            // echo json_encode($output);
            // print_r($output);
            // return $drivers;
            //$this->load->view('admin/_nearbydrivers',$userData); 
        }

        $this->response(array(
            'status' => true,
            'drivers' => $output
        ), parent::HTTP_OK);

        
            // $response = array(
            //     "status" => true,
            //     "drivers" => $this->_getDriversNearByZone($this->input->post('id'))
            // );

            // print_r($response);
        // print_r($drivers);

        //echo $this->input->post('id');

        //$this->response($response, parent::HTTP_OK);
    }

    public function searchByKeywordProvider_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($postData->latitude != '' && $postData->longitude != '' && $postData->providerType != '' && $postData->keyword != '') {
            $response = array(
                "status" => true,
                "restaurant" => $this->_searchByKeywordProvider($postData->keyword, $postData->latitude, $postData->longitude, $postData->providerType),
                "items" => $this->_searchByKeywordItem($postData->keyword, $postData->latitude, $postData->longitude, $postData->providerType)
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function getDriverLocation_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($postData->rideId != '' && $postData->vehicleType != '') {
            $response = array(
                "status" => true,
                "drivers" => $this->_getDriverLocation($postData->rideId, $postData->vehicleType)
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function confirmRideOrder_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($postData->vehicleType != '' && $postData->sourceLatitude != '' && $postData->sourceLongitude != '' && $postData->destinationLatitude != '' && $postData->destinationLongitude != '' && $postData->estimationFare != '' && $postData->paymentMode != '') {

            $rideDetails = array(
                'vehicleType' => $postData->vehicleType,
                'sourceLatitude' => $postData->sourceLatitude,
                'sourceLongitude' => $postData->sourceLongitude,
                'destinationLatitude' => $postData->destinationLatitude,
                'destinationLongitude' => $postData->destinationLongitude,
                'estimationFare' => $postData->estimationFare,
                'sourceAddress' => $postData->sourceAddress,
                'destinationAddress' => $postData->destinationAddress,
                'distance' => $postData->distance,
                'estimatedTime' => $postData->estimatedTime,
                'ridePath' => $postData->ridePath,
                'paymentMode' => $postData->paymentMode
            );

            $ride = $this->_confirmRideOrder($decoded->UserId, $rideDetails);

            if($ride['rideId'] == 'InsufficientFund') {
                $response = array(
                    "status" => false,
                    "orderStatus" => "Insufficient Fund in User Wallet"
                );
            } elseif ($ride['rideId']) {
                $response = array(
                    "status" => true,
                    "rideId" => $ride['rideId'],
                    "otp" => $ride['otp'],
                    "orderStatus" => "Order confirmed"
                );
            } else {
                $response = array(
                    "status" => false,
                    "orderStatus" => "Order not confirmed"
                );
            }
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function acceptRideOrder_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($postData->vehicleType != '' && $postData->rideId != '') {

            $rideDetails = array(
                'vehicleType' => $postData->vehicleType,
                'rideId' => $postData->rideId
            );

            $acceptStatus = $this->_acceptRideOrder($decoded->UserId, $rideDetails);

            if($acceptStatus) {
                $response = array(
                    "status" => true,
                    "orderStatus" => "Order status accepted"
                );
            } else {
                $response = array(
                    "status" => false,
                    "orderStatus" => "Order status not accepted"
                );
            }
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function startRideOrder_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($postData->vehicleType != '' && $postData->rideId != '' && $postData->otp != '') {

            $rideDetails = array(
                'vehicleType' => $postData->vehicleType,
                'rideId' => $postData->rideId,
                'otp' => $postData->otp
            );

            $startRideStatus = $this->_startRideOrder($decoded->UserId, $rideDetails);

            if($startRideStatus) {
                $response = array(
                    "status" => true,
                    "orderStatus" => "Order status started"
                );
            } else {
                $response = array(
                    "status" => false,
                    "orderStatus" => "Invalid Otp"
                );
            }
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function completeRideOrder_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        $consolidatedFare = $this->_completeRideFare($decoded->UserId, $postData->kilometer, $postData->orderTime, $postData->pincode, $postData->vehicleType, $postData->rideId);

        if($consolidatedFare) {
            $response = array(
                "status" => true,
                "fareDetails" => $consolidatedFare
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function signup_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($postData->firstName == '' || $postData->lastName == '' || $postData->email == '' || $postData->phone == '' || $postData->placeOfBirth == '' || $postData->password == '' || $postData->dob == '') {
            $this->response(array(
                "status"=>"false",
                "message"=>"Please fill all the required fields"
            ), parent::HTTP_OK);
        } else {
            //checking for valid email
            if(!$this->valid_email($postData->email)) {
                $this->response(array(
                    "status"=>"false",
                    "message"=>"Please enter valid email"
                ), parent::HTTP_OK);
            }
            
            //checking for valid mobile
            if(!$this->valid_mobile($postData->phone)) {
                $this->response(array(
                    "status"=>"false",
                    "message"=>"Please enter valid mobile number"
                ), parent::HTTP_OK);
            }

            $dataReturned = $this->_userSignup($postData);

            if($dataReturned) {
                $this->response(array(
                    "status"=>"true",
                    "message"=>"User Registered Successfully"
                ), parent::HTTP_OK);
            } else {
                $this->response(array(
                    "status"=>"false",
                    "message"=>"Woops Something error.."
                ), parent::HTTP_OK);
            }
        }
    }

    public function valid_email($str) {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
    }

    public function valid_mobile($mobile) {
        return preg_match('/^[0-9]{10}+$/', $mobile);
    }

    public function login_post() {

        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        if($postData->phone == '' || $postData->password == '' || $postData->registrationId == '' || $postData->userType == '') {
            $this->response(array(
                "status"=>"false",
                "message"=>"Please fill all the required fields.."
            ), parent::HTTP_OK);
        } else {
            $isLogin = $this->_userLogin($postData);
            if($isLogin) {
                $token = AUTHORIZATION::generateToken([
                    'UserId' => $isLogin['UserId'],
                    'UserType' => $isLogin['UserType']
                ]);

                $data = array(
                    'RegistrationId' => $postData->registrationId
                );

                if($this->_updateRegistrationId($isLogin['UserId'], $data)) {
                    $response = array(
                        "status"=>"true",
                        "message"=>"Successfully Logged in..",
                        "token" => $token,
                        "userId" => $isLogin['UserId']
                    );
                    
                    $response['profileDetails'] = $this->_getProfileDetails($isLogin['UserId'])[0];

                    if($postData->userType=='D')
                    {
                        if($vehicle = $this->_getVehicleType($isLogin['UserId'])) {
                        $response['vehicleType'] = $vehicle->JobCategory_JobCategoryId;
                        } else {
                            $response['vehicleType'] = '';
                        }
                    }

                    $this->response($response, parent::HTTP_OK);
                } else {
                    $this->response(array(
                        "status"=>"false",
                        "message"=>"Something goes wrong!"
                    ), parent::HTTP_OK);
                }
                
            } else {
                $this->response(array(
                    "status"=>"false",
                    "message"=>"Invalid login credetials.."
                ), parent::HTTP_OK);
            }
        }
    }
    public function forgotPassword_post() {
            $token = $this->tokencreation();
            $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
            $postData = json_decode($stream_clean);
            $mobile = $postData->mobile;
            $otp = mt_rand(100000, 999999);
            $data=array(
                'mobile'=> $mobile,
                'otp'=>$otp,
                'userType'=>$postData->userType
             );
           
            $isEmailExists=$this->_getUserMobile($data);
            if($isEmailExists)
                {
                    if($this->_sendOtp($data)) {
                    $error=array(
                        'status'=> true,
                        'message'=>'OTP has been sent to your mobile..'
                    );
                    } else {
                        $error=array(
                            'status'=> false,
                            'message'=>'Something Goes wrong..'
                        );
                    }
                }
                else
                {
                    $error=array(
                        'status'=> false,
                        'message'=>'Mobile Number Not Found..',
                    );
                }
            
            $this->response($error, parent::HTTP_OK);
    }

    public function verifyOtp_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        $otp = $postData->otp;
        $verifyData = array(
            "ForgotPasswordOtp"=>$otp,
            "Phone"=>$postData->mobile,
        );
        
        $response=array(
                        'status'=> false,
                        'message'=>'Invalid Otp..',
        );
        if($this->_verifyOtp($verifyData)){
                 $response=array(
                            'status'=> true,
                            'message'=>'Otp verified successfully..',
                 );
        }
        $this->response($response, parent::HTTP_OK);
    }

    public function resetPassword_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        $mobile = $postData->mobile;
        $password = $postData->password;
        $confirmPassword = $postData->confirmPassword;

        $response=array(
                        'status'=> false,
                        'message'=>'Something Goes Wrong..',
        );

        if($password==$confirmPassword) {
            $newPassword = password_hash($password, PASSWORD_BCRYPT);
            if($this->_resetPassword($mobile,$newPassword)) {
                $response=array(
                            'status'=> true,
                            'message'=>'Password has been changed successfully..',
                 );
            }
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function tokencreation() {
		$chars = "abcdefghijkmnopqrstuvwxyz023456789"; 
		srand((double)microtime()*1000000);
		$i = 0; 
		$pass = '' ;
		 while ($i <= 8)
		 { 
			$num = rand() % 33; 
			$tmp = substr($chars, $num, 1); 
			$pass= $pass . $tmp;
			$i++;
		 }
		 $pass = md5($pass);
		 return $pass;
    }

    public function forgotTokenActivate_get($token) {
        $isAvailable=$this->_forgotToken($token);
        if($isAvailable)
        {
            $success=array(
                'status'=> true,
                'message'=>'Forgot Token matched',
             );
             $this->response($success, parent::HTTP_OK);
        }
        else{
            $success=array(
                'status'=> false,
                'message'=>'Invalid Token..',
             );
             $this->response($success, parent::HTTP_OK);
        }
    }
    
    public function userPasswordReset_get($token) {
        if($this->input->post('password')=='')
        {
            $error=array(
                'status'=> false,
                'message'=>'Please fill password',
             );
             $this->response($error, parent::HTTP_OK);
        }
        
        $password=password_hash($this->input->post('password'),PASSWORD_DEFAULT);
        $data=array(
            'token'=> $token,
            'password'=>$password,
         );

        $isReset=$this->_resetPassword($data);
        if($isReset)
        {
            $success=array(
                'status'=> true,
                'message'=>'Password has been changed successfully..',
             );
             $this->response($success, parent::HTTP_OK);
        }
        else{
            $success=array(
                'status'=> false,
                'message'=>'Something went wrong',
             );
             $this->response($success, parent::HTTP_OK);
        }
    }

    public function userPasswordChange_get() {
        $id=21;; // loggined user id
        $newPassword=$this->input->get('newPassword');
        $confirmPassword=$this->input->get('confirmPassword');
        if($newPassword=='' || $confirmPassword=='')
        {
            $error=array(
                'status'=> true,
                'message'=>'Please fill all the required fields..',
             );
            $this->response($error, parent::HTTP_OK);
        }
        if($newPassword!=$confirmPassword)
        {
            $error=array(
                'status'=> true,
                'message'=>'Confirm Password Not Match..',
             );
            $this->response($error, parent::HTTP_OK);
        }
        $password=password_hash($newPassword,PASSWORD_DEFAULT);
        $data=array(
            'id'=> $id,
            'password'=>$password,
         );

        $isChange=$this->_changePassword($data);
        if($isChange)
        {
            $success=array(
                'status'=> true,
                'message'=>'Password has been changed successfully..',
             );
             $this->response($success, parent::HTTP_OK);
        }
        else{
            $error=array(
                'status'=> false,
                'message'=>'Something went wrong',
             );
             $this->response($error, parent::HTTP_OK);
        }
    }

    public function getCountries_get() {

        $response = array(
            "status" => true,
            "countries" => []
        );

        $response['countries'] = $this->_getCountries();

        $this->response($response, parent::HTTP_OK);

    }

    public function getPromocode_get() {
        $response = array(
            "status" => false,
            "message" => "Something Goes Wrong"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        $response = array(
            "status" => true,
            "promocodes" => $this->_getPromocode($postData->vehicleType)
        );

        $this->response($response, parent::HTTP_OK);
    }

    public function getCountryList_get() {
        $this->response(
            json_decode(file_get_contents(FCPATH."/assets/list_country.json")),
            parent::HTTP_OK
        );
    }

    public function withdrawRequest_post() {
        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        $response = array(
            "status" => false,
            "message" => "Something Goes Wrong",
        );

        $requestData = array(
            'Users_UserId' => $decoded->UserId,
            'Amount' => $postData->amount,
            'Status' => 'P'
        );

        if($this->_getUserWalletBalance($decoded->UserId)[0]->WalletBalance < $postData->amount) {
            $response = array(
                "status" => false,
                "message" => "Requested amount is larger than wallet balance..",
            );
            
        } else {

            if($this->_saveWithdrawRequest($requestData)) {
                $response = array(
                    "status" => true,
                    "message" => "Withdraw Request Accepted. Will process your request soon.",
                );
            }
        }
        $this->response($response, parent::HTTP_OK);
    }
            
    public function generateOrder_post() {
        // $response = array(
        //     "status" => false,
        //     "message" => "Something Goes Wrong"
        // );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        $response = array(
            "status" => true,
            "orderDetails" => $this->_generateOrder($decoded->UserId, $decoded->UserType, $postData->amount)
        );

        $this->response($response, parent::HTTP_OK);
    }

    public function getRideDriverDetails_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        $response = array(
            "status" => true,
            "driverDetails" => $this->_getRideDriverDetails($decoded->UserId, $postData->rideId)
        );

        $this->response($response, parent::HTTP_OK);
    }

    public function getRideCustomerDetails_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        $response = array(
            "status" => true,
            "customerDetails" => $this->_getRideCustomerDetails($decoded->UserId, $postData->rideId)
        );

        $this->response($response, parent::HTTP_OK);
    }

    public function isPhonenumberExists_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        $isPhoneExists = $this->_isPhonenumberExists($postData->phone);

        $response = array(
            "status" => ($isPhoneExists >= 1) ? false : true,
            "message" => ($isPhoneExists >= 1) ? 'Already Exists' : 'Valid'
        );

        $this->response($response, parent::HTTP_OK);
    }

    public function getGoCabList_get() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );
        
        $decoded = $this->verifyRequest();

        $response = array(
            "status" => true,
            "goCabList" => $this->_getGoCabList()
        );
        
        $this->response($response, parent::HTTP_OK);
    }
    
    public function providerDetails_get() {

        $decoded = $this->verifyRequest();
        $providerDetails = $this->_getProviderDetails($this->input->get('userId'));
          
        $this->response($providerDetails, parent::HTTP_OK);

    }

    public function providerCategories_get() {

        $decoded = $this->verifyRequest();
        $categories = $this->_getAllProviderCategories($this->input->get('categoryId'));
          
        $this->response($categories, parent::HTTP_OK);

    }

    public function getProvidersNearBy_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($postData->latitude != '' && $postData->longitude != '') {
            $response = array(
                "status" => true,
                "providers" => $this->_getProvidersNearBy($postData->latitude, $postData->longitude, $postData->providerType)
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function getRideHistory_get() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($decoded->UserId != '' && $decoded->UserType != '') {
            $response = array(
                "status" => true,
                "history" => $this->_getRideHistory($decoded->UserId,$decoded->UserType)
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function getWalletTransactionHistory_get() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($decoded->UserId != '' && $decoded->UserType != '') {
            $response = array(
                "status" => true,
                "id" => $decoded->UserId,
                "type" => $decoded->UserType,
                "history" => $this->_getWalletTransactionHistory($decoded->UserId,$decoded->UserType)
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function getWithdrawRequest_post() {

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        if($postData->amount != '') {
            $status = $this->_sendWithdrawRequest($decoded->UserId, $postData->amount);
           if($status == 'SUCCESS') {
                $response = array(
                    "status" => true,
                    "message" =>'Withdraw Successfull..'
                );
           } else {
                    $response = array(
                        "status" => false,
                        "message" => $status,
                    );
           }

        }

        $this->response($response, parent::HTTP_OK);
    }

    public function changeVehicleDetails_post() {

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        $response = array(
            "status" => false,
            "message" =>'Something Goes Wrong..',
        );
        if($this->_changeVehicleDetails($postData)){
            $response = array(
                "status" => true,
                "message" =>'Vehicle Details Changed Successfully..',
            );
        } 
        $this->response($response, parent::HTTP_OK);
    }

    public function changeAccountDetails_post() {

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        $response = array(
            "status" => false,
            "message" =>'Something Goes Wrong..',
        );
        if($this->_changeAccountDetails($postData)){
            $response = array(
                "status" => true,
                "message" =>'Vehicle Details Changed Successfully..',
            );
        } 
        $this->response($response, parent::HTTP_OK);
    }

    public function getRC_post() {

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        $response = array(
            "status" => false,
            "message" =>'Something Goes Wrong..',
        );
        if($this->_getRC($postData->vehicleId)){
            $response = array(
                "status" => true,
                "vehiclePermit" => $this->_getRC($postData->vehicleId),
            );
        } 
        $this->response($response, parent::HTTP_OK);
    }

    public function getListOfBanks_get() {

        $response = array(
            "status" => true,
            "banks" => $this->_getListOfBanks()
        );

        $this->response($response, parent::HTTP_OK);
    }

    public function sendPaymentDetails_post() {

        $decoded = $this->verifyRequest();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        $response = array(
            "status" => false,
            "message" =>'Something Goes Wrong..',
        );
        if($postData->userId!='' && $postData->bankId!='' && $postData->bankName!='' && $postData->ifsc!='' && $postData->branchName!='' && $postData->accountNumber!='' && $postData->accountHolderName!='' && $postData->evidenceDocument!='' && $postData->amount!='' ){
            
            $base64Decoded = base64_decode($postData->evidenceDocument);
            $evidence = "uploads/evidence/" . $decoded->UserId . '-EvidenceDocument-' . time() . ".png";
            file_put_contents($evidence, $base64Decoded);

            if($this->_saveTransaction($postData,$evidence)) {
                $response = array(
                    "status" => true,
                    "message" =>'Payment verification under process..we will process as soon as possible...',
                );
            }
        } 
        $this->response($response, parent::HTTP_OK);
    }

     public function getRideStatus_get() {

        $response = array(
            "status" => true,
            "message" => 'Something Goes Wrong'
        );

        $decoded = $this->verifyRequest();

        $lastRide = $this->_getRideStatus($decoded->UserId,$decoded->UserType);
        if($lastRide) {
             $response = array(
            "status" => true,
            "rideDetails" => $this->_getRideStatus($decoded->UserId,$decoded->UserType)
            );
        } else {
            $response = array(
            "status" => false,
            "userId" => $decoded->UserId,
            "message" => 'No records found..'
            );
        }

        $this->response($response, parent::HTTP_OK);

    }

}
