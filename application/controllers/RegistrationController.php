<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once('ServiceController.php');

class RegistrationController extends ServiceController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('upload');
    }
    
    public function index()
    {
        $this->load->view('registration/header');
        $this->load->view('registration/index');
        $this->load->view('registration/footer');
    }

    public function gocab()
    {
        $this->load->view('registration/header');
        $this->load->view('registration/gocab', array(
            'redirectView' => 'gocab',
            'jobCategoryName' => 'Go Cab',
            'jobCategory' => 1
        ));
        $this->load->view('registration/footer');
    }

    public function goride()
    {
        $this->load->view('registration/header');
        $this->load->view('registration/gocab', array(
            'redirectView' => 'goride',
            'jobCategoryName' => 'Go Ride',
            'jobCategory' => 3
        ));
        $this->load->view('registration/footer');
    }

    public function goauto()
    {
        $this->load->view('registration/header');
        $this->load->view('registration/gocab', array(
            'redirectView' => 'goauto',
            'jobCategoryName' => 'Go Auto',
            'jobCategory' => 4
        ));
        $this->load->view('registration/footer');
    }
    
    public function gofood()
    {
        $this->load->view('registration/header');
        $this->load->view('registration/gofood', array(
            'redirectView' => 'gofood',
            'jobCategoryName' => 'Go Food',
            'jobCategory' => 12
        ));
        $this->load->view('registration/footer');
    }

    public function gofoodDeliveryPartner()
    {
        $this->load->view('registration/header');
        $this->load->view('registration/gocab', array(
            'redirectView' => 'gofooddeliverypartner',
            'jobCategoryName' => 'Go Food Delivery Partner',
            'jobCategory' => 13
        ));
        $this->load->view('registration/footer');
    }

    public function gobox()
    {
        $this->load->view('registration/header');
        $this->load->view('registration/gocab', array(
            'redirectView' => 'gobox',
            'jobCategoryName' => 'Go Box',
            'jobCategory' => 5
        ));
        $this->load->view('registration/footer');
    }
    
    public function gosend()
    {
        $this->load->view('registration/header');
        $this->load->view('registration/gocab', array(
            'redirectView' => 'gosend',
            'jobCategoryName' => 'Go Send',
            'jobCategory' => 10
        ));
        $this->load->view('registration/footer');
    }

    public function gocargo()
    {
        $this->load->view('registration/header');
        $this->load->view('registration/gocab', array(
            'redirectView' => 'gocargo',
            'jobCategoryName' => 'Go Cargo',
            'jobCategory' => 11
        ));
        $this->load->view('registration/footer');
    }

    function thankyou()
    {
        $this->load->view('registration/header');
        $this->load->view('registration/thankyou');
        $this->load->view('registration/footer');
    }

    public function saveRegistrationDetails()
    {
        $config['allowed_types']  = 'gif|jpg|png';
        $config['max_size']       = 0;

        if (!empty($_FILES['DriverLicense']['name'])) {
            $config['upload_path'] = APPPATH . '../uploads/drivers/';
            $drivingLicenceDocName = time() . "-DL-" . $_FILES["DriverLicense"]['name'];
            $config['file_name'] = $drivingLicenceDocName;

            $this->upload->initialize($config, true);

            if (!$this->upload->do_upload('DriverLicense')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();

                $DriverLicense = 'uploads/drivers/'.$config['file_name'];
            }
        }

        if (!empty($_FILES['DisplayPicture']['name'])) {
            $config['upload_path'] = APPPATH . '../uploads/users/';
            $profilePictureName = time() . "-DP-" . $_FILES["DisplayPicture"]['name'];
            $config['file_name'] = $profilePictureName;

            $this->upload->initialize($config, true);

            if (!$this->upload->do_upload('DisplayPicture')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();

                $DisplayPicture = 'uploads/users/'.$config['file_name'];
            }
        }

        if (!empty($_FILES['IdentityDocument']['name'])) {
            $config['upload_path'] = APPPATH . '../uploads/users/';
            $identityDocumentName = time() . "-ID-" . $_FILES["IdentityDocument"]['name'];
            $config['file_name'] = $identityDocumentName;

            $this->upload->initialize($config, true);

            if (!$this->upload->do_upload('IdentityDocument')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();

                $IdentityDocument = 'uploads/users/'.$config['file_name'];
            }
        }


        if (!empty($_FILES['VehiclePermit']['name'])) {
            $config['upload_path'] = APPPATH. '../uploads/permit/';
            
            $vehiclePermitDocName = time().'-VP-'.$_FILES["VehiclePermit"]['name'];
            $config['file_name'] = $vehiclePermitDocName;

            $this->upload->initialize($config, true);

            if (!$this->upload->do_upload('VehiclePermit')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();
                
                $VehiclePermit = 'uploads/permit/'.$config['file_name'];
            }
        }

        $uploadedFiles = array(
            'IdentityDocument' => $IdentityDocument,
            'DriverLicense' => $DriverLicense,
            'DisplayPicture' => $DisplayPicture,
            'VehiclePermit' => $VehiclePermit,
        );

        $postData = array(
            'Firstname' => $this->input->post('Firstname'),
            'Lastname' => $this->input->post('Lastname'),
            'Email' => $this->input->post('Email'),
            'Phone' => $this->input->post('Phone'),
            'DOB' => $this->input->post('DOB'),
            'PlaceOfBirth' => $this->input->post('PlaceOfBirth'),
            'IdentityNo' => $this->input->post('IdentityNo'),
            'Pincode' => $this->input->post('PinCode'),
            'City' => $this->input->post('City'),
            'State' => $this->input->post('State'),
            'VehicleNumber' => $this->input->post('VehicleNumber'),
            'VehicleBrand' => $this->input->post('VehicleBrand'),
            'VehicleColor' => $this->input->post('VehicleColor'),
            'VehicleType' => $this->input->post('VehicleType'),
            'DriverName' => $this->input->post('DriverName'),
            'DrivingLicense' => $this->input->post('DriverLicenseNo'),
            'AccountNo' => $this->input->post('accountNo'),
            'BankName' => $this->input->post('bank'),
            'Ifsc' => $this->input->post('ifsc'),
            'Branch' => $this->input->post('branch'),
            'AccountHolderName' => $this->input->post('holderName'),
            'Country' => $this->input->post('Country'),
            'JobCategory' => $this->input->post('JobCategory'),
            'UserType' => $this->input->post('UserType')
        );




        $status = $this->_saveRegistrationDetails($uploadedFiles, $postData);

        if ($status) {
            redirect('registration/thank-you');
        } else {
            $this->load->view($this->input->post('redirectView'), $error);
            redirect('registration/gocab');
        }
    }

    public function saveRestaurantDetails()
    {

     /*   $address = $this->input->post('RestaurantAddress'); // Address
        $address = 'BTM 2nd Stage, Bengaluru, Karnataka 560076'; // Address
        $apiKey = 'AIzaSyA1epBC9tWb70G_xi9tkFeaCvBPHfvao_A'; // Google maps now requires an API key.
        // Get JSON results from this request
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key='.$apiKey);
        $geo = json_decode($geo, true); // Convert the JSON to an array
        print_r($geo);
        if (isset($geo['status']) && ($geo['status'] == 'OK')) {
        echo $latitude = $geo['results'][0]['geometry']['location']['lat']; // Latitude
        echo $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude
        }
      */

      print_r($_POST);exit;
        $config['allowed_types']  = 'gif|jpg|png';
        $config['max_size']       = 0;  

       if (!empty($_FILES['DisplayPicture']['name'])) {
            $config['upload_path'] = APPPATH . '../uploads/users/';
            $profilePictureName = time() . "-DP-" . $_FILES["DisplayPicture"]['name'];
            $config['file_name'] = $profilePictureName;

            $this->upload->initialize($config, true);

            if (!$this->upload->do_upload('DisplayPicture')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();

                $DisplayPicture = 'uploads/users/'.$config['file_name'];
            }
        }

        if (!empty($_FILES['IdentityDocument']['name'])) {
            $config['upload_path'] = APPPATH . '../uploads/users/';
            $identityDocumentName = time() . "-ID-" . $_FILES["IdentityDocument"]['name'];
            $config['file_name'] = $identityDocumentName;

            $this->upload->initialize($config, true);

            if (!$this->upload->do_upload('IdentityDocument')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();

                $IdentityDocument = 'uploads/users/'.$config['file_name'];
            }
        }

        if (!empty($_FILES['RestaurantPicture']['name'])) {
            $config['upload_path'] = APPPATH . '../uploads/restaurant/';
            $restaurantPictureName = time() . "-RP-" . $_FILES["RestaurantPicture"]['name'];
            $config['file_name'] = $restaurantPictureName;

            $this->upload->initialize($config, true);

            if (!$this->upload->do_upload('RestaurantPicture')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();

                $restaurantPicture = 'uploads/restaurant/'.$config['file_name'];
            }
        }
    
        $uploadedFiles = array(
            'IdentityDocument' => $IdentityDocument,
            'DisplayPicture' => $DisplayPicture,
            'ProviderImage' => $restaurantPicture
        );

        $postData = array(
            'Firstname' => $this->input->post('Firstname'),
            'Lastname' => $this->input->post('Lastname'),
            'Email' => $this->input->post('Email'),
            'Phone' => $this->input->post('Phone'),
            'DOB' => $this->input->post('DOB'),
            'PlaceOfBirth' => $this->input->post('PlaceOfBirth'),
            'IdentityNo' => $this->input->post('IdentityNo'),
            'Pincode' => $this->input->post('PinCode'),
            'City' => $this->input->post('City'),
            'State' => $this->input->post('State'),
            'Country' => $this->input->post('Country'),
            'JobCategory' => $this->input->post('JobCategory'),
            'UserType' => $this->input->post('UserType'),
            'ProviderName' => $this->input->post('RestaurantName'),
            'ProviderAddress' => $this->input->post('RestaurantAddress'),
            'ProviderPhone' => $this->input->post('RestaurantPhoneNumber'),
            'OpeningTime' => $this->input->post('OpeningTime'),
            'ClosingTime' => $this->input->post('ClosingTime'),
            'Latitude' => $this->input->post('Latitude'),
            'Longitude' => $this->input->post('Longitude'),
            'Description' => $this->input->post('Description')
        );

        $status = $this->_saveRestaurantDetails($uploadedFiles, $postData);

        if ($status) {
            redirect('registration/thank-you');
        } else {
            $this->load->view($this->input->post('redirectView'), $error);
            redirect('registration/gofood');
        }
    }
}
