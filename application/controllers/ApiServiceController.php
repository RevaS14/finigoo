<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require FCPATH . 'vendor/autoload.php';
use chriskacerguis\RestServer\RestController;

class ApiServiceController extends RestController {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('ride_model');
        $this->load->model('commission_model');
        $this->load->model('driver_model');
        $this->load->model('vehicle_model');
        $this->load->model('establishment_model');
        $this->load->model('promocode_model');
        $this->load->model('topup_model');
        $this->load->model('provider_model');
        $this->load->model('provider_sub_category_model');
        $this->load->model('item_model');
        $this->load->model('provider_category_model');
        $this->load->model('bank_model');
        $this->load->model('region_model');
        $this->load->model('tracking_model');
        $this->load->model('withdrawal_model');
        $this->load->model('manual_transaction_model');
    }
    
    public function _getUserWalletBalance($userId) {
        return $this->user_model->_getUserWallet($userId);
    }

    public function _getProfileDetails($userId) {
        return $this->user_model->_getProfileDetails($userId);
    }

    public function _updateProfileDetails($userId, $profileDetails) {
        return $this->user_model->_updateProfileDetails($userId, $profileDetails);
    }

    public function _updateRegistrationId($userId, $profileDetails) {
        $this->user_model->_updateProfileDetails($userId, $profileDetails);
        return $this->tracking_model->_updateDriverRegistrationId($userId, $profileDetails['RegistrationId']);
    }

    public function _updateKycDocuments($userId, $data) {
        return $this->user_model->_updateKycDocuments($userId, $data);
    }

    public function _getKycDocuments($userId) {
        return $this->user_model->_getKycDocuments($userId);
    }

    public function _getVehicleDocuments($userId) {
        return $this->vehicle_model->_getVehicleDocuments($userId);
    }

    public function _updateRating($userId, $txnId, $ratedBy, $rating, $feedback) {
        $status = false;

        $isRatingUpdated = false;

        switch ($ratedBy) {
            case 'Driver':
                $isRatingUpdated = $this->ride_model->_updateRating($userId, $txnId, 'UsersRating', 'rides', $rating, 'UsersFeedback', $feedback);
                break;

            case 'User':
                $isRatingUpdated = $this->ride_model->_updateRating($userId, $txnId, 'DriversRating', 'rides', $rating, 'DriversFeedback', $feedback);
                break;
        }

        if($isRatingUpdated) {
            $overallRating = $this->user_model->_getOverallRating($userId);

            if($overallRating[0]->Rating != 0) $newOverallRating = ($overallRating[0]->Rating + $rating) / 2;
            else $newOverallRating = $rating;

            $this->user_model->_updateOverallRating($userId, $newOverallRating);
            $status = true;
        } else {
            $status = 'rating not updated';
        }
        
        return $status;
    }

    public function _calculateRideFare($kilometer, $orderTime, $pincode, $vehicleType) {
        $fareDetails = $this->commission_model->_calculateRideFare($kilometer, $orderTime, $pincode, $vehicleType);

        if(isset($fareDetails[0])) {
            $consolidatedFare = array(
                'totalFare' => 0 + $fareDetails[0]->CalculatedFare,
                'adminCharges' => $fareDetails[0]->AdminCharges,
                'gstPercentage' => $fareDetails[0]->GSTPercentage,
                'gstCharge' => $fareDetails[0]->GSTCharge
            );
        } else {
            $consolidatedFare = false;
        }
        return $consolidatedFare;
    }

    public function _updateDriverLocation($userId, $latitude, $longitude) {
        $this->tracking_model->_updateDriverLocation($userId, $latitude, $longitude);
    }

    public function _getDriversNearBy($latitude, $longitude, $vehicleType) {
        $nearByDrivers = array();

        $drivers = $this->tracking_model->_getDriversNearBy($latitude, $longitude, $vehicleType);

        foreach ($drivers as $driver) {
            $nearByDrivers[] = $driver;
        }

        return $nearByDrivers;
        
    }

    public function _getDriversNearByZone($driverId) {
        // return $this->tracking_model->_getDriversNearByZone($driverId);
        $nearByDrivers = array();

        $drivers = $this->tracking_model->_getDriversNearByZone($driverId);

        foreach ($drivers as $driver) {
            $nearByDrivers[] = $driver->location;
        }

        return $nearByDrivers;
    }

    public function _searchByKeywordProvider($keyword, $latitude, $longitude, $providerType) {
        return $this->provider_model->_searchByKeywordProvider($keyword, $latitude, $longitude, $providerType);
    }

    public function _searchByKeywordItem($keyword, $latitude, $longitude, $providerType) {
        // where lat llong
            // keyword
            // providerType
            // limit 0,f

        $filteredItems = $this->provider_model->_searchByKeywordItem($keyword, $latitude, $longitude, $providerType);
        $filterByItems = array();

        foreach($filteredItems as $item) {
            if(!isset($filterByItems[$item->ProviderId])) {
                $filterByItems[$item->ProviderId] = array(
                    'ProviderId' => $item->ProviderId,
                    'ProviderName' => $item->ProviderName,
                    'ProviderImage' => $item->ProviderImage,
                );
            }
            $filterByItems[$item->ProviderId]['Items'][] = array(
                'ItemName' => $item->ItemName,
                'IsVeg' => $item->IsVeg,
                'ItemImage' => $item->ItemImage,
                'Price' => $item->Price,
                'PreparationTime' => $item->PreparationTime,
                'OutOfStock' => $item->OutOfStock,
                'AvailableOn' => $item->AvailableOn,
            );
        }

        return $filterByItems;
    }

    public function _getDriverLocation($rideId, $vehicleType) {
        $nearByDrivers = array();

        $driverDetails = $this->ride_model->_getDriverIdByRide($rideId);

        $drivers = $this->tracking_model->_getDriverLocation($driverDetails[0]->AcceptedBy, $vehicleType);

        foreach ($drivers as $driver) {
            $nearByDrivers[] = $driver;
        }

        return $nearByDrivers;
        
    }

    public function _confirmRideOrder($userId, $rideDetails) {
        date_default_timezone_set("Asia/Calcutta");
        $ride = array(
            'Users_UserId' => $userId,
            'JobCategories_JobCategoryId' => $rideDetails['vehicleType'],
            'SourceLatitude' => $rideDetails['sourceLatitude'],
            'SourceLongitude' => $rideDetails['sourceLongitude'],
            'DestinationLatitude' => $rideDetails['destinationLatitude'],
            'DestinationLongitude' => $rideDetails['destinationLongitude'],
            'EstimatedFare' => $rideDetails['estimationFare'],
            'SourceAddress' => $rideDetails['sourceAddress'],
            'DestinationAddress' => $rideDetails['destinationAddress'],
            'Distance' => $rideDetails['distance'],
            'RidePath' => $rideDetails['ridePath'],
            'EstimatedTime' => $rideDetails['estimatedTime'],
            'Otp' => rand(100000,999999),
            'CreatedOn'=>date('Y-m-d H:i:s')
        );

        if($rideDetails['paymentMode'] == 'W') {
            $walletBalance = $this->user_model->_getUserWallet($userId);

            if($rideDetails['estimationFare'] > $walletBalance) {
                return 'InsufficientFund';
            }
        }

        return array(
            "rideId" => $this->ride_model->_insertRide($ride),
            "otp" => $ride['Otp']
        );
    }

    public function _acceptRideOrder($userId, $rideDetails) {
        return $this->ride_model->_acceptRideOrder($userId, $rideDetails);
    }

    public function _startRideOrder($userId, $rideDetails) {
        $status = false;

        if($this->ride_model->_verifyOtp($userId, $rideDetails) == 1) {
            $status = $this->ride_model->_startRideOrder($userId, $rideDetails);
        }
        
        return $status;
    }

    public function _userSignup($postData) {

        $userData =array(
            'firstName'=> $postData->firstName,
            'lastName'=> $postData->lastName,
            'email'=> $postData->email,
            'phone'=> $postData->phone,
            'placeOfBirth'=> $postData->placeOfBirth,
            'password'=> password_hash($postData->password, PASSWORD_DEFAULT),
            'dob'=> $postData->dob,
            'ipAddress'=> $this->input->ip_address(),
            'city'=> $postData->city,
            'state'=> $postData->state,
            'Establishment_EstablishmentId'=> $postData->establishmentId,
            'kYCVerified'=> 'U',
            'pincode'=> $postData->pincode,
            'userType'=> 'U',
            'status'=> 'A'
        );

        $isDataInserted = $this->user_model->insertUser($userData);

        if($isDataInserted) {
            return true;
        } else {
            return false;
        }
    }
    
    public function _userLogin($postData) {
        return $this->user_model->login($postData);
    }
    
    public function _getUserMobile($data)
	{
        return $this->user_model->_getUserMobile($data); 
    }

    public function _sendOtp($data){
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,  "http://sms.haqto.com/api/smsapi?key=a1ace77d10445176ad73760c82c5aa5a&route=3&sender=quickl&number=".$data['mobile']."&sms=".$data['otp']."");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$buffer = curl_exec($ch);
		curl_close($ch);
        return $buffer;
    }
    public function _verifyOtp($data)
    {
        return $this->user_model->_verifyOtp($data);
    }

    public function _forgotToken($token)
	{
        if($this->user_model->getForgotToken($token))  
        {  
             return true;
        }  
        else  
        {  
            return false;
        }  
         
    }
    public function _resetPassword($mobile,$password)
	{
        return $this->user_model->_resetPassword($mobile,$password);
    }

    public function _changePassword($data)
	{
        if($this->user_model->changePassword($data))  
        {  
             return true;
        }  
        else  
        {  
            return false;
        }  
         
    }
    
    public function _getCountries() {
        return $this->establishment_model->_getCountries();
    }

    public function _updateDriverStatus($userId, $driverStatus) {
        $this->tracking_model->_updateDriverAvailablity($userId, $driverStatus);
        $vehicleDetails = $this->vehicle_model->_getVehicleIdByDriverId($userId);
        return $this->vehicle_model->_updateDriverStatus($vehicleDetails[0]->VehicleID, $driverStatus);
    }

    public function _getDriverStatus($userId) {
        $vehicleDetails = $this->vehicle_model->_getVehicleIdByDriverId($userId);
        return $this->vehicle_model->_getDriverStatus($vehicleDetails[0]->VehicleID);
    }

    public function _completeRideFare($driverId, $kilometer, $orderTime, $pincode, $vehicleType, $rideId) {
        return $this->ride_model->_completeRideFare($driverId, $kilometer, $orderTime, $pincode, $vehicleType, $rideId);
    }

    public function _getPromocode($vehicleType) {
        return $this->promocode_model->_getPromocode($vehicleType);
    }
    
    public function _saveWithdrawRequest($requestData) {
        return $this->withdrawal_model->_saveWithdrawRequest($requestData);
    }

    public function _generateOrder($userId, $userType, $amount) {
        date_default_timezone_set("Asia/Calcutta");
        $topUpData = array(
            'Users_UserId' => $userId,
            'PaymentMethod'=>'O',
            'Amount'=> $amount,
            'Remarks'=> 'Self Topup - ' . (($userType == 'D') ? 'Driver' : 'User'),
            'Status'=> 'P',
            'CreatedOn'=> date("Y-m-d H:i:s")
        );

        $topup = $this->topup_model->insertTopup($topUpData);

        if($topup){
            // $url = "https://test.cashfree.com/api/v2/cftoken/order";
            $url = "https://api.cashfree.com/api/v2/cftoken/order";


            $request_headers = array();
            $request_headers[] = 'Content-Type: application/json ';
            $request_headers[] = 'x-client-id: 5362094fc76b830d46847dc0302635';
            $request_headers[] = 'x-client-secret: 305f7df287b237103325b7e1b2ae03c980cfc978';
            // $request_headers[] = 'x-client-id: 17586d27d83c928fa2ed9d46c68571';
            // $request_headers[] = 'x-client-secret: fa86f1f22beb8ee2ffff7fd6dfb856e9c5c8e2ba';

            $postData = json_encode(array(
                "orderId" => "QUICKLAR".$topup,
                "orderAmount" => $amount,
                "orderCurrency" => "INR"
            ));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, strlen($postData));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            $data = curl_exec($ch);

            if (curl_errno($ch)) {
                print "Error: " . curl_error($ch);
            }
            // Show me the result

            $transaction = json_decode($data, TRUE);

            curl_close($ch);

            if($transaction['status'] == 'OK') {
                $this->response(array(
                    "status" => true,
                    "orderId" => "QUICKLAR".$topup,
                    "amount" => $amount,
                    "cftoken" => $transaction['cftoken']
                ), 200);  
            } else {
                $message = array(
                    "status" => false,
                    'message' => 'Something goes wrong',
                    'PaymentStatus'=> $transaction['status']
                );
                $this->response($message, 200);
            }
        } else {
            $message = array(
                "status" => false,
                'message' => 'Something goes wrong'
            );
            $this->response($message, 200);            
        } 
    }

    public function _getRideDriverDetails($userId, $rideId) {
        $driverDetails = $this->ride_model->_getDriverIdByRide($rideId);
        return $this->vehicle_model->_getRideDriverDetails($driverDetails[0]->AcceptedBy);
    }

    public function _getRideCustomerDetails($userId, $rideId) {
        $customerId = $this->ride_model->_getCustomerIdByRide($rideId);
        return $this->user_model->_getRideCustomerDetails($customerId[0]->Users_UserId);
    }

    public function _isPhonenumberExists($phone) {
        return $this->user_model->_isPhonenumberExists($phone);
    }

    public function _getGoCabList() {
        return $this->ride_model->_getGoCabList();
    }
    
    public function _findProvider($userId) {
        return $this->provider_model->_findProvider($userId);
    }

    public function _getSubCategories($providerId) {
        return $this->provider_sub_category_model->_getSubCategories($providerId);
    }

    public function _getItems($subCategoryId) {
        return $this->item_model->_getItems($subCategoryId);
    }

    public function _getProviderDetails($userId) {
        $providerData = $this->provider_model->_findProviderByUserId($userId);
        $providerCategories = $this->provider_sub_category_model->_getSubCategories($providerData->ProviderId);

        $providerArray = array();
        $providerArray['Provider Name'] = $providerData->ProviderName;
        $providerArray['Ratings'] = $providerData->Rating;
        $providerArray['items'] = [];

        foreach ($providerCategories as $key => $value) {
        $providerArray['items'][$value->SubCategory] = $this->_getItems($value->ProviderSubCategoryId);  
        
        }
        return $providerArray;
    }

    public function _getAllProviderCategories($type) {
        return $this->provider_category_model->_getAllProviderCategories($type);
    }

    public function _getProvidersNearBy($latitude, $longitude, $providerType) {
        return $this->provider_model->_getProvidersNearBy($latitude, $longitude, $providerType);
    }

    public function _getRideHistory($userId,$type) {
        $rideHistory = array();
        $rides = $this->ride_model->_getRideHistory($userId,$type);

        foreach ($rides as $ride) {
                $rideHistory[] = $ride;
        }
        return $rideHistory;
    }

    public function _getWalletTransactionHistory($userId,$type) {
        $walletHistory = array();
        $wallets = $this->topup_model->_getWalletTransactionHistory($userId,$type);

        foreach ($wallets as $wallet) {
                $walletHistory[] = $wallet;
        }
        return $walletHistory;
    }

    public function _sendWithdrawRequest($userId,$amount) {

            $walletBalance = $this->user_model->_getUserWallet($userId);

            if($amount > $walletBalance[0]->WalletBalance) {
                return 'Insufficient Fund';
            }
            else if($amount < 0) {
                return 'Invalid Fund..';
            } else {
                $data = array(
                    'Users_UserId' => $userId,
                    'Amount' => $amount,
                    'Status' => 'A'
                );
                $withdraw = $this->withdrawal_model->_saveWithdrawRequest($data);
                $topUpData=array(
                    'Users_UserId'=> $userId,
                    'PaymentMethod'=> 'W',
                    'Amount'=> -$amount,
                    'Remarks'=> 'Withdraw Request By Driver - '.$userId,
                    'Status'=> 'A'
                );
                $topup = $this->topup_model->insertTopup($topUpData);
                $wallet = $this->user_model->updateWallet($userId,$amount,'debit');
                if($withdraw && $topup && $wallet) {
                    return 'SUCCESS';
                } else {
                    return 'Something Goes Wrong..';
                }
            }
    }

    public function _changeVehicleDetails($postData) {
        $vehicleId = $postData->vehicleID;
        $vehicleData = array(
          "RegistrationId"=> $postData->registrationId,  
          "VehiclePermit"=> $postData->vehiclePermit,  
          "VehicleNumber"=> $postData->vehicleNumber,  
          "VehicleBrand"=> $postData->vehicleBrand,  
          "VehicleColor"=> $postData->vehicleColor,  
          "VehicleType"=> $postData->vehicleType,  
        );
        return $this->vehicle_model->_changeVehicleDetails($vehicleId,$vehicleData);
        
    }

    public function _changeAccountDetails($postData) {
        $userId = $postData->userId;
        $userData = array(
          "AccountNo"=> $postData->accountNo,  
          "BankName"=> $postData->bankName,  
          "Ifsc"=> $postData->ifsc,  
          "Branch"=> $postData->branch,  
          "AccountHolderName"=> $postData->accountHolderName,   
        );
        return $this->user_model->updateUser($userId,$userData);
        
    }

    public function _getRC($vehicleId) {
        $data = $this->vehicle_model->_getRC($vehicleId);
        return $data->VehiclePermit;
        
    }

    public function _sendMail($to, $subject, $message) {
        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'revathi@haqto.in';
        $config['smtp_pass']    = 'revathisunder';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->load->library('email', $config);
        $this->email->initialize($config);

        $this->email->from('revathi@haqto.in', APPNAME);
        $this->email->to($to); 
        $this->email->subject($subject);
        $this->email->message($message);  

        $this->email->send();
    }

    public function _getVehicleType($userId) {
        return $this->vehicle_model->_getVehicleType($userId);
    }

    public function _getListOfBanks() {
        return $this->bank_model->_getListOfBanks();
    }

    public function _saveTransaction($postData,$evidence) {
        $transData =array(
            'Banks_BankId'=> $postData->bankId,
            'BankName'=> $postData->bankName,
            'Ifsc'=> $postData->ifsc,
            'BranchName'=> $postData->branchName,
            'AccountNumber'=> $postData->accountNumber,
            'AccountHolderName'=> $postData->accountHolderName,
            'Amount'=> $postData->amount,
            'EvidenceDocument'=> $evidence,
            'Users_UserId'=> $postData->userId,
        );
        return $this->manual_transaction_model->_saveTransaction($transData); 
    }

    public function _getRideStatus($userId,$userType) {
        return $this->ride_model->_getRideStatus($userId,$userType);
    }

    public function _getPincodes($userid) {
        return $this->region_model->_getPincodes($userid);
    }

    public function _getDrivers($pincodes) {
        return $this->user_model->_getDrivers($pincodes);
    }



}
