<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('ServiceController.php');

class AdminController extends ServiceController {

    public function __construct() {
		parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->helper(array('form'));
        $this->load->library('session');
        $this->load->library('upload');
        $this->load->library('email');
        $this->load->helper(['jwt', 'authorization']);
    }

    public function index() {

        if($this->session->userdata('user_id')) {
            redirect('admin/dashboard');
        }
        $this->load->view('admin/loginheader');
        $this->load->view('admin/index'); 
        $this->load->view('admin/footer');
    }

    public function login() {
        $username=$this->input->post('email');
        $password=$this->input->post('password');
        $type=$this->input->post('loginType');
		$loginData=array(
            'username' => $username,
            'password' => $password
        );
        if ($username=='' || $password=='') { 
            $this->session->set_flashdata('errorMessage','Please fill all the required fields..');
            redirect('admin');
        } 
        else { 
            $loginStatus = $this->_adminLogin($loginData,$type);
            if($loginStatus){
                if($loginStatus=='D') {
                    $this->session->set_flashdata('errorMessage','User Access Denied...');
                    redirect('admin');
                }
                $userData=$this->_findUser($loginStatus); 
                $token = AUTHORIZATION::generateToken([
                    'UserId' => $loginStatus,
                    'UserType' => $userData->UserType
                ]);
                $this->session->set_userdata(array(
                    'user_id'  => $loginStatus,
                    'username' => $username,
                    'userType' => $userData->UserType,
                    'token' => $token,
                    'user_logged_in' => TRUE
                ));
                redirect('admin/dashboard');
            }
            else{
                $this->session->set_flashdata('errorMessage','Incorrect Username or Password..');
                redirect('admin');
            }
        } 
    }

    public function dashboard() {
        $this->secure();
        //$viewData['data'] =  $this->_getZones();
        $viewData['data'] =  '';
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/dashboard',$viewData); 
        $this->load->view('admin/footer');
    }

    public function reset() {
        $franchise = $this->_reset();
    }

    
    public function updateStatus() {
        $userId=$this->input->post('id');
        $statusType=$this->input->post('request_for');
        $updateUserStatus = $this->_updateUserStatus($userId,$statusType);
        return $updateUserStatus;
    }

    public function setCost($id) {
        $this->secure(); 
        $costData['category'] = $id;

        $checkCommission = $this->_checkCommission($this->session->userdata('user_id'),$id);
        
        $getZones = $this->_getZones();
        $costData['zones'] = $getZones;
        if(!$checkCommission) {
            $costData['commission'] = '';
            $costData['peakHours'] = '';
        } else {
            $getPeakHours = $this->_getPeakHours($this->session->userdata('user_id'),$checkCommission->Zones_ZoneId,$id);
            $costData['commission'] = $checkCommission;
            $costData['peakHours'] = $getPeakHours;
            
        }
        $submit = $this->input->post('save');
        if(isset($submit)) {
            $basicRate=$this->input->post('basicRate');
            $minimumRate=$this->input->post('minimumRate');
            $peakHourRate=$this->input->post('peakHourRate');
            $zoneId=$this->input->post('zone');
            $perUnit=$this->input->post('perUnit');
            if($this->session->userdata('userType')=='A') { 
                $adminCharge=$this->input->post('adminCharge');
            } else {
                $adminCharge=$checkCommission->AdminCharge;
            }
            
            $franchiseCharge=$this->input->post('franchiseCharge');
            $gstPercentage=$this->input->post('gstPercentage');
            $jobCategoryId=$this->input->post('jobCategoryId');


            $costData = array(
                'JobCategories_JobCategoryId' => $jobCategoryId,
                'Users_UserId' => $this->session->userdata('user_id'),
                'BaseFare'  =>  $basicRate,
                'PeakHourBaseFare' => $peakHourRate,
                'MinimumFare' => $minimumRate,
                'Zones_ZoneId' => $zoneId,
                'PerUnit' => $perUnit,
                'AdminCharge' => $adminCharge,
                'GstPercentage' => $gstPercentage,
                'Status' => ($this->session->userdata('userType')=='A') ? 'A' : 'P'
            );

            if($basicRate=='' || $minimumRate=='' || $peakHourRate=='' || $perUnit=='' || $gstPercentage=='' || $jobCategoryId=='') {
                $this->session->set_flashdata('errorMessage','Please fill all the required fields..');
                redirect('admin/set-cost/'.$id);
            } else {
                if(!$checkCommission) {
                    $costStatus = $this->_saveCommission($costData);                    
                    $this->session->set_flashdata('successMessage','Commision added successfully..');
                    redirect('admin/set-cost/'.$id);
                } else {
                        if($checkCommission->Status=='P') { 
                            $this->session->set_flashdata('errorMessage','Your Commissions not Approved by Admin...');
                            redirect('admin/set-cost/'.$id);
                        }
                        if($this->session->userdata('userType')=='A') { 
                            $adminCommission = $this->_addAdminCommission($costData,$checkCommission->CommissionId);
                        } else {
                            $costStatus = $this->_saveCommission($costData);
                        }
                        $this->session->set_flashdata('successMessage','Commision updated successfully..');
                        redirect('admin/set-cost/'.$id);
                }
                
            }
        } else {
            $this->load->view('admin/header');
            $this->load->view('admin/navigation');
            $this->load->view('admin/set-cost',$costData); 
            $this->load->view('admin/footer');
        }
 
    }

    public function viewCommission($commissionId) {
        $this->secure(); 
        $viewCommission = $this->_viewCommissions($commissionId);
        $getPeakHours = $this->_getPeakHours($viewCommission->Franchises_FranchiseId,$viewCommission->Zones_ZoneId,$viewCommission->JobCategories_JobCategoryId);
        if(!$viewCommission) {
            $costData['commission'] = '';
            $costData['peakHours'] = '';
        } else {
            $costData['commission'] = $viewCommission;
            $costData['peakHours'] = $getPeakHours;
        }
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/set-cost',$costData); 
        $this->load->view('admin/footer');
    }

    public function changeCommissionStatus($id,$status,$franchiseId,$jobCategoryId) {
        $this->secure();
        $this->_updateCommissionStatus($id,$status,$franchiseId,$jobCategoryId);
        $this->session->set_flashdata('successMessage','Status Changed Successfully..');
        redirect('admin/view-commission/'.$id);
    }

    public function changePeakHourStatus($id,$status,$commissionId) {
        $this->secure();
        $this->_updatPeakHourStatus($id,$status);
        $this->session->set_flashdata('successMessage','Status Changed Successfully..');
        redirect('admin/view-commission/'.$commissionId);
    }

    public function updatePeakHours() {
        $postData = $this->input->post();
        $startTime = $this->input->post('startTime');
        $endTime = $this->input->post('endTime');
        $zoneId = $this->input->post('zoneId');
        $jobCategoryId = $this->input->post('jobCategoryId');

        $peakHoursData = array(
            'JobCategories_JobCategoryId' => $jobCategoryId,
            'Users_UserId' => $this->session->userdata('user_id'),
            'StartTime' => date('H:i:s', strtotime($startTime)),
            'EndTime' => date('H:i:s', strtotime($endTime)),
            'Zones_ZoneId' => $zoneId,
            'Status' => 'A',
        );
        $peakHourStatus = $this->_savePeakHours($peakHoursData);
        $getPeakHours = $this->_getPeakHours($this->session->userdata('user_id'),$zoneId, $jobCategoryId);
        $peakHour['peakHours'] = $getPeakHours;
        return $this->load->view('admin/_peakhours',$peakHour);
    }

    public function deletePeakHours() {
        $peakHourId = $this->input->post('peakHourId');
        $jobCategoryId = $this->input->post('jobCategoryId');
        $zoneId = $this->input->post('zoneId');
        $peakHourStatus = $this->_deletePeakHour($peakHourId);
        $getPeakHours = $this->_getPeakHours($this->session->userdata('user_id'),$zoneId, $jobCategoryId);
        $peakHour['peakHours'] = $getPeakHours;
        return $this->load->view('admin/_peakhours',$peakHour);
    }

    public function commissions($link) {
        $this->secure();
        if($link=='approved') {
            $status='A';
        } elseif($link=='pending') {
            $status='P';
        } elseif($link=='declined') {
            $status='C';
        }
        $commissionsData['status']=$status;
        $commissions = $this->_getAllCommissions($status,$this->session->userdata('user_id'));
        if($commissions){
            $commissionsData['commission'] = $commissions;
           } else {
            $commissionsData['commission'] = '';
           }
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/commissions',$commissionsData); 
        $this->load->view('admin/footer');
    }
    

    public function topup($type,$userId) {
        $this->secure();
        $topupData['type'] = $type;
        $topupData['userId'] = $userId;
        $rechargeButton = $this->input->post('rechargeButton');
        if(isset($rechargeButton)) {
                    $this->_updateWallet($this->session->userdata('user_id'),$this->input->post('rechargeAmount'),'debit');
                    $topUpData=array(
                        'Users_UserId'=> $this->session->userdata('user_id'),
                        'PaymentMethod'=> 'F',
                        'Amount'=> -$this->input->post('rechargeAmount'),
                        'Remarks'=> 'Top up to Driver - '.$userId,
                        'Status'=> 'A'
                    );

                    $topUpDriver=array(
                        'Users_UserId'=> $userId,
                        'PaymentMethod'=> 'F',
                        'Amount'=> $this->input->post('rechargeAmount'),
                        'Remarks'=> 'Topup by Franchise  - '.$this->session->userdata('user_id'),
                        'Status'=> 'A'
                    );
            $this->_insertTopup($topUpData);
            $this->_updateWallet($userId,$this->input->post('rechargeAmount'),'credit');
            $this->_insertTopup($topUpDriver);
            $this->session->set_flashdata('successMessage','Topup Successfull..');
            redirect('admin/topup/'.$type.'/'.$userId);
        }
                
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/topup',$topupData); 
        $this->load->view('admin/footer');
    }

    public function response() {
        $secretkey = "37d970a6013d842b00bda4f23bfe866715fd711d";
        $orderId = $this->input->post('orderId');
        $orderAmount = $this->input->post('orderAmount');
        $referenceId = $this->input->post('referenceId');
        $txStatus = $this->input->post('txStatus');
        $paymentMode = $this->input->post('paymentMode');
        $txMsg = $this->input->post('txMsg');
        $txTime = $this->input->post('txTime');
        $signature = $this->input->post('signature');
        $responseData['orderId'] = $orderId;
        $responseData['orderAmount'] = $orderAmount;
        $responseData['referenceId'] = $referenceId;
        $responseData['paymentMode'] = $paymentMode;
        $responseData['txMsg'] = $txMsg;
        $responseData['txTime'] = $txTime;
        $responseData['txStatus'] = $txStatus;
        $responseData['signature'] = $signature;
        $data = $orderId.$orderAmount.$referenceId.$txStatus.$paymentMode.$txMsg.$txTime;
        $hash_hmac = hash_hmac('sha256', $data, $secretkey, true) ;
        $computedSignature = base64_encode($hash_hmac);
        $responseData['computedSignature'] = $computedSignature;
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/response',$responseData); 
        $this->load->view('admin/footer');
            
    }

    public function payment() {
        $this->secure();   
        $rechargeButton = $this->input->post('rechargeButton'); 
        if(isset($rechargeButton)) {
            $topUpData=array(
                    'Users_UserId'=> $this->session->userdata('user_id'),
                    'PaymentMethod'=> 'O',
                    'Amount'=> $this->input->post('rechargeAmount'),
                    'Remarks'=> 'Self Topup - Franchise',
                    'Status'=> 'P'
            );
            $insertTopup = $this->_insertTopup($topUpData);
            $orderId = APPNAME. $insertTopup;
            $rechargeData['userId'] = $this->session->userdata('user_id');
            $rechargeData['amount'] = $this->input->post('rechargeAmount');
            $rechargeData['orderId'] = $orderId;
            $rechargeData['note'] = 'Self Topup - Franchise';
            $this->load->view('admin/header');
            $this->load->view('admin/navigation');
            $this->load->view('admin/payment',$rechargeData); 
            $this->load->view('admin/footer');
        }
 
    }

    public function request() {
        $this->load->view('admin/request'); 
    }

    public function notify() {

        $orderId = $_POST["orderId"];
        $search = 'QUICKLAR';
        $topUpId = str_replace($search, '', $orderId) ;
        $findTopup = $this->_findTopup($topUpId);
        $userId = $findTopup->Users_UserId; 
        $orderAmount = $_POST["orderAmount"];
        $referenceId = $_POST["referenceId"];
        $txStatus = $_POST["txStatus"];
        $paymentMode = $_POST["paymentMode"];
        $txMsg = $_POST["txMsg"];
        $txTime = $_POST["txTime"];
        $signature = $_POST["signature"];
        $data = $orderId.$orderAmount.$referenceId.$txStatus.$paymentMode.$txMsg.$txTime;
        $hash_hmac = hash_hmac('sha256', $data, CASHFREE_SECRET, true) ;
        $computedSignature = base64_encode($hash_hmac);

        if ($signature == $computedSignature) {
            if($txStatus == 'SUCCESS') {
            //$updateWallet = $this->_updateWallet($userId,$orderAmount,'credit');
            $updateData=array(
                'TransactionId' => $referenceId,
                'TransactionStatus' => $txStatus,
                'Status' => 'A'
            );
            $updateTopup = $this->_updateTopup($topUpId,$updateData);
                if($updateTopup) {
                    echo "Wallet Updated Successfully..";
                }
            } else {
                $updateData=array(
                    'TransactionId' => $referenceId,
                    'TransactionStatus' => $txStatus,
                    'Status' => 'F'
                );
                $updateTopup = $this->_updateTopup($topUpId,$updateData);
                echo 'Topup Failed..';
            }
        }
        
    }

    public function allTransactions($id) {
        $this->secure();
        $transaction = $this->_getAllTransactions($id);
        if($transaction){
            $transactionData['transaction'] = $transaction;
           } else {
            $transactionData['transaction'] = '';
           }
        $transactionData['user'] = $this->_findUser($id);
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/alltransactions',$transactionData); 
        $this->load->view('admin/footer');
    }

    public function driverCollections($link) {
        $this->secure();
        if($link=='approved') {  $status='A'; } elseif($link=='pending') {  $status='P'; }

        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];

        $collectionData['status']=$status;
        $collectionData['collections'] = $this->_getDriverCollections($status,$franchiseId,$regions);
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/collections',$collectionData); 
        $this->load->view('admin/footer');
    }

    public function collectAmount() {
        $this->_collectAmount($this->input->post('id'));
        return $rideId;
    }

    public function withdrawRequests($link) {
        $this->secure();
        if($link=='approved') {  $status='A'; } elseif($link=='pending') {  $status='P'; } elseif($link=='cancelled') {  $status='C'; }

        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];

        $requestData['status']=$status;
        $requestData['requests'] = $this->_getWithdrawRequests($status, $franchiseId, $regions);
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/withdraw-requests',$requestData); 
        $this->load->view('admin/footer');
    }

    public function changeWithdrawStatus() {
        $this->secure();
        $this->_changeWithdrawStatus($this->input->post('id'),$this->input->post('status'));

        $this->session->set_flashdata('successMessage','Status Changed successfully..');
        redirect('admin/withdraw-requests/pending');
    }

    public function changeTransactionStatus() {
        $this->secure();
        $this->_changeTransactionStatus($this->input->post('id'),$this->input->post('status'));
         $transInfo= $this->_getTransactionInfo($this->input->post('id')); 
       
         $topUpData=array(
                        'Users_UserId'=> $transInfo[0]->Users_UserId,
                        'PaymentMethod'=> 'B',
                        'Amount'=> $transInfo[0]->Amount,
                        'TransactionId'=> $transInfo[0]->ManualTransactionId,
                        'Remarks'=> 'Manual Topup - Driver',
                        'Status'=> 'A'
                    );
        $this->_insertTopup($topUpData);
        $this->_updateWallet($transInfo[0]->Users_UserId,$transInfo[0]->Amount,'credit');
        return true;
        // $this->session->set_flashdata('successMessage','Status Changed successfully..');
        // redirect('admin/manual-transactions/pending');
    }

    public function zones() {
        $this->secure();
        $zone = $this->_getZones();
        if($zone){
            $zones['zoneData'] = $zone;
           } else {
            $zones['zoneData'] = '';
           }
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/zones',$zones); 
        $this->load->view('admin/footer');
    }

    public function addZone()
	{
		$this->secure();
		$id='';
		$this->zoneInfo($id);
	}

	public function editzone($id)
	{
		$this->secure();
		$this->zoneInfo($id);
    }
    
    public function zoneInfo($id) {
        $this->secure();
        $zone[] ='';
        if ($id!='') {
            $data=$this->_findZone($id); 
            $zone['zoneData'] = $data;
            $zone['regions'] = $this->_getRegions($data->ZoneId);
        }
       
        $save = $this->input->post('save');
        if(isset($save)) {
            $name=$this->input->post('firstName');
            $regions=$this->input->post('allRegions');
           
            $zoneData = array(
                'Name'  =>  $name,
                'Status' => 'A'
            );

            if($name=='') {
                $this->session->set_flashdata('errorMessage','Please fill all the required fields..');
                redirect('admin/add-zone');
            } else {
            if($id!='') {

                $updateUser = $this->_updateZone($id,$zoneData);
                $updateRegions= $this->_updateRegions($id,$regions);
                $this->session->set_flashdata('successMessage','Zone Updated Successfully..');
                redirect('admin/edit-zone/'.$id);
            } else {
                    $user_id = $this->_saveZone($zoneData);
                    if($user_id) {
                        $saveRegions= $this->_saveRegions($user_id,$regions);
                        $this->session->set_flashdata('successMessage','Zone Added Successfully..');      
                        redirect('admin/add-zone');
                    } else {
                        $this->session->set_flashdata('errorMessage','Something Error..');
                        redirect('admin/add-zone');
                    }

            }
        }
            
           
        }
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/zoneInfo',$zone); 
        $this->load->view('admin/footer');
    }

    public function deleteZone($id) {
        $this->_deleteAllRegions($id);
        $deleteFranchise = $this->_deleteZone($id);
        $this->session->set_flashdata('successMessage','Zone Deleted Successfully..');
        redirect('admin/zones');
    }

    public function deleteUser($id) {
        $this->_deleteUser($id);
        $this->session->set_flashdata('successMessage','User Deleted Successfully..');
        redirect('admin/users');
    }

    public function rides() {
        $this->secure();
        $ride = $this->_getRides();
        if($ride){
            $rides['rideData'] = $ride;
           } else {
            $rides['rideData'] = '';
           }
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/rides',$rides); 
        $this->load->view('admin/footer');
    }


    public function secure() {
        if(!$this->session->userdata('user_id')) {
            redirect('admin');
        } elseif($this->session->userdata('userType')=='A' || $this->session->userdata('userType')=='F') {
            return true;
        } else {
            $this->session->sess_destroy();
            redirect('admin');
        }
    }

    public function logout() {
        $this->session->sess_destroy();
            redirect('admin');
    }

    public function addDriverGocab() {
        $this->secure();
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/adddrivergocab', array(
            'redirectView' => 'gocab',
            'jobCategory' => 1
        ));
        $this->load->view('admin/footer');
    }

    public function addDriverGoride() {
        $this->secure();

        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/adddrivergocab', array(
            'redirectView' => 'goride',
            'jobCategory' => 3
        ));
        $this->load->view('admin/footer');
    }

    public function addDriverGoauto() {
        $this->secure();

        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/adddrivergocab', array(
            'redirectView' => 'goauto',
            'jobCategory' => 4
        ));
        $this->load->view('admin/footer');
    }

    public function addRestaurantGofood() {
        $this->secure();

        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/addrestaurantgofood', array(
            'redirectView' => 'gofood',
            'jobCategory' => 12 
        ));
        $this->load->view('admin/footer');
    }
    
    public function addDriverGobox() {
        $this->secure();

        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/adddrivergocab', array(
            'redirectView' => 'gobox',
            'jobCategory' => 5
        ));
        $this->load->view('admin/footer');
    }

    public function addDriverGosend() {
        $this->secure();

        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/adddrivergocab', array(
            'redirectView' => 'gosend',
            'jobCategory' => 10
        ));
        $this->load->view('admin/footer');
    }

    public function addDriverGofoodDeliveryPartner() {
        $this->secure();

        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/adddrivergocab', array(
            'redirectView' => 'gofooddeliverypartner',
            'jobCategory' => 13
        ));
        $this->load->view('admin/footer');
    }
    
    public function addDriverGocargo() {
        $this->secure();

        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/adddrivergocab', array(
            'redirectView' => 'gocargo',
            'jobCategory' => 11
        ));
        $this->load->view('admin/footer');
    }

    public function saveDriverDetails() {
        $config['allowed_types']  = 'gif|jpg|png';
        $config['max_size']       = 0;

        if (!empty($_FILES['DriverLicense']['name'])) {
            $config['upload_path'] = APPPATH. '../uploads/drivers/';
            $drivingLicenceDocName = time()."-DL-".$_FILES["DriverLicense"]['name'];
            $config['file_name'] = $drivingLicenceDocName;

            $this->upload->initialize($config,true);

            if (!$this->upload->do_upload('DriverLicense')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();
                $DriverLicense = 'uploads/drivers/'.$config['file_name'];
            }
        }

        if (!empty($_FILES['DisplayPicture']['name'])) {
            $config['upload_path'] = APPPATH. '../uploads/users/';
            $profilePictureName = time()."-DP-".$_FILES["DisplayPicture"]['name'];
            $config['file_name'] = $profilePictureName;

            $this->upload->initialize($config,true);
                
            if ( ! $this->upload->do_upload('DisplayPicture')){
                $error = array('error' => $this->upload->display_errors());
            }else{
                $upload_data = $this->upload->data();
                $DisplayPicture = 'uploads/users/'.$config['file_name'];
            }
        }
        
        if (!empty($_FILES['IdentityDocument']['name'])) {
            $config['upload_path'] = APPPATH. '../uploads/users/';
            $identityDocumentName = time()."-ID-".$_FILES["IdentityDocument"]['name'];
            $config['file_name'] = $identityDocumentName;

            $this->upload->initialize($config,true);

            if ( ! $this->upload->do_upload('IdentityDocument')){
                $error = array('error' => $this->upload->display_errors());
            }else{
                $upload_data = $this->upload->data();
                $IdentityDocument = 'uploads/users/'.$config['file_name'];
            }
        }

        if (!empty($_FILES['VehiclePermit']['name'])) {
            $config['upload_path'] = APPPATH. '../uploads/permit/';
            $vehiclePermitDocName = time().'-VP'.$_FILES["VehiclePermit"]['name'];
            $config['file_name'] = $vehiclePermitDocName;

            $this->upload->initialize($config,true);

            if ( ! $this->upload->do_upload('VehiclePermit')){
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();
                $VehiclePermit = 'uploads/permit/'.$config['file_name'];
            }
        } 
        
        $uploadedFiles = array(
            'IdentityDocument' => $IdentityDocument,
            'DriverLicense' => $DriverLicense,
            'DisplayPicture' => $DisplayPicture,
            'VehiclePermit' => $VehiclePermit,
        );

        $postData = array(
            'Firstname' => $this->input->post('Firstname'),
            'Lastname' => $this->input->post('Lastname'),
            'Email' => $this->input->post('Email'),
            'Phone' => $this->input->post('Phone'),
            'DOB' => $this->input->post('DOB'),
            'PlaceOfBirth' => $this->input->post('PlaceOfBirth'),
            'IdentityNo' => $this->input->post('IdentityNo'),
            'Pincode' => $this->input->post('PinCode'),
            'City' => $this->input->post('City'),
            'State' => $this->input->post('State'),
            'VehicleNumber' => $this->input->post('VehicleNumber'),
			'VehicleBrand' => $this->input->post('VehicleBrand'),
			'VehicleColor' => $this->input->post('VehicleColor'),
            'VehicleType' => $this->input->post('VehicleType'),
            'DriverName' => $this->input->post('DriverName'),
            'DrivingLicense' => $this->input->post('DriverLicenseNo'),
            'Country' => $this->input->post('Country'),
            'JobCategory' => $this->input->post('JobCategory'),
            'UserType' => $this->input->post('UserType')
        );

        $status = $this->_saveRegistrationDetails($uploadedFiles, $postData);

        if($status) {
            redirect('admin/listdrivers/gocab');
        } else {
            $this->load->view($this->input->post('redirectView'), $error);
            redirect('admin/listdrivers/gocab');
        }
    }

    public function saveRestaurantDetails()
    {
        $config['allowed_types']  = 'gif|jpg|png';
        $config['max_size']       = 0;

       if (!empty($_FILES['DisplayPicture']['name'])) {
            $config['upload_path'] = APPPATH . '../uploads/users/';
            $profilePictureName = time() . "-DP-" . $_FILES["DisplayPicture"]['name'];
            $config['file_name'] = $profilePictureName;

            $this->upload->initialize($config, true);

            if (!$this->upload->do_upload('DisplayPicture')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();

                $DisplayPicture = 'uploads/users/'.$config['file_name'];
            }
        }

        if (!empty($_FILES['IdentityDocument']['name'])) {
            $config['upload_path'] = APPPATH . '../uploads/users/';
            $identityDocumentName = time() . "-ID-" . $_FILES["IdentityDocument"]['name'];
            $config['file_name'] = $identityDocumentName;

            $this->upload->initialize($config, true);

            if (!$this->upload->do_upload('IdentityDocument')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();

                $IdentityDocument = 'uploads/users/'.$config['file_name'];
            }
        }

        if (!empty($_FILES['RestaurantPicture']['name'])) {
            $config['upload_path'] = APPPATH . '../uploads/restaurant/';
            $restaurantPictureName = time() . "-RP-" . $_FILES["RestaurantPicture"]['name'];
            $config['file_name'] = $restaurantPictureName;

            $this->upload->initialize($config, true);

            if (!$this->upload->do_upload('RestaurantPicture')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();

                $restaurantPicture = 'uploads/restaurant/'.$config['file_name'];
            }
        }
    
        $uploadedFiles = array(
            'IdentityDocument' => $IdentityDocument,
            'DisplayPicture' => $DisplayPicture,
            'ProviderImage' => $restaurantPicture
        );

        $postData = array(
            'Firstname' => $this->input->post('Firstname'),
            'Lastname' => $this->input->post('Lastname'),
            'Email' => $this->input->post('Email'),
            'Phone' => $this->input->post('Phone'),
            'DOB' => $this->input->post('DOB'),
            'PlaceOfBirth' => $this->input->post('PlaceOfBirth'),
            'IdentityNo' => $this->input->post('IdentityNo'),
            'Pincode' => $this->input->post('PinCode'),
            'City' => $this->input->post('City'),
            'State' => $this->input->post('State'),
            'Country' => $this->input->post('Country'),
            'JobCategory' => $this->input->post('JobCategory'),
            'UserType' => $this->input->post('UserType'),
            'ProviderName' => $this->input->post('RestaurantName'),
            'ProviderAddress' => $this->input->post('RestaurantAddress'),
            'ProviderPhone' => $this->input->post('RestaurantPhoneNumber'),
            'OpeningTime' => $this->input->post('OpeningTime'),
            'ClosingTime' => $this->input->post('ClosingTime'),
            'Latitude' => $this->input->post('Latitude'),
            'Longitude' => $this->input->post('Longitude'),
            'Description' => $this->input->post('Description')
        );

        $status = $this->_saveRestaurantDetails($uploadedFiles, $postData);

        if($status) {
            redirect('admin/listrestaurants/gofood');
        } else {
            $this->load->view($this->input->post('redirectView'), $error);
            redirect('admin/listdrivers/gofood');
        }
    }

    public function approveGocabDriver() {
        $this->secure();

        $gocabData = array(
            'JobCategory' => array(1, 2),
            'UserType' => 'D',
            'Status' => 'P'
        );
        $data['gocab'] = $this->_listUsersDetails($gocabData);
        $data['redirectView'] = 'gocab';
        
        $this->load->view('admin/header');  
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocab', $data);
        $this->load->view('admin/footer');
    }

    public function approveGorideDriver() {
        $this->secure();

        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];

        $gocabData = array(
            'JobCategory' => array(3),
            'UserType' => 'D',
            'Status' => 'P'
        );
        $data['gocab'] = $this->_listUsersDetails($gocabData,$franchiseId,$regions);
        $data['redirectView'] = 'goride';
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocab', $data);
        $this->load->view('admin/footer');
    }

    public function approveGoautoDriver() {

        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];

        $gocabData = array(
            'JobCategory' => array(4),
            'UserType' => 'D',
            'Status' => 'P'
        );
        $data['gocab'] = $this->_listUsersDetails($gocabData,$franchiseId,$regions);
        $data['redirectView'] = 'goauto';
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocab', $data);
        $this->load->view('admin/footer');
    }

    public function approveGofoodDeliveryPartner() {

        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];

        $gocabData = array(
            'JobCategory' => array(13),
            'UserType' => 'D',
            'Status' => 'P'
        );
        $data['gocab'] = $this->_listUsersDetails($gocabData,$franchiseId,$regions);
        $data['redirectView'] = 'gofood-deliverypartner';
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocab', $data);
        $this->load->view('admin/footer');
    }
    
    public function approveGoboxDriver() {
        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];

        $gocabData = array(
            'JobCategory' => array(5, 6, 7, 8, 9),
            'UserType' => 'D',
            'Status' => 'P'
        );
        $data['gocab'] = $this->_listUsersDetails($gocabData,$franchiseId,$regions);
        $data['redirectView'] = 'gobox';
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocab', $data);
        $this->load->view('admin/footer');
    }
    
    public function approveGosendDriver() {
        
        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];

        $gocabData = array(
            'JobCategory' => array(10),
            'UserType' => 'D',
            'Status' => 'P'
        );
        $data['gocab'] = $this->_listUsersDetails($gocabData,$franchiseId,$regions);
        $data['redirectView'] = 'gosend';
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocab', $data);
        $this->load->view('admin/footer');
    }

    public function approveGocargoDriver() {

        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];

        $gocabData = array(
            'JobCategory' => array(11),
            'UserType' => 'D',
            'Status' => 'P'
        );
        $data['gocab'] = $this->_listUsersDetails($gocabData,$franchiseId,$regions);
        $data['redirectView'] = 'gocargo';
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocab', $data);
        $this->load->view('admin/footer');
    }

    public function approveGofoodRestaurant() {
        
        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];

        $gocabData = array(
            'JobCategory' => array(12),
            'UserType' => 'R',
            'Status' => 'P'
        );
        $data['gofood'] = $this->_listRestaurantsDetails($gocabData,$franchiseId,$regions);
        $data['redirectView'] = 'gofood';
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gofood', $data);
        $this->load->view('admin/footer');
    }

    public function verifyStatus() {
        $userId = $this->input->post('StatusId');
        $status = $this->input->post('verifystatus');
        $jobCategory = $this->input->post('JobCategory');
        $userType = $this->input->post('UserType');
        $vehicleId = $this->input->post('VehicleID');
        $DriverEmail = $this->input->post('DriverEmail');
        $DriverPhone = $this->input->post('DriverPhone');
        $redirectView = $this->input->post('redirectView');
        
        if($status == 'A') {
            $generate = time().'1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
            $password_generate = substr(str_shuffle($generate), 0, 10);

            $userData = array(
                'UserId' => $userId,
                'Status' => $status,
                'JobCategory' => $jobCategory,
                'UserType' => $userType,
                'VehicleId' => $vehicleId,
                'Password' => password_hash($password_generate, PASSWORD_BCRYPT),
                'DriverPassword' => $password_generate,
                'DriverEmail' => $DriverEmail,
                'DriverPhone' => $DriverPhone,
                'ApprovedBy' => $this->session->userdata('user_id')
            );

            $data['userDetails'] = $this->_setVerifyStatus($userData);

            $this->_sendMail($userData['DriverEmail'], ''.APPNAME.'- New Driver Credentials', $this->load->view('admin/sendemaildriver.php', $userData, TRUE) );
            
            redirect('/admin/approve-driver/'.$redirectView.'?status=approved');
        } else {
            redirect('admin/approve-driver/'.$redirectView);
        }
    }

    public function verifyRestaurantStatus() {
        $userId = $this->input->post('StatusId');
        $status = $this->input->post('verifystatus');
        $jobCategory = $this->input->post('JobCategory');
        $userType = $this->input->post('UserType');
        $ProviderId = $this->input->post('ProviderId');
        $ProvicerEmail = $this->input->post('ProviderEmail');
        $ProviderPhone = $this->input->post('ProviderPhone');
        $redirectView = $this->input->post('redirectView');
        
        if($status == 'A') {
            $generate = time().'1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
            $password_generate = substr(str_shuffle($generate), 0, 10);

            $userData = array(
                'UserId' => $userId,
                'Status' => $status,
                'JobCategory' => $jobCategory,
                'UserType' => $userType,
                'ProviderId' => $ProviderId,
                'Password' => password_hash($password_generate, PASSWORD_BCRYPT),
                'ProviderPassword' => $password_generate,
                'ProviderEmail' => $ProvicerEmail,
                'ProviderPhone' => $ProviderPhone,
                'ApprovedBy' => $this->session->userdata('user_id')
            );

            $data['userDetails'] = $this->_setRestaurantVerifyStatus($userData);

            $this->_sendMail($userData['ProviderEmail'], ''.APPNAME.' - New Restaurant Credentials', $this->load->view('admin/sendemailprovider.php', $userData, TRUE) );
            
            redirect('/admin/approve-restaurant/'.$redirectView.'?status=approved');
        } else {
            redirect('admin/approve-driver/'.$redirectView);
        }
    }

    public function listDriverGocab() {
        $this->secure();

        $gocabData = array(
            'JobCategory' => array(1, 2),
            'UserType' => 'D',
            'Status' => 'A'
        );
        $data['gocab'] = $this->_listUsersDetails($gocabData,$this->session->userdata('user_id'));
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocablist', $data);
        $this->load->view('admin/footer');
    }

    public function listDriverGoride() {
        $this->secure();

        $gocabData = array(
            'JobCategory' => array(3),
            'UserType' => 'D',
            'Status' => 'A'
        );
        $data['gocab'] = $this->_listUsersDetails($gocabData,$this->session->userdata('user_id'));
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocablist', $data);
        $this->load->view('admin/footer');
    }

    public function listDriverGoauto() {
        $this->secure();

        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];

        $gocabData = array(
            'JobCategory' => array(4),
            'UserType' => 'D',
            'Status' => 'A'
        );
        $data['gocab'] = $this->_listUsersDetails($gocabData,$franchiseId,$regions);
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocablist', $data);
        $this->load->view('admin/footer');
    }

    public function listRestaurantGofood() {
        $this->secure();

        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];

        $gofoodData = array(
            'JobCategory' => array(12),
            'UserType' => 'R',
            'Status' => 'A'
        );
        $data['gofood'] = $this->_listRestaurantsDetails($gofoodData,$franchiseId,$regions);
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gofoodlist', $data);
        $this->load->view('admin/footer');
    }

    public function listDriverGofoodDeliveryPartner() {
        $this->secure();

        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];

        $gocabData = array(
            'JobCategory' => array(13),
            'UserType' => 'D',
            'Status' => 'A'
        );
        $data['gocab'] = $this->_listUsersDetails($gocabData,$franchiseId,$regions);
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocablist', $data);
        $this->load->view('admin/footer');
    }

    public function listDriverGobox() {
        $this->secure();

        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];

        $gocabData = array(
            'JobCategory' => array(5, 6, 7, 8, 9),
            'UserType' => 'D',
            'Status' => 'A'
        );
        $data['gocab'] = $this->_listUsersDetails($gocabData,$franchiseId,$regions);
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocablist', $data);
        $this->load->view('admin/footer');
    }

    public function listDriverGosend() {
        $this->secure();

        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];

        $gocabData = array(
            'JobCategory' => array(10),
            'UserType' => 'D',
            'Status' => 'A'
        );
        $data['gocab'] = $this->_listUsersDetails($gocabData,$franchiseId,$regions);
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocablist', $data);
        $this->load->view('admin/footer');
    }

    public function listDriverGocargo() {
        $this->secure();

        $regionInfo = $this->getFranchisePincodes();
        $franchiseId = $regionInfo['franchiseId'];
        $regions = $regionInfo['regions'];
        
        $gocabData = array(
            'JobCategory' => array(11),
            'UserType' => 'D',
            'Status' => 'A'
        );
        $data['gocab'] = $this->_listUsersDetails($gocabData,$franchiseId,$regions);
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocablist', $data);
        $this->load->view('admin/footer');
    }

    public function listDriverDetails() {
     //   $this->secure();
        if (isset($_GET['view'])) {
            $userId = $_GET['view'];
        }
        else if (isset($_GET['update'])) {
            $userId = $_GET['update'];
        } else if(isset($_GET['delete'])) {
            $userId = $_GET['delete'];
        }
        $jobId = $_GET['job'];
        $userData = array(
            'JobCategory' => $jobId,
            'UserType' => 'D',
            'Status' => 'A',
            'UserId' => $userId
        );

        $data['userDetails'] = $this->_getUserDetails($userData);
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/gocabuserdetails', $data);
        $this->load->view('admin/footer');
    }

    public function listRestaurantDetails() {
        //   $this->secure();
           if (isset($_GET['view'])) {
               $userId = $_GET['view'];
           }
           else if (isset($_GET['update'])) {
               $userId = $_GET['update'];
           } else if(isset($_GET['delete'])) {
               $userId = $_GET['delete'];
           }
           $jobId = $_GET['job'];
           $userData = array(
               'JobCategory' => $jobId,
               'UserType' => 'R',
               'Status' => 'A',
               'UserId' => $userId
           );
   
           $data['userDetails'] = $this->_getRestaurantDetails($userData);
    
           $this->load->view('admin/header');
           $this->load->view('admin/navigation'); 
           $this->load->view('admin/gofooduserdetails', $data);
           $this->load->view('admin/footer');
       }
    
    public function updateDriverDetails() {
        $this->secure();
        $config['allowed_types']  = 'gif|jpg|png';
        $config['max_size']       = 2000;

        if (!empty($_FILES['DriverLicense']['name'])) {
            $config['upload_path'] = APPPATH. '../uploads/drivers/';
            $drivingLicenceDocName = time()."-DL-".$_FILES["DriverLicense"]['name'];
            $config['file_name'] = $drivingLicenceDocName;

            $this->upload->initialize($config,true);

            if (!$this->upload->do_upload('DriverLicense')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();
                $DriverLicense = 'uploads/drivers/'.$config['file_name'];
            }
        }

        if (!empty($_FILES['DisplayPicture']['name'])) {
            $config['upload_path'] = APPPATH. '../uploads/users/';
            $profilePictureName = time()."-DP-".$_FILES["DisplayPicture"]['name'];
            $config['file_name'] = $profilePictureName;

            $this->upload->initialize($config,true);
                
            if ( ! $this->upload->do_upload('DisplayPicture')){
                $error = array('error' => $this->upload->display_errors());
            }else{
                $upload_data = $this->upload->data();
                $DisplayPicture = 'uploads/users/'.$config['file_name'];
            }
        }
        
        if (!empty($_FILES['IdentityDocument']['name'])) {
            $config['upload_path'] = APPPATH. '../uploads/users/';
            $identityDocumentName = time()."-ID-".$_FILES["IdentityDocument"]['name'];
            $config['file_name'] = $identityDocumentName;

            $this->upload->initialize($config,true);

            if ( ! $this->upload->do_upload('IdentityDocument')){
                $error = array('error' => $this->upload->display_errors());
            }else{
                $upload_data = $this->upload->data();
                $IdentityDocument = 'uploads/users/'.$config['file_name'];
            }
        }

        if (!empty($_FILES['VehiclePermit']['name'])) {
            $config['upload_path'] = APPPATH. '../uploads/permit/';
            
            $vehiclePermitDocName = time().'-VP'.$_FILES["VehiclePermit"]['name'];
            $config['file_name'] = $vehiclePermitDocName;

            $this->upload->initialize($config,true);

            if ( ! $this->upload->do_upload('VehiclePermit')){
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();
                $VehiclePermit = 'uploads/permit/'.$config['file_name'];
            }
        } 

        $DisplayPicture = isset($DisplayPicture) ? $DisplayPicture : $this->input->post('ProfileAvatarOld');
		$IdentityDocument = isset($IdentityDocument) ? $IdentityDocument : $this->input->post('IdentityDocumentOld') ;
		$VehiclePermit = isset($VehiclePermit) ? $VehiclePermit : $this->input->post('VehiclePermitOld');
        $DriverLicense = isset($DriverLicense) ? $DriverLicense: $this->input->post('IdentityDocumentOld');
        
        $uploadedFiles = array(
            'IdentityDocument' => $IdentityDocument,
            'DriverLicense' => $DriverLicense,
            'DisplayPicture' => $DisplayPicture,
            'VehiclePermit' => $VehiclePermit,
        );

        $postData = array(
            'UserId' => $this->input->post('UserId'),
            'Firstname' => $this->input->post('Firstname'),
            'Lastname' => $this->input->post('Lastname'),
            'Email' => $this->input->post('Email'),
            'Phone' => $this->input->post('Phone'),
            'DOB' => $this->input->post('DOB'),
            'PlaceOfBirth' => $this->input->post('PlaceOfBirth'),
            'IdentityNo' => $this->input->post('IdentityNo'),
            'Pincode' => $this->input->post('PinCode'),
            'Country' => $this->input->post('Country'),
            'JobCategory' => $this->input->post('JobCategory'),
            'DisplayPicture' => $DisplayPicture,
            'IdentityDocument' => $IdentityDocument,
            'VehicleID' => $this->input->post('VehicleID'),
            'VehicleNumber' => $this->input->post('VehicleNumber'),
            'VehicleBrand' => $this->input->post('VehicleBrand'),
            'VehicleColor' => $this->input->post('VehicleColor'),
            'VehicleType' => $this->input->post('VehicleType'),
            'VehiclePermit' => $VehiclePermit,
            'JobCategory_JobCategoryId' => $this->input->post('JobCategory'),
            'DriverId'=> $this->input->post('DriverId'),
            'DriverName'=> $this->input->post('DriverName'),
            'DrivingLicense'=> $this->input->post('DriverLicenseNo'),
            'DrivingLicenseDocument'=> $DriverLicense
        );

        $data['updateUserDetails'] = $this->_updateUserDetails($postData);
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/updatesuccess');
        $this->load->view('admin/footer');   
    }

    public function updateRestaurantDetails() {
        $this->secure();
        $config['allowed_types']  = 'gif|jpg|png';
        $config['max_size']       = 2000;

        
        if (!empty($_FILES['DisplayPicture']['name'])) {
            $config['upload_path'] = APPPATH. '../uploads/users/';
            $profilePictureName = time()."-DP-".$_FILES["DisplayPicture"]['name'];
            $config['file_name'] = $profilePictureName;

            $this->upload->initialize($config,true);
                
            if ( ! $this->upload->do_upload('DisplayPicture')){
                $error = array('error' => $this->upload->display_errors());
            }else{
                $upload_data = $this->upload->data();
                $DisplayPicture = 'uploads/users/'.$config['file_name'];
            }
        }
        
        if (!empty($_FILES['IdentityDocument']['name'])) {
            $config['upload_path'] = APPPATH. '../uploads/users/';
            $identityDocumentName = time()."-ID-".$_FILES["IdentityDocument"]['name'];
            $config['file_name'] = $identityDocumentName;

            $this->upload->initialize($config,true);

            if ( ! $this->upload->do_upload('IdentityDocument')){
                $error = array('error' => $this->upload->display_errors());
            }else{
                $upload_data = $this->upload->data();
                $IdentityDocument = 'uploads/users/'.$config['file_name'];
            }
        }

        if (!empty($_FILES['RestaurantPicture']['name'])) {
            $config['upload_path'] = APPPATH . '../uploads/restaurant/';
            $restaurantPictureName = time() . "-RP-" . $_FILES["RestaurantPicture"]['name'];
            $config['file_name'] = $restaurantPictureName;

            $this->upload->initialize($config, true);

            if (!$this->upload->do_upload('RestaurantPicture')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();

                $restaurantPicture = 'uploads/restaurant/'.$config['file_name'];
            }
        }

        $DisplayPicture = isset($DisplayPicture) ? $DisplayPicture : $this->input->post('ProfileAvatarOld');
		$IdentityDocument = isset($IdentityDocument) ? $IdentityDocument : $this->input->post('IdentityDocumentOld') ;
        $restaurantPicture = isset($restaurantPicture) ? $restaurantPicture: $this->input->post('RestaurantPictureOld');
        
        $uploadedFiles = array(
            'IdentityDocument' => $IdentityDocument,
            'DisplayPicture' => $DisplayPicture,
            'ProviderImage' => $restaurantPicture,
        );

        $postData = array(
            'UserId' => $this->input->post('UserId'),
            'Firstname' => $this->input->post('Firstname'),
            'Lastname' => $this->input->post('Lastname'),
            'Email' => $this->input->post('Email'),
            'Phone' => $this->input->post('Phone'),
            'DOB' => $this->input->post('DOB'),
            'PlaceOfBirth' => $this->input->post('PlaceOfBirth'),
            'IdentityNo' => $this->input->post('IdentityNo'),
            'Pincode' => $this->input->post('PinCode'),
            'Country' => $this->input->post('Country'),
            'JobCategory' => $this->input->post('JobCategory'),
            'DisplayPicture' => $DisplayPicture,
            'IdentityDocument' => $IdentityDocument,
            'ProviderId' => $this->input->post('ProviderId'),
            'ProviderName' => $this->input->post('RestaurantName'),
            'ProviderAddress' => $this->input->post('RestaurantAddress'),
            'ProviderPhone' => $this->input->post('RestaurantPhoneNumber'),
            'OpeningTime' => $this->input->post('OpeningTime'),
            'ClosingTime' => $this->input->post('ClosingTime'),
            'Latitude' => $this->input->post('Latitude'),
            'Longitude' => $this->input->post('Longitude'),
            'Description' => $this->input->post('Description'),
            'ProviderImage' => $restaurantPicture
        );
        
        $data['updateUserDetails'] = $this->_updateRestaurantDetails($postData);
        
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/updatesuccess');
        $this->load->view('admin/footer');   
    }

    public function deleteDriverDetails() {
        
        $postData = array(
            'UserId' => $this->input->post('UserId'),
            'VehicleID' => $this->input->post('VehicleID'),
            'DriverId'=> $this->input->post('VehicleID')
        );
        $data['deleteUserDetails'] = $this->_deleteUserDetails($postData);
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/deletesuccess', $data);
        $this->load->view('admin/footer');
    }

    public function deleteRestaurantDetails() {
        
        $postData = array(
            'UserId' => $this->input->post('UserId'),
            'ProviderId' => $this->input->post('ProviderId')
        );
        $data['deleteUserDetails'] = $this->_deleteRestaurantDetails($postData);
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/deletesuccess', $data);
        $this->load->view('admin/footer');
    }

    public function getFranchisePincodes() {
        $regions =' ';
        $franchiseId = ' ';
        $franchiseData = $this->_findFranchise($this->session->userdata('user_id'));
        $franchiseId = ($this->session->userdata('userType')=='A') ? '' : $franchiseData->FranchiseId;
        $getRegions = $this->_getPincodes($franchiseId);
        $regions = array ();
        $regionData['franchiseId'] = $franchiseId;
        if($franchiseId!='') {
            foreach($getRegions as $reg) {
                $regions[] = $reg->Pincode;
            }
        }
        $regionData['regions'] = $regions;
        return $regionData;
    }

    public function manualTransactions($link) {
        $this->secure();
        if($link=='approved') {  $status='A'; } elseif($link=='pending') {  $status='P'; } elseif($link=='cancelled') {  $status='C'; }


        $requestData['status']=$status;
        $requestData['requests'] = $this->_getManualTransactions($status);
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/manual-transactions',$requestData); 
        $this->load->view('admin/footer');
    }

    public function transactionDetails($id) {
        $transData['data'] = $this->_getTransactionInfo($id); 
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/transaction-details',$transData); 
        $this->load->view('admin/footer');
    }

    public function users() {
        $this->secure();
        $users = $this->_getAllUsers();
        if($users){
            $userData['users'] = $users;
           } else {
            $userData['users'] = '';
           }
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/users',$userData); 
        $this->load->view('admin/footer');
    }

    public function ViewUser($id) {
        $userData['data'] = $this->_findUser($id); 
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/view-user',$userData); 
        $this->load->view('admin/footer');
    }

    public function ViewDriver($id) {
        $userData['data'] = $this->_findUserDetails($id); 
        $userData['rides'] = $this->_driverRides($id); 
        $userData['topups'] = $this->_getAllTransactions($id);
        $userData['totalrides'] = count($this->_driverRides($id));
        $userData['accepted'] = count($this->_driverRidesStatus($id,'C'));
        $userData['cancelled'] = count($this->_driverRidesStatus($id,'D'));
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/view-driver',$userData); 
        $this->load->view('admin/footer');
    }

    public function ViewRideDetails($id) {
        $userData['data'] = $this->_findRide($id); 
        $this->load->view('admin/header');
        $this->load->view('admin/navigation');
        $this->load->view('admin/view-ride',$userData); 
        $this->load->view('admin/footer');
    }

    public function GetZoneDrivers() {
        $regions = $this->_getPincodes($this->input->post('id'));
        if($regions) {
             $pincodes = array();
             foreach($regions as $reg) {
                    $pincodes[] = $reg->Pincode;
             }
            $drivers = $this->_getDrivers($pincodes);
            $output = array();
            foreach ($drivers as $row) {
            $output[] = array( 'id' => $row->UserId);
            }
            echo json_encode($output);
            // return $drivers;
            //$this->load->view('admin/_nearbydrivers',$userData); 
        }
       
    }



}