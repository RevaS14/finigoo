<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('ServiceController.php');


class HomeController extends CI_Controller {

    public function __construct() {
		parent::__construct();
    }

	public function index()
	{
	    $this->load->view('home/header');
        $this->load->view('home/index');
        $this->load->view('home/footer');
	}

	public function aboutUs()
	{
	    $this->load->view('home/header');
        $this->load->view('home/aboutus');
        $this->load->view('home/footer');
	}

	public function android()
	{
	    $this->load->view('home/header');
        $this->load->view('home/android');
        $this->load->view('home/footer');
	}
	
	public function bike()
	{
	    $this->load->view('home/header');
        $this->load->view('home/bike');
        $this->load->view('home/footer');
	}

	public function bills()
	{
	    $this->load->view('home/header');
        $this->load->view('home/bills');
        $this->load->view('home/footer');
	}

	public function cabs()
	{
	    $this->load->view('home/header');
        $this->load->view('home/cabs');
        $this->load->view('home/footer');
	}

	public function career()
	{
	    $this->load->view('home/header');
        $this->load->view('home/career');
        $this->load->view('home/footer');
	}
	
	public function contactUs()
	{
	    $this->load->view('home/header');
        $this->load->view('home/contactus');
        $this->load->view('home/footer');
	}
	
	public function expressDeliveryDriver()
	{
	    $this->load->view('home/header');
        $this->load->view('home/expressdeliverydriver-tab');
        $this->load->view('home/footer');
	}

	public function foodDeliveryDriver()
	{
	    $this->load->view('home/header');
        $this->load->view('home/fooddeliverydriver-tab');
        $this->load->view('home/footer');
	}

	public function goDriver()
	{
	    $this->load->view('home/header');
        $this->load->view('home/godriver-tab');
        $this->load->view('home/footer');
	}

	public function franchise()
	{
	    $this->load->view('home/header');
        $this->load->view('home/franchise');
        $this->load->view('home/footer');
	}

	public function goAds()
	{
	    $this->load->view('home/header');
        $this->load->view('home/goads');
        $this->load->view('home/footer');
	}

	public function goAuto()
	{
	    $this->load->view('home/header');
        $this->load->view('home/goauto');
        $this->load->view('home/footer');
	}
	
	public function goBeauty()
	{
	    $this->load->view('home/header');
        $this->load->view('home/gobeauty');
        $this->load->view('home/footer');
	}

	public function goCashBack()
	{
	    $this->load->view('home/header');
        $this->load->view('home/gocashback');
        $this->load->view('home/footer');
	}

	public function goDemand()
	{
	    $this->load->view('home/header');
        $this->load->view('home/godemand');
        $this->load->view('home/footer');
	}

	public function goFood()
	{
	    $this->load->view('home/header');
        $this->load->view('home/gofood');
        $this->load->view('home/footer');
	}

	public function GoKiosk()
	{
	    $this->load->view('home/header');
        $this->load->view('home/gokiosk');
        $this->load->view('home/footer');
	}

	public function GoLoan()
	{
	    $this->load->view('home/header');
        $this->load->view('home/goloan');
        $this->load->view('home/footer');
	}

	public function GoMart()
	{
	    $this->load->view('home/header');
        $this->load->view('home/gomart');
        $this->load->view('home/footer');
	}

	public function GoMedical()
	{
	    $this->load->view('home/header');
        $this->load->view('home/gomedical');
        $this->load->view('home/footer');
	}

	public function GoPack()
	{
	    $this->load->view('home/header');
        $this->load->view('home/gopack');
        $this->load->view('home/footer');
	}

	public function GoPay()
	{
	    $this->load->view('home/header');
        $this->load->view('home/gopay');
        $this->load->view('home/footer');
	}

	public function GoPoints()
	{
	    $this->load->view('home/header');
        $this->load->view('home/gopoints');
        $this->load->view('home/footer');
	}

	public function GoUpi()
	{
	    $this->load->view('home/header');
        $this->load->view('home/goupi');
        $this->load->view('home/footer');
	}

	public function Grocery()
	{
	    $this->load->view('home/header');
        $this->load->view('home/grocery');
        $this->load->view('home/footer');
	}

	public function Help()
	{
	    $this->load->view('home/header');
        $this->load->view('home/help');
        $this->load->view('home/footer');
	}

	public function Jobs()
	{
	    $this->load->view('home/header');
        $this->load->view('home/jobs');
        $this->load->view('home/footer');
	}

	public function Login()
	{
        $this->load->view('home/login');
	}

	public function money()
	{
	    $this->load->view('home/header');
        $this->load->view('home/money');
        $this->load->view('home/footer');
	}

	public function pressMeet()
	{
	    $this->load->view('home/header');
        $this->load->view('home/press-meet');
        $this->load->view('home/footer');
	}

	public function pricing()
	{
	    $this->load->view('home/header');
        $this->load->view('home/pricing');
        $this->load->view('home/footer');
	}

	public function privacyPolicy()
	{
	    $this->load->view('home/header');
        $this->load->view('home/privacy-policy');
        $this->load->view('home/footer');
	}

	public function promotor()
	{
        $this->load->view('home/promotor');
	}

	public function safety()
	{
	    $this->load->view('home/header');
        $this->load->view('home/safety');
        $this->load->view('home/footer');
	}

	public function send()
	{
	    $this->load->view('home/header');
        $this->load->view('home/send');
        $this->load->view('home/footer');
	}

	public function signup()
	{
        $this->load->view('home/signup');
	}
	
	public function tab()
	{
        $this->load->view('home/tab');
	}

	public function tracking()
	{
        $this->load->view('home/tracking');
	}

}
