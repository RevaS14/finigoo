<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('ServiceController.php');

class AddDriverController extends ServiceController {

    public function __construct() {
		parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('upload');
    }

    public function addDriverGocab() {
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/adddrivergocab');
        $this->load->view('admin/footer');
    }

    public function addDriverGoride() {
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/adddrivergoride');
        $this->load->view('admin/footer');
    }

    public function addDriverGoauto() {
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/adddrivergoauto');
        $this->load->view('admin/footer');
    }
    
    function thankyou() {
        $this->load->view('admin/header');
        $this->load->view('admin/navigation'); 
        $this->load->view('admin/thankyou');
        $this->load->view('admin/footer');
    }

    public function saveDriverDetails() {
        if (!empty($_FILES['DriverLicense']['name'])) {
            //set DriverAvatar file upload settings 
            $config['upload_path'] = APPPATH. '../uploads/drivers/';
            $config['allowed_types']  = 'gif|jpg|png';
            $config['max_size']       = 100;
            $new_name = time().$_FILES["DriverLicense"]['name'];
            $config['file_name'] = $new_name;
            //$this->load->library('upload');
            $this->upload->initialize($config,true);

            if ( ! $this->upload->do_upload('DriverLicense')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();

                $data['DriverLicense'] = $upload_data['file_name'];
                $DriverLicense = $data['DriverLicense'];
            }
        }

        if (!empty($_FILES['ProfileAvatar']['name'])) {
            $config['upload_path'] = APPPATH. '../uploads/users/';
            $config['allowed_types']  = 'gif|jpg|png';
            $config['max_size']       = 100;
            $new_name = time().$_FILES["ProfileAvatar"]['name'];
            $config['file_name'] = $new_name;
            //$this->load->library('upload');
            $this->upload->initialize($config,true);
                
            if ( ! $this->upload->do_upload('ProfileAvatar')){
                $error = array('error' => $this->upload->display_errors());
            }else{
                $upload_data = $this->upload->data();

                $data['ProfileAvatar'] = $upload_data['file_name'];
                $ProfileAvatar = $data['ProfileAvatar'];
            }
        }
        
        if (!empty($_FILES['IdentityDocument']['name'])) {
            $config['upload_path'] = APPPATH. '../uploads/users/';
            $config['allowed_types']  = 'gif|jpg|png';
            $config['max_size']       = 100;
            $new_name = time().$_FILES["IdentityDocument"]['name'];
            $config['file_name'] = $new_name;

            $this->upload->initialize($config,true);
            if ( ! $this->upload->do_upload('IdentityDocument')){
                $error = array('error' => $this->upload->display_errors());
            }else{
                $upload_data = $this->upload->data();

                $data['IdentityDocument'] = $upload_data['file_name'];
                $IdentityDocument = $data['IdentityDocument'];
            }
        }


        if (!empty($_FILES['VehiclePermit']['name'])) {
            //set VehicleAvatar file upload settings
            $config['upload_path']    = APPPATH. '../uploads/permit/';
            $config['allowed_types']  = 'gif|jpg|png';
            $config['max_size']       = 100;    
            $new_name = time().$_FILES["VehiclePermit"]['name'];
            $config['file_name'] = $new_name;

            $this->upload->initialize($config,true);

            if ( ! $this->upload->do_upload('VehiclePermit')){
                $error = array('error' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();
                
                $data['VehiclePermit'] = $upload_data['file_name'];
                $VehiclePermit = $data['VehiclePermit'];
            }
        } 
        
        $ProfileAvatar = isset($data['ProfileAvatar']) ? $data['ProfileAvatar'] : '' ;
		$IdentityDocument = isset($data['IdentityDocument']) ? $data['IdentityDocument'] : '' ;
		$VehiclePermit = isset($data['VehiclePermit']) ? $data['VehiclePermit'] : '' ;
        $DriverLicense = isset($data['DriverLicense']) ? $data['DriverLicense'] : '' ;
        
        $uploadedFiles = array(
            'IdentityDocument' => $IdentityDocument,
            'DriverLicense' => $DriverLicense,
            'ProfileAvatar' => $ProfileAvatar,
            'VehiclePermit' => $VehiclePermit,
        );

        $postData = array(
            'Firstname' => $this->input->post('Firstname'),
            'Lastname' => $this->input->post('Lastname'),
            'Email' => $this->input->post('Email'),
            'Phone' => $this->input->post('Phone'),
            'DOB' => $this->input->post('DOB'),
            'PlaceOfBirth' => $this->input->post('PlaceOfBirth'),
            'IdentityNo' => $this->input->post('IdentityNo'),
            'Pincode' => $this->input->post('PinCode'),
            'VehicleNumber' => $this->input->post('VehicleNumber'),
			'VehicleBrand' => $this->input->post('VehicleBrand'),
			'VehicleColor' => $this->input->post('VehicleColor'),
            'VehicleType' => $this->input->post('VehicleType'),
            'DriverName' => $this->input->post('DriverName'),
            'DrivingLicense' => $this->input->post('DriverLicenseNo'),
            'Country' => $this->input->post('Country'),
            'JobCategory' => $this->input->post('JobCategory'),
            'UserType' => $this->input->post('UserType')
        );

        $status = $this->_saveRegistrationDetails($uploadedFiles, $postData);

        if($status) {
            redirect('admin/adddriver/thank-you/');
        } else {
            $this->load->view($this->input->post('redirectView'), $error);
            redirect('admin/adddriver/gocab');
        }
    }
}   