<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceController extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('driver_model');
        $this->load->model('vehicle_model');
        $this->load->model('franchise_model');
        $this->load->model('franchises_region_model');
        $this->load->model('commission_model');
        $this->load->model('peakhour_model');
        $this->load->model('topup_model');
        $this->load->model('provider_model');
        $this->load->model('driver_collection_model');
        $this->load->model('withdrawal_model');
        $this->load->model('provider_category_model');
        $this->load->model('job_category_model');
        $this->load->model('provider_model');
        $this->load->model('provider_sub_category_model');
        $this->load->model('item_model');
        $this->load->model('provider_promotion_model');
        $this->load->model('zone_model');
        $this->load->model('region_model');
        $this->load->model('ride_model');
        $this->load->model('manual_transaction_model');
        
        $this->load->model('tracking_model');
    }
    
	public function _saveRegistrationDetails($uploadedFiles, $postData) {
        $status = true;
        try {
            $establishmentId = $postData['Country'];
            
            $userData = array(
                'FirstName' => $postData['Firstname'],
                'LastName' => $postData['Lastname'],
                'Email' => $postData['Email'],
                'Phone' => $postData['Phone'],
                'DOB' => $postData['DOB'],
                'PlaceOfBirth' => $postData['PlaceOfBirth'],
                'DisplayPicture' => $uploadedFiles['DisplayPicture'],
                'IdentityNo' => $postData['IdentityNo'],
                'IdentityDocument' => $uploadedFiles['IdentityDocument'],
                'Establishment_EstablishmentId' => $establishmentId,
                'Pincode' => $postData['Pincode'],
                'City' => $postData['City'],
                'State' => $postData['State'],
                'UserType' => $postData['UserType'],
                'Status' => 'P'
             );

           $userId = $this->user_model->insertUser($userData);
    
            $countryid = $postData['Country'];
            $getJobId = $postData['JobCategory'];
                
            $vehicleData = array(
                'Users_UserId' => $userId,
                'VehicleNumber' => $postData['VehicleNumber'],
                'VehicleBrand' => $postData['VehicleBrand'],
                'VehicleColor' => $postData['VehicleColor'],
                'VehicleType' => $postData['VehicleType'],
                'VehiclePermit' => $uploadedFiles['VehiclePermit'],
                'JobCategory_JobCategoryId' => $getJobId
            );

            $vehicleId = $this->vehicle_model->insertVehicle($vehicleData);
    
            $driverData = array(
                'Vehicles_VehicleId' => $vehicleId,
                'DriverName' => $postData['DriverName'],
                'DrivingLicense' => $postData['DrivingLicense'],
                'DrivingLicenseDocument' => $uploadedFiles['DriverLicense'],
                'Status' => 'P'
            );
            
            $vehicleId = $this->driver_model->insertDriver($driverData);

        } catch (Exception $e) {
            $status = false;
        }

        return $status;
    }

    public function _saveRestaurantDetails($uploadedFiles, $postData) {
        $status = true;
        try {
            $establishmentId = $postData['Country'];
            
            $userData = array(
                'FirstName' => $postData['Firstname'],
                'LastName' => $postData['Lastname'],
                'Email' => $postData['Email'],
                'Phone' => $postData['Phone'],
                'DOB' => $postData['DOB'],
                'PlaceOfBirth' => $postData['PlaceOfBirth'],
                'DisplayPicture' => $uploadedFiles['DisplayPicture'],
                'IdentityNo' => $postData['IdentityNo'],
                'IdentityDocument' => $uploadedFiles['IdentityDocument'],
                'Establishment_EstablishmentId' => $establishmentId,
                'Pincode' => $postData['Pincode'],
                'City' => $postData['City'],
                'State' => $postData['State'],
                'UserType' => $postData['UserType'],
                'Status' => 'P'
             );

           $userId = $this->user_model->insertUser($userData);
    
            $providersData = array(
                'Users_UserId' => $userId,
                'ProviderName' => $postData['ProviderName'],
                'ProviderAddress' => $postData['ProviderAddress'],
                'ProviderPhone' => $postData['ProviderPhone'],
                'OpeningTime' => $postData['OpeningTime'],
                'ClosingTime' => $postData['ClosingTime'],
                'Latitude' => $postData['Latitude'],
                'Longitude' => $postData['Longitude'],
                'Description' => $postData['Description'],
                'ProviderImage' => $uploadedFiles['ProviderImage'],
                'JobCategories_JobCategoryId' => $postData['JobCategory'],
                'Status' => 'P'
            );

            $providersId = $this->provider_model->insertprovider($providersData);

        } catch (Exception $e) {
            $status = false;
        }

        return $status;
    }
    
    public function _adminLogin($loginData, $type) {
        return $this->user_model->adminLogin($loginData, $type);
    }

    public function _getAllFranchise() {
        return $this->user_model->getAllFranchise();
    }

    public function _listUsersDetails($gocabData) {
        return $this->user_model->listUsersDetails($gocabData);
    }

    public function _getUserDetails($userData) {
        return $this->user_model->getUserDetails($userData);
    }

    public function _getRestaurantDetails($userData) {
        return $this->user_model->getRestaurantDetails($userData);
    }

    public function _updateUserDetails($postData) {
        return $this->user_model->updateUserDetails($postData);
    }

    public function _updateRestaurantDetails($postData) {
        return $this->user_model->updateRestaurantDetails($postData);
    }

    public function _updateVehicleDetails($postData) {
        return $this->vehicle_model->updateVehicleDetails($postData);
    }
    
    public function _updateDriverDetails($postData) {
        return $this->driver_model->updateDriverDetails($postData);
    }

    public function _deleteUserDetails($postData) {
        return $this->user_model->deleteUserDetails($postData);
    }

    public function _deleteRestaurantDetails($postData) {
        return $this->user_model->deleteRestaurantDetails($postData);
    }

    public function _setVerifyStatus($userData) {
        $this->user_model->setVerifyStatus($userData);
        $this->vehicle_model->setVerifyStatus($userData);
        $this->driver_model->setVerifyStatus($userData);


    //     $trackingData = array(
    //         'driverId' => $userData['UserId'],
    //         'vehicleType' => $userData['JobCategory'],
    //         'available' => 'N',
    //         'zoneId' => 1
    //     );
        
    //    $this->tracking_model->_insertDriver($trackingData);
        return true;
    }

    public function _setRestaurantVerifyStatus($userData) {
        $this->user_model->setVerifyStatus($userData);
        $this->provider_model->setVerifyStatus($userData);

        $trackingData = array(
            'driverId' => $userData['UserId'],
            'vehicleType' => $userData['JobCategory'],
            'available' => 'N'
        );
        
       $this->tracking_model->_insertDriver($trackingData);
        return true;
    }

    public function _sendMail($to, $subject, $message) {
        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'revathi@haqto.in';
        $config['smtp_pass']    = 'revathisunder';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->load->library('email', $config);
        $this->email->initialize($config);

        $this->email->from('revathi@haqto.in', 'Haqto');
        $this->email->to($to); 
        $this->email->subject($subject);
        $this->email->message($message);  

        $this->email->send();
    }
    
    public function _getAllEstablishment() {
        return $this->user_model->getAllEstablishment();
    }

    public function _saveUser($userData) {
        return $this->user_model->insertUser($userData);
    }

    public function _saveFranchise($userData) {
        return $this->franchise_model->insertFranchise($userData);
    }
    public function _saveRegions($franchise_id,$pincode) {
        return $this->region_model->saveRegions($franchise_id,$pincode);
    }
    public function _findUser($id) {
        return $this->user_model->findUser($id);
    }

    public function _findFranchise($id) {
        return $this->franchise_model->findFranchise($id);
    }

    public function _updateUser($id,$userData) {
        return $this->user_model->updateUser($id,$userData);
    }
    public function _updateFranchise($id,$franchiseData) {
        return $this->franchise_model->updateFranchise($id,$franchiseData);
    }

    public function _getRegions($franchise_id) {
        return $this->region_model->getRegions($franchise_id);
    }

    public function _getPincodes($franchise_id) {
        return $this->region_model->_getPincodes($franchise_id);
    }

    public function _getDrivers($pincodes) {
        return $this->user_model->_getDrivers($pincodes);
    }

    public function _updateRegions($franchise_id,$regions) {
        return $this->region_model->updateRegions($franchise_id,$regions);
    }

    public function _updateUserStatus($userId,$statusType) {
       return $this->user_model->updateUserStatus($userId,$statusType);
    }
    public function _updateFranchiseStatus($userId,$statusType) {
        return $this->franchise_model->updateFranchiseStatus($userId,$statusType);
    }
    public function _deleteUser($id) {
        return $this->user_model->deleteUser($id);
    }

    public function _saveCommission($costData) {
       return $this->commission_model->saveCommission($costData);
    }

    public function _savePeakHours($peakHoursData) {
        return $this->peakhour_model->savePeakHours($peakHoursData);
    }

    public function _checkCommission($userId,$jobCategoryId) {
        return $this->commission_model->checkCommission($userId,$jobCategoryId);
    }
    public function _getAllCategoryCommission($userId) {
        return $this->commission_model->getAllCategoryCommission($userId);
    }

    public function _updateCommission($commissionId,$costData) {
        return $this->commission_model->updateCommission($commissionId,$costData);
    }

    public function _getPeakHours($franchiseId,$zoneId,$jobCategoryId) {
        return $this->peakhour_model->getPeakHours($franchiseId,$zoneId,$jobCategoryId);
    }

    public function _deletePeakHour($peakHourId) {
        return $this->peakhour_model->deletePeakHour($peakHourId);
    }

    public function _getAllCommissions($status,$userId) {
        return $this->commission_model->getAllCommissions($status,$userId);
    }

    public function _viewCommissions($commissionId) {
        return $this->commission_model->viewCommissions($commissionId);
    }

    public function _updateCommissionStatus($commissionId,$status,$franchiseId,$jobCategoryId) {
        return $this->commission_model->updateCommissionStatus($commissionId,$status,$franchiseId,$jobCategoryId);
    }

    public function _addAdminCommission($costData,$commissionId) {
        return $this->commission_model->addAdminCommission($costData,$commissionId);
    }

    public function _updatPeakHourStatus($peakId,$status) {
        return $this->peakhour_model->updatePeakHourStatus($peakId,$status);
    }

    public function _getFranchiseIdByUserId($userId) {
        return $this->franchise_model->_getFranchiseIdByUserId($userId);
    }
    public function _updateWallet($userId,$orderAmount,$type) {
        return $this->user_model->updateWallet($userId,$orderAmount,$type);
    }

    public function _updateTopUp($topUpId,$updateData) {
        return $this->topup_model->updateTopUp($topUpId,$updateData);
    }

    public function _insertTopup($topUpData) {
        return $this->topup_model->insertTopup($topUpData);
    }

    public function _findTopup($topupId) {
       return $this->topup_model->findTopup($topupId);
    }

    public function _listRestaurantsDetails($gocabData, $franchiseId, $regions) {
        return $this->user_model->listRestaurantsDetails($gocabData, $franchiseId, $regions);
    }

    public function _getAllTransactions($id) {
        return $this->topup_model->getAllTransactions($id);
    }

    public function _getDriverCollections($status,$franchiseId, $regions) {
        return $this->driver_collection_model->getDriverCollections($status,$franchiseId, $regions);
    }
    public function _collectAmount($rideId) {
        return $this->driver_collection_model->collectAmount($rideId);
    }

    public function _getWithdrawRequests($status, $franchiseId, $regions) {
        return $this->withdrawal_model->_getWithdrawRequests($status, $franchiseId, $regions);
    }

    public function _changeWithdrawStatus($withdrawId, $status) {
        return $this->withdrawal_model->_changeWithdrawStatus($withdrawId, $status);
    }

    public function _changeTransactionStatus($withdrawId, $status) {
        return $this->manual_transaction_model->_changeTransactionStatus($withdrawId, $status);
    }

    public function _getAllProviderCategories($type) {
        return $this->provider_category_model->_getAllProviderCategories($type);
    }

    public function _getAllJobCategories() {
        return $this->job_category_model->_getAllJobCategories();
    }

    public function _findCategory($categoryId) {
        return $this->provider_category_model->_findCategory($categoryId);
    }

    public function _updateProviderCategory($id,$categoryData) {
        return $this->provider_category_model->_updateProviderCategory($id,$categoryData);
    }

    public function _saveProviderCategory($categoryData) {
        return $this->provider_category_model->_saveProviderCategory($categoryData);
    }

    public function _deleteProviderCategory($categoryId) {
        return $this->provider_category_model->_deleteProviderCategory($categoryId);
    }

    public function _findProvider($userId) {
        return $this->provider_model->_findProvider($userId);
    }

    public function _getSubCategories($providerId) {
        return $this->provider_sub_category_model->_getSubCategories($providerId);
    }

    public function _findSubCategory($categoryId) {
        return $this->provider_sub_category_model->_findSubCategory($categoryId);
    }

    public function _updateSubCategory($id,$categoryData) {
        return $this->provider_sub_category_model->_updateSubCategory($id,$categoryData);
    }

    public function _saveSubCategory($categoryData) {
        return $this->provider_sub_category_model->_saveSubCategory($categoryData);
    }

    public function _deleteSubCategory($categoryId) {
        return $this->provider_sub_category_model->_deleteSubCategory($categoryId);
    }

    public function _getAllItems() {
        return $this->item_model->_getAllItems();
    }

    public function _findItem($itemId) {
        return $this->item_model->_findItem($itemId);
    }

    public function _updateItem($id,$itemId) {
        return $this->item_model->_updateItem($id,$itemId);
    }

    public function _saveItem($itemData) {
        return $this->item_model->_saveItem($itemData);
    }

    public function _deleteItem($itemId) {
        return $this->item_model->_deleteItem($itemId);
    }

    public function _getAllProviderPromotions() {
        return $this->provider_promotion_model->_getAllProviderPromotions();
    }

    public function _findPromotion($promotionId) {
        return $this->provider_promotion_model->_findPromotion($promotionId);
    }

    public function _updatePromotion($id,$promotionData) {
        return $this->provider_promotion_model->_updatePromotion($id,$promotionData);
    }

    public function _savePromotion($promotionData) {
        return $this->provider_promotion_model->_savePromotion($promotionData);
    }

    public function _deletePromotion($promotionId) {
        return $this->provider_promotion_model->_deletePromotion($promotionId);
    }

    public function _getZones() {
        return $this->zone_model->_getZones();
    }

    public function _getRides() {
        return $this->ride_model->_getAllRides();
    }

    public function _findZone($id) {
        return $this->zone_model->_findZone($id);
    }

    public function _updateZone($id,$zoneData) {
        return $this->zone_model->_updateZone($id,$zoneData);
    }

    public function _saveZone($zoneData) {
        return $this->zone_model->_saveZone($zoneData);
    }

    public function _deleteAllRegions($zoneId) {
        return $this->region_model->_deleteAllRegions($zoneId);
    }

    public function _deleteZone($zoneId) {
        return $this->zone_model->_deleteZone($zoneId);
    }

    public function _getManualTransactions($status) {
        return $this->manual_transaction_model->_getManualTransactions($status);
    }

    public function _getTransactionInfo($id) {
        return $this->manual_transaction_model->_getTransactionInfo($id);
    }

    public function _getAllUsers() {
        return $this->user_model->getAllUsers();
    }

    public function _driverRides($driverId) {
       return $this->ride_model->_getRideHistory($driverId,'D');
    }

    public function _driverRidesStatus($driverId,$status) {
       return $this->ride_model->_driverRidesStatus($driverId,$status);
    }

    public function _findRide($id) {
        return $this->ride_model->_findRide($id);
    }

    public function _findUserDetails($id) {
        return $this->user_model->_findUserDetails($id);
    }

}
