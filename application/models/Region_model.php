<?php
class Region_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
  }

  public function saveRegions($id,$pincode) {
    $regions = explode(',',$pincode);
        foreach($regions as $pin) {
            $userData=array(
                'Zones_ZoneId' => $id,
                'Pincode' => $pin,
                'Status' => 'A'
            );
            $this->db->insert('regions', $userData);
            }
            return true;
  }

  public function getRegions($id){
        $where = array(
            'Status'   => 'A',
            'Zones_ZoneId' => $id,
        );
        $this->db->select('*');
        $this->db->from('regions');
        $this->db->where($where); 
        $query = $this->db->get();
            if ( $query->num_rows() > 0 ) {
                return $query->result();
            }
    }

    public function _getPincodes($id){
        $where = array(
            'Status'   => 'A',
            'Zones_ZoneId' => $id,
        );
        $this->db->select('Pincode');
        $this->db->from('regions');
        $this->db->where($where); 
        $query = $this->db->get();
            if ( $query->num_rows() > 0 ) {
                return $query->result();
            }
    }

    public function updateRegions($id,$pincode) {
        $this->db->where('Zones_ZoneId', $id);
        $this->db->delete('regions');
        $regions = explode(',',$pincode);
        if(!empty($regions))  {
            foreach($regions as $pin) {
                $userData=array(
                    'Zones_ZoneId' => $id,
                    'Pincode' => $pin,
                    'Status' => 'A'
                );
                if($pin!=''){
                    $this->db->insert('regions', $userData);
                }
                
            }
        }
            return $id;
    }
    
    public function deleteRegions($id) {
        $this->db->where('Zones_ZoneId', $id);
        if($this->db->delete('regions')) {
            return true;
        }
    }

    public function _deleteAllRegions($id) {
        $this->db->where('Zones_ZoneId', $id);
        $this->db->delete('regions');
    }
}
