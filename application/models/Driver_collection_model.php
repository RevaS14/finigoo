<?php
class Driver_collection_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
  }

  public function getDriverCollections($status, $franchiseId, $regions){
    $this->db->select('collection.Status,collection.RideId,collection.PendingAmount,collection.PendingAmount,collection.CalculatedFee,users.FirstName,collection.DriverCollectionId');
    $this->db->from('driver_collections as collection');
    $this->db->where('collection.Status',$status); 
    $this->db->join('users', 'users.UserId = collection.DriverId');
    if($franchiseId) {  
      $this->db->where('collection.FranchiseId',$franchiseId); 
      $this->db->where_in('users.Pincode', $regions);
    }
    return $this->db->get()->result();
    }

    public function collectAmount($rideId) {
        return $this->db->query("CALL collectedRideFare('".$rideId."')");
    }

}
