<?php
class Commission_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function _calculateRideFare($kilometer, $orderTime, $pincode, $vehicleType) {
        return $this->db->query("CALL calculateRideFare('".$kilometer."', '".$orderTime."', '".$pincode."', '".$vehicleType."', @calculatedFare, @adminCharge, @franchiseCharge, @gstPercentage, @gstCharge)")
                        ->result();
    }

    public function saveCommission($costData) {
      $this->db->insert('commissions', $costData);
      return $this->db->insert_id();
    }

    public function addAdminCommission($costData,$commissionId) {
      $this->db->set('Status','D');
      $this->db->where('CommissionId', $commissionId);
      $this->db->update('commissions');

      $this->db->insert('commissions', $costData);
      return $this->db->insert_id();
    }

    public function getAllCategoryCommission($userId) {
          $this->db->select('*');
          $this->db->from('commissions');
          $this->db->where('franchises.Users_UserId', $userId);
          $this->db->join('franchises', 'franchises.FranchiseId = commissions.Franchises_FranchiseId');
          $this->db->where('commissions.Status','A');
          $this->db->order_by("CommissionId", "desc");
          return $this->db->get()->result();
    }
    public function checkCommission($userId,$jobCategoryId) {

      $this->db->select('commissions.*')
      ->order_by('commissions.CommissionId', "desc")
      ->where('Franchises_FranchiseId',$userId);

      if($jobCategoryId > 0) {
        $this->db->where('commissions.JobCategories_JobCategoryId',$jobCategoryId);
      }

      $query = $this->db->where_in('commissions.Status', array('P', 'A'))
      ->get('commissions');

      return $query->row();
      
    }
    
    public function updateCommission($commissionId,$costData) { 
      // $this->db->set('Status', 'D'); 
      // $this->db->where('CommissionId', $commissionId); 
      // $this->db->update('commissions'); 
      $this->db->insert('commissions', $costData);
      return $this->db->insert_id();
    }
    
    public function getAllCommissions($status,$userId){
      $where = array(
        'commissions.Status' => $status,
        );
      $this->db->select('*');
      $this->db->from('commissions');
      $this->db->where($where);
      $this->db->where('Franchises_FranchiseId != ',$userId);
      $this->db->join('job_categories', 'job_categories.JobCategoryId = commissions.JobCategories_JobCategoryId');
      $this->db->join('franchises', 'franchises.FranchiseId = commissions.Franchises_FranchiseId');
      return $this->db->get()->result();
    }

    public function viewCommissions($id) {
      $query = $this->db->get_where('commissions', array('CommissionId' => $id));
      return $query->row();
    }

    public function updateCommissionStatus($commissionId,$status,$franchiseId,$jobCategoryId) {
      if($status=='A') {
        $where = array(
          'Franchises_FranchiseId' => $franchiseId,
          'Status' => 'A',
          'JobCategories_JobCategoryId' => $jobCategoryId,
          );
        $this->db->set('Status', 'D'); 
        $this->db->where($where);
        $this->db->update('commissions');
      }
      $this->db->set('Status', $status); 
      $this->db->where('CommissionId', $commissionId); 
      $this->db->update('commissions'); 
      return $commissionId;
    }

    public function _deleteFranchiseCommissions($franchiseId) {
      $this->db->where('Franchises_FranchiseId', $franchiseId);
      if($this->db->delete('commissions')) {
          return true;
      }
    }
}