<?php
class Franchise_Model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
  }

  public function insertFranchise($userData) {
    $this->db->insert('franchises', $userData);
    return $this->db->insert_id();
  }

  public function findFranchise($id) {
    $query = $this->db->get_where('franchises', array('Users_UserId' => $id));
    return $query->row();
  }

  public function updateFranchise($id,$franchiseData) { 			
    $this->db->update('franchises',$franchiseData,array('Users_UserId' => $id));
    return $this->db->affected_rows();
  }

  public function updateFranchiseStatus($userId,$statusType) { 			
    $this->db->where('Users_UserId', $userId);
    $this->db->update('franchises', array('Status'=>$statusType));
    return $this->db->affected_rows();
  }

  public function deleteFranchise($id) { 			
    $this->db->where('Users_UserId', $id);
    if($this->db->delete('franchises')) {
        return true;
    }
  }

  public function _getFranchiseIdByUserId($userId) {
    $this->db->select('FranchiseId');
    $this->db->where('Users_UserId', $userId);
    return $this->db->get('franchises')->result();
  }
}
