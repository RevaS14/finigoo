<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Withdrawal_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function _saveWithdrawRequest($requestData) {
        $this->db->insert('withdrawal', $requestData);
		return $this->db->insert_id();
    }

    public function _getWithdrawRequests($status, $franchiseId, $regions){
        $this->db->select('withdrawal.Status,withdrawal.Amount,withdrawal.CreatedOn,withdrawal.WithdrawId,users.FirstName');
        $this->db->from('withdrawal');
        $this->db->where('withdrawal.Status',$status); 
        if($franchiseId!=''){
			$this->db->where_in('users.Pincode', $regions);		
		}
        $this->db->join('users', 'users.UserId = withdrawal.Users_UserId');
        return $this->db->get()->result();
    }

    public function _changeWithdrawStatus($withdrawId, $status) {
        $this->db->set('Status', $status);
        $this->db->where('WithdrawId', $withdrawId);
        $this->db->update('withdrawal');
        return $this->db->affected_rows();
    }
}
