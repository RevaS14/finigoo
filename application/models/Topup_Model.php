<?php
class Topup_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function insertTopup($topUpData) {
        $this->db->insert('topup', $topUpData);
        $insertedValue = $this->db->insert_id();
        $this->db->set('ReferenceTopup',$this->db->insert_id());
        $this->db->where('TopupId', $this->db->insert_id());
        $this->db->update('topup');
        return $insertedValue;
        
    }

    public function updateTopup($topupId,$updateData) {
        $this->db->update('topup',$updateData,array('TopupId' => $topupId));
        return true;
    }

    public function findTopup($topupId) {
		$query = $this->db->get_where('topup', array('TopupId' => $topupId));
		return $query->row();
    }
    
    public function getAllTransactions($userId){
        $this->db->select('*');
        $this->db->from('topup');
        $this->db->where('topup.Users_UserId', $userId);
        $this->db->join('users', 'users.UserId = topup.Users_UserId');
        return $this->db->get()->result();
    }
    
    public function _getWalletTransactionHistory($userId,$type) {
        $this->db->select('topup.TopupId,
                            users.FirstName as Name,
                            topup.PaymentMethod,
                            topup.Amount,
                            topup.TransactionId,
                            topup.TransactionStatus,
                            topup.EvidenceDocument,
                            topup.ReferenceTopup,
                            topup.Remarks,
                            topup.Status,
                            topup.CreatedOn');

        $this->db->where('topup.PaymentMethod','O');
        $this->db->where('topup.TransactionStatus is NULL');
        $this->db->or_where('topup.TransactionStatus!=','P');
        // $this->db->where('topup.Status','A');
        $this->db->where('Users_UserId', $userId);
        $this->db->join('users', 'users.UserId = topup.Users_UserId' );
        $this->db->order_by('topup.TopupId', 'DESC');
        return $this->db->get('topup')->result();
    }

}