<?php
class Provider_model extends CI_Model {
  public function __construct() {
      $this->load->database();
  }
  
  public function _searchByKeywordProvider($keyword, $latitude, $longitude, $providerType) {
      $this->db->select(array(
          'SQRT(POW(69.1 * (providers.Latitude - 40.78509100), 2) + POW(69.1 * (-73.96828500 - providers.Logitude) * COS(providers.Latitude / 57.3), 2)) AS distance',
          'providers.Users_UserId AS ProviderId',
          'providers.ProviderName',
          'providers.ProviderImage',
          '(MATCH (providers.ProviderName) AGAINST ("*'.$keyword.'*" IN BOOLEAN MODE)) AS provider_relavence'
      ));
      $this->db->from('providers');
      // $this->db->join('items', 'providers.ProviderId = items.Provider_ProviderId');
      $this->db->where('providers.JobCategories_JobCategoryId', $providerType);
      $this->db->where('NOW() BETWEEN providers.OpeningTime AND providers.ClosingTime');
      // $this->db->where('providers.Available', 'Y');
      // $this->db->having('distance <=',  10);
      $this->db->having('provider_relavence >',  0);
      $this->db->order_by('provider_relavence', 'DESC');

      return $this->db->get()->result();
  }
  
  public function _searchByKeywordItem($keyword, $latitude, $longitude, $providerType) {
      $this->db->select(array(
          'SQRT(POW(69.1 * (providers.Latitude - 40.78509100), 2) + POW(69.1 * (-73.96828500 - providers.Logitude) * COS(providers.Latitude / 57.3), 2)) AS distance',
          'providers.Users_UserId AS ProviderId',
          'providers.ProviderName',
          'providers.ProviderImage',
          'items.ItemName',
          'items.IsVeg',
          'items.ItemImage',
          'items.Price',
          'items.PreparationTime',
          'items.OutOfStock',
          'items.AvailableOn',
          '(MATCH (items.ItemName) AGAINST ("*'.$keyword.'*" IN BOOLEAN MODE)) AS item_relavence'
      ));
      $this->db->from('providers');
      $this->db->join('items', 'providers.ProviderId = items.Provider_ProviderId');
      $this->db->where('providers.JobCategories_JobCategoryId', $providerType);
      $this->db->where('NOW() BETWEEN providers.OpeningTime AND providers.ClosingTime');
      // $this->db->where('providers.Available', 'Y');
      // $this->db->having('distance <=',  10);
      $this->db->having('item_relavence >',  0);
      $this->db->order_by('item_relavence', 'DESC');

      return $this->db->get()->result();
  }

  public function insertProvider($providersData) {
    $this->db->insert('providers', $providersData);
    return $this->db->insert_id();
  }

  public function setVerifyStatus($userData) {
    $userId = $userData['UserId'];
    $status = $userData['Status'];
    $jobCategory = $userData['JobCategory'];
    $userType = $userData['UserType'];
    $providerId = $userData['ProviderId'];
    $approveby = $userData['ApprovedBy'];

    $updateProvider = $this->db->get_where('providers', array(
        'ProviderId' => $providerId
    ));

    if ( $updateProvider->num_rows() >= 1 ) {
      $this->db->where('ProviderId', $providerId); 
      $this->db->where('Users_UserId', $userId); 
      $updateData = array(
        "Status" => $status,
        "ApprovedBy" => $approveby
      ); 
      $this->db->update('providers', $updateData);
      return true;
    } else {
      return false;
    }
  }
  
  public function _findProvider($userId) {
    return $this->db->get_where('providers', array('Users_UserId' => $userId))->row();
  }

  public function _findProviderByUserId($userId) {
    $this->db->select(array(
      'users.Rating',
      'providers.ProviderName',
      'providers.ProviderId',
    ));
    $this->db->where('Users_UserId', $userId);
    $this->db->join('users', 'users.UserId = providers.Users_UserId' );
    return $this->db->get('providers')->row();
  }

  public function _getProvidersNearBy($latitude, $longitude, $providerType) {
    $this->db->select(array(
      'providers.Users_UserId AS ProviderId',
      'providers.ProviderName',
      'providers.Latitude',
      'providers.Logitude',
      'providers.ProviderImage',
      'providers.Available',
      'SQRT( POW(69.1 * (providers.Latitude - '.$latitude.'), 2) + POW(69.1 * ('.$longitude.' - providers.Logitude) * COS(providers.Latitude / 57.3), 2) ) AS distance',
      'GROUP_CONCAT(provider_categories.ProviderCategory) AS ProviderCategory'
    ));

    $this->db->join('provider_category_mappings', 'providers.ProviderId = provider_category_mappings.Providers_ProviderId');
    $this->db->join('provider_categories', 'provider_categories.ProviderCategoriesId = provider_category_mappings.ProviderCategories_ProviderCategoryId');

    $this->db->where('providers.JobCategories_JobCategoryId', $providerType);
    $this->db->where('NOW() BETWEEN providers.OpeningTime AND providers.ClosingTime');
    $this->db->where('providers.Available', 'Y');

    $this->db->having('distance <= ', '10');
    
    $this->db->group_by('providers.Users_UserId');

    return $this->db->get('providers')->result();
  }

}