<?php
class Zone_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function _getZones(){
		$this->db->from('zones');
        $this->db->where('Status', 'A');
		return $this->db->get()->result();
    }

    public function _findZone($id) {
		return $this->db->get_where('zones', array('ZoneId' => $id))->row();
    }
    
    public function _updateZone($id,$zoneData) { 			
        $this->db->update('zones',$zoneData,array('ZoneId' => $id));
        return $this->db->affected_rows();
    }

    public function _saveZone($zoneData) {
        $this->db->insert('zones', $zoneData);
        return $this->db->insert_id();
    }

    public function _deleteZone($id) { 			
        $this->db->where('ZoneId', $id);
        if($this->db->delete('zones')) {
            return true;
        }
      }

}