<?php
class Provider_sub_category_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function _getSubCategories($providerId){
		$this->db->from('provider_sub_categories');
        $this->db->where('Providers_ProviderId', $providerId);
	  	return $this->db->get()->result();
    }
    
    public function _findSubCategory($categoryId) {
		    return $this->db->get_where('provider_sub_categories', array('ProviderSubCategoryId' => $categoryId))->row();
    }

    public function _saveSubCategory($categoryData) {
        $this->db->insert('provider_sub_categories', $categoryData);
        return $this->db->insert_id();
    }
    
    public function _updateSubCategory($id,$categoryData) { 			
        $this->db->update('provider_sub_categories',$categoryData,array('ProviderSubCategoryId' => $id));
        return $this->db->affected_rows();
    }
    
    public function _deleteSubCategory($categoryId) { 			
        $this->db->where('ProviderSubCategoryId', $categoryId);
        $this->db->delete('provider_sub_categories');	
        return true;	
	}

}