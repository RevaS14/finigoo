<?php
class Peakhour_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function savePeakHours($peakHoursData) {
        $this->db->insert('peakhours',$peakHoursData);
        return $this->db->affected_rows();
    }

    public function getPeakHours($franchiseId,$zoneId,$jobCategoryId) {
        $where = array(
            'Franchises_FranchiseId' => $franchiseId,
            'JobCategories_JobCategoryId' =>$jobCategoryId,
            'Zones_ZoneId' =>$zoneId,
		  );
        $this->db->select('*');
		$this->db->from('peakhours');
		$this->db->where($where);
		return $this->db->get()->result();
    }

    public function deletePeakHour($peakHourId) { 			
		$this->db->where('PeakHourId', $peakHourId);
		if($this->db->delete('peakhours')) {
			return true;
		}
    }
    
    public function updatePeakHourStatus($peakId,$status) { 
        $this->db->set('Status', $status); 
        $this->db->where('PeakHourId', $peakId); 
        $this->db->update('peakhours'); 
        return true;
      }
}