<?php
class Franchises_region_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
  }

  public function saveRegions($franchise_id,$pincode) {
    $regions = explode(',',$pincode);
        foreach($regions as $pin) {
            $userData=array(
                'Franchises_FranchiseId' => $franchise_id,
                'Pincode' => $pin,
                'Status' => 'A'
            );
            $this->db->insert('franchises_regions', $userData);
            }
            return true;
  }

  public function getRegions($franchise_id){
        $where = array(
            'Status'   => 'A',
            'Franchises_FranchiseId' => $franchise_id,
        );
        $this->db->select('*');
        $this->db->from('franchises_regions');
        $this->db->where($where); 
        $query = $this->db->get();
            if ( $query->num_rows() > 0 ) {
                return $query->result();
            }
    }

    public function _getPincodes($franchise_id){
        $where = array(
            'Status'   => 'A',
            'Franchises_FranchiseId' => $franchise_id,
        );
        $this->db->select('Pincode');
        $this->db->from('franchises_regions');
        $this->db->where($where); 
        $query = $this->db->get();
            if ( $query->num_rows() > 0 ) {
                return $query->result();
            }
    }

    public function updateRegions($franchise_id,$pincode) {
        $this->db->where('Franchises_FranchiseId', $franchise_id);
        $this->db->delete('franchises_regions');
        $regions = explode(',',$pincode);
            foreach($regions as $pin) {
                $userData=array(
                    'Franchises_FranchiseId' => $franchise_id,
                    'Pincode' => $pin,
                    'Status' => 'A'
                );
                $this->db->insert('franchises_regions', $userData);
                }
            return $franchise_id;
    }
    
    public function deleteRegions($franchise_id) {
        $this->db->where('Franchises_FranchiseId', $franchise_id);
        if($this->db->delete('franchises_regions')) {
            return true;
        }
    }
}
