<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function insertVehicle($vehicleData) {
        $this->db->insert('vehicles', $vehicleData);
		return $this->db->insert_id();
    }

    public function setVerifyStatus($userData) {
		$userId = $userData['UserId'];
        $status = $userData['Status'];
        $jobCategory = $userData['JobCategory'];
        $userType = $userData['UserType'];
        $vehicleId = $userData['VehicleId'];
        $approveby = $userData['ApprovedBy'];

        $updateVehicle = $this->db->get_where('vehicles', array(
            'VehicleID' => $vehicleId
        ));

        if ( $updateVehicle->num_rows() == 1 ) {
            $this->db->where('VehicleID', $vehicleId); 
            $this->db->where('Users_UserId', $userId); 
     		$updateData = array(
                "Status" => $status,
                "ApprovedBy" => $approveby
			); 
            $this->db->update('vehicles', $updateData);
            return true;
        } else {
			return false;
        }
    }

    public function _changeVehicleDetails($vehicleId,$vehicleData) { 
        $this->db->update('vehicles',$vehicleData,array('VehicleID' => $vehicleId));
        return $this->db->affected_rows();
    }

    public function updateVehicleDetails($postData) { 
        $updateVehicle = $this->db->get_where('vehicles', array(
            'VehicleID' => $postData['VehicleID']
        ));

        if ( $updateVehicle->num_rows() == 1 ) {
			$this->db->where('VehicleID', $postData['VehicleID']); 
     		$updateData = array(
                "VehicleID" => $postData['VehicleID'],
                'RegistrationId'=> $postData['RegistrationId'],
                "VehicleNumber" => $postData['VehicleNumber'],
                "VehicleBrand" => $postData['VehicleBrand'],
                "VehicleColor" => $postData['VehicleColor'],
                "VehicleType" => $postData['VehicleType'],
                "VehiclePermit" => $postData['VehiclePermit'],
			); 
            $this->db->update('vehicles', $updateData);
            return true;
        } 
        else {
			return false;
        }
    }

    public function _updateDriverStatus($vehicleId, $vehicleStatus) {
        $this->db->set('Available', $vehicleStatus);
        $this->db->where('VehicleID', $vehicleId);
        $this->db->update('vehicles');
        return $this->db->affected_rows();
    }

    public function _getDriverStatus($vehicleId) {
        $this->db->select('Available');
        $this->db->where('VehicleID', $vehicleId);
        return $this->db->get('vehicles')->result();
    }
    
    public function _getVehicleIdByDriverId($driverId) {
        $this->db->select('VehicleID');
        $this->db->where('Users_UserId', $driverId);
        return $this->db->get('vehicles')->result();
    }

    public function _getRideDriverDetails($userId) {
        $this->db->select(array(
            'vehicles.VehicleNumber',
            'vehicles.VehicleBrand',
            'vehicles.VehicleColor',
            'vehicles.VehicleType',
            'drivers.DriverName',
            'drivers.DisplayPicture',
            'users.Phone'
        ));
        $this->db->where('Users_UserId', $userId);
        $this->db->join('drivers', 'vehicles.VehicleID = drivers.Vehicles_VehicleId');
        $this->db->join('users', 'users.UserId = vehicles.Users_UserId');
        return $this->db->get('vehicles')->result();
    }
	
	public function _getVehicleDocuments($userId) {
		$this->db->select(array(
			"VehiclePermit",
			"VehicleNumber",
			"VehicleBrand",
			"VehicleColor",
			"VehicleType AS 'VehicleModel'"
		));
		$this->db->where('Users_UserId', $userId);
		return $this->db->get('vehicles')->result();
    }

    public function _getVehicleType($userId) {
        $query = $this->db->get_where('vehicles', array('Users_UserId' => $userId));
        return $query->row();
    }

    public function _getRC($id) {
        $query = $this->db->get_where('vehicles', array('VehicleID' => $id));
        return $query->row();
    }
}
