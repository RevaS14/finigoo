<?php
class Provider_promotion_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function _getAllProviderPromotions(){
        $this->db->select('promotion.ProviderPromotionId, promotion.PromotionBanner, promotion.FilterBy, promotion.Filter, promotion.Status, job.CategoryName');
		$this->db->from('provider_promotions as promotion');
        $this->db->join('job_categories as job', 'promotion.JobCategories_JobCategoryId = job.JobCategoryId');
	  	return $this->db->get()->result();
    }
    
    public function _findPromotion($promotionId) {
		return $this->db->get_where('provider_promotions', array('ProviderPromotionId' => $promotionId))->row();
    }

    public function _savePromotion($promotionData) {  
        $this->db->insert('provider_promotions', $promotionData);
        return $this->db->insert_id();
    }
    
    public function _updatePromotion($id,$promotionData) { 			
        $this->db->update('provider_promotions',$promotionData,array('ProviderPromotionId' => $id));
        return $this->db->affected_rows();
    }
    
    public function _deletePromotion($promotionId) { 			
        $this->db->where('ProviderPromotionId', $promotionId);
        $this->db->delete('provider_promotions');	
        return true;	
	}

}