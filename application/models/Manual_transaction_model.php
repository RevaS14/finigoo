<?php
class Manual_transaction_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
  }

	public function _saveTransaction($transData) {
		$this->db->insert('manual_transactions', $transData);
		return $this->db->insert_id();
    }

    public function _getManualTransactions($status){
        $this->db->select('manual.Status,
        	manual.Amount,
        	manual.CreatedOn,
        	manual.BankName,
        	manual.Ifsc,
        	manual.BranchName,
        	manual.AccountNumber,
        	manual.AccountHolderName,
        	manual.ManualTransactionId,
        	users.FirstName');
        $this->db->from('manual_transactions as manual');
        $this->db->where('manual.Status',$status); 
        $this->db->join('users', 'users.UserId = manual.Users_UserId');
        return $this->db->get()->result();
    }

    public function _getTransactionInfo($id) {
		 $this->db->select('manual.Status,
        	manual.Amount,
        	manual.CreatedOn,
        	manual.EvidenceDocument,
        	manual.BankName as FromBankName,
        	manual.Ifsc as FromIfsc,
        	manual.BranchName as FromBranchName,
        	manual.AccountNumber as FromAccountNumber,
        	manual.AccountHolderName as FromAccountHolderName,

        	banks.BankName as ToBankName,
        	banks.Ifsc as ToIfsc,
        	banks.BranchName as ToBranchName,
        	banks.AccountNumber as ToAccountNumber,
        	banks.AccountHolderName as ToAccountHolderName,

        	manual.ManualTransactionId,
        	manual.Users_UserId,
        	manual.Banks_BankId,
        	users.FirstName');
        $this->db->from('manual_transactions as manual');
        $this->db->where('manual.ManualTransactionId',$id); 
        $this->db->join('users', 'users.UserId = manual.Users_UserId');
        $this->db->join('banks', 'banks.BankId = manual.Banks_BankId');
        return $this->db->get()->result();
	}

	 public function _changeTransactionStatus($withdrawId, $status) {

        $this->db->set('Status', $status);
        $this->db->where('ManualTransactionId', $withdrawId);
        $this->db->update('manual_transactions');
        return $this->db->affected_rows();
    }
}
