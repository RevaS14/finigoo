<?php
class User_model extends CI_Model {

    public function __construct() {
		parent::__construct();
        $this->load->database();
    }

    public function _getUserWallet($userId) {
        $this->db->select('WalletBalance');
        $this->db->where('UserId', $userId);
        return $this->db->get('users')->result();
    }

    public function _getProfileDetails($userId) {
        $this->db->select(array(
			'FirstName',
			'LastName',
			'Email',
			'Phone',
			'DOB',
			'PlaceOfBirth',
			'DisplayPicture',
			'Rating'
		));
        $this->db->where('UserId', $userId);
        return $this->db->get('users')->result();
    }

    public function _updateProfileDetails($userId, $data) {
        $this->db->where('UserId', $userId);
        $this->db->update('users', $data);
		// return $this->db->affected_rows();
		return true;
    }

    public function _updateKycDocuments($userId, $data) {
        $this->db->where('UserId', $userId);
        $this->db->update('users', $data);
        return $this->db->affected_rows();
	}
	
	public function _getKycDocuments($userId) {
		$this->db->select(array(
			"AadharNo",
			"AadharDocumentFront",
			"AadharDocumentBack",
			"PanNo",
			"PanDocument"
		));
		$this->db->where('UserId', $userId);
		$this->db->where('UserType', "D");
		return $this->db->get('users')->result();
	}

    public function _getOverallRating($userId) {
        $this->db->select('Rating');
        $this->db->where('UserId', $userId);
        return $this->db->get('users')->result();
    }

    public function _updateOverallRating($userId, $rating) {
        $this->db->set('Rating', $rating);
        $this->db->where('UserId', $userId);
        $this->db->update('users');
        return $this->db->affected_rows();
    }

	public function insertUser($userData) {
		$this->db->insert('users', $userData);
		return $this->db->insert_id();
    }
    
    public function saverecords($data) {
		$this->db->insert('users', $data);
		return $this->db->affected_rows();
	}

	public function login($postData) {  
	  $this->db->select(array('UserId', 'UserType', 'Password'));
	  $this->db->from('users');
	  $this->db->where('phone', $postData->phone);
	  $this->db->where('UserType',$postData->userType);
	  $this->db->limit(1);
	  $query = $this->db->get();
  
	  if ($query->num_rows() == 1) {
		  $record = $query->row_array();
		  if(password_verify($postData->password, $record['Password'])) {
			  return array(
				'UserId' => $record['UserId'],
				'UserType' => $record['UserType']
			  );
		  } else {
			  return false;
		  }
	  } else {
		  return false;
	  }
	}  
	
	public function _getUserMobile($data) {  
		$query = $this->db->get_where('users', array(
            'Phone' => $data['mobile'],
            'UserType' => $data['userType']
		));

		if ($query->num_rows() == 1) 
		{
			$this->db->where('Phone', $data['mobile']); 
     		$updateData = array(
				"ForgotPasswordOtp" => $data['otp']
			); 
			$this->db->update('users', $updateData);
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getForgotToken($token) { 
		$query = $this->db->get_where('users', array(
            'forgotPasswordToken' => $token
		));
		$data=$query->row();
		if ($query->num_rows() == 1) 
		{
			if(date("Y-m-d H:i:s")<$data->ForgotPasswordExpire) {	
				return true;
			}
			else{
				return false;
			}
		}
	}
	

	public function adminLogin($loginData,$type){
		$where = array(
			'Email' => $loginData['username'],
			'Status'   => 'A',
		  );
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get('users')->row();
		if($query && password_verify($loginData['password'], $query->Password)){
			if($type=='admin') { 

				if($query->UserType=='A' || $query->UserType=='F') {
					return $query->UserId;
				} else {
					return 'D';
				}

			} elseif($type=='partner') {

				if($query->UserType=='R' || $query->UserType=='M') {
					return $query->UserId;
				} else {
					return 'D';
				}
			}
			
		}
	}

	public function getAllFranchise(){
		$where = array(
			'UserType' => 'F',
		  );
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where($where);
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ) {
			return $query->result();
		}
	}
	
	public function findUser($id) {
		return $this->db->get_where('users', array('UserId' => $id))->row();
	}

	public function getAllEstablishment(){
		$this->db->select('*');
		$this->db->from('establishment');
		$this->db->where('Status','A'); 
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ) {
			return $query->result();
		}
	}

	public function changePassword($data) { 
		$query = $this->db->get_where('users', array(
            'userId' => $data['id']
		));
		if ($query->num_rows() == 1) 
		{
			$this->db->where('userId', $data['id']); 
     		$updateData = array(
				"password" => $data['password']
			); 
			$this->db->update('users', $updateData);
			return true;
		}
		else {
			return false;
		}
	}

    public function listUsersDetails($gocabData) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users.UserType', $gocabData['UserType']);
        $this->db->where('users.Status', $gocabData['Status']);
		$this->db->where_in('vehicles.JobCategory_JobCategoryId', $gocabData['JobCategory']);
		// if($franchiseId!=''){
		// 	$this->db->where_in('users.Pincode', $getRegions);		
		// }
		$this->db->join('vehicles', 'users.UserId = vehicles.Users_UserId' );
		
        return $query = $this->db->get()->result();
	}

	public function _getDrivers($pincodes) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users.UserType', 'D');
        $this->db->where('users.Status', 'A');
        $this->db->where('vehicles.Available', 'Y');
		$this->db->where_in('users.Pincode', $pincodes);	
		$this->db->join('vehicles', 'vehicles.Users_UserId = users.UserId');	
		
        return $query = $this->db->get()->result();
	}
	
	public function listRestaurantsDetails($gofoodData, $franchiseId, $getRegions) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users.UserType', $gofoodData['UserType']);
        $this->db->where('users.Status', $gofoodData['Status']);
		$this->db->where_in('providers.JobCategories_JobCategoryId', $gofoodData['JobCategory']);
		if($franchiseId!=''){
			$this->db->where_in('users.Pincode', $getRegions);		
		}
        
		$this->db->join('providers', 'users.UserId = providers.Users_UserId' );
        return $query = $this->db->get()->result();
	}
	
    
    public function getUserDetails($userData) {
        $userId = $userData['UserId'];
        $status = $userData['Status'];
        $jobCategory = $userData['JobCategory'];
        $userType = $userData['UserType'];

		//$this->db->select('*');
		$this->db->select('users.UserId, users.FirstName, users.LastName, users.Email, users.Phone, users.DOB, users.DOB, users.PlaceOfBirth, 
		users.DisplayPicture, users.IdentityNo, users.IdentityDocument, users.City, users.State, users.Establishment_EstablishmentId, users.Pincode, 
		users.UserType, users.Status, users.CreatedOn, vehicles.VehicleID, vehicles.Users_UserId, vehicles.VehiclePermit, vehicles.VehicleNumber,
		vehicles.VehicleBrand, vehicles.VehicleColor, vehicles.VehicleType, vehicles.JobCategory_JobCategoryId, vehicles.Status, vehicles.CreatedOn,
		drivers.DriverId, drivers.DriverName, drivers.DrivingLicense, drivers.DrivingLicenseDocument, drivers.Status');
        $this->db->from('users');
       
        $this->db->where('users.UserType', $userType);
        $this->db->where('users.Status', $status);
        $this->db->where('users.UserId', $userId);
       
        $this->db->where('vehicles.JobCategory_JobCategoryId', $jobCategory);
        $this->db->join('vehicles', 'users.UserId = vehicles.Users_UserId' );
        $this->db->join('drivers', 'vehicles.VehicleID = drivers.Vehicles_VehicleId' );
		
        return $query = $this->db->get()->result();
    }
	
	public function getRestaurantDetails($userData) {
        $userId = $userData['UserId'];
        $status = $userData['Status'];
        $jobCategory = $userData['JobCategory'];
		$userType = $userData['UserType'];
		
		//$this->db->select('*');
		$this->db->select('users.UserId, users.FirstName, users.LastName, users.Email, users.Phone, users.DOB, users.DOB, users.PlaceOfBirth, 
		users.DisplayPicture, users.IdentityNo, users.IdentityDocument, users.City, users.State, users.Establishment_EstablishmentId, users.Pincode, 
		users.UserType, users.Status, users.CreatedOn, providers.ProviderId, providers.Users_UserId, providers.ProviderName, providers.ProviderAddress,
		providers.ProviderPhone, providers.OpeningTime, providers.ClosingTime, providers.Latitude, providers.Longitude, providers.Description,
		providers.ProviderImage, providers.JobCategories_JobCategoryId, providers.Status');
        $this->db->from('users');
       
        $this->db->where('users.UserType', $userType);
        $this->db->where('users.Status', $status);
        $this->db->where('users.UserId', $userId);
       
        $this->db->where('providers.JobCategories_JobCategoryId', $jobCategory);
        $this->db->join('providers', 'users.UserId = providers.Users_UserId' );
		
        return $query = $this->db->get()->result();
	}
	
    public function setVerifyStatus($userData) {
        $userId = $userData['UserId'];
        $status = $userData['Status'];
        $jobCategory = $userData['JobCategory'];
		$userType = $userData['UserType'];
		$password = $userData['Password'];
		$this->db->set('Status', $status);
		$this->db->set('Password', $password);
        $this->db->where('UserId', $userId);
        $this->db->update('users');

        if ($this->db->affected_rows() >= 0) {
            return true;
        } 
    }
    
    public function updateUserDetails($postData) {
        $updateUser = $this->db->get_where('users', array(
            'UserId' => $postData['UserId']
        ));
        $updateVehicle = $this->db->get_where('vehicles', array(
            'VehicleID' => $postData['VehicleID']
        ));
        $updateDriver = $this->db->get_where('drivers', array(
            'DriverId' => $postData['DriverId']
		));
		if ( ($updateUser->num_rows() == 1) && ($updateVehicle->num_rows() == 1) && ($updateDriver->num_rows() == 1) ) 
		{
			$this->db->where('UserId', $postData['UserId']); 
     		$updateData = array(
				"Firstname" => $postData['Firstname'],
                "Lastname" => $postData['Lastname'],
                "Email" => $postData['Email'],
                "Phone" => $postData['Phone'],
                "DOB" => $postData['DOB'],
                "PlaceOfBirth" => $postData['PlaceOfBirth'],
                "Pincode" => $postData['Pincode'],
                "Establishment_EstablishmentId" => $postData['Country'],
                "DisplayPicture" => $postData['DisplayPicture'],
                "IdentityNo" => $postData['IdentityNo'],
                "IdentityDocument" => $postData['IdentityDocument'],
			); 
            $this->db->update('users', $updateData);

            $this->db->where('VehicleID', $postData['VehicleID']); 
     		$updateData = array(
				"VehicleID" => $postData['VehicleID'],
                "VehicleNumber" => $postData['VehicleNumber'],
                "VehicleBrand" => $postData['VehicleBrand'],
                "VehicleColor" => $postData['VehicleColor'],
                "VehicleType" => $postData['VehicleType'],
                "VehiclePermit" => $postData['VehiclePermit'],
			); 
            $this->db->update('vehicles', $updateData);

            $this->db->where('DriverId', $postData['DriverId']); 
            $updateData = array(
               "DriverId" => $postData['DriverId'],
               "DriverName" => $postData['DriverName'],
               "DrivingLicense" => $postData['DrivingLicense'],
               "DrivingLicenseDocument" => $postData['DrivingLicenseDocument'],
           ); 
           $this->db->update('drivers', $updateData);
           return $postData['UserId'];
        }
        else {
			return false;
		}
	}
	
	public function updateRestaurantDetails($postData) {
        $updateUser = $this->db->get_where('users', array(
            'UserId' => $postData['UserId']
        ));
        $updateProvider = $this->db->get_where('providers', array(
            'ProviderId' => $postData['ProviderId']
        ));
    
		if ( ($updateUser->num_rows() == 1) && ($updateProvider->num_rows() == 1)  ) 
		{
			$this->db->where('UserId', $postData['UserId']); 
     		$updateData = array(
				"Firstname" => $postData['Firstname'],
                "Lastname" => $postData['Lastname'],
                "Email" => $postData['Email'],
                "Phone" => $postData['Phone'],
                "DOB" => $postData['DOB'],
                "PlaceOfBirth" => $postData['PlaceOfBirth'],
                "Pincode" => $postData['Pincode'],
                "Establishment_EstablishmentId" => $postData['Country'],
                "DisplayPicture" => $postData['DisplayPicture'],
                "IdentityNo" => $postData['IdentityNo'],
                "IdentityDocument" => $postData['IdentityDocument'],
			); 
            $this->db->update('users', $updateData);

            $this->db->where('ProviderId', $postData['ProviderId']); 
     		$updateData = array(
			'ProviderName' => $postData['ProviderName'],
            'ProviderAddress' => $postData['ProviderAddress'],
            'ProviderPhone' => $postData['ProviderPhone'],
            'OpeningTime' => $postData['OpeningTime'],
            'ClosingTime' => $postData['ClosingTime'],
            'Latitude' => $postData['Latitude'],
            'Longitude' => $postData['Longitude'],
            'Description' => $postData['Description'],
            'ProviderImage' => $postData['ProviderImage']
			); 
            $this->db->update('providers', $updateData);
           return $postData['UserId'];
        }
        else {
			return false;
		}
    }

    public function deleteUserDetails($postData) {
        $updateUser = $this->db->get_where('users', array(
            'UserId' => $postData['UserId']
        ));
        $updateVehicle = $this->db->get_where('vehicles', array(
            'VehicleID' => $postData['VehicleID']
        ));
        $updateDriver = $this->db->get_where('drivers', array(
            'Vehicles_VehicleId' => $postData['DriverId']
		));
		if( ($updateUser->num_rows() == 1) && ($updateVehicle->num_rows() == 1) && ($updateDriver->num_rows() == 1) ) {
            $this->db->where('UserId', $postData['UserId']); 
            
     		$updateData = array(
				"Status" => "D"
            ); 
            $this->db->update('users', $updateData);

            $this->db->where('VehicleID', $postData['VehicleID']);
     		$updateData = array(
				"Status" => "D"
                
			); 
            $this->db->update('vehicles', $updateData);

            $this->db->where('DriverId', $postData['DriverId']); 
            $updateData = array(
               "Status" => "D"
           ); 
           $this->db->update('drivers', $updateData);
           return $postData['UserId'];
        }
        else {
			return false;
		}
	}
	
	public function deleteRestaurantDetails($postData) {
        $updateUser = $this->db->get_where('users', array(
            'UserId' => $postData['UserId']
        ));
        $updateProvider = $this->db->get_where('providers', array(
            'ProviderId' => $postData['ProviderId']
        ));
      
		if( ($updateUser->num_rows() == 1) && ($updateProvider->num_rows() == 1) ) {
            $this->db->where('UserId', $postData['UserId']); 
            
     		$updateData = array(
				"Status" => "D"
            ); 
            $this->db->update('users', $updateData);

            $this->db->where('ProviderId', $postData['ProviderId']);
     		$updateData = array(
				"Status" => "D"
                
			); 
            $this->db->update('providers', $updateData);

           return $postData['UserId'];
        }
        else {
			return false;
		}
	}
	
	public function updateUser($id,$userData) { 			
		$this->db->update('users',$userData,array('UserId' => $id));
		return $this->db->affected_rows();
	}

	public function updateUserStatus($userId,$statusType) { 			
        $this->db->where('UserId', $userId);
        $this->db->update('users', array('Status'=>$statusType));
        return true;
	}

	public function deleteUser($id) { 			
		$this->db->where('UserId', $id);
		if($this->db->delete('users')) {
			return true;
		}
	}

	public function updateWallet($userId, $amount,$type) {
		$query = $this->db->get_where('users', array('UserId' => $userId));
		$userData = $query->row();
		if($type=='credit') { 
			$updatedAmount= $userData->WalletBalance +  $amount;
		} else {
			$updatedAmount= $userData->WalletBalance -  $amount;
		}
        $this->db->set('WalletBalance', $updatedAmount);
        $this->db->where('UserId', $userId);
        $this->db->update('users');
        return true;
	}
	
	public function _getRideCustomerDetails($userId) {
		$this->db->select(array(
			'FirstName',
			'LastName',
			'Phone',
			'DisplayPicture'
		));
		$this->db->where('UserId', $userId);
		return $this->db->get('users')->result();
	}

	public function _isPhonenumberExists($phone) {
		$this->db->select('Phone');
		$this->db->where('Phone', $phone);
		return $this->db->get('users')->num_rows();
	}

	public function _verifyOtp($data) {
        $this->db->where($data);
        return $this->db->get('users')->result();
    }

    public function _resetPassword($mobile,$password) { 			
	    $this->db->where('Phone', $mobile);
        $this->db->update('users', array('Password'=>$password,'ForgotPasswordOtp'=>''));
	    return $this->db->affected_rows();
    }

    public function getAllUsers(){
		$where = array(
			'UserType' => 'U',
		  );
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where($where);
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ) {
			return $query->result();
		}
	}

	public function _findUserDetails($userId){
        $this->db->select('users.FirstName,
        	users.LastName,
        	users.Email,
        	users.Phone,
        	users.DOB,
        	users.PlaceOfBirth,
        	users.DisplayPicture,
        	users.WalletBalance,
        	users.Rating,
        	users.City,
        	users.State,
        	users.Establishment_EstablishmentId,
        	users.Pincode,
        	users.Status,
        	vehicles.RegistrationId,
        	vehicles.VehiclePermit,
        	vehicles.VehicleNumber,
			vehicles.VehicleBrand,
        	vehicles.VehicleColor,
        	vehicles.VehicleType,
        	vehicles.Available,
        	vehicles.CreatedOn as VehicleCreatedOn,
        	job_categories.CategoryName,
        	establishment.CountryName,
        	users.CreatedOn');
        $this->db->from('users');
        $this->db->where('users.UserId', $userId);
        //$this->db->where('vehicles.Availabe', 'Y');
        $this->db->join('establishment', 'establishment.EstablishmentId = users.Establishment_EstablishmentId');
        $this->db->join('vehicles', 'vehicles.Users_UserId = users.UserId');
        $this->db->join('job_categories', 'job_categories.JobCategoryId = vehicles.JobCategory_JobCategoryId');
        return $this->db->get()->result();
    }
}