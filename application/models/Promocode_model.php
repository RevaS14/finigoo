<?php
class Promocode_model extends CI_Model {
    public function __construct() {
        $this->load->database();
    }

    public function _getPromocode($vehicleType) {
        $this->db->select(array(
            'PromoCodeId',
            'PromoCode',
            'RewardPercentage',
            'RewardAmount',
            'MaxReward'
        ));
        $this->db->where('JobCategories_JobCategoryId', $vehicleType);
        $this->db->where('StartDate <=', date(time()));
        $this->db->where('ExpiryDate >=', date(time()));
        $this->db->where('Status', 'A');
        $this->db->get('promocodes')->result();

        var_dump($this->db->last_query());
    }
}