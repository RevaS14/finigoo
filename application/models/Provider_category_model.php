<?php
class Provider_category_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function _getAllProviderCategories($type){
        $this->db->select('pc.Status,pc.ProviderCategory,pc.CategoryImage,pc.JobCategories_JobCategoryId,pc.ProviderCategoriesId,jc.CategoryName');
		    $this->db->from('provider_categories as pc');
        $this->db->where('pc.JobCategories_JobCategoryId', $type);
        $this->db->join('job_categories as jc', 'pc.JobCategories_JobCategoryId = jc.JobCategoryId' );
		    return $this->db->get()->result();
    }
    
    public function _findCategory($categoryId) {
		    return $this->db->get_where('provider_categories', array('ProviderCategoriesId' => $categoryId))->row();
    }

    public function _saveProviderCategory($categoryData) {
        $this->db->insert('provider_categories', $categoryData);
        return $this->db->insert_id();
    }
    
    public function _updateProviderCategory($id,$categoryData) { 			
        $this->db->update('provider_categories',$categoryData,array('ProviderCategoriesId' => $id));
        return $this->db->affected_rows();
    }
    
    public function _deleteProviderCategory($categoryId) { 			
		    $this->db->where('ProviderCategoriesId', $categoryId);
        $this->db->delete('provider_categories');	
        return true;	
	}

}