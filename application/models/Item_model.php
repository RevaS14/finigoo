<?php
class Item_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function _getAllItems(){
        $this->db->select('items.ItemId, items.ItemName, items.Price, items.ProviderSubCategories_ProviderSubCategoryId, items.Status, sub.SubCategory');
		$this->db->from('items');
        // $this->db->where('pc.JobCategories_JobCategoryId', $type);
        $this->db->join('provider_sub_categories as sub', 'items.ProviderSubCategories_ProviderSubCategoryId = sub.ProviderSubCategoryId');
	  	return $this->db->get()->result();
    }

    public function _getItems($subCategoryId){
        $this->db->select(array(
            'ItemId',
            'ItemName',
            'IsVeg',
            'ItemImage',
            'Price',
            'PreparationTime',
            'OutOfStock',
            'Status',
          ));
		$this->db->from('items');
        $this->db->where('ProviderSubCategories_ProviderSubCategoryId', $subCategoryId);
	  	return $this->db->get()->result();
    }
    
    public function _findItem($itemId) {
		return $this->db->get_where('items', array('ItemId' => $itemId))->row();
    }

    public function _saveItem($itemData) {  
        $this->db->insert('items', $itemData);
        return $this->db->insert_id();
    }
    
    public function _updateItem($id,$itemData) { 			
        $this->db->update('items',$itemData,array('ItemId' => $id));
        return $this->db->affected_rows();
    }
    
    public function _deleteItem($itemId) { 			
        $this->db->where('ItemId', $itemId);
        $this->db->delete('items');	
        return true;	
	}

}