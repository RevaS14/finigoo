<?php
class Job_category_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function _getAllJobCategories(){
		$this->db->from('job_categories');
        $this->db->where('Status', 'A');
		return $this->db->get()->result();
	}

}