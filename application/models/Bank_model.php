<?php
class Bank_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
  }

  public function _getListOfBanks() {
    $this->db->where('Status', 'A');
    return $this->db->get('banks')->result();
  }
}
