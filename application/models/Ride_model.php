<?php
class Ride_model extends CI_Model {
    public function __construct() {
        $this->load->database();
    }

    public function _updateRating($userId, $rideId, $updateColumn, $tableName, $rating, $feedbackColumn, $feedback) {
        $set = array(
            $updateColumn=>$rating,
            $feedbackColumn=>$feedback
        );
        $this->db->set($set);
        if($updateColumn == 'UsersRating') {
            $this->db->where('AcceptedBy', $userId);
        } else {
            $this->db->where('Users_UserId', $userId);
        }
        $this->db->where('RideId', $rideId);
        $this->db->update($tableName);
        return $this->db->affected_rows();
    }

    public function _insertRide($ride) {
        $this->db->insert('rides', $ride);
        return $this->db->insert_id();
    }

    public function _acceptRideOrder($userId, $rideStatus) {
        $this->db->set('RideStatus', 'A');
        $this->db->set('AcceptedBy', $userId);
        $this->db->where('JobCategories_JobCategoryId', $rideStatus['vehicleType']);
        $this->db->where('RideId', $rideStatus['rideId']);
        $this->db->update('rides');
        return $this->db->affected_rows();
    }

    public function _verifyOtp($userId, $rideStatus) {
        $this->db->where('JobCategories_JobCategoryId', $rideStatus['vehicleType']);
        $this->db->where('RideId', $rideStatus['rideId']);
        $this->db->where('Otp', $rideStatus['otp']);
        $this->db->where('RideStatus', 'C');
        $this->db->get('rides');
        return $this->db->count_all_results();
    }

    public function _startRideOrder($userId, $rideStatus) {
        $this->db->set('RideStatus', 'P');
        $this->db->set('Otp', null);
        $this->db->where('JobCategories_JobCategoryId', $rideStatus['vehicleType']);
        $this->db->where('RideId', $rideStatus['rideId']);
        $this->db->update('rides');
        return $this->db->affected_rows();
    }

    public function _getDriverIdByRide($rideId) {
        $this->db->select('AcceptedBy');
        $this->db->where('RideId', $rideId);
        return $this->db->get('rides')->result();
    }

    public function _getCustomerIdByRide($rideId) {
        $this->db->select('Users_UserId');
        $this->db->where('RideId', $rideId);
        return $this->db->get('rides')->result();
    }

    public function _completeRideFare($driverId, $kilometer, $orderTime, $pincode, $vehicleType, $rideId) {
        //var_dump("CALL completeRide('".$kilometer."', '".$orderTime."', '".$pincode."', '".$vehicleType."', '".$rideId."', '".$driverId."')");
        return $this->db->query("CALL completeRide('".$kilometer."', '".$orderTime."', '".$pincode."', '".$vehicleType."', '".$rideId."', '".$driverId."')")
                        ->result();
    }

    public function _getGoCabList() {
        $this->db->select(array(
            "JobCategoryId",
            "CategoryName",
            "JobCategoryImage"
        ));
        $this->db->where_in('JobCategoryId', array('1', '2'));
        return $this->db->get('job_categories')->result();
    }

    public function _getRideHistory($userId,$type) {
        $this->db->select('rides.RideId,
                            customer.FirstName as CustomerName,
                            driver.FirstName as DriverName,
                            rides.Topup_TopupId as TopupId,
                            rides.SourceLatitude,
                            rides.SourceLongitude,
                            rides.DestinationLatitude,
                            rides.DestinationLongitude,
                            rides.SourceAddress,
                            rides.DestinationAddress,
                            rides.Distance,
                            rides.EstimatedTime,
                            rides.RidePath,
                            rides.EstimatedFare,
                            rides.ActualFare,
                            rides.PaymentMode,
                            rides.RideStatus,
                            vehicle.CategoryName as RideType,
                            vehicles.VehicleNumber,
                            vehicles.VehicleColor,
                            vehicles.VehicleBrand,
                            driver.DisplayPicture as DriverImg,
                            rides.UsersRating,
                            rides.DriversRating,
                            rides.UsersFeedback,
                            rides.DriversFeedback,
                            rides.Promocodes_PromoCodeId as PromoCodeId,
                            rides.Status,
                            rides.CreatedOn');
        if($type=='D') { 
            $this->db->where('rides.AcceptedBy', $userId);
        } elseif($type=='U') {
            $this->db->where('rides.Users_UserId', $userId);
        }
        $this->db->join('users as customer', 'customer.UserId = rides.Users_UserId' );
        $this->db->join('vehicles', 'vehicles.Users_UserId = rides.AcceptedBy' );
        $this->db->join('users as driver', 'driver.UserId = rides.AcceptedBy' );
        $this->db->join('job_categories as vehicle', 'vehicle.JobCategoryId = rides.JobCategories_JobCategoryId' );
        $this->db->order_by('rides.RideId', 'DESC');
        return $this->db->get('rides')->result();
    }

    public function _getAllRides(){
        $this->db->select('rides.RideId,
        customer.FirstName as Customer,
        driver.FirstName as Driver,
        vehicle.CategoryName as vehicle,
        rides.Topup_TopupId,
        rides.SourceLatitude,
        rides.SourceLongitude,
        rides.DestinationLatitude,
        rides.DestinationLongitude,
        rides.SourceAddress,
        rides.DestinationAddress,
        rides.Distance,
        rides.EstimatedTime,
        rides.RidePath,
        rides.EstimatedFare,
        rides.ActualFare,
        rides.PaymentMode,
        rides.RideStatus,
        rides.Promocodes_PromoCodeId,
        rides.Status,
        rides.CreatedOn');
        $this->db->where('rides.Status', 'A');
        $this->db->join('users as customer', 'customer.UserId = rides.Users_UserId' );
        $this->db->join('users as driver', 'driver.UserId = rides.AcceptedBy' );
        $this->db->join('job_categories as vehicle', 'vehicle.JobCategoryId = rides.JobCategories_JobCategoryId' );
        $this->db->order_by('rides.RideId', 'DESC');
        return $this->db->get('rides')->result();
    }

        public function _findRide($rideId) {
        $this->db->select('rides.RideId,
                            customer.FirstName as CustomerName,
                            driver.FirstName as DriverName,
                            rides.Topup_TopupId as TopupId,
                            rides.SourceLatitude,
                            rides.SourceLongitude,
                            rides.DestinationLatitude,
                            rides.DestinationLongitude,
                            rides.SourceAddress,
                            rides.DestinationAddress,
                            rides.Distance,
                            rides.EstimatedTime,
                            rides.RidePath,
                            rides.EstimatedFare,
                            rides.ActualFare,
                            rides.PaymentMode,
                            rides.RideStatus,
                            vehicle.CategoryName as RideType,
                            vehicles.VehicleNumber,
                            vehicles.VehicleColor,
                            vehicles.VehicleBrand,

                            topup.PaymentMethod as Payment,
                            topup.Amount,
                            topup.TransactionId,
                            topup.TransactionStatus,
                            topup.Remarks,
                            topup.Status as TopupStatus,
                            topup.CreatedOn as TopupCreatedOn,

                            driver.DisplayPicture as DriverImg,
                            rides.UsersRating,
                            rides.DriversRating,
                            rides.UsersFeedback,
                            rides.DriversFeedback,
                            rides.Promocodes_PromoCodeId as PromoCodeId,
                            rides.Status,
                            rides.AcceptedBy,
                            rides.CreatedOn');
        $this->db->where('rides.RideId', $rideId);
        $this->db->join('users as customer', 'customer.UserId = rides.Users_UserId' );
        $this->db->join('vehicles', 'vehicles.Users_UserId = rides.AcceptedBy' );
        $this->db->join('users as driver', 'driver.UserId = rides.AcceptedBy' );
        $this->db->join('topup', 'topup.TopupId = rides.Topup_TopupId' );
        $this->db->join('job_categories as vehicle', 'vehicle.JobCategoryId = rides.JobCategories_JobCategoryId' );
        return $this->db->get('rides')->result();
    }

    public function _driverRidesStatus($userId,$status){
        $this->db->select('*');
        $this->db->from('rides');
        $this->db->where('AcceptedBy', $userId);
        $this->db->where('RideStatus', $status);
        return $this->db->get()->result();
    }

    public function _getRideStatus($userId,$userType){
         $this->db->select('rides.RideId,
                            customer.FirstName as CustomerName,
                            driver.FirstName as DriverName,
                            rides.Topup_TopupId as TopupId,
                            rides.SourceLatitude,
                            rides.SourceLongitude,
                            rides.DestinationLatitude,
                            rides.DestinationLongitude,
                            rides.SourceAddress,
                            rides.DestinationAddress,
                            rides.Distance,
                            rides.EstimatedTime,
                            rides.RidePath,
                            rides.EstimatedFare,
                            rides.ActualFare,
                            rides.PaymentMode,
                            rides.RideStatus,
                            vehicle.CategoryName as RideType,
                            vehicles.VehicleNumber,
                            vehicles.VehicleColor,
                            vehicles.VehicleBrand,

                            driver.DisplayPicture as DriverImg,
                            rides.UsersRating,
                            rides.DriversRating,
                            rides.UsersFeedback,
                            rides.DriversFeedback,
                            rides.Promocodes_PromoCodeId as PromoCodeId,
                            rides.Status,
                            rides.AcceptedBy,
                            rides.CreatedOn');
        if($userType=='U') {
            $this->db->where('rides.Users_UserId', $userId);
        } elseif($userType=='D') {
            $this->db->where('rides.AcceptedBy', $userId);
        }
        
        
        $this->db->join('users as customer', 'customer.UserId = rides.Users_UserId' );
        $this->db->join('vehicles', 'vehicles.Users_UserId = rides.AcceptedBy' );
        $this->db->join('users as driver', 'driver.UserId = rides.AcceptedBy' );
        //$this->db->join('topup', 'topup.TopupId = rides.Topup_TopupId' );
        $this->db->join('job_categories as vehicle', 'vehicle.JobCategoryId = rides.JobCategories_JobCategoryId' );
        $this->db->limit(1);
        $this->db->order_by('rides.RideId',"DESC");
        return $this->db->get('rides')->result();
    }
    

}