<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
	}
    
    public function insertDriver($driverData) {
        $this->db->insert('drivers', $driverData);
		return $this->db->insert_id();
    }

    public function setVerifyStatus($userData) {
		$userId = $userData['UserId'];
        $status = $userData['Status'];
        $jobCategory = $userData['JobCategory'];
        $userType = $userData['UserType'];
        $vehicleId = $userData['VehicleId'];

        $this->db->set('Status', $status);
        $this->db->where('Vehicles_VehicleId', $vehicleId);
        $this->db->update('drivers');

        if ($this->db->affected_rows() >= 0) {
            return true;
        } 
    }

    public function updateDriverDetails($postData) { 
        $updateVehicle = $this->db->get_where('drivers', array(
            'DriverId' => $postData['DriverId']
        ));

        if ( $updateVehicle->num_rows() == 1 ) {
			$updateData = array(
                "DriverId" => $postData['DriverId'],
                "DriverName" => $postData['DriverName'],
                "DrivingLicense" => $postData['DrivingLicense'],
                "DrivingLicenseDocument" => $postData['DrivingLicenseDocument'],
            ); 
            $this->db->update('drivers', $updateData);
            return true;
        }
        else {
			return false;
		}
    }
}
