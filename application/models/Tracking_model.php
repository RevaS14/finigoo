<?php
class Tracking_model extends CI_Model {
    public $driverManager;

    public function __construct() {
        //$this->driverManager = new MongoDB\Driver\Manager("mongodb://localhost:27017");
    }

    public function _insertDriver($driver) {
        $bulk = new MongoDB\Driver\BulkWrite;
        $doc = [
            '_id' => new MongoDB\BSON\ObjectID,
            'driverId' => $driver['driverId'],
            'vehicleType' => $driver['vehicleType'],
            'available' => $driver['available'],
            'zoneId' => $driver['zoneId'],
            'updatedAt' => time()
        ];
        $bulk->insert($doc);
        return $this->driverManager->executeBulkWrite('rideapp.drivers', $bulk);
    }

    public function _updateDriverAvailablity($userId, $driverStatus) {
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->update(['driverId' => $userId],
            [
                '$set' => [
                    'available' => $driverStatus
                ]
            ]
        );
        return $this->driverManager->executeBulkWrite('finigoo.drivers', $bulk);
    }

    public function _updateDriverRegistrationId($userId, $registrationId) {
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->update(['driverId' => $userId],
            [
                '$set' => [
                    'registrationId' => $registrationId
                ]
            ]
        );
        return $this->driverManager->executeBulkWrite('finigoo.drivers', $bulk);
    }

    public function _updateDriverLocation($userId, $latitude, $longitude) {
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->update(['driverId' => $userId],
            [
                '$set' => [
                    'location' => [
                        'type' => 'Point',
                        'coordinates' => [
                            ($latitude+0), ($longitude+0)
                        ]
                    ],
                    'updatedAt' => time()
                ]
            ]
        );
        $this->driverManager->executeBulkWrite('finigoo.drivers', $bulk);
    }

    public function _getDriverLocation($driverId, $vehicleType) {
        $filter = [
            'vehicleType' => $vehicleType,
            'driverId' => $driverId
        ];

        $options = ["projection" => [
            '_id' => 0,
            'vehicleType' => 0,
            'available' => 0,
            'updatedAt' => 0,
            'driverId' => 0
        ]];
        
        $query = new MongoDB\Driver\Query($filter, $options);
        
        return $this->driverManager->executeQuery("finigoo.drivers", $query);
    }

    public function _getDriversNearBy($latitude, $longitude, $vehicleType) {
        $filter = [
            'location' => [
                '$near' => [
                    '$geometry' => [
                        'type' => 'Point',
                        'coordinates' => [
                            $latitude,
                            $longitude
                        ]
                    ],
                    '$maxDistance' => 2000
                ]
            ],
            'vehicleType' => [
                '$in' => explode(",", $vehicleType)
            ],
            'available' => 'Y'
        ];

        $options = ["projection" => ['_id' => 0]];
        
        $query = new MongoDB\Driver\Query($filter, $options);
        
        return $this->driverManager->executeQuery("finigoo.drivers", $query);
    }

    public function _searchProvidersNearBy($latitude, $longitude, $providerType) {
        $filter = [
            'location' => [
                '$near' => [
                    '$geometry' => [
                        'type' => 'Point',
                        'coordinates' => [
                            $latitude,
                            $longitude
                        ]
                    ],
                    '$maxDistance' => 10000
                ]
            ],
            'providerType' => $providerType,
            'available' => 'Y'
        ];

        $options = ["projection" => ['_id' => 0]];
        
        $query = new MongoDB\Driver\Query($filter, $options);
        
        return $this->driverManager->executeQuery("finigoo.providers", $query);
    }

    public function _getDriversNearByZone($driverId) {
        $filter = [
            'driverId' => $driverId
        ];
        
        $options = ["projection" => ['_id' => 0]];
        
        $query = new MongoDB\Driver\Query($filter, $options);
        
        return $this->driverManager->executeQuery("rideapp.drivers", $query);
    }

}