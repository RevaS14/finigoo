<?php
class Establishment_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
  }

  public function _getCountries() {
    $this->db->select(array('EstablishmentId', 'CountryName'));
    $this->db->where('Status', 'A');
    return $this->db->get('establishment')->result();
  }
}
