<style>
    .services .card-content {
        min-height: 115px;
    }

    .services .card-content span {
        font-size: 12px;
    }

    .dashboard-box {
        background-color: rgba(255, 255, 255, 0.9);
        box-shadow: 0 0 5px 0 rgba(43, 43, 43, 0.1), 0 2px 6px -7px rgba(43, 43, 43, 0.1) !important;
    }

    .dashboard-title {
        padding: 1rem !important;
    }

    .dashboard-title h5 {
        font-size: 16px;
    }
</style>
<div class="container">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">PARTNER</h3>
            </div>
            <div class="content-header-right col-md-6 col-12"></div>
        </div>
        <div class="content-body">
            <div id="render-content">
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <div class="card dashboard-box">
                            <div class="card-block dashboard-title">
                                <h5>REGISTER YOURSELF AS A DRIVER, PARTNER, OUR BUSINESS PARTNER</h5>
                            </div>
                        </div>
                    </div>
                    <!-- social statusric start -->
                    <div class="services col-xl-12">
                        <div class="row">
                            <div class="col-xl-3 col-lg-6 col-12">
                                <a data-toggle="tooltip" data-placement="top" title="" class="render_link" href="<?php echo base_url(); ?>registration/gocab" data-original-title="CAB RIDE">
                                    <div class="card  green ">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="media">
                                                    <div class="media-body text-left w-100">
                                                        <h3 class="primary">0</h3>
                                                        <span>GOCAB</span>
                                                    </div>
                                                    <div class="media-right media-middle"> <span class="circle-border">
                                                            <img src="<?php echo base_url();?>/assets/images/service-category/car.png" height="40" width="40">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-12">
                                <a data-toggle="tooltip" data-placement="top" title="" class="render_link" href="<?php echo base_url(); ?>registration/goride" data-original-title="BIKE RIDE">
                                    <div class="card  green ">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="media">
                                                    <div class="media-body text-left w-100">
                                                        <h3 class="primary">0</h3>
                                                        <span>GORIDE</span>
                                                    </div>
                                                    <div class="media-right media-middle"> <span class="circle-border">
                                                            <img src="<?php echo base_url();?>/assets/images/service-category/bike-ride.png" height="40" width="40">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-12">
                                <a data-toggle="tooltip" data-placement="top" title="" class="render_link" href="<?php echo base_url(); ?>registration/goauto" data-original-title="AUTO RIDE">
                                    <div class="card  purple ">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="media">
                                                    <div class="media-body text-left w-100">
                                                        <h3 class="primary">0</h3>
                                                        <span>GOAUTO</span>
                                                    </div>
                                                    <div class="media-right media-middle"> <span class="circle-border">
                                                            <img src="<?php echo base_url();?>/assets/images/service-category/shareauto.png" height="40" width="40">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-12">
                                <a data-toggle="tooltip" data-placement="top" title="" class="render_link" href="<?php echo base_url(); ?>registration/gobox" data-original-title="BOX RIDE">
                                    <div class="card  purple ">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="media">
                                                    <div class="media-body text-left w-100">
                                                        <h3 class="primary">0</h3>
                                                        <span>GOBOX</span>
                                                    </div>
                                                    <div class="media-right media-middle"> <span class="circle-border">
                                                            <img src="<?php echo base_url();?>/assets/images/service-category/tow-truck.png" height="40" width="40">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-xl-3 col-lg-6 col-12">
                                <a data-toggle="tooltip" data-placement="top" title="" class="render_link" href="<?php echo base_url(); ?>registration/gosend" data-original-title="AUTO RIDE">
                                    <div class="card  purple ">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="media">
                                                    <div class="media-body text-left w-100">
                                                        <h3 class="primary">0</h3>
                                                        <span>GOSEND</span>
                                                    </div>
                                                    <div class="media-right media-middle"> <span class="circle-border">
                                                            <img src="<?php echo base_url();?>/assets/images/service-category/courier-service-1.png" height="40" width="40">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-12">
                                <a data-toggle="tooltip" data-placement="top" title="" class="render_link" href="<?php echo base_url(); ?>registration/gocargo" data-original-title="CARGO RIDE">
                                    <div class="card  purple ">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="media">
                                                    <div class="media-body text-left w-100">
                                                        <h3 class="primary">0</h3>
                                                        <span>GOCARGO</span>
                                                    </div>
                                                    <div class="media-right media-middle"> <span class="circle-border">
                                                            <img src="<?php echo base_url();?>/assets/images/service-category/tow-truck.png" height="40" width="40">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-12">
                                <a data-toggle="tooltip" data-placement="top" title="" class="render_link" href="<?php echo base_url(); ?>registration/gofood" data-original-title="GO FOOD">
                                    <div class="card  purple ">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="media">
                                                    <div class="media-body text-left w-100">
                                                        <h3 class="primary">0</h3>
                                                        <span>GO FOOD</span>
                                                    </div>
                                                    <div class="media-right media-middle"> <span class="circle-border">
                                                            <img src="<?php echo base_url();?>/assets/images/service-category/food-delivery.png" height="40" width="40">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-12">
                                <a data-toggle="tooltip" data-placement="top" title="" class="render_link" href="<?php echo base_url(); ?>registration/gofood-deliverypartner" data-original-title="GO FOOD - DELIVERY PARTNER">
                                    <div class="card  purple ">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="media">
                                                    <div class="media-body text-left w-100">
                                                        <h3 class="primary">0</h3>
                                                        <span>GO FOOD - DELIVERY PARTNER</span>
                                                    </div>
                                                    <div class="media-right media-middle"> <span class="circle-border">
                                                            <img src="<?php echo base_url();?>/assets/images/service-category/food-delivery.png" height="40" width="40">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- social statusric end -->
                </div>
            </div>
        </div>
    </div>
</div>