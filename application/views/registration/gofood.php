<div class="container">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <?php if($jobCategory == 12) { ?>
               <h3 class="content-header-title mb-0">Go Food Registration Form</h3>
            <?php } ?>   
         </div>
         <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group">
               <a href="<?php echo base_url();?>registrationcontroller"
                  class="btn btn-outline-primary m-b-0 btn-right render_link"> Back</a>
            </div>
         </div>
      </div>
      <div class="content-body">
         <section>
            <div id="render-content">
               <div class="row">
                  <div class="col-12">
                     <?php if(isset($status)) { 
                        ?>
                     <div class="alert alert-success" role="alert">
                        Registration Successfully..!
                     </div>
                     <?php } ?>
                     <form id="main" method="post"
                        action="<?php echo site_url();?>registration/gofood-save"
                        enctype="multipart/form-data">
                        <input type="hidden" name="JobCategory" value="<?php echo $jobCategory ?>">
                        <input type="hidden" name="redirectView" value="<?php echo $redirectView ?>">
                        <input type="hidden" name="UserType" value="R">
                        <input type="hidden" name="_token" value="">
                        <div class="row">
                           <span id="service_category_append"></span>
                           <div class="form-group col-sm-12">
                              <div class="card">
                                 <!-- <div class="card-header">
                                    <h5>Add Driver of Bike Ride</h5>
                                    </div> -->
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label">First Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Firstname"
                                                required id="Firstname" placeholder="First Name"value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Last Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Lastname"
                                                required id="Lastname" placeholder="Last Name" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Email:<sup
                                                class="error">*</sup></label>
                                             <input type="email" class="form-control" name="Email"
                                                required id="email" placeholder="Unique Email" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class=" col-form-label">Mobile Phone:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control"
                                                name="Phone" required id="Phone"
                                                placeholder="Unique Contact Number" value="">
                                             <span class="error"></span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Place of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PlaceOfBirth" required
                                                id="POB" placeholder="Place of Birth" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Date of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="Date" class="form-control" name="DOB" required
                                                id="POB" placeholder="Date of Birth" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Identity number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="IdentityNo"
                                                required id="In" placeholder="Identity number" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">PinCode<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PinCode"
                                                required id="PC" placeholder="Pin Code" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">City<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="City"
                                                required id="City" placeholder="City" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">State<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="State"
                                                required id="State" placeholder="State" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Country<sup
                                                class="error">*</sup></label>
                                             <select class="form-control" id="Country" name="Country">
                                                <option value="1" selected>India</option>
                                             </select>
                                             <span class="error"></span>
                                          </div>
                                         
                                         
                                          <?php if($jobCategory == 1) { ?>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">JobType<sup
                                                class="error">*</sup></label>
                                             <select class="form-control" id="JobType" name="JobCategory">
                                                <option value="1" selected>Go Mini</option>
                                                <option value="2" >Go Micro</option>
                                             </select>
                                             <span class="error"></span>
                                          </div>
                                       
                                          <?php } ?>
                                          </div>
                                       
                                       <div class="row">
                                          
                                          
                                          <div class="col-sm-6">
                                             <label class="col-form-label image image-label">Photo
                                             Profile:</label>
                                             <div id="profile-image-preview">
                                                <label for="profile-upload" id="profile-label">Upload
                                                Image</label>
                                                <input type="file" id="profile-upload"
                                                   name="DisplayPicture" accept=".jpg,.jpeg,.png" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label image image-label">Identity
                                             Document Upload:</label>
                                             <div id="identity-image-preview">
                                                <label for="identity-upload" id="identity-label">Upload
                                                Image</label>
                                                <input type="file" id="identity-upload"
                                                   name="IdentityDocument" accept=".jpg,.jpeg,.png" required = "required" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-sm-12">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>RESTAURANT INFORMATION
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Restaurant Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="RestaurantName"
                                                required id="RestaurantName" placeholder="Restaurant Name"
                                                value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Restaurant Address:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="RestaurantAddress"
                                                required id="RestaurantAddress" placeholder="Restaurant Address"
                                                value="" required>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Restaurant Phone Number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="RestaurantPhoneNumber"
                                                required id="RestaurantPhoneNumber" placeholder="Restaurant Phone Number"
                                                value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Restaurant Category:<sup
                                                class="error">*</sup></label>
                                                <select class="form-control selectpicker" id="RestaurantCategory" name="RestaurantCategory[]"  multiple="multiple" tabindex="1" >
                                                <option value="1" selected>Italian</option>
                                                <option value="1">Pizza</option>
                                             </select>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Opening Time:<sup
                                                class="error">*</sup></label>
                                                <select class="form-control" id="OpeningTime" name="OpeningTime">
                                                <option value="01:00">01:00</option>
                                                <option value="01:30">01:30</option>
                                                <option value="02:00">02:00</option>
                                                <option value="02:30">02:30</option> 
                                                <option value="03:00">03:00</option>
                                                <option value="03:30">03:30</option>
                                                <option value="04:00">04:00</option>
                                                <option value="04:30">04:30</option>
                                                <option value="05:00">05:00</option>
                                                <option value="05:30">05:30</option>
                                                <option value="06:00">06:00</option>
                                                <option value="06:30">06:30</option>
                                                <option value="07:00">07:00</option>
                                                <option value="07:30">07:30</option>
                                                <option value="08:00">08:00</option>
                                                <option value="08:30">08:30</option>
                                                <option value="09:00">09:00</option>
                                                <option value="09:30">09:30</option>
                                                <option value="10:00">10:00</option>
                                                <option value="10:30">10:30</option>
                                                <option value="11:00">11:00</option>
                                                <option value="11:30">11:30</option>
                                                <option value="12:00">12:00</option>
                                                <option value="12:30">12:30</option>
                                                <option value="13:00">13:00</option>
                                                <option value="13:30">13:30</option>
                                                <option value="14:00">14:00</option>
                                                <option value="14:30">14:30</option>
                                                <option value="15:00">15:00</option>
                                                <option value="15:30">15:30</option>
                                                <option value="16:00">16:00</option>
                                                <option value="16:30">16:30</option>
                                                <option value="17:00">17:00</option>
                                                <option value="17:30">17:30</option>
                                                <option value="18:00">18:00</option>
                                                <option value="18:30">18:30</option>
                                                <option value="19:00">19:00</option>
                                                <option value="19:30">19:30</option>
                                                <option value="20:00">20:00</option>
                                                <option value="20:30">20:30</option>
                                                <option value="21:00">21:00</option>
                                                <option value="21:30">21:30</option>
                                                <option value="22:00">22:00</option>
                                                <option value="22:30">22:30</option>
                                                <option value="23:00">23:00</option>
                                                <option value="23:30">23:30</option>
                                                <option value="23:59">23:59</option>
                                             </select>
                                             <span class="error"></span>
                                          </div>

                                          <div class="col-sm-4">
                                             <label class="col-form-label">Closing Time:<sup
                                                class="error">*</sup></label>
                                                <select class="form-control" id="ClosingTime" name="ClosingTime">
                                                <option value="01:00">01:00</option>
                                                <option value="01:30">01:30</option>
                                                <option value="02:00">02:00</option>
                                                <option value="02:30">02:30</option> 
                                                <option value="03:00">03:00</option>
                                                <option value="03:30">03:30</option>
                                                <option value="04:00">04:00</option>
                                                <option value="04:30">04:30</option>
                                                <option value="05:00">05:00</option>
                                                <option value="05:30">05:30</option>
                                                <option value="06:00">06:00</option>
                                                <option value="06:30">06:30</option>
                                                <option value="07:00">07:00</option>
                                                <option value="07:30">07:30</option>
                                                <option value="08:00">08:00</option>
                                                <option value="08:30">08:30</option>
                                                <option value="09:00">09:00</option>
                                                <option value="09:30">09:30</option>
                                                <option value="10:00">10:00</option>
                                                <option value="10:30">10:30</option>
                                                <option value="11:00">11:00</option>
                                                <option value="11:30">11:30</option>
                                                <option value="12:00">12:00</option>
                                                <option value="12:30">12:30</option>
                                                <option value="13:00">13:00</option>
                                                <option value="13:30">13:30</option>
                                                <option value="14:00">14:00</option>
                                                <option value="14:30">14:30</option>
                                                <option value="15:00">15:00</option>
                                                <option value="15:30">15:30</option>
                                                <option value="16:00">16:00</option>
                                                <option value="16:30">16:30</option>
                                                <option value="17:00">17:00</option>
                                                <option value="17:30">17:30</option>
                                                <option value="18:00">18:00</option>
                                                <option value="18:30">18:30</option>
                                                <option value="19:00">19:00</option>
                                                <option value="19:30">19:30</option>
                                                <option value="20:00">20:00</option>
                                                <option value="20:30">20:30</option>
                                                <option value="21:00">21:00</option>
                                                <option value="21:30">21:30</option>
                                                <option value="22:00">22:00</option>
                                                <option value="22:30">22:30</option>
                                                <option value="23:00">23:00</option>
                                                <option value="23:30">23:30</option>
                                                <option value="23:59">23:59</option>
                                             </select>
                                             <span class="error"></span>
                                          </div>

                                          <div class="col-sm-4">
                                             <label class="col-form-label">Latitude:<sup
                                                class="error">*</sup></label>
                                                <input type="text" class="form-control" name="Latitude"
                                                required id="Latitude" placeholder="Latitude"
                                                value="">
                                               
                                             <span class="error"></span>
                                          </div>

                                          <div class="col-sm-4">
                                             <label class="col-form-label">Longitude:<sup
                                                class="error">*</sup></label>
                                                <input type="text" class="form-control" name="Longitude"
                                                required id="Longitude" placeholder="Longitude"
                                                value="">
                                             <span class="error"></span>
                                          </div>


                                          <div class="col-sm-6">
                                             <label class="col-form-label">Description:<sup
                                                class="error">*</sup></label>
                                                <textarea class="form-control" name="Description" rows="11" cols="500"></textarea>
                                             <span class="error"></span>
                                          </div>

                                          <div class="col-sm-6">
                                             <label class="col-form-label image image-label">Restaurant Photos:</label>
                                             <div id="restaurant-image-preview">
                                                <label for="restaurant-upload" id="restaurant-label">Upload
                                                Image</label>
                                                <input type="file" id="restaurant-upload"
                                                   name="RestaurantPicture" accept=".jpg,.jpeg,.png" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>

                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!--<div class="form-group col-sm-4">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Terms and Services
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <ol>
                                             <li>Driver must provide their own car</li>
                                             <li>Driver must have a personal Android device</li>
                                             <li>Driver must have Driver license which is still active at
                                                least 6 months ahead
                                             </li>
                                          </ol>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>-->
                           <div class="form-group col-sm-12">
                              <center>
                                 <button type="submit" class="btn btn-success m-b-0">Submit</button>
                              </center>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>