<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <title> service list </title>
      <link rel="icon" href="#" type="image/x-icon">
      <link href="<?php echo base_url(); ?>assets/css/font.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/css/all.css" rel="stylesheet">
      <!-- BEGIN VENDOR CSS-->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/vendors.css">
      <!-- END VENDOR CSS-->
      <!-- BEGIN STACK CSS-->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/app.css">
      <!-- END STACK CSS-->
      <!-- BEGIN Page Level CSS-->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/core/menu/menu-types/vertical-menu.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/extensions/toastr.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sweetalert.min.css">
      <!-- END Page Level CSS-->
      <!-- BEGIN Custom CSS-->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/assets/css/style.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/style-changes.css">
      <!-- END Custom CSS-->
      <link href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
      <style>
      </style>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/country-code/intlTelInput.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
      
      <style>
      </style>
   </head>
   <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click"
      data-menu="vertical-menu" data-col="2-columns">
      <div id="render-css-link"></div>
      <div id="render-css"></div>
      <!-- fixed-top-->