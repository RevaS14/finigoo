<div id="render-js"></div>
<script src="<?php echo base_url() ?>app-assets/vendors/js/vendors.min.js"></script>
<script src="<?php echo base_url() ?>app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
<script src="<?php echo base_url() ?>app-assets/js/core/app-menu.js"></script>
<script src="<?php echo base_url() ?>app-assets/js/core/app.js"></script>
<script src="<?php echo base_url() ?>app-assets/vendors/js/extensions/toastr.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<script>
   $('#close').click(function () {
       if ($("#checkbox1").prop('checked') == true) {
           $('#driver_service').val(1);
       } else {
           $('#driver_service').val(0);
       }
       if ($("#checkbox2").prop('checked') == true) {
           $('#delivery_boy').val(1);
       } else {
           $('#delivery_boy').val(0);
       }
   
       if ($("#checkbox3").prop('checked') == true) {
           $('#courier_boy').val(1);
       } else {
           $('#courier_boy').val(0);
       }
       $('#model_overlay').removeClass('model_overlay');
   });
   
   var display_category = "1";
   if (display_category == 1) {
       $(window).on('load', function () {
           $("#provider-services-modal").modal('show');                    
       });                
   }
</script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/upload_image.js"></script>
<script type="text/javascript">
   $(document).ready(function () {
       $.uploadPreview({
           input_field: "#vehicle-upload",   // Default: .image-upload
           preview_box: "#vehicle-image-preview",  // Default: .image-preview
           label_field: "#vehicle-label",    // Default: .image-label
           label_default: "Choose Icon",   // Default: Choose File
           label_selected: "Change image",  // Default: Change File
           no_label: false                 // Default: false
       });
   
       $.uploadPreview({
           input_field: "#driver-upload",   // Default: .image-upload
           preview_box: "#driver-image-preview",  // Default: .image-preview
           label_field: "#driver-label",    // Default: .image-label
           label_default: "Choose Icon",   // Default: Choose File
           label_selected: "Change image",  // Default: Change File
           no_label: false                 // Default: false
       });
   
       $.uploadPreview({
           input_field: "#profile-upload",   // Default: .image-upload
           preview_box: "#profile-image-preview",  // Default: .image-preview
           label_field: "#profile-label",    // Default: .image-label
           label_default: "Choose Icon",   // Default: Choose File
           label_selected: "Change image",  // Default: Change File
           no_label: false                 // Default: false
       });
   
   
       $.uploadPreview({
           input_field: "#identity-upload",   // Default: .image-upload
           preview_box: "#identity-image-preview",  // Default: .image-preview
           label_field: "#identity-label",    // Default: .image-label
           label_default: "Choose Icon",   // Default: Choose File
           label_selected: "Change image",  // Default: Change File
           no_label: false                 // Default: false
       });

       $.uploadPreview({
           input_field: "#restaurant-upload",   // Default: .image-upload
           preview_box: "#restaurant-image-preview",  // Default: .image-preview
           label_field: "#restaurant-label",    // Default: .image-label
           label_default: "Choose Icon",   // Default: Choose File
           label_selected: "Change image",  // Default: Change File
           no_label: false                 // Default: false
       });
   
       
   });
</script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript">
   var today = new Date();
   var startDate = new Date(today.getFullYear());
   var endDate = new Date(today.getFullYear(), 6, 31);
   $('.form_datetime').datetimepicker({
       format: "yyyy",
       startView: 'decade',
       minView: 'decade',
       viewSelect: 'decade',
       startDate: startDate,
       endDate: endDate,
       autoclose: true,
   });
</script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/country-code/intlTelInput.min.js"></script>
<script type="text/javascript">
   var input = document.querySelector("#phone");
   window.intlTelInput(input, {
       // allowDropdown: false,
       // autoHideDialCode: false,
       // autoPlaceholder: "off",
       // dropdownContainer: document.body,
       // excludeCountries: ["us"],
       // formatOnDisplay: false,
       // geoIpLookup: function(callback) {
       //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
       //     var countryCode = (resp && resp.country) ? resp.country : "";
       //     callback(countryCode);
       //   });
       // },
       hiddenInput: "full_number",
       // initialCountry: "auto",
       // localizedCountries: { 'de': 'Deutschland' },
       // nationalMode: false,
        onlyCountries: ['in'],
       // placeholderNumberType: "MOBILE",
       // preferredCountries: ['cn', 'jp'],
       separateDialCode: true,
       // initialCountry: "auto",
       // geoIpLookup: function(success, failure) {
       //     $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
       //         var countryCode = (resp && resp.country) ? resp.country : "";
       //         success(countryCode);
       //     });
       // },
       utilsScript: "<?php echo base_url() ?>assets/js/country-code/utils.js",
   });
   
</script>
<script src="<?php echo base_url() ?>app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<script type="text/javascript">
   $('.icheck_minimal input').iCheck({
      checkboxClass: 'icheckbox_minimal',
      radioClass: 'iradio_minimal',
   });
</script>
<script></script>
<script>
   $("[data-toggle='tooltip']").click(function () {
       var $this = $(this);
       $(".tooltip").fadeOut("fast", function () {
           $this.blur();
       });
   });
   $(document).ready(function ($) {
       //Use this inside your document ready jQuery
       $(window).on('popstate', function () {
           location.reload(true);
       });


   });
</script>
<script type="text/javascript">
   var elements = document.querySelectorAll('input,select,textarea');
   
   for (var i = elements.length; i--;) {
   elements[i].addEventListener('invalid', function () {
     this.scrollIntoView(false);
   });
   }
</script>
</body>
</html>