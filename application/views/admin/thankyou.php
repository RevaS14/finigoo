<style>
            .icon{
                text-align: center;
    margin: 30px 0px;
            }
            .icon img{
                width: 100px;
                padding: 10px;
            }
            .card-container{
                width: 40%;
            }

            @media(max-width: 767px){
                .card-container{
                width: 80%;
            }
            }
            @media(min-width: 768px) and (max-width: 992px){
                .card-container{
                width: 60%;
            }
            }
            .thank-txt{
                font-size: 24px;
    font-weight: bold;
            }
            .back-btn{
                margin: 30px 0px;
    text-align: center;
            }
            .back-btn button  {
                border-radius: 50px;
                padding: 10px 20px;
                border: 2px solid #df4d60db;
                background: transparent;
                color: #00000080;
            }
            .back-btn button:hover  {
                background: #df4d60db;
                color: #fff;
            }
            .back-btn button a{
                color: #000;
            }
            .back-btn button a:hover{
                color: #fff;
            }
		</style>
		 <div class="container card-container">
	<div class="content-wrapper">
<div class="content-body">
			<section>
				<div id="render-content">
					<div class="row">
						<div class="col-12">
													</div>
						<div class="form-group col-sm-12 card">
                            <div class="icon">
                                <img src="<?php echo base_url();?>assets/images/service-category/mail.png" />
                            </div>
                           
							<h3 class="card-body text-center thank-txt">Thank you for registration</h3>
							<p class="card-body text-center">
                                We have received your message.<br/>
                                We'll reach you out immediately!
                            </p>
                            <div class="back-btn">
                                <button class="btn"><a href="<?php echo base_url();?>registration/gocab"> Back to homepage</a></button>
                            </div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>