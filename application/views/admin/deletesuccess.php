<div class="container">
	<div class="content-wrapper">
		<div class="content-header row mh100-pixel">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">
					<?php if(isset($deleteUserDetails)) {?>
				Driver Details Deleted Successfully
					<?php } ?>
				</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
				<div class="btn-group float-md-right" role="group"> <a href="<?php echo base_url();?>admin/listdrivers/gocab" class="btn btn-outline-primary m-b-0 btn-right render_link"> Back</a>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div id="render-content">
					<div class="row">
						<div class="col-12">
							<?php if(isset($status)) { ?>
							<div class="alert alert-success" role="alert">Registration Successfully..!</div>
							<?php } ?>
						</div>
						<div class="form-group col-sm-12 card">
							<h3 class="card-body">Thankyou</h3>
							<p class="card-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>