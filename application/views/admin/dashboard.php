<div class="app-content content">
<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">Dashboard</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
			</div>
		</div>
		<div class="content-body">
			<style type="text/css">
				.selectize-control::before {
				-moz-transition: opacity 0.2s;
				-webkit-transition: opacity 0.2s;
				transition: opacity 0.2s;
				content: ' ';
				z-index: 2;
				position: absolute;
				display: block;
				top: 12px;
				right: 34px;
				width: 16px;
				height: 16px;
				background: url(images/spinner.gif);
				background-size: 16px 16px;
				opacity: 0;
			}
			.selectize-control.loading::before {
				opacity: 0.4;
			}
				 .boxes, .driver-box {
					box-shadow: 0 10px 40px 0 rgba(170, 169, 175, 0), 0 2px 9px 0 rgba(173, 173, 177, 0.44);
					padding: 10px;
					background: #fff;
						}
						.driver-box,#googleMap
						{
							margin-top: 20px;
						}
				 .boxes img {
				 	width: 60px;
    				margin-top: -29px;
				 }
				 .box-list .list-group-item{
				 	padding: 10px;
				 	border-top:1px solid #ccc;
				 	border-bottom: 0;
				 	border-left: 0;
				 	border-right: 0;
				 }
				 .driver-table tr td{
				 	padding: 6px;
				 }
				 .sub-dropdown {
				 	left: -38px !important;
				 	top:12px !important;
    				width: 330px;
				 }
					 .demo {
			-webkit-border-radius: 3px;
			-moz-border-radius: 3px;
			border-radius: 3px;
		}
	.demo:last-child {
		border-bottom: 0 none;
	}
	.demo select, .demo input, .demo .selectize-control {
		width: 100%;
	}
	.demo > *:first-child {
		margin-top: 0;
	}
	.demo > *:last-child {
		margin-bottom: 0;
	}
	.demo .value {
		margin: 0 0 10px 0;
		font-size: 12px;
	}
	.demo .value span {
		font-family: Monaco, "Bitstream Vera Sans Mono", "Lucida Console", Terminal, monospace;
	}

			</style>
<div id="wrapper">
			<div id="render-content">
				<div class="row">
					<div class="col-xl-12 col-md-12">
						<div class="card dashboard-box">
							<div class="card-block dashboard-title">
								<input type="hidden" id="token" value="<?php echo $this->session->userdata('token'); ?>" >
							<div id="wrapper">
								<div class="row">
								    <div class="col-md-3">
									  	<div class="card-block boxes">
									  	<h4>Booked Trips <a href=""><i class="fa fa-share-square-o float-right"></i></a></h4>
									  	<h1>540</h1>

									  	<div class="float-right">
									  	<img src="<?php echo base_url(); ?>assets/images/cab.png">
									  	</div>
									  	<div class="clearfix"></div>

										</div>
									</div>

									 <div class="col-md-3">
									  	<div class="card-block boxes">
									  	<h4>Total Drivers <a href=""><i class="fa fa-share-square-o float-right"></i></a></h4>
									  	<h1>300</h1>
									  	<div class="float-right">
									  	<img src="<?php echo base_url(); ?>assets/images/cab.png">
									  	</div>
									  	<div class="clearfix"></div>

										</div>
									</div>

									 <div class="col-md-3">
									  	<div class="card-block boxes">
									  	<h4>Total Customers <a href=""><i class="fa fa-share-square-o float-right"></i></a></h4>
									  	<h1>100</h1>
									  	<div class="float-right">
									  	<img src="<?php echo base_url(); ?>assets/images/cab.png">
									  	</div>
									  	<div class="clearfix"></div>

										</div>
									</div>

									 <div class="col-md-3">
									  	<div class="card-block boxes">
									  	<h4>Total Franchise <a href=""><i class="fa fa-share-square-o float-right"></i></a></h4>
									  	<h1>50</h1>
									  	<div class="float-right">
									  	<img src="<?php echo base_url(); ?>assets/images/cab.png">
									  	</div>
									  	<div class="clearfix"></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-8" style="padding-right: 0">
										 <div id="googleMap" style="height: 384px;">
								    </div>
									</div>
									<div class="col-md-4" style="padding-left: 0">
										<div class="driver-box demo">
											<h4 style="margin-top: 6px;">Available Drivers  <a href=""><i class="fa fa-share-square-o float-right"></i></a></h4>
											<hr>
											
						<div class="demo">
						<div class="control-group">
							<select id="select-beast" class="demo-default" placeholder="Select  Zone...">
								<option value="">Select Zone...</option>
								<?php if($data) { foreach($data as $row) { ?>
								<option value="<?php echo $row->ZoneId; ?>" <?php if($row->ZoneId==1) { echo 'selected'; } ?>> <?php echo $row->Name; ?></option>
								 <?php }} ?>
							</select>
						</div>

						<table class="driver-table table" id="near-drivers">

						</table>
						
					</div>
											
										</div>
									</div>
								</div>
								   


								    <div id="over_map">
								    	

								<div class="row">
									<div class="offset-md-8 col-md-4">

									</div>
								</div>
							</div>
							</div>
						</div>
					</div>
					
					<!-- social statusric start -->
					
				</div>
			</div>

		</div>
	</div>
</div>
</div>
<script>
// Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: -25.344, lng: 131.036};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('googleMap'), {zoom: 4, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}
    </script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeyrtvCXbTQEEbDtuywP7IsbDrmg1hXi0&callback=initMap"></script>

