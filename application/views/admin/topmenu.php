<nav class="navbar navbar-expand-lg navbar-light bg-primary inner-page-nav-bar">
   
   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-transport" aria-controls="navbar-transport" aria-expanded="false" aria-label="Toggle navigation">
   <span class="navbar-toggler-icon"></span>
   </button>
   <div class="collapse navbar-collapse" id="navbar-transport">
      <ul class="navbar-nav">
         <li class="nav-item"><a class="nav-link" href=""><i class="fa fa-home"></i>Summary</a></li>
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="drivers-menu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> Drivers</a>
            <div class="dropdown-menu" aria-labelledby="drivers-menu">
               <a class="dropdown-item" href="../add-provider.html">
                <i class="fa fa-angle-double-right"></i> Add Driver</a>
               <a class="dropdown-item" href="">
                <i class="fa fa-angle-double-right"></i> Approved Drivers</a>
               <a class="dropdown-item" href="">
                <i  class="fa fa-angle-double-right"></i> Un-Approved Drivers</a>
               <a class="dropdown-item"   href="">
                <i class="fa fa-angle-double-right"></i> Blocked Drivers</a>
               <a class="dropdown-item"  href="">
               <i class="fa fa-angle-double-right"></i> Reject Drivers</a>
            </div>
         </li>
                  <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="rides-menu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-motorcycle"></i>
                        Rides</a>
            <div class="dropdown-menu" aria-labelledby="rides-menu">
               <a class="dropdown-item" href="">
                <i class="fa fa-angle-double-right"></i> All Rides</a>
               <a class="dropdown-item" href="">
                <i class="fa fa-angle-double-right"></i> Scheduled Rides</a>
               <a class="dropdown-item" href="">
                <i class="fa fa-angle-double-right"></i> Pending Rides</a>
               <a class="dropdown-item" href="">
                <i class="fa fa-angle-double-right"></i> Cancelled Rides</a>
            </div>
         </li>
         <li class="nav-item"><a class="nav-link" href="">
            <i class="fa fa-edit"></i> Manual Ride Booking</a></li>
         <li class="nav-item"><a class="nav-link"  href="">
            <i class="fa fa-star"></i> Reviews</a></li>
         <li class="nav-item">
            <a class="nav-link" href="">
            <i class="fa fa-money"></i> Payment Reports
            </a>
         </li>
                  <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="settings-menu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> Settings</a>
            <div class="dropdown-menu" aria-labelledby="settings-menu">
               <a class="dropdown-item" href=""><i
                  class="fa fa-angle-double-right"></i> God`s Views</a>
               <a class="dropdown-item" href=""><i
                  class="fa fa-angle-double-right"></i> Vehicle Type</a>
               <a class="dropdown-item" href=""><i
                  class="fa fa-angle-double-right"></i> Promocode</a>
               <a class="dropdown-item word-wrap" href=""><i
                  class="fa fa-angle-double-right"></i> Required Documents</a>
               <a class="dropdown-item" href=""><i
                  class="fa fa-angle-double-right"></i> Service Setting</a>
               
            </div>
         </li>

      </ul>
   </div>
</nav>