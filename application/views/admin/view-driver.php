<style type="text/css">
	.d-inline {
		display: inline-block;
	}
	.label {
		font-size: 15px;
		font-weight: bold;
	}
	.vehicle-title {
            		margin-top: 20px;
            		margin-bottom: 20px;
            	}
</style>
<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">Driver Details</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
				<a  id="" href="<?php echo base_url(); ?>admin/dashboard" class="btn grey btn-outline-secondary float-right mr-1">Back</a>
			</div>
		</div>
		<div class="content-body">
            <?php if($this->session->flashdata('successMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('successMessage'); ?>
                    </div>
            <?php } ?>
			<?php if($this->session->flashdata('errorMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('errorMessage'); ?>
                    </div>
            <?php } ?>

			<section>
				<div id="render-content">

					<div class="row">
						<div class="col-12">
							<div class="card">
								
								<div class="card-content">
									<div class="card-body">
											<div class="row">
												<div class="col-md-6">
													<p class="col-sm-5 label" style="display: inline-block;">First Name : </p>
													<p class="col-sm-5" style="display: inline-block;"><?php echo $data[0]->FirstName; ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">Email : </p>
													<p class="col-sm-5" style="display: inline-block;"><?php echo $data[0]->Email; ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">DOB : </p>
													<p class="col-sm-5" style="display: inline-block;"><?php echo date('d-m-Y',strtotime($data[0]->DOB)); ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">Display Picture : </p>
													<p class="col-sm-5 d-inline"><img src="<?php echo base_url(); ?><?php echo $data[0]->DisplayPicture; ?>" width="100" height="100" class="rounded-circle"></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">Country : </p>
													<p class="col-sm-5 d-inline"><?php echo $data[0]->CountryName; ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">City : </p>
													<p class="col-sm-5 d-inline"><?php echo $data[0]->City; ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">Status : </p>
													<p class="col-sm-5 d-inline label"><?php if($data[0]->Status=='A') { echo 'Active'; } elseif($data[0]->Status=='D') { echo 'Deactive'; } elseif($data[0]->Status=='P') { echo 'Deactive'; } ?></p>
													<div class="clearfix"></div>

												</div>
												<div class="col-md-6">
													<p class="col-sm-5 label" style="display: inline-block;">Last Name : </p>
													<p class="col-sm-5" style="display: inline-block;"><?php echo $data[0]->LastName; ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">Phone : </p>
													<p class="col-sm-5" style="display: inline-block;"><?php echo $data[0]->Phone; ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">Place Of Birth : </p>
													<p class="col-sm-5 label" style="display: inline-block;"><?php echo $data[0]->PlaceOfBirth; ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">Wallet Balance : </p>
													<p class="col-sm-5" style="display: inline-block;"><?php echo $data[0]->WalletBalance; ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">State : </p>
													<p class="col-sm-5" style="display: inline-block;"><?php echo $data[0]->State; ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">Pincode : </p>
													<p class="col-sm-5" style="display: inline-block;"><?php echo $data[0]->Pincode; ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">Overall Rating : </p>
													<p class="col-sm-5" style="display: inline-block;"><?php echo $data[0]->Rating; ?></p>
													<div class="clearfix"></div>


													<p class="col-sm-5 label" style="display: inline-block;">Registered On : </p>
													<p class="col-sm-5" style="display: inline-block;"><?php echo date('d-m-Y',strtotime($data[0]->CreatedOn)); ?></p>
													<div class="clearfix"></div>

												</div>
											</div>
											<div class="row">

												<div class="col-md-6">
													<h3 class="vehicle-title">Vehicle Details</h3>
													<p class="col-sm-5 label" style="display: inline-block;">Registration Id: </p>
													<p class="col-sm-5" style="display: inline-block;"><?php echo $data[0]->RegistrationId; ?></p>
													<div class="clearfix"></div>

													

													<p class="col-sm-5 label" style="display: inline-block;">Vehicle Permit : </p>
													<p class="col-sm-5 d-inline"><img src="<?php echo base_url(); ?><?php echo $data[0]->VehiclePermit; ?>" width="100" height="100" class="rounded-circle"></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">Vehicle Number : </p>
													<p class="col-sm-5 d-inline"><?php echo $data[0]->VehicleNumber; ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">Vehicle Brand : </p>
													<p class="col-sm-5 d-inline"><?php echo $data[0]->VehicleBrand; ?></p>
													<div class="clearfix"></div>



												</div>
												<div class="col-md-6">
													<h3 class="vehicle-title" style="color: #fff">Vehicle Details</h3>
												<p class="col-sm-5 label" style="display: inline-block;">Vehicle Color : </p>
													<p class="col-sm-5 d-inline"><?php echo $data[0]->VehicleColor; ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">Vehicle Type : </p>
													<p class="col-sm-5 d-inline"><?php echo $data[0]->VehicleType; ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">Vehicle Category : </p>
													<p class="col-sm-5 d-inline"><?php echo $data[0]->CategoryName; ?></p>
													<div class="clearfix"></div>	

													<p class="col-sm-5 label" style="display: inline-block;">Available : </p>
													<p class="col-sm-5 d-inline label"><?php if($data[0]->Available=='Y') { echo 'Yes'; } elseif($data[0]->Available=='N') { echo 'No'; } ?></p>
													<div class="clearfix"></div>

													<p class="col-sm-5 label" style="display: inline-block;">Registered On : </p>
													<p class="col-sm-5" style="display: inline-block;"><?php echo date('d-m-Y',strtotime($data[0]->VehicleCreatedOn)); ?></p>
													<div class="clearfix"></div>
												</div>
											</div>

											<hr>
											<h3 class="vehicle-title">Ride Details</h3>
										<?php  if($rides) { ?>
										<div class="table-responsive">
											<table id="new-cons"
												class="table table-striped table-bordered responsive nowrap"
												style="width: 100%!important;">
												<thead>
													<tr>
														<th>No</th>
														<th>Ordered By</th>
                                                    
                                                        <th>Vehicle Type</th>
                                                        <th>Estimated Fare</th>
                                                         <th>Payment Mode</th>
                                                         <th>Ride Status</th>
                                                         <th>Actions</th>
														
													</tr>
												</thead>
												<tbody>
													<?php $i=1; foreach($rides as $row) { ?>
													<tr id="hide_66">
														<td><?php echo $i; ?></td>
														<td class="td-icon">
															<?php echo $row->CustomerName; ?>
														</td>
                                                       
                                                         <td class="td-icon">
															<?php echo $row->RideType; ?>
														</td>
                                                        <td class="td-icon">
															<?php echo $row->EstimatedFare; ?>
														</td>
                                                        <td class="td-icon">
															<?php  if($row->PaymentMode=='C') { echo 'Cash On Delivery'; } elseif($row->PaymentMode=='W'){ echo 'Wallet';} ?>
														</td>
                                                        <td class="td-icon">
															<?php if($row->RideStatus=='A') {
                                                                echo 'Accepted';
                                                            }elseif($row->RideStatus=='P'){
                                                                echo 'Pending';
                                                            }elseif($row->RideStatus=='C') {
                                                                echo 'Completed';
                                                            } elseif($row->RideStatus=='D') {
                                                                echo 'Declined';
                                                            } ?>
														</td>
														<td>
															<a href="<?php echo base_url(); ?>admin/view-ride-details/<?php echo $row->RideId; ?>" class="delete btn btn-outline-primary"
																data-toggle="tooltip" data-placement="top" title="VIew">
																<i class="ft-eye"></i>
                                                            </a>
														</td>

														
													</tr>
													<?php $i++; } ?>

													</tr>
												</tbody>
											</table>
										</div>
										<?php } else { echo 'No Records Found..'; } ?>
										<div class="row">
											<div class="col-md-12">
												<h3 class="vehicle-title">Transaction Details</h3>
										<?php  if($topups) { ?>
										<div class="table-responsive">
											<table id="new-cons"
												class="table table-striped table-bordered responsive nowrap"
												style="width: 100%!important;">
												<thead>
													<tr>
														<th>No</th>
														<th>Name</th>
														<th>Payment Method</th>
                                                        <th>Amount</th>
                                                        <th>TransactionId</th>
                                                        <th>Transaction Date</th>
                                                        <th>Reference Topup	:</th>
                                                        <th>Transaction Status :</th>
                                                        <th>Remarks	:</th>
                                                   
														<th data-priority="3">Status</th>
													</tr>
												</thead>
												<tbody>
													<?php $i=1; foreach($topups as $allData) { ?>
													<tr id="hide_66">
														<td><?php echo $i; ?></td>
														<td class="td-icon">
															<?php echo $allData->FirstName; ?>
														</td>

														<td class="td-icon">
                                                            <?php if($allData->PaymentMethod=='O') { echo 'Online'; }
                                                            elseif($allData->PaymentMethod=='F')  { echo 'Franchise'; } 
                                                            elseif($allData->PaymentMethod=='B')  { echo 'Bank'; } 
                                                            ?>
														</td>

                                                        <td><?php echo $allData->Amount; ?></td>
                                                        <td><?php echo $allData->TransactionId; ?></td>
                                                        <td><?php echo date('d-m-Y',strtotime($allData->CreatedOn)); ?></td>
                                                        <td><?php echo $allData->ReferenceTopup; ?></td>
                                                        <td><?php if($allData->TransactionStatus=='A') { echo 'Active'; } elseif($allData->TransactionStatus=='P') { echo 'Pending'; } ?></td>
                                                        <td><?php echo $allData->Remarks; ?></td>
                                                         
                                                       

														<td class="action">
                                                        <?php if($allData->Status=='A') { echo 'Active'; } else { echo 'Pending'; } ?>
														</td>
													</tr>
													<?php $i++; } ?>

													</tr>
												</tbody>
											</table>
										</div>
										<?php } else { echo 'No Records Found..'; } ?>
									</div>
										</div>
									</div>
									<div class="row">

					<div class="col-xl-12 col-md-12">
						<div class="card dashboard-box">
							<div class="card-block dashboard-title">
								<h3>Summary</h3>
							</div>
						</div>
					</div>
					<!-- product profit start -->
					<div class="col-xl-4 col-lg-6 col-12" style="margin-left:10px; max-width:31%">
						<div class="card">
							<div class="card-content">
								<div class="media align-items-stretch">
									<div class="p-2 text-center bg-primary bg-darken-2">
										<i class="fa fa-money dashboard-icon white"></i>
									</div>
									<div class="p-2 bg-gradient-x-primary white media-body">
										<h6>Total Rides</h6>
										<h5 class="text-bold-400 mb-0">
										<?php echo $totalrides; ?></h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-12">
						<div class="card">
							<div class="card-content">
								<div class="media align-items-stretch">
									<div class="p-2 text-center bg-danger bg-darken-2">
										<i class="icon-arrow-up font-large-2 white"></i>
									</div>
									<div class="p-2 bg-gradient-x-danger white media-body">
										<h5>Total Accpeted Rides</h5>
										<h5 class="text-bold-400 mb-0">
											<?php echo $accepted; ?></h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-6 col-12">
						<div class="card">
							<div class="card-content">
								<div class="media align-items-stretch">
									<div class="p-2 text-center bg-danger bg-darken-2">
										<i class="icon-arrow-down font-large-2 white"></i>
									</div>
									<div class="p-2 bg-gradient-x-danger white media-body">
										<h5>Total cancelled Rides</h5>
										<h5 class="text-bold-400 mb-0">
											<?php echo $cancelled; ?></h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
								</div>
							</div>
						</div>
					</div>

				</div>

			</section>
		</div>
	</div>
</div>