<!DOCTYPE html>
<html class="loaded" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title> App </title>
    <link rel="icon" href="#" type="image/x-icon">
    <link   href="<?php echo base_url(); ?>assets/css/font.css"  rel="stylesheet">
  

    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/vendors.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/app.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/extensions/toastr.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sweetalert.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->        
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/style-changes.css">
    <!-- END Custom CSS-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/widget/widget.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>app-assets/vendors/css/extensions/unslider.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/weather-icons/climacons.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/weather-icons/climacons.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/meteocons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/charts/morris.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/core/colors/palette-gradient.css">
    <?php if($this->uri->segment(2)!='view-user') { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/tables/datatable/datatables.min.css">
    <?php } ?>
    <!-- <link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>app-assets/vendors/css/tables/extensions/responsive.dataTable.min.css"> -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

    <style>
        .services .card-content {
            min-height: 115px;
        }

        .services .card-content span {
            font-size: 12px;
        }

        .dashboard-box {
            background-color: rgba(255, 255, 255, 0.9);
            box-shadow: 0 0 5px 0 rgba(43, 43, 43, 0.1), 0 2px 6px -7px rgba(43, 43, 43, 0.1) !important;
        }

        .dashboard-title {
            padding: 1rem !important;
        }

        .dashboard-title h5 {
            font-size: 16px;
        }
        table.hours-table th, table.hours-table td {
	padding: 7px;
	border-width: 1px;
	border-style: solid;
	border-color: #ccc #ccc;
    }
    </style>
</head>

<body class="vertical-layout vertical-menu 2-columns fixed-navbar menu-expanded pace-done" data-open="click"
    data-menu="vertical-menu" data-col="2-columns">
    <div class="pace  pace-inactive">
        <div class="pace-progress" data-progress-text="100%" data-progress="99"
            style="transform: translate3d(100%, 0px, 0px);">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div>
    </div>
    <div id="render-css-link"></div>
    <div id="render-css"></div>
