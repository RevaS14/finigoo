<style>
	/*datatable style*/
	table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before,
	table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
		background: #55d090;
	}

	.page-item.active .page-link {
		background: #55d090;
		border-color: #55d090;
	}

	.page-link {
		color: #55d090;
	}

	/*datatable td link*/
	.document a,
	.ride a,
	.ratings a {
		color: #55d090;
		font-weight: bold;
		font-size: 14px;
	}

	.document i,
	.ride i,
	.ratings i {
		font-size: 18px;
	}

	.ratings i {
		font-size: 16px;
	}

	.icon-list-demo i {
		height: auto;
		line-height: 10px;
		border: none;
		margin-right: 5px;
		color: #55d090;
	}

	/* Status style*/
	.toggle {
		display: inline-block;
	}

	.toggle input[type="checkbox"]:checked+.button-indecator:before {
		color: #55d090;
	}

	.toggle input[type="checkbox"]+.button-indecator:before {
		color: #55d090;
	}

	/* Vehicle type styles for the modal */
	.md-perspective,
	.md-perspective body {
		height: 100%;
		overflow: hidden;
	}

	.md-perspective body {
		background: #222;
		-webkit-perspective: 600px;
		-moz-perspective: 600px;
		perspective: 600px;
	}

	.md-modal {
		position: fixed;
		top: 50%;
		left: 50%;
		/*width: 50%;*/
		width: 30%;
		max-width: 630px;
		min-width: 300px;
		height: auto;
		z-index: 2000;
		visibility: hidden;
		-webkit-backface-visibility: hidden;
		-moz-backface-visibility: hidden;
		backface-visibility: hidden;
		-webkit-transform: translateX(-50%) translateY(-50%);
		-moz-transform: translateX(-50%) translateY(-50%);
		-ms-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}

	.md-show {
		visibility: visible;
	}

	.md-overlay {
		position: fixed;
		width: 100%;
		height: 100%;
		visibility: hidden;
		top: 0;
		left: 0;
		z-index: 1000;
		opacity: 0;
		background: rgba(55, 58, 60, 0.65);
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
	}

	.md-show~.md-overlay {
		opacity: 1;
		visibility: visible;
	}

	/* Content styles */
	.md-content {
		color: #666666;
		background: #fff;
		position: relative;
		border-radius: 3px;
		margin: 0 auto;
	}

	.md-content h3 {
		color: #fff;
		margin: 0;
		/*padding: 0.4em;*/
		padding: 0.6em 0.4em 0.6em 1em;
		text-align: left;
		font-weight: 400;
		font-size: 1.5em;
		opacity: 0.8;
		border-radius: 3px 3px 0 0;
	}

	.md-content>div {
		padding: 15px 25px 30px 25px;
		margin: 0;
		font-size: 1em;
		/*font-weight: 300;*/
		/*font-size: 1.15em;*/
	}

	.md-content>div>div {
		width: 40%;
		margin: 0 auto;
		padding: 10px 0;
		justify-content: space-around;
		display: flex;
	}

	.md-content>div>div>img {
		border-radius: 50%;
		padding: 4px;
		border: 2px solid #2ed8b6;
	}

	.md-content>div ul {
		margin: 0;
		padding: 0 0 30px 0;
	}

	.md-content>div ul li {
		padding: 5px 0;
	}

	/* Individual modal styles with animations/transitions */
	.md-effect-1 .md-content {
		-webkit-transform: scale(0.7);
		-moz-transform: scale(0.7);
		-ms-transform: scale(0.7);
		transform: scale(0.7);
		opacity: 0;
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
	}

	.md-show.md-effect-1 .md-content {
		-webkit-transform: scale(1);
		-moz-transform: scale(1);
		-ms-transform: scale(1);
		transform: scale(1);
		opacity: 1;
	}

	.md-trigger:hover {
		color: #64b0f2;
		cursor: pointer;
	}

	.md-trigger img:hover {
		opacity: 0.7;
		cursor: pointer;
	}
</style>

<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">All Transactions</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
			</div>
		</div>
		<div class="content-body">
            <?php if($this->session->flashdata('successMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('successMessage'); ?>
                    </div>
            <?php } ?>
			<?php if($this->session->flashdata('errorMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('errorMessage'); ?>
                    </div>
            <?php } ?>

			<section>
				<div id="render-content">

					<div class="row">
						<div class="col-12">
							<div class="card">
								<div>
								<?php if($user->UserType=='U') { ?> 
								<a  id="" style="margin-top: 10px;" href="<?php echo base_url(); ?>admin/<?php if($user->UserType=='U') { echo 'users'; } ?>"
											class="btn grey btn-outline-secondary float-right mr-1">Back</a>
								<?php } ?>
							</div>
								
								<div class="card-content">
									<div class="card-body">
										<?php  if($transaction) { ?>
										<div class="table-responsive">
											<table id="new-cons"
												class="table table-striped table-bordered responsive nowrap"
												style="width: 100%!important;">
												<thead>
													<tr>
														<th>No</th>
														<th>Name</th>
														<th>Payment Method</th>
                                                        <th>Amount</th>
                                                        <th>TransactionId</th>
                                                        <th>Transaction Date</th>
                                                        <th>Reference Topup	:</th>
                                                        <th>Transaction Status :</th>
                                                        <th>Remarks	:</th>
                                                   
														<th data-priority="3">Status</th>
													</tr>
												</thead>
												<tbody>
													<?php $i=1; foreach($transaction as $allData) { ?>
													<tr id="hide_66">
														<td><?php echo $i; ?></td>
														<td class="td-icon">
															<?php echo $allData->FirstName; ?>
														</td>

														<td class="td-icon">
                                                            <?php if($allData->PaymentMethod=='O') { echo 'Online'; }
                                                            elseif($allData->PaymentMethod=='F')  { echo 'Franchise'; } 
                                                            elseif($allData->PaymentMethod=='B')  { echo 'Bank'; } 
                                                            ?>
														</td>

                                                        <td><?php echo $allData->Amount; ?></td>
                                                        <td><?php echo $allData->TransactionId; ?></td>
                                                        <td><?php echo date('d-m-Y',strtotime($allData->CreatedOn)); ?></td>
                                                        <td><?php echo $allData->ReferenceTopup; ?></td>
                                                        <td><?php if($allData->TransactionStatus=='A') { echo 'Active'; } elseif($allData->TransactionStatus=='P') { echo 'Pending'; } ?></td>
                                                        <td><?php echo $allData->Remarks; ?></td>
                                                         
                                                       

														<td class="action">
                                                        <?php if($allData->Status=='A') { echo 'Active'; } else { echo 'Pending'; } ?>
														</td>
													</tr>
													<?php $i++; } ?>

													</tr>
												</tbody>
											</table>
										</div>
										<?php } else { echo 'No Records Found..'; } ?>
									</div>
								</div>
							</div>
						</div>
					</div>


					<div class="md-modal md-effect-1" id="modal-1">
						<div class="md-content">
							<h3 class="bg-c-green" id="driver_name_type">Nixon</h3>
							<div>
								<div>
									<img id="vehicle_icon" src="">
								</div>
								<ul>
									<li><strong>Vehicle Type : </strong> <span id="vehicle_name"></span></li>
									<li><strong>Model Name : </strong> <span id="model_name"></span></li>
									<li><strong>License Plate : </strong> <span id="license_name"></span></li>
									<li><strong>Vehicle Year : </strong> <span id="vehicle_year"></span></li>
									<li><strong>Vehicle Color : </strong> <span id="vehicle_color"></span></li>
								</ul>

								<a href="" id="driver_vehicle_details">
									<button type="button" class="btn btn-success waves-effect">Edit</button>
								</a>
								<button type="button" class="btn btn-success waves-effect md-close">Close</button>
							</div>

						</div>
					</div>


					<div class="md-modal md-effect-1" id="modal-2">
						<div class="md-content">
							<h3 class="bg-c-green" id="driver_name">Nixon</h3>
							<div>
								<ul>
									<li><strong>Total Request : </strong> <span id="total_request"></span></li>
									<li><strong>Total Completed : </strong> <span id="total_completed"></span></li>
									<li><strong>Total Rejected : </strong> <span id="total_rejected"></span></li>
								</ul>

								<button type="button" class="btn btn-success waves-effect md-close">Close</button>
							</div>

						</div>
					</div>
					<div class="md-overlay"></div>
				</div>

			</section>
		</div>
	</div>
</div>