<div class="app-content content">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">Restraurant Details</h3>
			</div>
			<div class="content-header-right col-md-6 col-12"></div>
		</div>
		<div class="content-body">
			<section>
				<div id="render-content">
					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h5>Restraurant Details as Below
                           <a href="<?php echo base_url(); ?>admin/alltransactions/<?php echo $userDetails[0]->UserId;?>"
                                 class="btn grey btn-outline-secondary float-right">All Transactions</a>
                                 <a href="<?php echo base_url(); ?>admin/topup/driver/<?php echo $userDetails[0]->UserId;?>"
											class="btn grey btn-outline-secondary float-right mr-1">Topup</a>
                           </h5>
								</div>
								<div class="card-content">
									<div class="card-body">
                    <?php if(isset($_GET['view'])) {
                      ?>
					<form id="main" method="post"
                        action="#"
                        enctype="multipart/form-data">
                        <input type="hidden" name="JobCategory" value="<?php echo $userDetails[0]->JobCategories_JobCategoryId;?>">
						<input type="hidden" name="redirectView" value="gocab">
						<input type="hidden" name="UserId" value="<?php echo $userDetails[0]->UserId;?>">
						<input type="hidden" name="VehicleID" value="<?php echo $userDetails[0]->ProviderId;?>">
                        <input type="hidden" name="_token" value="">
                        <div class="row">
                           <span id="service_category_append"></span>
                           <div class="form-group col-sm-12">
                              <div class="card">
                                 <!-- <div class="card-header">
                                    <h5>Add Driver of Bike Ride</h5>
                                    </div> -->
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label">First Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Firstname"
                                                required id="Firstname" placeholder="First Name"value="<?php echo $userDetails[0]->FirstName;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Last Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Lastname"
                                                required id="Lastname" placeholder="Last Name" value="<?php echo $userDetails[0]->LastName;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Email:<sup
                                                class="error">*</sup></label>
                                             <input type="email" class="form-control" name="Email"
                                                required id="email" placeholder="Unique Email" value="<?php echo $userDetails[0]->Email;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class=" col-form-label">Mobile Phone:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control"
                                                name="Phone" required id="Phone"
                                                placeholder="Unique Contact Number" value="<?php echo $userDetails[0]->Phone;?>" readonly>
                                             <span class="error"></span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Place of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PlaceOfBirth" required
                                                id="POB" placeholder="Place of Birth" value="<?php echo $userDetails[0]->PlaceOfBirth;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Date of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="Date" class="form-control" name="DOB" required
                                                id="POB" placeholder="Date of Birth" value="<?php echo $userDetails[0]->DOB;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Identity number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="IdentityNo"
                                                required id="In" placeholder="Identity number" value="<?php echo $userDetails[0]->IdentityNo;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">PinCode<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PinCode"
                                                required id="PC" placeholder="Pin Code" value="<?php echo $userDetails[0]->Pincode;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Country<sup
                                                class="error">*</sup></label>
                                             <select class="form-control" id="Country" name="Country" disabled="true">
												 <?php if($userDetails[0]->Establishment_EstablishmentId == 1) { ?>
												  <option value="1" selected>India</option>
												<?php } ?>
                                             </select>
                                             <span class="error"></span>
                                          </div>
                                         
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo
                                             Profile:</label>
                                             <div id="profile-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->DisplayPicture;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="profile-upload" id="profile-label"><a href="<?php echo base_url().'/'. $userDetails[0]->DisplayPicture;?>" target="new">View
                                                Image</a></label>
                                                <input type="file" id="profile-upload"
												   name="DisplayPicture" accept=".jpg,.jpeg,.png" disabled />
												   <input type="hidden" id="profile-upload"
                                                   name="ProfileAvatarOld" value="<?php echo $userDetails[0]->DisplayPicture;?>" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Identity
                                             Document Upload:</label>
                                             <div id="identity-image-preview"  style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->IdentityDocument;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="identity-upload" id="identity-label"><a href="<?php echo base_url().'/'. $userDetails[0]->IdentityDocument;?>" target="new">View
                                                Image</a></label>
                                                <input type="file" id="identity-upload"
												   name="IdentityDocument" accept=".jpg,.jpeg,.png" disabled/>
												   <input type="hidden" id="identity-upload"
                                                   name="IdentityDocumentOld" value="<?php echo $userDetails[0]->IdentityDocument;?>" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:	
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
						   </div>
                           <div class="form-group col-sm-12">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Restraurant Information
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          

                                       <div class="col-sm-4">
                                             <label class="col-form-label">Restraurant Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="RestraurantName" required
                                                id="RestraurantName" placeholder="Restraurant Name" value="<?php echo $userDetails[0]->ProviderName;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Restraurant Address:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="RestaurantAddress" required
                                                id="RestaurantAddress" placeholder="Restaurant Address" value="<?php echo $userDetails[0]->ProviderAddress;?>" readonly>
                                             <span class="error"></span>
                                          </div>

                                          <div class="col-sm-4">
                                             <label class="col-form-label">Restaurant Phone Number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="RestaurantPhoneNumber" required
                                                id="RestaurantPhoneNumber" placeholder="Restaurant Phone Number" value="<?php echo $userDetails[0]->ProviderPhone;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Opening Time:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="OpeningTime" required
                                                id="OpeningTime" placeholder="Opening Time" value="<?php echo $userDetails[0]->OpeningTime;?>" readonly>
                                             <span class="error"></span>
                                          </div>

                                          <div class="col-sm-4">
                                             <label class="col-form-label">Closing Time:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="ClosingTime" required
                                                id="ClosingTime" placeholder="Closing Time" value="<?php echo $userDetails[0]->ClosingTime;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Latitude:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Latitude" required
                                                id="Latitude" placeholder="Latitude" value="<?php echo $userDetails[0]->Latitude;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Longitude:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Longitude" required
                                                id="Longitude" placeholder="Longitude" value="<?php echo $userDetails[0]->Longitude;?>" readonly>
                                             <span class="error"></span>
                                          </div>

                                          <div class="col-sm-4">
                                             <label class="col-form-label">Description:<sup
                                                class="error">*</sup></label>
                                                <textarea class="form-control" name="Description" rows="11" cols="500" readonly><?php echo $userDetails[0]->Description;?></textarea>
                                             <span class="error"></span>
                                          </div>

                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Restaurant Photos:</label>
                                             <div id="restaurant-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->ProviderImage;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="restaurant-upload" id="restaurant-label"><a href="<?php echo base_url().'/'. $userDetails[0]->ProviderImage;?>" target="new">View
                                                Image</a></label>
                                                <input type="file" id="restaurant-upload"
                                                   name="RestaurantPicture" accept=".jpg,.jpeg,.png" disabled/>
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>


                                          

                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                          <!-- <div class="form-group col-sm-4">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Terms and Services
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <ol>
                                             <li>Driver must provide their own car</li>
                                             <li>Driver must have a personal Android device</li>
                                             <li>Driver must have Driver license which is still active at
                                                least 6 months ahead
                                             </li>
                                          </ol>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div> -->
                        </div>
					 </form>
					 
												 <?php } else if(isset($_GET['update'])) {
                                        
                                        ?>
													<form id="main" method="post"
                        action="<?php echo site_url();?>admin/listrestaurants/update"
                        enctype="multipart/form-data">
                        <input type="hidden" name="JobCategory" value="<?php echo $userDetails[0]->JobCategories_JobCategoryId;?>">
						<input type="hidden" name="redirectView" value="<?php echo $userDetails[0]->JobCategories_JobCategoryId;?>">
						<input type="hidden" name="UserId" value="<?php echo $userDetails[0]->UserId;?>">
						<input type="hidden" name="ProviderId" value="<?php echo $userDetails[0]->ProviderId;?>">
                        <input type="hidden" name="_token" value="">
                        <div class="row">
                           <span id="service_category_append"></span>
                           <div class="form-group col-sm-12">
                              <div class="card">
                                 <!-- <div class="card-header">
                                    <h5>Add Driver of Bike Ride</h5>
                                    </div> -->
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label">First Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Firstname"
                                                required id="Firstname" placeholder="First Name"value="<?php echo $userDetails[0]->FirstName;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Last Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Lastname"
                                                required id="Lastname" placeholder="Last Name" value="<?php echo $userDetails[0]->LastName;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Email:<sup
                                                class="error">*</sup></label>
                                             <input type="email" class="form-control" name="Email"
                                                required id="email" placeholder="Unique Email" value="<?php echo $userDetails[0]->Email;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class=" col-form-label">Mobile Phone:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control"
                                                name="Phone" required id="Phone"
                                                placeholder="Unique Contact Number" value="<?php echo $userDetails[0]->Phone;?>">
                                             <span class="error"></span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Place of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PlaceOfBirth" required
                                                id="POB" placeholder="Place of Birth" value="<?php echo $userDetails[0]->PlaceOfBirth;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Date of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="Date" class="form-control" name="DOB" required
                                                id="POB" placeholder="Date of Birth" value="<?php echo $userDetails[0]->DOB;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Identity number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="IdentityNo"
                                                required id="In" placeholder="Identity number" value="<?php echo $userDetails[0]->IdentityNo;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">PinCode<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PinCode"
                                                required id="PC" placeholder="Pin Code" value="<?php echo $userDetails[0]->Pincode;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Country<sup
                                                class="error">*</sup></label>
                                             <select class="form-control" id="Country" name="Country">
												 <?php if($userDetails[0]->Establishment_EstablishmentId == 1) { ?>
												  <option value="1" selected>India</option>
												<?php } ?>
                                             </select>
                                             <span class="error"></span>
                                          </div>
                                         
                                          
                                       </div>
                                       <div class="row">
                                          
                                          
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo
                                             Profile:</label>
                                             <div id="profile-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->DisplayPicture;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="profile-upload" id="profile-label">Upload
                                                Image</label>
                                                <input type="file" id="profile-upload"
												   name="DisplayPicture" accept=".jpg,.jpeg,.png" />
												   <input type="hidden" id="profile-upload"
                                                   name="ProfileAvatarOld" value="<?php echo $userDetails[0]->DisplayPicture;?>" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Identity
                                             Document Upload:</label>
                                             <div id="identity-image-preview"  style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->IdentityDocument;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="identity-upload" id="identity-label">Upload
                                                Image</label>
                                                <input type="file" id="identity-upload"
												   name="IdentityDocument" accept=".jpg,.jpeg,.png" />
												   <input type="hidden" id="identity-upload"
                                                   name="IdentityDocumentOld" value="<?php echo $userDetails[0]->IdentityDocument;?>" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:	
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
						   </div>
                     <div class="form-group col-sm-12">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Restrarant Information
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Restaurant Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="RestaurantName"
                                                required id="RestaurantName" placeholder="Restaurant Name"
                                                value="<?php echo $userDetails[0]->ProviderName;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Restaurant Address:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="RestaurantAddress"
                                                required id="RestaurantAddress" placeholder="Restaurant Address"
                                                value="<?php echo $userDetails[0]->ProviderAddress;?>" required>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Restaurant Phone Number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="RestaurantPhoneNumber"
                                                required id="RestaurantPhoneNumber" placeholder="Restaurant Phone Number"
                                                value="<?php echo $userDetails[0]->ProviderPhone;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Restaurant Category:<sup
                                                class="error">*</sup></label>
                                                <select class="form-control" id="RestaurantCategory" name="RestaurantCategory">
                                                <option value="1" selected>Italian</option>
                                                <option value="1" selected>Pizza</option>
                                             </select>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Opening Time:<sup
                                                class="error">*</sup></label>
                                                <select class="form-control" id="OpeningTime" name="OpeningTime">
                                                <option value="01:00">01:00</option>
                                                <option value="01:30">01:30</option>
                                                <option value="02:00">02:00</option>
                                                <option value="02:30">02:30</option> 
                                                <option value="03:00">03:00</option>
                                                <option value="03:30">03:30</option>
                                                <option value="04:00">04:00</option>
                                                <option value="04:30">04:30</option>
                                                <option value="05:00">05:00</option>
                                                <option value="05:30">05:30</option>
                                                <option value="06:00">06:00</option>
                                                <option value="06:30">06:30</option>
                                                <option value="07:00">07:00</option>
                                                <option value="07:30">07:30</option>
                                                <option value="08:00">08:00</option>
                                                <option value="08:30">08:30</option>
                                                <option value="09:00">09:00</option>
                                                <option value="09:30">09:30</option>
                                                <option value="10:00">10:00</option>
                                                <option value="10:30">10:30</option>
                                                <option value="11:00">11:00</option>
                                                <option value="11:30">11:30</option>
                                                <option value="12:00">12:00</option>
                                                <option value="12:30">12:30</option>
                                                <option value="13:00">13:00</option>
                                                <option value="13:30">13:30</option>
                                                <option value="14:00">14:00</option>
                                                <option value="14:30">14:30</option>
                                                <option value="15:00">15:00</option>
                                                <option value="15:30">15:30</option>
                                                <option value="16:00">16:00</option>
                                                <option value="16:30">16:30</option>
                                                <option value="17:00">17:00</option>
                                                <option value="17:30">17:30</option>
                                                <option value="18:00">18:00</option>
                                                <option value="18:30">18:30</option>
                                                <option value="19:00">19:00</option>
                                                <option value="19:30">19:30</option>
                                                <option value="20:00">20:00</option>
                                                <option value="20:30">20:30</option>
                                                <option value="21:00">21:00</option>
                                                <option value="21:30">21:30</option>
                                                <option value="22:00">22:00</option>
                                                <option value="22:30">22:30</option>
                                                <option value="23:00">23:00</option>
                                                <option value="23:30">23:30</option>
                                                <option value="23:59">23:59</option>
                                             </select>
                                             <span class="error"></span>
                                          </div>

                                          <div class="col-sm-4">
                                             <label class="col-form-label">Closing Time:<sup
                                                class="error">*</sup></label>
                                                <select class="form-control" id="ClosingTime" name="ClosingTime">
                                                <option value="01:00">01:00</option>
                                                <option value="01:30">01:30</option>
                                                <option value="02:00">02:00</option>
                                                <option value="02:30">02:30</option> 
                                                <option value="03:00">03:00</option>
                                                <option value="03:30">03:30</option>
                                                <option value="04:00">04:00</option>
                                                <option value="04:30">04:30</option>
                                                <option value="05:00">05:00</option>
                                                <option value="05:30">05:30</option>
                                                <option value="06:00">06:00</option>
                                                <option value="06:30">06:30</option>
                                                <option value="07:00">07:00</option>
                                                <option value="07:30">07:30</option>
                                                <option value="08:00">08:00</option>
                                                <option value="08:30">08:30</option>
                                                <option value="09:00">09:00</option>
                                                <option value="09:30">09:30</option>
                                                <option value="10:00">10:00</option>
                                                <option value="10:30">10:30</option>
                                                <option value="11:00">11:00</option>
                                                <option value="11:30">11:30</option>
                                                <option value="12:00">12:00</option>
                                                <option value="12:30">12:30</option>
                                                <option value="13:00">13:00</option>
                                                <option value="13:30">13:30</option>
                                                <option value="14:00">14:00</option>
                                                <option value="14:30">14:30</option>
                                                <option value="15:00">15:00</option>
                                                <option value="15:30">15:30</option>
                                                <option value="16:00">16:00</option>
                                                <option value="16:30">16:30</option>
                                                <option value="17:00">17:00</option>
                                                <option value="17:30">17:30</option>
                                                <option value="18:00">18:00</option>
                                                <option value="18:30">18:30</option>
                                                <option value="19:00">19:00</option>
                                                <option value="19:30">19:30</option>
                                                <option value="20:00">20:00</option>
                                                <option value="20:30">20:30</option>
                                                <option value="21:00">21:00</option>
                                                <option value="21:30">21:30</option>
                                                <option value="22:00">22:00</option>
                                                <option value="22:30">22:30</option>
                                                <option value="23:00">23:00</option>
                                                <option value="23:30">23:30</option>
                                                <option value="23:59">23:59</option>
                                             </select>
                                             <span class="error"></span>
                                          </div>

                                          <div class="col-sm-4">
                                             <label class="col-form-label">Latitude:<sup
                                                class="error">*</sup></label>
                                                <input type="text" class="form-control" name="Latitude"
                                                required id="Latitude" placeholder="Latitude"
                                                value="<?php echo $userDetails[0]->Latitude;?>">
                                               
                                             <span class="error"></span>
                                          </div>

                                          <div class="col-sm-4">
                                             <label class="col-form-label">Longitude:<sup
                                                class="error">*</sup></label>
                                                <input type="text" class="form-control" name="Longitude"
                                                required id="Longitude" placeholder="Longitude"
                                                value="<?php echo $userDetails[0]->Longitude;?>">
                                             <span class="error"></span>
                                          </div>


                                          <div class="col-sm-6">
                                             <label class="col-form-label">Description:<sup
                                                class="error">*</sup></label>
                                                <textarea class="form-control" name="Description" rows="11" cols="500"><?php echo $userDetails[0]->Description;?></textarea>
                                             <span class="error"></span>
                                          </div>

                                          <div class="col-sm-6">
                                             <label class="col-form-label image image-label">Restaurant Photos:</label>
                                             <div id="restaurant-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->ProviderImage;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="restaurant-upload" id="restaurant-label">Upload
                                                Image</label>
                                                <input type="file" id="restaurant-upload"
                                                   name="RestaurantPicture" accept=".jpg,.jpeg,.png" />
                                                   <input type="hidden" id="identity-upload"
                                                   name="RestaurantPictureOld" value="<?php echo $userDetails[0]->ProviderImage;?>" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>

                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-sm-4">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Terms and Services
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <ol>
                                             <li>Driver must provide their own car</li>
                                             <li>Driver must have a personal Android device</li>
                                             <li>Driver must have Driver license which is still active at
                                                least 6 months ahead
                                             </li>
                                          </ol>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-sm-12">
                              <center>
                                 <button type="submit" class="btn btn-success m-b-0">Submit</button>
                              </center>
                           </div>
                        </div>
					 </form>
												 <?php } else if(isset($_GET['delete'])){?>
													<form id="main" method="post"
                        action="<?php echo site_url();?>admin/listdrivers/delete"
                        enctype="multipart/form-data">
                        <input type="hidden" name="JobCategory" value="1">
						<input type="hidden" name="redirectView" value="gocab">
						<input type="hidden" name="UserId" value="<?php echo $userDetails[0]->UserId;?>">
						<input type="hidden" name="VehicleID" value="<?php echo $userDetails[0]->VehicleID;?>">
						<input type="hidden" name="DriverId" value="<?php echo $userDetails[0]->DriverId;?>">
                        <input type="hidden" name="_token" value="">
                        <div class="row">
                           <span id="service_category_append"></span>
                           <div class="form-group col-sm-12">
						   <div class="form-group col-sm-12">
                              <center>
								 <button type="submit" class="btn btn-success m-b-0">Delete</button>
								 <a href="<?php echo base_url();?>admin/listdrivers/gocab" ?><button type="button" class="btn btn-success m-b-0">Cancel</button></a>
                              </center>
                           </div>
                              <div class="card">
                                 <!-- <div class="card-header">
                                    <h5>Add Driver of Bike Ride</h5>
                                    </div> -->
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label">First Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Firstname"
                                                required id="Firstname" placeholder="First Name"value="<?php echo $userDetails[0]->FirstName;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Last Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Lastname"
                                                required id="Lastname" placeholder="Last Name" value="<?php echo $userDetails[0]->LastName;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Email:<sup
                                                class="error">*</sup></label>
                                             <input type="email" class="form-control" name="Email"
                                                required id="email" placeholder="Unique Email" value="<?php echo $userDetails[0]->Email;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class=" col-form-label">Mobile Phone:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control"
                                                name="Phone" required id="Phone"
                                                placeholder="Unique Contact Number" value="<?php echo $userDetails[0]->Phone;?>">
                                             <span class="error"></span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Place of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PlaceOfBirth" required
                                                id="POB" placeholder="Place of Birth" value="<?php echo $userDetails[0]->PlaceOfBirth;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Date of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="Date" class="form-control" name="DOB" required
                                                id="POB" placeholder="Date of Birth" value="<?php echo $userDetails[0]->DOB;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Identity number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="IdentityNo"
                                                required id="In" placeholder="Identity number" value="<?php echo $userDetails[0]->IdentityNo;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">PinCode<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PinCode"
                                                required id="PC" placeholder="Pin Code" value="<?php echo $userDetails[0]->Pincode;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Country<sup
                                                class="error">*</sup></label>
                                             <select class="form-control" id="Country" name="Country">
												 <?php if($userDetails[0]->Establishment_EstablishmentId == 1) { ?>
												  <option value="1" selected>India</option>
												<?php } ?>
                                             </select>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Driver Name<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="DriverName" required
                                                id="Pc" placeholder="Driver Name" value="<?php echo $userDetails[0]->DriverName;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Driver Licence No<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="DriverLicenseNo" required
                                                id="Pc" placeholder="Driver Licence No" value="<?php echo $userDetails[0]->DrivingLicense;?>">
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo
                                             Vehicle Permit:</label>	
                                             <div id="vehicle-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->VehiclePermit;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;"">
                                                <label for="vehicle-upload" id="vehicle-label">Upload
                                                Image</label>
                                                <input type="file" id="vehicle-upload"
												   name="VehiclePermit" accept=".jpg,.jpeg,.png" value="<?php echo $userDetails[0]->VehiclePermit;?>"/>
												   <input type="hidden" id="vehicle-upload"
												   name="VehiclePermitOld" value="<?php echo $userDetails[0]->VehiclePermit;?>"/>
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo Driver
                                             license:</label>
                                             <div id="driver-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->DrivingLicenseDocument;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="driver-upload" id="driver-label">Upload
                                                Image</label>
                                                <input type="file" id="driver-upload"
												   name="DriverLicense" accept=".jpg,.jpeg,.png" />
												   <input type="hidden" id="driver-upload"
												   name="DriverLicenseOld" value="<?php echo $userDetails[0]->DrivingLicenseDocument;?>"/>
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo
                                             Profile:</label>
                                             <div id="profile-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->DisplayPicture;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="profile-upload" id="profile-label">Upload
                                                Image</label>
                                                <input type="file" id="profile-upload"
												   name="DisplayPicture" accept=".jpg,.jpeg,.png" />
												   <input type="hidden" id="profile-upload"
                                                   name="ProfileAvatarOld" value="<?php echo $userDetails[0]->DisplayPicture;?>" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Identity
                                             Document Upload:</label>
                                             <div id="identity-image-preview"  style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->IdentityDocument;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="identity-upload" id="identity-label">Upload
                                                Image</label>
                                                <input type="file" id="identity-upload"
												   name="IdentityDocument" accept=".jpg,.jpeg,.png" />
												   <input type="hidden" id="identity-upload"
                                                   name="IdentityDocumentOld" value="<?php echo $userDetails[0]->IdentityDocument;?>" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:	
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
						   </div>
                           <div class="form-group col-sm-8">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Vehicle Data
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Brand:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleBrand"
                                                required id="VehicleBrand" placeholder="Vehicle Brand"
                                                value="<?php echo $userDetails[0]->VehicleBrand;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Type:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleType"
                                                required id="VehicleType" placeholder="Vehicle Type"
                                                value="<?php echo $userDetails[0]->VehicleType;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleNumber"
                                                required id="VehicleNumber" placeholder="Vehicle Number"
                                                value="<?php echo $userDetails[0]->VehicleNumber;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Color:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleColor"
                                                required id="VehicleColor" placeholder="Vehicle Color"
                                                value="<?php echo $userDetails[0]->VehicleColor;?>">
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-sm-4">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Terms and Services
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <ol>
                                             <li>Driver must provide their own car</li>
                                             <li>Driver must have a personal Android device</li>
                                             <li>Driver must have Driver license which is still active at
                                                least 6 months ahead
                                             </li>
                                          </ol>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                          
                        </div>
					 </form>
												 <?php } ?>
									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
<style>
    #new-cons td, #new-cons th {display:table-cell !important;}
 </style>

