
                                                                        <?php if($peakHours) { ?> 
																		<table class="hours-table">
																			<thead>
																				<th>Start Time</th>
																				<th>End Time</th>
																				<th>Status</th>
																				<th></th>
																			</thead>
																			<tbody>
																			<?php foreach($peakHours as $peak) { ?>
																				<tr>
																					<td><?php  echo date('g:i A', strtotime($peak->StartTime)); ?></td>
																					<td><?php  echo date('g:i A', strtotime($peak->EndTime));   ?></td>
																					<td><?php if($peak->Status=='P') { echo 'Pending'; } else { echo 'Active'; }  ?></td>
																					<td><a href="" id="<?php  echo $peak->PeakHourId;  ?>" type="button" class="deleteHours" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
																						<i class="ft-trash"></i>
																					</a></td>
																				</tr>
																			<?php } ?> 
																			</tbody>
																		</table>
																	<?php } ?>	