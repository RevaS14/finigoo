<div id="render-js"></div>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/vendors.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/core/app-menu.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/core/app.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/toastr.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/extensions/unslider-min.js"></script>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip({ 'placement': 'top' });
        });
    </script> 
    <script>
        $("[data-toggle='tooltip']").click(function () {
            var $this = $(this);
            $(".tooltip").fadeOut("fast", function () {
                $this.blur();
            });
        });

    </script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js"></script>
   <script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/tables/datatable/datatables.min.js"></script> 
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script type="text/javascript">$('.table').DataTable();</script>
    
    <script src="<?php echo base_url(); ?>assets/js/classie.js" type="text/javascript"></script>


    <script>
        var ModalEffects = (function () {
            function init() {
                var overlay = document.querySelector('.md-overlay');
                [].slice.call(document.querySelectorAll('.md-trigger')).forEach(function (el, i) {
                    var modal = document.querySelector('#' + el.getAttribute('data-modal')),
                        close = modal.querySelector('.md-close');

                    function removeModal(hasPerspective) {
                        classie.remove(modal, 'md-show');
                        if (hasPerspective) {
                            classie.remove(document.documentElement, 'md-perspective');
                        }
                    }

                    function removeModalHandler() {
                        removeModal(classie.has(el, 'md-setperspective'));
                    }

                    el.addEventListener('click', function (ev) {

                        var model_type = $(this).attr('model_type');


                        if (model_type == 0) {
                            $('#model_type_driver').css("display", "none");
                            $('#model_type_vehicle').css("display", "block");
                            var vehicle_id = $(this).attr('vehicle_id');
                            var img_path = $('#vehicle_type_icon_' + vehicle_id).attr('src');
                            if (img_path == '') {
                                $("#vehicle_icon").attr("src");
                            } else {
                                $('#vehicle_icon').attr('src', img_path);
                            }
                            var vehicle_type_name = $(this).attr('vehicle_type_name');
                            var vehicle_company = $(this).attr('vehicle_company');
                            var plat_no = $(this).attr('plat_no');
                            var model_year = $(this).attr('model_year');
                            var color = $(this).attr('color');
                            var url = $(this).attr('url');
                            var driver_name = $(this).attr('driver_name');
                            $('#driver_name').text(driver_name);
                            $('#vehicle_name').text(vehicle_type_name);
                            $('#model_name').text(vehicle_company);
                            $('#license_name').text(plat_no);
                            $('#vehicle_year').text(model_year);
                            $('#vehicle_color').text(color);
                            $('#driver_vehicle_details').attr('href', url);
                        }
                        if (model_type == 1) {
                            $('#model_type_vehicle').css("display", "none");
                            $('#model_type_driver').css("display", "block");
                            var total_request = $(this).attr('total_request');
                            var total_completed = $(this).attr('total_completed');
                            var total_cancelled = $(this).attr('total_cancelled');
                            var driver_name = $(this).attr('driver_name');
                            $('#driver_name_type').text(driver_name);
                            $('#total_request').text(total_request);
                            $('#total_completed').text(total_completed);
                            $('#total_rejected').text(total_cancelled);
                        }

                        classie.add(modal, 'md-show');
                        overlay.removeEventListener('click', removeModalHandler);
                        overlay.addEventListener('click', removeModalHandler);
                        if (classie.has(el, 'md-setperspective')) {
                            setTimeout(function () {
                                classie.add(document.documentElement, 'md-perspective');
                            }, 25);
                        }
                    });
                    close.addEventListener('click', function (ev) {
                        ev.stopPropagation();
                        removeModalHandler();
                    });
                });
            }

            init();
        })();
    </script>

    <script>
        $(document).on('click', '.block', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            var service_cat_id = "1";
            var request_for = "D";
            swal({
                    title: "Block User?",
                    text: "if press yes then user is block!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: 'post',
                            url: 'update-status',
                            data: {id: id, request_for: request_for, service_cat_id: service_cat_id},
                            success: function (result) {
                                    swal("Success", "User block successfully", "success");
                                    $('#'+id).removeClass("block").addClass("unblock");
                                    $('#'+id).prop('checked', false);
                            }
                        })
                    } else {
                        swal("Cancelled", "driver status not change", "error");
                    }
                });
        });
        $(document).on('click', '.unblock', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            var service_cat_id = "2";
            var request_for = "A";
            swal({
                    title: "Activate User?",
                    text: "if press yes then user will be active!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: 'post',
                            url: 'update-status',
                            data: {id: id, request_for: request_for, service_cat_id: service_cat_id},
                            success: function (result) {
                                    swal("Success", "User unblocked successfully", "success");
                                    $('#'+id).removeClass("unblock").addClass("block");
                                    $('#'+id).prop('checked', true);
                            }
                        })
                    } else {
                        swal("Cancelled", "Driver status not change", "error");
                    }
                });
        });
        $(document).on('click', '.approve', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            var service_cat_id = "1"
            swal({
                    title: "Approve Driver?",
                    text: "if press yes then driver is unblock!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: 'get',
                            url: 'admin/transport/update-provider-status',
                            data: {id: id, request_for: 1, service_cat_id: service_cat_id},
                            success: function (result) {
                                if (result.success == true) {
                                    var new_id = "#hide_" + id;
                                    swal("Success", "driver approve successfully", "success");
                                    $(new_id).hide();
                                } else {
                                    console.log(result);
                                }
                            }
                        })
                    } else {
                        swal("Cancelled", "driver status not change", "error");
                    }
                });
        });
        $(document).on('click', '.reject', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            var service_cat_id = "1"
            swal({
                    title: "Reject Driver?",
                    text: "if press yes then driver is unblock!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                                title: "Reject Driver!",
                                text: "if reject driver then Write reject reason:",
                                type: "input",
                                showCancelButton: true,
                                closeOnConfirm: false,
                                animation: "slide-from-top",
                                inputPlaceholder: "Reject Reason"
                            },
                            function (inputValue) {
                                if (inputValue === false) return false;
                                if (inputValue === "") {
                                    swal.showInputError("You need to write reject driver reason!");
                                    return false
                                }
                                $.ajax({
                                    type: 'get',
                                    url: 'admin/transport/update-provider-status',
                                    data: {
                                        id: id,
                                        request_for: 3,
                                        service_cat_id: service_cat_id,
                                        rejected_reason: inputValue
                                    },
                                    success: function (result) {
                                        if (result.success == true) {
                                            var new_id = "#hide_" + id;
                                            swal("Success", "driver reject successfully", "success");
                                            $(new_id).hide();
                                        } else {
                                            console.log(result);
                                        }
                                    }
                                })
                            });
                    } else {
                        swal("Cancelled", "driver status not change", "error");
                    }
                });
        });
    </script>

      <script>
         $("[data-toggle='tooltip']").click(function () {
             var $this = $(this);
             $(".tooltip").fadeOut("fast", function () {
                 $this.blur();
             });
         });
         $(document).ready(function ($) {
             //Use this inside your document ready jQuery
             $(window).on('popstate', function () {
                 location.reload(true);
             });
             $("#regionButton").click(function(){
                var additem = $(".pincode").val();
                all=$('.Additem').val();
                var pincodeArray = all.split(',');
                var alreadyExists= jQuery.inArray( additem, pincodeArray );
                if(additem!='') {
                    if(alreadyExists==-1){ 
                    $("#list").append("<li class='list-group-item d-flex justify-content-between align-items-center'>" + additem + "<a class='fa fa-trash remove' id=" + additem + "></a> </li>");
                    $(".Additem").val(($(".Additem").val() + ',' + additem).replace(/^,/,''));
                    } else {
                        alert('Pincode Already Exists..');
                    }
                    $(".pincode").val('');
                } else {
                    alert('Please enter the pincode..')
                }
               
            });

            $("#timePickerButton").click(function(){
                var startTime = $("#startTime").val();
                var endTime = $("#endTime").val();
                var zoneId = $("#zone").val();
                if(startTime=='' || endTime=='') {
                    alert('Please fill peak hours');
                } else if(startTime==endTime) {
                    alert('Please choose different start time and end time..');
                    $("#startTime").val('');
                    $("#endTime").val('');
                } else {
                var jobCategoryId = $("#jobCategoryId").val();
                $.ajax({
                        type: 'post',
                        url: '/finigoo-web/admin/update-peak-hours',
                        data: {
                              startTime: startTime,
                              endTime: endTime,
                              jobCategoryId: jobCategoryId,
                              zoneId: zoneId
                              },
                              success: function (result) {
                                  if (result) {
                                    $("#startTime").val('');
                                    $("#endTime").val('');
                                    $("#hours").html(result);
                                    }
                              }
                        });
                }
            });

            $("body").delegate(".remove", "click", function(){
            
                if (confirm("Are you sure? Want to delete")) {
                    val=$(this).attr('id');
                    $(this).closest('.list-group-item').remove();
                    all=$('.Additem').val();
                    var pincodeArray = all.split(',');
                    var removeItem = val;
                    pincodeArray = jQuery.grep(pincodeArray, function(value) {
                    return value != removeItem;
                    });
                    separator = ','; 
                    allRegions = pincodeArray.join(separator);
                    $(".Additem").val(allRegions);
                } 
                return false;
                
            });

            $("body").delegate(".deleteHours", "click", function(e){
                e.preventDefault();

                if (confirm("Are you sure? Want to delete")) {
                    $.ajax({
                            type: 'post',
                            url: '/finigoo-web/admin/delete-peak-hours',
                            data: {
                                    peakHourId: $(this).attr('id'),
                                    jobCategoryId: $("#jobCategoryId").val(),
                                    zoneId: $("#zone").val()
                                },
                                success: function (result) {
                                    if (result) { $("#hours").html(result); }
                                }
                            });
                } 
                    return false;
                
            });
           
         });
      </script>
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
        <script type="text/javascript">
            $('.form_time').datetimepicker({
                format: "HH:ii P",
                startView: 0,
                minView: 0,
                viewSelect: 0,
                defaultDate:'now',
                pickDate: false,
                pick12HourFormat: true,
                autoclose: true,
                showMeridian: true,
                showClear: true,
            });
        </script>

      <script type="text/javascript">
      var elements = document.querySelectorAll('input,select,textarea');

      for (var i = elements.length; i--;) {
      elements[i].addEventListener('invalid', function () {
        this.scrollIntoView(false);
      });
      }
      </script>
       <script>
            $(document).ready(function(){
                $("#redirectForm").submit();
            });
        </script>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/upload_image.js"></script>
<script type="text/javascript">
   $(document).ready(function () {
       $.uploadPreview({
           input_field: "#vehicle-upload",   // Default: .image-upload
           preview_box: "#vehicle-image-preview",  // Default: .image-preview
           label_field: "#vehicle-label",    // Default: .image-label
           label_default: "Choose Icon",   // Default: Choose File
           label_selected: "Change image",  // Default: Change File
           no_label: false                 // Default: false
       });
   
       $.uploadPreview({
           input_field: "#driver-upload",   // Default: .image-upload
           preview_box: "#driver-image-preview",  // Default: .image-preview
           label_field: "#driver-label",    // Default: .image-label
           label_default: "Choose Icon",   // Default: Choose File
           label_selected: "Change image",  // Default: Change File
           no_label: false                 // Default: false
       });
   
       $.uploadPreview({
           input_field: "#profile-upload",   // Default: .image-upload
           preview_box: "#profile-image-preview",  // Default: .image-preview
           label_field: "#profile-label",    // Default: .image-label
           label_default: "Choose Icon",   // Default: Choose File
           label_selected: "Change image",  // Default: Change File
           no_label: false                 // Default: false
       });
   
   
       $.uploadPreview({
           input_field: "#identity-upload",   // Default: .image-upload
           preview_box: "#identity-image-preview",  // Default: .image-preview
           label_field: "#identity-label",    // Default: .image-label
           label_default: "Choose Icon",   // Default: Choose File
           label_selected: "Change image",  // Default: Change File
           no_label: false                 // Default: false
       });
       
       $.uploadPreview({
           input_field: "#restaurant-upload",   // Default: .image-upload
           preview_box: "#restaurant-image-preview",  // Default: .image-preview
           label_field: "#restaurant-label",    // Default: .image-label
           label_default: "Choose Icon",   // Default: Choose File
           label_selected: "Change image",  // Default: Change File
           no_label: false                 // Default: false
       });
       
   });
</script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript">
   var today = new Date();
   var startDate = new Date(today.getFullYear());
   var endDate = new Date(today.getFullYear(), 6, 31);
   $('.form_datetime').datetimepicker({
       format: "yyyy",
       startView: 'decade',
       minView: 'decade',
       viewSelect: 'decade',
       startDate: startDate,
       endDate: endDate,
       autoclose: true,
   });
</script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/country-code/intlTelInput.min.js"></script>

<script src="<?php echo base_url() ?>app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<script type="text/javascript">
   $('.icheck_minimal input').iCheck({
      checkboxClass: 'icheckbox_minimal',
      radioClass: 'iradio_minimal',
   });
</script>
<script></script>
<script>
   $("[data-toggle='tooltip']").click(function () {
       var $this = $(this);
       $(".tooltip").fadeOut("fast", function () {
           $this.blur();
       });
   });
   $(document).ready(function ($) {
       //Use this inside your document ready jQuery
       $(window).on('popstate', function () {
           location.reload(true);
       });
   });
</script>
<script type="text/javascript">
   var elements = document.querySelectorAll('input,select,textarea');
   
   for (var i = elements.length; i--;) {
   elements[i].addEventListener('invalid', function () {
     this.scrollIntoView(false);
   });
   }
</script>
    <script src="<?php echo base_url() ?>app-assets/js/core/custom.js?ver=9"></script>  
    <script src="<?php echo base_url() ?>app-assets/js/core/selectize.js"></script>
    <script src="<?php echo base_url() ?>app-assets/js/core/index.js?ver=6"></script>
    <script>
                $('#select-beast').selectize({
                    create: true,
                    sortField: {
                        field: 'text',
                        direction: 'asc'
                    },
                    dropdownParent: 'body'
                });
                </script>
   </body>

</html>