
<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">
				Topup
				</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
				
			</div>
		</div>
		<br>
		<div class="content-body">
			<section>
				<div id="render-content">
					<div class="row">
						<div class="col-12">
							<div class="">
								<div class="">
									<div class="">
												<div class="card">
													<div class="card-content">
														<div class="card-body">
														<?php if ($signature == $computedSignature && $txStatus == 'SUCCESS') {  ?>
															<div class="container">
																<div class="panel panel-success">
																	<div class="panel-heading">Signature Verification Successful</div>
																	<div class="panel-body">
																		<!-- <div class="container"> -->
																		<table class="table table-hover">
																			<tbody>
																				<tr>
																					<td>Order ID</td>
																					<td><?php echo $orderId; ?></td>
																				</tr>
																				<tr>
																					<td>Order Amount</td>
																					<td><?php echo $orderAmount; ?></td>
																				</tr>
																				<tr>
																					<td>Reference ID</td>
																					<td><?php echo $referenceId; ?></td>
																				</tr>
																				<tr>
																					<td>Transaction Status</td>
																					<td><?php echo $txStatus; ?></td>
																				</tr>
																				<tr>
																					<td>Payment Mode </td>
																					<td><?php echo $paymentMode; ?></td>
																				</tr>
																				<tr>
																					<td>Message</td>
																					<td><?php echo $txMsg; ?></td>
																				</tr>
																				<tr>
																					<td>Transaction Time</td>
																					<td><?php echo $txTime; ?></td>
																				</tr>
																			</tbody>
																		</table>
																		<!-- </div> -->

																	</div>
																</div>
															</div>
															<?php } else { ?>
															<div class="container">
																<div class="panel panel-danger">
																	<div class="panel-heading">Signature Verification failed</div>
																	<div class="panel-body">
																		<!-- <div class="container"> -->
																		<table class="table table-hover">
																			<tbody>
																				<tr>
																					<td>Order ID</td>
																					<td><?php echo $orderId; ?></td>
																				</tr>
																				<tr>
																					<td>Order Amount</td>
																					<td><?php echo $orderAmount; ?></td>
																				</tr>
																				<tr>
																					<td>Reference ID</td>
																					<td><?php echo $referenceId; ?></td>
																				</tr>
																				<tr>
																					<td>Transaction Status</td>
																					<td><?php echo $txStatus; ?></td>
																				</tr>
																				<tr>
																					<td>Payment Mode </td>
																					<td><?php echo $paymentMode; ?></td>
																				</tr>
																				<tr>
																					<td>Message</td>
																					<td><?php echo $txMsg; ?></td>
																				</tr>
																				<tr>
																					<td>Transaction Time</td>
																					<td><?php echo $txTime; ?></td>
																				</tr>
																			</tbody>
																		</table>
																		<!-- </div> -->
																	</div>
																</div>
															</div>

															<?php } ?>
														</div>
													</div>
												</div>
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>

			</section>
		</div>
	</div>
</div>

	