
<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">Zone List</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
			</div>
		</div>
		<div class="content-body">
            <?php if($this->session->flashdata('successMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('successMessage'); ?>
                    </div>
            <?php } ?>
			<?php if($this->session->flashdata('errorMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('errorMessage'); ?>
                    </div>
            <?php } ?>

			<section>
				<div id="render-content">

					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h5> Zone List <a href="<?php echo base_url(); ?>admin/add-zone"
											class="btn grey btn-outline-secondary float-right">Add Zone</a></h5>

								</div>
								<div class="card-content">
									<div class="card-body">
										<?php  if($zoneData) { ?>
										<div class="table-responsive">
											<table id="new-cons"
												class="table table-striped table-bordered responsive nowrap"
												style="width: 100%!important;">
												<thead>
													<tr>
														<th>No</th>
														<th>Name</th>
														<th data-priority="3">Actions</th>
													</tr>
												</thead>
												<tbody>
													<?php $i=1; foreach($zoneData as $row) { ?>
													<tr id="hide_66">
														<td><?php echo $i; ?></td>
														<td class="td-icon">
															<?php echo $row->Name; ?>
														</td>

														<td class="action">
															
															<a href="<?php echo base_url(); ?>admin/edit-zone/<?php echo $row->ZoneId; ?>" class="delete btn btn-outline-primary"
																data-toggle="tooltip" data-placement="top" title="Edit">
																<i class="ft-edit"></i>
                                                            </a>
                                                            <a href="<?php echo base_url(); ?>admin/delete-zone/<?php echo $row->ZoneId; ?>" class="delete btn btn-outline-primary"
																data-toggle="tooltip" data-placement="top" title="Delete">
																<i class="ft-trash"></i>
															</a>
														</td>
													</tr>
													<?php $i++; } ?>

													</tr>
												</tbody>
											</table>
										</div>
										<?php } else { echo 'No Records Found..'; } ?>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

			</section>
		</div>
	</div>
</div>