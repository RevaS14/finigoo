<div class="app-content content">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-12 mb-2">
				<h3 class="content-header-title mb-0">All Driver List</h3><br>
				<?php if(isset($_GET['status'])) { 
                        ?>
                     <div class="alert alert-success" role="alert">
                        Status Updated Successfully..!
                     </div>
                     <?php } ?>
				<?php if(isset($userDetails) && ($userDetails != ''))  { ?>
                       <br><div class="alert alert-success col-md-4 text-white" role="alert">Status Updated Successfully..!</div>
                <?php } ?>
			</div>
			
			<div class="content-header-right col-md-6 col-12"></div>
		</div>
		<div class="content-body">
			<section>
				<div id="render-content">
					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h5> All List of Drivers</h5>
									<?php if(isset($userDetails) && ($userDetails != ''))  { ?>
									   <a href=""><button class="float-right btn btn-success">Back</button></a>
                                    <?php } ?>
								</div>

								<?php 
								  	if(isset($userDetails)) {
										$gocab = $userDetails; 
									}
								?>
								<div class="card-content">
									<div class="card-body">
										<div class="table-responsive">
											<div id="new-cons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
												<div class="row">
													<div class="col-sm-12">
														<table id="new-cons" class="table table-striped table-bordered responsive dataTable no-footer dtr-inline collapsed" style="width: 100%;" role="grid" aria-describedby="new-cons_info">
															<?php if($gocab != '' ) { ?>
															<thead>
																<tr role="row">
											
																	<th>Full Name</th>
																	<th>Mobile</th>
																	<th>Email</th>
																	<th>Vehicle Number</th>
																	<th>Vehicle Type</th>
                                                                    <th>Vehicle Brand</th>
                                                                    <th>Status</th>
                                                                    <th>Verify</th>
																</tr>
															</thead>
															<tbody>
																<?php 
																foreach($gocab as $v) { ?>
                                                                    <tr role="row" class="odd">
                                                                        <td><?php echo $v->FirstName.' '.$v->LastName;?></td>
                                                                        <td><?php echo $v->Phone;?></td>
                                                                        <td><?php echo $v->Email;?></td>
                                                                        <td><?php echo $v->VehicleNumber;?></td>
                                                                        <td><?php echo $v->VehicleBrand;?></td>
                                                                        <td><?php echo $v->VehicleType;?></td>
                                                                        <td><?php if($v->Status == 'P') { ?>
																			 Pending
																		<?php }?></td>
                                                                      <!--  <td class="gocab-actions">
																			<div class="col-md-12">
																			  <a href="<?php echo base_url();?>admin/listdrivers/details?view=<?php echo $v->UserId;?>&job=1"><button type="button" class="btn btn-success"><i class="fa fa-eye"></i></button></a>
																			  <a href="<?php echo base_url();?>admin/listdrivers/details?update=<?php echo $v->UserId;?>&job=1"><button type="button" class="btn btn-success"><i class="fa fa-edit"></i></button></a>
																			  <a href="<?php echo base_url();?>admin/listdrivers/details?delete=<?php echo $v->UserId;?>&job=1"><button type="button" class="btn btn-success"><i class="fa fa-trash"></i></button></a></div>
																			  <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="mydeleteLabel">
																				<div class="modal-dialog" role="document">
																					<div class="modal-content">
																					<div class="modal-header">
																						<h4 class="modal-title" id="myModalLabel">Verify Status</h4>
																						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																						
																					</div>
																					<form method="post" action="<?php echo base_url();?>/admin/approve-driver/gocab">
																						<div class="modal-body">
																							<div class="form-group">
																								<label for="topupAmount">Change Status</label>
																								<input type="hidden" value="<?php echo $v->UserId;?>" name="StatusId"/>
																								<input type="hidden" value="<?php echo $v->UserType;?>" name="UserType"/>
																								<input type="hidden" value="<?php echo $v->JobCategory_JobCategoryId;?>" name="JobCategory"/>
																								
																								<select class="form-control" name="verifystatus" placeholder="Enter Amount">
																									<option value="A" selected>Activated</option>
																									<option value="D" >Deactivated</option>
																								</select>   
																							</div>
																						</div>
																						<div class="modal-footer">
																							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																							<button type="submit" class="btn btn-primary">Submit</button>
																						</div>
																					</form>
																					</div>
																				</div>
																		    </div>
																		</td>	-->
																		</td>
                                                                        <td>
																		   <a><button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Approve</button></a>
																		   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																				<div class="modal-dialog" role="document">
																					<div class="modal-content">
																					<div class="modal-header">
																						<h4 class="modal-title" id="myModalLabel">Verify Status</h4>
																						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																						
																					</div>
																					<form method="post" action="<?php echo base_url();?>admin/listdrivers/verify">
																						<div class="modal-body">
																							<div class="form-group">
																								<label for="statusupdate">Change Status</label>
																								<input type="hidden" value="<?php echo $v->UserId;?>" name="StatusId"/>
																								<input type="hidden" value="<?php echo $v->UserType;?>" name="UserType"/>
																								<input type="hidden" value="<?php echo $v->JobCategory_JobCategoryId;?>" name="JobCategory"/>
																								<input type="hidden" value="<?php echo $v->VehicleID;?>" name="VehicleID"/>
																								<input type="hidden" value="<?php echo $v->Phone;?>" name="DriverPhone"/>
																								<input type="hidden" value="<?php echo $v->Email;?>" name="DriverEmail"/>
																								<input type="hidden" value="<?php echo $redirectView;?>" name="redirectView"/>
																								
																								<select class="form-control" name="verifystatus" placeholder="Enter Amount">
																									<option value="A" selected>Approve</option>
																									<option value="D" >Disapprove</option>
																								</select>   
																							</div>
																						</div>
																						<div class="modal-footer">
																							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																							<button type="submit" class="btn btn-primary">Submit</button>
																						</div>
																					</form>
																					</div>
																				</div>
																		    </div>
																		</td>
                                                                    </tr>
                                                                <?php } ?>
															
															</tbody>
															<?php } else { ?>
                                                               <tr>
															       <td class="float-left"><a href="<?php echo base_url();?>admin/listdrivers/gocab"><button type="submit" class="btn btn-primary">Redirect to Gocab</button></a></td>
															   </tr>
															<?php } ?>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
<style>
 #new-cons td, #new-cons th { display:table-cell !important; }
 .alert.alert-success.col-md-4.text-white { color: #ffffff !important; }
</style>