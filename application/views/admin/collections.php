
<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">Collections List</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
			</div>
		</div>
		<div class="content-body">
            <?php if($this->session->flashdata('successMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('successMessage'); ?>
                    </div>
            <?php } ?>
			<?php if($this->session->flashdata('errorMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('errorMessage'); ?>
                    </div>
            <?php } ?>

			<section>
				<div id="render-content">

					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h5> Driver Collections List </h5>
								</div>
								<div class="card-content">
									<div class="card-body">
										<?php  if($collections) { ?>
										<div class="table-responsive">
											<table id="new-cons"
												class="table table-striped table-bordered responsive nowrap"
												style="width: 100%!important;">
												<thead>
													<tr>
														<th>No</th>
														<th>Driver Name</th>
														<th>Pending Amount</th>
														<th>Calculated Fees</th>
														<th>Status</th>
														<?php if($this->uri->segment(3)=='pending') { ?>
														<th data-priority="3">Actions</th>
														<?php } ?>
													</tr>
												</thead>
												<tbody>
													<?php $i=1; foreach($collections as $allData) { ?>
													<tr id="hide_66">
														<td><?php echo $i; ?></td>
														<td class="td-icon">
															<?php echo $allData->FirstName; ?>
														</td>

														<td class="td-icon">
															<?php echo $allData->PendingAmount; ?>
														</td>

														<td><?php echo $allData->CalculatedFee; ?></td>

														<td><?php if($allData->Status=='A') { echo 'Active'; } else { echo 'Pending'; } ?></td>
														<?php if($allData->Status=='P') { ?>
														<td class="action">
															<a data-toggle="tooltip" id="<?php echo $allData->DriverCollectionId; ?>" class="collect btn btn-info" data-placement="top" title="Collect" href="">Collect</a>
														</td>
														<?php } ?>
													</tr>
													<?php $i++; } ?>

													</tr>
												</tbody>
											</table>
										</div>
										<?php } else { echo 'No Records Found..'; } ?>
									</div>
								</div>
							</div>
						</div>
					</div>


					<div class="md-overlay"></div>
				</div>

			</section>
		</div>
	</div>
</div>