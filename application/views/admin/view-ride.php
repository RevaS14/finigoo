
<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">Ride Details</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
				<a  id="" href="<?php echo base_url(); ?>admin/view-driver/<?php echo $data[0]->AcceptedBy; ?>" class="btn grey btn-outline-secondary float-right mr-1">Back</a>
			</div>
		</div>
		<div class="content-body">

            <style type="text/css">
            	.bank-table th, .bank-table td {
            		border-bottom: 1px solid #e3ebf3 !important;
            		text-align: left;
            		border-top: 1px solid #e3ebf3;
            	}
            	.table th, .table td {
            		border-top: 1px solid #e3ebf3;
            	}
            	.vehicle-title {
            		margin-top: 20px;
            		margin-bottom: 20px;
            	}
            </style>
			<section>
				<div id="render-content">

					<div class="row">
						<div class="col-12">
							<div class="card">
								
								<div class="card-content">
									<div class="card-body">
										<div class="row">
										<div class="col-md-6">
											
											<table class="table bank-table">
												<tr>
													<th>Customer Name :</th>
													<td><?php echo $data[0]->CustomerName; ?></td>
												</tr>
												<tr>
													<th>Source Address :</th>
													<td><?php echo $data[0]->SourceAddress; ?></td>
												</tr>
												<tr>
													<th>Distance :</th>
													<td><?php echo $data[0]->Distance; ?></td>
												</tr>
												
												<tr>
													<th>Estimated Fare :</th>
													<td><?php echo $data[0]->EstimatedFare; ?></td>
												</tr>

												
												<tr>
													<th>Users Rating :</th>
													<td><?php echo $data[0]->UsersRating; ?></td>
												</tr>
												<tr>
													<th>Payment Mode :</th>
													<td><?php echo $data[0]->PaymentMode; ?></td>
												</tr>
												<tr>
													<th>Status :</th>
													<td><?php if($data[0]->Status=='A') { echo 'Active'; } elseif($data->Status=='D') { echo 'Deactive'; } elseif($data[0]->Status=='P') { echo 'Deactive'; } ?></td>
												</tr>
												

											</table>

										</div>
										<div class="col-md-6">
											
											<table class="table bank-table">
												<tr>
													<th>Driver Name :</th>
													<td><?php echo $data[0]->DriverName; ?></td>
												</tr>
												
												<tr>
													<th>Destination Address:</th>
													<td><?php echo $data[0]->DestinationAddress; ?></td>
												</tr>
												<tr>
													<th>Estimated Time :</th>
													<td><?php echo $data[0]->EstimatedTime; ?></td>
												</tr>
												<tr>
													<th>Actual Fare :</th>
													<td><?php echo $data[0]->ActualFare; ?></td>
												</tr>
												<tr>
													<th>Users Feedback :</th>
													<td><?php echo $data[0]->UsersFeedback; ?></td>
												</tr>
												
												<tr>
													<th>Ride Date</th>
													<td><?php echo date('d-m-Y',strtotime($data[0]->CreatedOn)); ?></td>
												</tr>
												

											</table>

									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
											<h3 class="vehicle-title">Transaction Details</h3>
											<table class="table bank-table">
												<tr>
													<th>Amount :</th>
													<td><?php echo $data[0]->Amount; ?></td>
												</tr>
												<tr>
													<th>Payment Method :</th>
													<td><?php if($data[0]->Payment=='O') { echo 'Online'; } elseif($data[0]->Payment=='B') { echo 'Bank'; }  ?></td>
												</tr>
												
												<tr>
													<th>Transaction Id :</th>
													<td><?php echo $data[0]->TransactionId; ?></td>
												</tr>

												
												<tr>
													<th>Transaction Status :</th>
													<td>
														<?php if($data[0]->TransactionStatus=='A') { echo 'Active'; } elseif($data[0]->TransactionStatus=='D') { echo 'Deactive'; } elseif($data[0]->TransactionStatus=='P') { echo 'Deactive'; } ?></td>
												</tr>
												<tr>
													<th>Remarks :</th>
													<td><?php echo $data[0]->Remarks; ?></td>
												</tr>
												<tr>
													<th>Status :</th>
													<td><?php if($data[0]->TopupStatus=='A') { echo 'Active'; } elseif($data[0]->TopupStatus=='D') { echo 'Deactive'; } elseif($data[0]->TopupStatus=='P') { echo 'Deactive'; } ?></td>
												</tr>
												

											</table>

										</div>
										<div class="col-md-6">
											<h3 class="vehicle-title">Vehicle Details</h3>
											<table class="table bank-table">
												<tr>
													<th>Vehicle Type :</th>
													<td><?php echo $data[0]->RideType; ?></td>
												</tr>
												<tr>
													<th>Vehicle Number :</th>
													<td><?php echo $data[0]->VehicleNumber; ?></td>
												</tr>
												
												<tr>
													<th>Vehicle Color :</th>
													<td><?php echo $data[0]->VehicleColor; ?></td>
												</tr>
		
												<tr>
													<th>Vehicle Brand :</th>
													<td><?php echo $data[0]->VehicleBrand; ?></td>
												</tr>
												
												

											</table>

										</div>
								</div>
								
								</div>
							</div>
						</div>
					</div>


					<div class="md-overlay"></div>
				</div>

			</section>
		</div>
	</div>
</div>