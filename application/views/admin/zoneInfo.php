<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0"><?php if(isset($zoneData)) { echo 'Edit'; } else { echo 'Add'; } ?> Zone
				</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
				<div class="btn-group float-md-right" role="group">
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div id="render-content">
					<div class="row">
						<div class="col-12">
							<form id="main" method="post" action="" enctype="multipart/form-data">
								<input type="hidden" name="_token" value="">
								<div class="row">
									<span id="service_category_append"></span>
									<div class="form-group col-sm-12">
									<?php if($this->session->flashdata('errorMessage')) { ?>
                                                    <div class="alert alert-danger" role="alert">
                                                    <?php echo $this->session->flashdata('errorMessage'); ?>
                                                    </div>
                                                <?php } ?>
                                                <?php if($this->session->flashdata('successMessage')) { ?>
                                                    <div class="alert alert-success" role="alert">
                                                    <?php echo $this->session->flashdata('successMessage'); ?>
                                                    </div>
                                                <?php } ?>
											
										<div class="card">
											<div class="card-content">
												<div class="card-body">
													<div class="row">
														<div class="form-group col-sm-6">
															<span class="error"></span>
															<div class="form-group row">
																<label class="col-sm-4 col-form-label">Name:<sup
																		class="error">*</sup></label>
																<div class="col-sm-8">
																	<input type="text" class="form-control" name="firstName"
																		 id="firstName" placeholder="Name"
																		value="<?php if(isset($zoneData)) { echo $zoneData->Name; } ?>">
																	<span class="error"></span>
																</div>
                                                            </div>
															
                                                        
															
														</div>
														
													</div>
												</div>
											</div>
										</div>
                                    </div>
                                    
                                    <div class="form-group col-sm-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5> Regions </h5>
                                                </div>
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="form-group col-sm-6">
                                                               
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label">Pincode:<sup class="error">*</sup></label>
                                                                    <div class="col-sm-6">
                                                                        <input type="text" class="form-control pincode" name="regions"
                                                                            id="regions" placeholder="Pincode" value="">
                                                                        <span class="error"></span>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                    <button type="button" name="regionButton" id="regionButton" class="btn btn-success m-b-0">Add</button>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <div class="form-group row">
                                                                    
                                                                    <div class="col-sm-8">
                                                                        <?php if(isset($zoneData)) { ?>   <h5> Selected Regions </h5> <?php } ?>
                                                                    <ol id="list" class="list-group">
                                                                    <?php $pin=array(); if(isset($zoneData) && $regions) { foreach($regions as $reg) { ?> 
                                                                        <li class='list-group-item d-flex justify-content-between align-items-center'>
                                                                            <?php echo $reg->Pincode; ?>
                                                                            <a class='fa fa-trash remove' id="<?php echo $reg->Pincode; $pin[]=$reg->Pincode; ?>"></a> 
                                                                        </li>
                                                                     <?php } } $str = implode (",", $pin); ?>
                                                                    </ol> 
                                                                    <input type="hidden" name="allRegions" class="Additem" value="<?php if(isset($zoneData)) { echo $str; } ?>" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
									
									<div class="form-group col-sm-12">


										<center>
											<button type="submit" name="save" class="btn btn-success m-b-0">Save</button>
										</center>


									</div>
								</div>
							</form>
						</div>
					</div>

					<div id="model_overlay" class="">
						<div id="provider-services-modal" class="modal fade text-left" id="default" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
							<!-- Modal content -->
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title">Driver Services</h4>
										<a href="" class="btn btn-outline-primary">
											&times;
										</a>
									</div>
									<div class="modal-body">
										<form>
											<div class="form-group row">
												<label class="col-sm-12 col-form-label">Service Category:<sup
														class="error">*</sup></label>
												<div class="col-sm-12">
													<div class="border-checkbox-section">
														<div class="border-checkbox-group border-checkbox-group-primary"
															style="margin-bottom: 8px;">
															<input name="provider_services_bike_ride" value="1"
																class="border-checkbox" checked="checked" value="1"
																type="checkbox" id="checkbox1">
															<label class="border-checkbox-label"
																for="checkbox1"></label>
															<img src="../../../assets/images/service-category/bike.png"
																class="provider_check_icon" width="40px" height="40px">
															Bike Ride
														</div>
														<div class="border-checkbox-group border-checkbox-group-primary"
															style="margin-bottom: 8px;">
															<input name="provider_services_store_delivery" value="1"
																class="border-checkbox" type="checkbox" id="checkbox2">
															<label class="border-checkbox-label"
																for="checkbox2"></label>
															<img src="../../../assets/images/service-category/courier-service-1.png"
																class="provider_check_icon" width="40px" height="40px">
															Store Delivery
														</div>
														<div class="border-checkbox-group border-checkbox-group-primary"
															style="margin-bottom: 8px;">
															<input name="provider_services_courier_service" value="3"
																class="border-checkbox" value="0" value=&#039;0&#039;
																type="checkbox" id="checkbox3">
															<label class="border-checkbox-label"
																for="checkbox3"></label>
															<img src="../../../assets/images/service-category/packageimg.png"
																class="provider_check_icon" width="40px" height="40px">
															Courier Services
														</div>
													</div>

												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer" style="justify-content: flex-start;">
										<button type="button" class="btn grey btn-outline-secondary"
											data-dismiss="modal">Done</button>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>
	</div>
</div>