    <!-- fixed-top-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-semi-dark navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-md-none mr-auto"><a
                            class="nav-link nav-menu-main menu-toggle hidden-xs is-active" href="dashboard#"><i
                                class="ft-menu font-large-1"></i></a></li>
                    <li class="nav-item"><a class="navbar-brand" href="<?php echo base_url(); ?>admin/dashboard">
                            <img class="img-fluid" src="<?php echo base_url(); ?>assets/images/website-logo-icon/finigoo.png"
                                alt="finigoo.png" style="max-width: 75%;">
                        </a></li>
                    <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse"
                            data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a></li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">

                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item d-none d-md-block"><a
                                class="nav-link nav-menu-main menu-toggle hidden-xs is-active" href="dashboard#"><i
                                    class="ft-menu"></i></a></li>
                    </ul>

                    <ul class="nav navbar-nav float-right">
                        <?php //echo $this->session->userdata('user_id'); ?>
                        <li class="dropdown dropdown-user nav-item"><a
                                class="dropdown-toggle nav-link dropdown-user-link" href="dashboard#"
                                data-toggle="dropdown"><span class="avatar avatar-online"><img
                                        src="<?php echo base_url(); ?>assets/images/service-category/avatar-s-1.png" alt="avatar"></span><span
                                    class="user-name">Super Admin</span></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="change-password">
                                    <i class="ft-edit"></i> Change Password
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>/admin/logout"><i class="ft-power"></i>Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="main-menu-content ps-container ps-theme-light ps-active-y">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

                <!-- General -->
                <li class=" navigation-header"><span>General</span><i class=" ft-minus" data-toggle="tooltip"
                        data-placement="right" data-original-title="General"></i>
                </li>
                <li class="nav-item  <?php if($this->uri->segment(2)=="dashboard") { echo 'active'; }?> "><a href="<?php echo base_url(); ?>admin/dashboard"><i class="ft-home"></i>
                    <span class="menu-title">Dashboard</span>  </a>
                </li>
                <?php if($this->session->userdata('userType')=='A') { ?> 
                
                <li class="nav-item has-sub">
                    <a href="dashboard#">
                        <i class="fa fa-percent"> </i>
                        <span class="menu-title">Commissions</span>
                    </a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="<?php echo base_url(); ?>admin/commissions/approved"><i class="fa fa-check"> </i> Approved</a>
                        </li>
                        <li><a class="menu-item" href="<?php echo base_url(); ?>admin/commissions/pending"><i class="fa fa-clock-o"> </i> Pending</a>
                        </li>
                        <li><a class="menu-item" href="<?php echo base_url(); ?>admin/commissions/declined"><i class="fa fa-ban"> </i> Declined</a>
                        </li>
                        
                    </ul>
                </li> 
                <?php } ?>
                <?php if($this->session->userdata('userType')=='F') { ?>
                <li class="nav-item <?php if($this->uri->segment(2)=="topup") { echo 'active'; }?> "><a href="<?php echo base_url(); ?>admin/topup/<?php if($this->session->userdata('userType')=='A') { echo 'admin'; }  ?>/<?php echo $this->session->userdata('user_id'); ?>"><i class="ft-credit-card"></i>
                    <span class="menu-title">Topup</span>  </a>
                </li>
                <?php } ?>
                <li class="nav-item has-sub">
                    <a href="dashboard#">
                        <i class="fa fa-money"> </i>
                        <span class="menu-title">Setting Price</span>
                    </a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="<?php echo base_url(); ?>admin/set-cost/1"><i class="fa fa-taxi"> </i> Go Cab</a>
                        </li>
                        <li><a class="menu-item" href="<?php echo base_url(); ?>admin/set-cost/3"><i class="fa fa-cab"> </i> Go Ride</a>
                        </li>
                                            
                    </ul>
                </li> 
                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/zones">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <span class="menu-title">Zones</span>
                    </a>
                   
                </li> 
                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/rides">
                    <i class="fa fa-bicycle"></i>
                        <span class="menu-title">Rides</span>
                    </a>
                   
                </li> 
                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/users">
                    <i class="fa fa-user"></i>
                        <span class="menu-title">Users</span>
                    </a>
                   
                </li> 
                <li class="nav-item has-sub">
                    <a href="#">
                    <i class="fa fa-exchange" aria-hidden="true"></i>
                        <span class="menu-title">Manual Trasactions</span>
                    </a>
                     <ul class="menu-content">
                        <li><a class="menu-item" href="<?php echo base_url(); ?>admin/manual-transactions/approved"><i class="fa fa-check"> </i> Approved</a>
                        </li>
                        <li><a class="menu-item" href="<?php echo base_url(); ?>admin/manual-transactions/pending"><i class="fa fa-clock-o"> </i> Pending</a>
                        </li>
                         <li><a class="menu-item" href="<?php echo base_url(); ?>admin/manual-transactions/cancelled"><i class="fa fa-ban"> </i> Cancelled</a>
                        </li>
                    </ul>
                   
                </li> 
                <li class="nav-item has-sub">
                    <a href="dashboard#">
                    <i class="fa fa-tags" aria-hidden="true"></i>
                        <span class="menu-title">Driver Collections</span>
                    </a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="<?php echo base_url(); ?>admin/collections/approved"><i class="fa fa-check"> </i> Approved</a>
                        </li>
                        <li><a class="menu-item" href="<?php echo base_url(); ?>admin/collections/pending"><i class="fa fa-clock-o"> </i> Pending</a>
                        </li>
                    </ul>
                </li> 
                <li class="nav-item has-sub">
                    <a href="dashboard#">
                    <i class="fa fa-paper-plane" aria-hidden="true"></i>
                        <span class="menu-title">Withdraw Requests</span>
                    </a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="<?php echo base_url(); ?>admin/withdraw-requests/approved"><i class="fa fa-check"> </i> Approved</a>
                        </li>
                        <li><a class="menu-item" href="<?php echo base_url(); ?>admin/withdraw-requests/pending"><i class="fa fa-clock-o"> </i> Pending</a>
                        </li>
                        <li><a class="menu-item" href="<?php echo base_url(); ?>admin/withdraw-requests/cancelled"><i class="fa fa-ban"> </i> Cancelled</a>
                        </li>
                    </ul>
                </li> 
               
               
                <li class="nav-item has-sub">
                    <a href="dashboard#">
                    <i class="fa fa-car"></i> 
                        <span class="menu-title">Approve Providers</span>
                    </a>
                    <ul class="menu-content" style="">
 
                        <li class="is-shown"><a class="menu-item" href="<?php echo base_url();?>admin/approve-driver/gocab"><i class="fa fa-car"></i> Go-Cab</a>
                        </li>
                        <li class="is-shown"><a class="menu-item" href="<?php echo base_url();?>admin/approve-driver/goride"><i class="fa fa-motorcycle"></i>Go-Ride</a>
                        </li>
                       
                    </ul>   
                </li>

                <li class="nav-item has-sub">
                    <a href="dashboard#">
                    <i class="fa fa-car"></i> 
                        <span class="menu-title">Add Providers</span>
                    </a>
                    <ul class="menu-content" style="">
 
                        <li class="is-shown"><a class="menu-item" href="<?php echo base_url();?>admin/add-driver/gocab"><i class="fa fa-car"></i> Go-Cab</a>
                        </li>
                        <li class="is-shown"><a class="menu-item" href="<?php echo base_url();?>admin/add-driver/goride"><i class="fa fa-motorcycle"></i>Go-Ride</a>
                        </li>
                       
                    </ul>   
                </li>

                <li class="nav-item has-sub">
                    <a href="dashboard#">
                    <i class="fa fa-car"></i> 
                        <span class="menu-title">List Providers</span>
                    </a>
                    <ul class="menu-content" style="">
 
                        <li class="is-shown"><a class="menu-item" href="<?php echo base_url();?>admin/listdrivers/gocab"><i class="fa fa-car"></i> Go-Cab</a>
                        </li>
                        <li class="is-shown"><a class="menu-item" href="<?php echo base_url();?>admin/listdrivers/goride"><i class="fa fa-motorcycle"></i>Go-Ride</a>
                        </li>
                        
                    </ul>   
                </li>
            </ul>
        </div>
    </div>