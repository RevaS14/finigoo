
<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0"><?php if($this->uri->segment(3)=="approved") { echo 'Approved'; } elseif($this->uri->segment(3)=="pending") { echo 'Pending'; } elseif($this->uri->segment(3)=="cancelled") { echo 'Cancelled'; }  ?> Transactions List</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
			</div>
		</div>
		<div class="content-body">
            <?php if($this->session->flashdata('successMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('successMessage'); ?>
                    </div>
            <?php } ?>
			<?php if($this->session->flashdata('errorMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('errorMessage'); ?>
                    </div>
            <?php } ?>

			<section>
				<div id="render-content">

					<div class="row">
						<div class="col-12">
							<div class="card">
							
								<div class="card-content">
									<div class="card-body">
										<?php  if($requests) { ?>
										<div class="table-responsive">
											<table id="new-cons"
												class="table table-striped table-bordered responsive nowrap"
												style="width: 100%!important;">
												<thead>
													<tr>
														<th>No</th>
														<th>User Name</th>
														<th>Amount</th>
														<th>Bank Name</th>
														<th>Deposited On</th>
														
														
														<th data-priority="3">Actions</th>
														
													</tr>
												</thead>
												<tbody>
													<?php $i=1; foreach($requests as $allData) { ?>
													<tr id="hide_66">
														<td><?php echo $i; ?></td>
														<td class="td-icon">
															<?php echo $allData->FirstName; ?>
														</td>

														<td class="td-icon">
															<?php echo $allData->Amount; ?>
														</td>

														<td class="td-icon">
															<?php echo $allData->BankName; ?>
														</td>

														<td><?php echo date('d-m-yy h:i A', strtotime($allData->CreatedOn)); ?></td>
														
														
														
														
														<td class="action">
															<a data-toggle="tooltip" id="<?php echo $allData->ManualTransactionId; ?>" class="btn btn-info" data-placement="top" title="View Details" href="<?php echo base_url(); ?>admin/transaction-details/<?php echo $allData->ManualTransactionId; ?>">View</a>
															
														</td>
														
													</tr>
													<?php $i++; } ?>

													</tr>
												</tbody>
											</table>
										</div>
										<?php } else { echo 'No Records Found..'; } ?>
									</div>
								</div>
							</div>
						</div>
					</div>


					<div class="md-overlay"></div>
				</div>

			</section>
		</div>
	</div>
</div>