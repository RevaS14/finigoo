<div class="container">
   <div class="content-wrapper col-md-12">
      <div class="content-header row mh100-pixel">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Driver Registration Form
            </h3>
         </div>
        
      </div>
      <div class="content-body">
         <section>
            <div id="render-content">
               <div class="row">
                  <div class="col-12">
                     <?php if(isset($status)) { 
                        ?>
                     <div class="alert alert-success" role="alert">
                        Registration Successfully..!
                     </div>
                     <?php } ?>
                     <form id="main" method="post"
                        action="<?php echo site_url();?>admin/add-driver/save"
                        enctype="multipart/form-data">
                        <input type="hidden" name="redirectView" value="<?php echo $redirectView ?>">
                        <input type="hidden" name="JobCategory" value="<?php echo $jobCategory ?>">
                        <input type="hidden" name="UserType" value="D">
                        <input type="hidden" name="_token" value="">
                        <div class="row">
                           <span id="service_category_append"></span>
                           <div class="form-group col-sm-12">
                              <div class="card">
                                 <!-- <div class="card-header">
                                    <h5>Add Driver of Bike Ride</h5>
                                    </div> -->
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label">First Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Firstname"
                                                required id="Firstname" placeholder="First Name"value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Last Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Lastname"
                                                required id="Lastname" placeholder="Last Name" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Email:<sup
                                                class="error">*</sup></label>
                                             <input type="email" class="form-control" name="Email"
                                                required id="email" placeholder="Unique Email" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class=" col-form-label">Mobile Phone:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control"
                                                name="Phone" required id="Phone"
                                                placeholder="Unique Contact Number" value="">
                                             <span class="error"></span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Place of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PlaceOfBirth" required
                                                id="POB" placeholder="Place of Birth" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Date of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="Date" class="form-control" name="DOB" required
                                                id="POB" placeholder="Date of Birth" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Identity number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="IdentityNo"
                                                required id="In" placeholder="Identity number" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">PinCode<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PinCode"
                                                required id="PC" placeholder="Pin Code" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">City<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="City"
                                                required id="City" placeholder="City" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">State<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="State"
                                                required id="State" placeholder="State" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Country<sup
                                                class="error">*</sup></label>
                                             <select class="form-control" id="Country" name="Country">
                                                <option value="1" selected>India</option>
                                             </select>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Driver Name<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="DriverName" required
                                                id="Pc" placeholder="Driver Name" value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Driver Licence No<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="DriverLicenseNo" required
                                                id="Pc" placeholder="Driver Licence No" value="">
                                             <span class="error"></span>
                                          </div>
                                          <?php if($jobCategory == 1) { ?>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">JobType<sup
                                                class="error">*</sup></label>
                                             <select class="form-control" id="JobType" name="JobCategory">
                                                <option value="1" selected>Go Mini</option>
                                                <option value="2" >Go Micro</option>
                                             </select>
                                             <span class="error"></span>
                                          </div>
                                       
                                          <?php } ?>

                                          <?php if($jobCategory == 5) { ?>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">JobType<sup
                                                class="error">*</sup></label>
                                             <select class="form-control" id="JobType" name="JobCategory">
                                                <option value="5" selected>Tata Ace</option>
                                                <option value="6">Ape</option>
                                                <option value="7">Dost</option>
                                                <option value="8">Tata 407</option>
                                                <option value="9">Scooter</option>
                                             </select>
                                             <span class="error"></span>
                                          </div>
                                       
                                          <?php } ?>

                                       </div>
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo
                                             Vehicle Permit:</label>	
                                             <div id="vehicle-image-preview">
                                                <label for="vehicle-upload" id="vehicle-label">Upload
                                                Image</label>
                                                <input type="file" id="vehicle-upload"
                                                   name="VehiclePermit" accept=".jpg,.jpeg,.png" required = "required"/>
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo Driver
                                             license:</label>
                                             <div id="driver-image-preview">
                                                <label for="driver-upload" id="driver-label">Upload
                                                Image</label>
                                                <input type="file" id="driver-upload"
                                                   name="DriverLicense" accept=".jpg,.jpeg,.png" required = "required"/>
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo
                                             Profile:</label>
                                             <div id="profile-image-preview">
                                                <label for="profile-upload" id="profile-label">Upload
                                                Image</label>
                                                <input type="file" id="profile-upload"
                                                   name="DisplayPicture" accept=".jpg,.jpeg,.png" required = "required" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Identity
                                             Document Upload:</label>
                                             <div id="identity-image-preview">
                                                <label for="identity-upload" id="identity-label">Upload
                                                Image</label>
                                                <input type="file" id="identity-upload"
                                                   name="IdentityDocument" accept=".jpg,.jpeg,.png"  required = "required"/>
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-sm-8">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Vehicle Data
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Brand:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleBrand"
                                                required id="VehicleBrand" placeholder="Vehicle Brand"
                                                value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Type:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleType"
                                                required id="VehicleType" placeholder="Vehicle Type"
                                                value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleNumber"
                                                required id="VehicleNumber" placeholder="Vehicle Number"
                                                value="">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Color:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleColor"
                                                required id="VehicleColor" placeholder="Vehicle Color"
                                                value="">
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-sm-4">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Terms and Services
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <ol>
                                             <li>Driver must provide their own car</li>
                                             <li>Driver must have a personal Android device</li>
                                             <li>Driver must have Driver license which is still active at
                                                least 6 months ahead
                                             </li>
                                          </ol>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-sm-12">
                              <center>
                                 <button type="submit" class="btn btn-success m-b-0">Submit</button>
                              </center>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>   