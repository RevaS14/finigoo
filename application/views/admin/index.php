<?php 
date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
echo date("Y-m-d H:i:s"); ?>
      <div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
         <section class="flexbox-container">
            <div class="col-12 d-flex align-items-center justify-content-center">
               <div class="col-md-4 col-10 box-shadow-2 p-0">
                  <div class="card border-grey border-lighten-3 m-0">
                     <div class="card-header border-0">
                        <div class="card-title text-center">
                           <div class="p-1">
                                <img src="<?php echo base_url(); ?>assets/images/website-logo-icon/finigoo.png"
                                 alt="logo-blue.png" >
                                                          
                           </div>
                        </div>
                        <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Admin Login</span></h6>
                     </div>
                     <?php if($this->session->flashdata('errorMessage')) { ?>
                           <div class="alert alert-danger" role="alert">
                              <?php echo $this->session->flashdata('errorMessage'); ?>
                           </div>
                     <?php } ?>
                     <div class="card-content">
                        <div class="card-body">
                           <form  method="post" class="form-horizontal form-simple" action="<?php echo base_url(); ?>index.php/admin/login" >
                              <input type="hidden" name="_token" value="bkxie7zOX7vvub3h65rYiTz2sd3kVBHzJauEf9gV">
                              <input type="hidden" name="loginType" value="admin">
                              <fieldset class="form-group position-relative has-icon-left">
                                 <input type="text" class="form-control form-control-lg fill focus_style" id="email" name="email" placeholder="Email" required>
                                 <div class="form-control-position">
                                    <i class="ft-user"></i>
                                 </div>
                                 <span class="error"></span>
                              </fieldset>
                              <fieldset class="form-group position-relative has-icon-left">
                                 <input type="password" class="form-control form-control-lg focus_style" id="password" name="password" placeholder="Password">
                                 <div class="form-control-position">
                                    <i class="fa fa-key"></i>
                                 </div>
                                 <span class="error"></span>
                              </fieldset>
                              
                            
                              <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="ft-unlock"></i> Login</button>
                           </form>
                        </div>
                     </div>
                   
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
  