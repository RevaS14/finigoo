
<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">Ride List</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
			</div>
		</div>
		<div class="content-body">
            <?php if($this->session->flashdata('successMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('successMessage'); ?>
                    </div>
            <?php } ?>
			<?php if($this->session->flashdata('errorMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('errorMessage'); ?>
                    </div>
            <?php } ?>

			<section>
				<div id="render-content">

					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-header">
									

								</div>
								<div class="card-content">
									<div class="card-body">
										<?php  if($rideData) { ?>
										<div class="table-responsive">
											<table id="new-cons"
												class="table table-striped table-bordered responsive nowrap"
												style="width: 100%!important;">
												<thead>
													<tr>
														<th>No</th>
														<th>Ordered By</th>
                                                        <th>Accepted By</th>
                                                        <th>Vehicle Type</th>
                                                        <th>Estimated Fare</th>
                                                         <th>Payment Mode</th>
                                                         <th>Ride Status</th>
														
													</tr>
												</thead>
												<tbody>
													<?php $i=1; foreach($rideData as $row) { ?>
													<tr id="hide_66">
														<td><?php echo $i; ?></td>
														<td class="td-icon">
															<?php echo $row->Customer; ?>
														</td>
                                                        <td class="td-icon">
															<?php echo $row->Driver; ?>
														</td>
                                                         <td class="td-icon">
															<?php echo $row->vehicle; ?>
														</td>
                                                        <td class="td-icon">
															<?php echo $row->EstimatedFare; ?>
														</td>
                                                        <td class="td-icon">
															<?php  if($row->PaymentMode=='C') { echo 'Cash On Delivery'; } elseif($row->PaymentMode=='W'){ echo 'Wallet';} ?>
														</td>
                                                        <td class="td-icon">
															<?php if($row->RideStatus=='A') {
                                                                echo 'Accepted';
                                                            }elseif($row->RideStatus=='P'){
                                                                echo 'Pending';
                                                            }elseif($row->RideStatus=='C') {
                                                                echo 'Completed';
                                                            } elseif($row->RideStatus=='D') {
                                                                echo 'Declined';
                                                            } ?>
														</td>

														
													</tr>
													<?php $i++; } ?>

													</tr>
												</tbody>
											</table>
										</div>
										<?php } else { echo 'No Records Found..'; } ?>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

			</section>
		</div>
	</div>
</div>