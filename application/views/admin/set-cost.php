<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">
				<?php if($this->uri->segment(2)!='view-commission') { 
						if($category==1) { echo 'Set Cost Go Mini'; }
							elseif($category==2) {
							echo 'Set Cost Go Micro';
							}
							elseif($category==3) {
								echo 'Set Cost Go Ride';
							}
							elseif($category==4) {
								echo 'Set Cost Go Auto';
							}
					}?>
				</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
				<div class="btn-group float-md-right" role="group">
				<?php if($this->uri->segment(2)!='view-commission') { ?> 
					<a href="<?php echo base_url(); ?>admin/dashboard"
						class="btn btn-primary"> Back</a>
				<?php } else { if($commission) { if($commission->Status=='P') { ?> 
					<a href="<?php echo base_url(); ?>admin/change-commission-status/<?php echo $commission->CommissionId; ?>/C/<?php echo $commission->Franchises_FranchiseId; ?>/<?php echo $commission->JobCategories_JobCategoryId; ?>"
						class="btn btn-primary"> Decline</a>
					<a href="<?php echo base_url(); ?>admin/change-commission-status/<?php echo $commission->CommissionId; ?>/A/<?php echo $commission->Franchises_FranchiseId; ?>/<?php echo $commission->JobCategories_JobCategoryId; ?>"
						class="btn btn-success"> Approve</a>
						<?php } elseif($commission->Status=='A') { ?> 
							<a href="<?php echo base_url(); ?>admin/change-commission-status/<?php echo $commission->CommissionId; ?>/C/<?php echo $commission->Franchises_FranchiseId; ?>/<?php echo $commission->JobCategories_JobCategoryId; ?>"
						class="btn btn-primary"> Decline</a>
						<?php } elseif($commission->Status=='C') { ?> 
							<a href="<?php echo base_url(); ?>admin/change-commission-status/<?php echo $commission->CommissionId; ?>/A/<?php echo $commission->Franchises_FranchiseId; ?>/<?php echo $commission->JobCategories_JobCategoryId; ?>"
						class="btn btn-primary"> Activate</a>
						<?php } } ?>
				<?php } ?>
				</div>
			</div>
		</div>
		<br>
		<div class="content-body">
			<section>
				<div id="render-content">
					<div class="row">
						<div class="col-12">
							<div class="">
							<?php if($commission) { if($commission->Status=='P') { ?>
								<div class="alert alert-danger" role="alert">
                                        Commission not approved by admin...
                                    </div>
							<?php } elseif($commission->Status=='A') { ?> 
								<div class="alert alert-info" role="alert">
                                        Commission currently Active....
                                    </div>
							<?php } elseif($commission->Status=='C') { ?> 
								<div class="alert alert-danger" role="alert">
                                        Commission Declined....
                                    </div>
							<?php }  } ?>
							<?php if($this->session->flashdata('errorMessage')) { ?>
                                    <div class="alert alert-danger" role="alert">
                                           <?php echo $this->session->flashdata('errorMessage'); ?>
                                    </div>
                             <?php } ?>
                              <?php if($this->session->flashdata('successMessage')) { ?>
                                      <div class="alert alert-success" role="alert">
                                             <?php echo $this->session->flashdata('successMessage'); ?>
                                       </div>
                               <?php } ?>
								<div class="">
									<div class="">
										<form id="main" method="post" action="">
											<input type="hidden" name="_token" value="">
											<div class="form-group col-sm-12">
												<div class="card">
													<div class="card-content">
														<div class="card-body">
														
															<div class="row">
																
																	<div class="form-group row col-sm-12">
																		<label class="col-sm-12 col-form-label">Basic
																			Rate (Rs): <sup class="error">*</sup></label>
																		<div class="col-sm-12">
																			<input type="text" class="form-control"
																				name="basicRate" required id="name"
																				placeholder="Basic Rate" value="<?php if($commission) { echo $commission->BaseFare; } ?>">
																			<span class="error"></span>
																		</div>
																	</div>
																
																	<div class="form-group row col-sm-12">
																		<label class="col-sm-12 col-form-label">Minimum
																			Rate (Rs):<sup class="error">*</sup></label>
																		<div class="col-sm-12">
                                                                        <input type="text" class="form-control"
																				name="minimumRate" required id="minimumFare"
																				placeholder="Minimum Rate" value="<?php if($commission) { echo $commission->MinimumFare; } ?>">
																			<span class="error"></span>
																		</div>
                                                                    </div>
                                                                    
                                                                    <div class="form-group row col-sm-12">
																		<label class="col-sm-12 col-form-label">Peak Hour
																			Rate (Rs):<sup class="error">*</sup></label>
																		<div class="col-sm-12">
                                                                        <input type="text" class="form-control"
																				name="peakHourRate" required id="peakHourRate"
																				placeholder="Peak Hour Rate" value="<?php if($commission) { echo $commission->PeakHourBaseFare; } ?>">
																			<span class="error"></span>
																		</div>
																	</div>
																
																	<div class="form-group row col-sm-12">
																		<label class="col-sm-12 col-form-label">Cost Per Unit :
                                                                            <sup class="error">*</sup></label>
																		<div class="col-sm-12">
																			<input type="number" class="form-control"
																				name="perUnit"
																				id="min_order_amount"
																				placeholder="Cost Per Unit"
																				step="1" value="<?php if($commission) { echo $commission->PerUnit; } ?>">
																			<span class="error"></span>
																		</div>
																	</div>
																	<?php if($this->session->userdata('userType')!='F') { ?>
																	<div class="form-group row col-sm-12">
																		<label class="col-sm-12 col-form-label">Admin Charge (%) :
                                                                            <sup class="error">*</sup></label>
																		<div class="col-sm-12">
                                                                        <input type="text" class="form-control"
																				name="adminCharge" required id="adminCharge"
																				placeholder="Admin Charge" value="<?php if($commission) { echo $commission->AdminCharge; } ?>">
																			<span class="error"></span>
																		</div>
                                                                    </div>
																	<?php } else { ?> 
																	
																	<div class="form-group row col-sm-12">
																		<label class="col-sm-12 col-form-label">Admin Charge :
                                                                            <sup class="error"></sup> <?php if($commission) { echo $commission->AdminCharge; } ?>  (%) </label>
													
                                                                    </div>

																	<?php } ?>
                                                                    
																	

                                                                    <div class="form-group row col-sm-12">
																		<label class="col-sm-12 col-form-label">GST (%) :
                                                                            <sup class="error">*</sup></label>
																		<div class="col-sm-12">
                                                                        <input type="text" class="form-control"
																				name="gstPercentage" required id="gstPercentage"
																				placeholder="Gst Percentage" value="<?php if($commission) { echo $commission->GstPercentage; } ?>">
																			<span class="error"></span>
																		</div>
                                                                    </div>
																	<div class="form-group row col-sm-12">
																		<label class="col-sm-12 col-form-label">Zone :
                                                                            <sup class="error">*</sup></label>
																		<div class="col-sm-12">
																		<select class="form-control" id="zone" name="zone">
																		<option value="">Select Zone </option>
																		<?php if($zones) { foreach($zones as $row) { ?>
																			<option value="<?php echo $row->ZoneId; ?>" <?php if($commission) { if($row->ZoneId==$commission->Zones_ZoneId) { echo 'selected'; } } ?>><?php echo $row->Name; ?></option>
																			<?php }} ?>
																		</select>
																			<span class="error"></span>
																		</div>
                                                                    </div>
																	<?php if($commission) { ?>
																	<div class="form-group row col-sm-12">
																		<label class="col-sm-12 col-form-label">Current Status:
                                                                            
                                                                       	<?php if($commission) { 
																					if($commission->Status=='A') {
																						echo 'Active';
																					 } elseif($commission->Status=='P') { 
																						 echo 'Pending';
																					 } elseif($commission->Status=='C') {
																						 echo 'Declined';
																					 }
																				 }  ?>
																			</label>
                                                                    </div>
																	<?php } ?>
                                                                    <input type="hidden" name="jobCategoryId" id="jobCategoryId" value="<?php if($this->uri->segment(2)!='view-commission') { echo $category; } ?>">
															</div>
														</div>
													</div>
												</div>
                                            </div>
                                            
                                            <div class="form-group col-sm-12">
                                            <div class="card">
											<?php if($this->uri->segment(2)=='view-commission') { ?> 
                                                <div class="card-header">
                                                    <h5>Peak Hours</h5>
                                                </div>
												<?php } ?>
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <div class="row">     
														<?php if($this->uri->segment(2)!='view-commission') { ?>                                                           
                                                                <div class="form-group row col-sm-6">
                                                                    <label class="col-sm-4 col-form-label">Start Time:<sup class="error">*</sup></label>
																	<div class="col-sm-8">
																		<div class="input-group date form_time">
																		<input name="startTime" type="text" class="form-control category" placeholder="Start Time" id="startTime" required="" value="" readonly="">
																			<span class="input-group-append" id="basic-addon3">
																			<label class="bg-c-green input-group-text">
																			<span class="fa fa-remove"></span>
																			</label>
																			</span>
																		<span class="input-group-append" id="basic-addon3">
																		<label class="bg-c-green input-group-text">
																		<span class="fa fa-th"></span>
																		</label>
																		</span>
																		</div>
																		<span class="error"></span>
																	</div>
                                                                  
                                                                </div>
                                                                
                                                            
                                                            <div class="form-group row col-sm-6">
                                                                    <label class="col-sm-3 col-form-label">End Time:<sup class="error">*</sup></label>
                                                                    <div class="col-sm-7">
																		<div class="input-group date form_time">
																		<input name="endTime" type="text" class="form-control category" placeholder="End Time" id="endTime" required="" value="" readonly="">
																			<span class="input-group-append" id="basic-addon3">
																			<label class="bg-c-green input-group-text">
																			<span class="fa fa-remove"></span>
																			</label>
																			</span>
																		<span class="input-group-append" id="basic-addon3">
																		<label class="bg-c-green input-group-text">
																		<span class="fa fa-th"></span>
																		</label>
																		</span>
																		</div>
																		<span class="error"></span>
																	</div>
																	<div class="col-sm-2">
                                                                    <button type="button" name="timePickerButton" id="timePickerButton" class="btn btn-success m-b-0">Add</button>
                                                                    </div>
															</div>
														<?php } ?>
															<div class="col-sm-6">
																<div id="hours">
																	<?php if($peakHours) { ?> 
																		<?php if($this->uri->segment(2)!='view-commission') { ?>   
																		<h5>Exisitng Peak Hours</h5>
																		<?php } ?>
																		<table class="hours-table">
																			<thead>
																				<th>Start Time</th>
																				<th>End Time</th>
																				<th>Status</th>
																				<th><?php if($this->uri->segment(2)=='view-commission') { echo 'Change Status'; } ?></th>
																			</thead>
																			<tbody>
																			<?php foreach($peakHours as $peak) { ?>
																				<tr>
																					<td><?php  echo date('g:i A', strtotime($peak->StartTime)); ?></td>
																					<td><?php  echo date('g:i A', strtotime($peak->EndTime));   ?></td>
																					<td><?php if($peak->Status=='P') { echo 'Pending'; } elseif($peak->Status=='A') { echo 'Active'; } elseif($peak->Status=='D') { echo 'Declined'; }  ?></td>
																					<?php if($this->uri->segment(2)!='view-commission') { ?> 
																					<td><a href="" id="<?php  echo $peak->PeakHourId;  ?>" type="button" class="deleteHours" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
																						<i class="ft-trash"></i>
																					</a></td>
																					<?php } else { ?> 
																						<td>
																						<?php if($peak->Status=='P') { ?>
																						<a href="<?php echo base_url(); ?>admin/change-peakhour-status/<?php  echo $peak->PeakHourId;  ?>/A/<?php echo $commission->CommissionId; ?>" id="<?php  echo $peak->PeakHourId;  ?>" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="">
																						Approve
																						</a>
																						<?php } elseif($peak->Status=='A') { ?> 
																							<a href="<?php echo base_url(); ?>admin/change-peakhour-status/<?php  echo $peak->PeakHourId;  ?>/D/<?php echo $commission->CommissionId; ?>" id="" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="">
																						Decline
																						</a>
																						<?php } elseif($peak->Status=='D') { ?> 
																							<a href="<?php echo base_url(); ?>admin/change-peakhour-status/<?php  echo $peak->PeakHourId;  ?>/A/<?php echo $commission->CommissionId; ?>" id="" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="">
																						Activate
																						</a>
																						<?php } ?>
																					</td>
																					<?php } ?>
																				</tr>
																			<?php } ?> 
																			</tbody>
																		</table>
																	<?php } ?>	
															</div>
															</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										<?php if($this->uri->segment(2)!='view-commission') { ?> 
											<div class="form-group row">
												<div class="col-sm-12">
													<center>
														<button type="submit" name="save"
															class="btn btn-primary m-b-0">Save</button>
													</center>
												</div>
											</div>
											<?php } ?>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>

			</section>
		</div>
	</div>
</div>