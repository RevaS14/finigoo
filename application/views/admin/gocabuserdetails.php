<div class="app-content content">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">Driver Details</h3>
			</div>
			<div class="content-header-right col-md-6 col-12"></div>
		</div>
		<div class="content-body">
			<section>
				<div id="render-content">
					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h5>Driver Details as Below
                           <a href="<?php echo base_url(); ?>admin/alltransactions/<?php echo $userDetails[0]->UserId;?>"
                                 class="btn grey btn-outline-secondary float-right">All Transactions</a>
                               
                           </h5>
								</div>
								<div class="card-content">
									<div class="card-body">
                    <?php if(isset($_GET['view'])) {
                      ?>
					<form id="main" method="post"
                        action="<?php echo site_url();?>admin/listdrivers/update"
                        enctype="multipart/form-data">
                        <input type="hidden" name="JobCategory" value="<?php echo $userDetails[0]->JobCategory_JobCategoryId;?>">
						<input type="hidden" name="redirectView" value="gocab">
						<input type="hidden" name="UserId" value="<?php echo $userDetails[0]->UserId;?>">
						<input type="hidden" name="VehicleID" value="<?php echo $userDetails[0]->VehicleID;?>">
						<input type="hidden" name="DriverId" value="<?php echo $userDetails[0]->DriverId;?>">
                        <input type="hidden" name="_token" value="">
                        <div class="row">
                           <span id="service_category_append"></span>
                           <div class="form-group col-sm-12">
                              <div class="card">
                                 <!-- <div class="card-header">
                                    <h5>Add Driver of Bike Ride</h5>
                                    </div> -->
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label">First Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Firstname"
                                                required id="Firstname" placeholder="First Name"value="<?php echo $userDetails[0]->FirstName;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Last Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Lastname"
                                                required id="Lastname" placeholder="Last Name" value="<?php echo $userDetails[0]->LastName;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Email:<sup
                                                class="error">*</sup></label>
                                             <input type="email" class="form-control" name="Email"
                                                required id="email" placeholder="Unique Email" value="<?php echo $userDetails[0]->Email;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class=" col-form-label">Mobile Phone:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control"
                                                name="Phone" required id="Phone"
                                                placeholder="Unique Contact Number" value="<?php echo $userDetails[0]->Phone;?>" readonly>
                                             <span class="error"></span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Place of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PlaceOfBirth" required
                                                id="POB" placeholder="Place of Birth" value="<?php echo $userDetails[0]->PlaceOfBirth;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Date of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="Date" class="form-control" name="DOB" required
                                                id="POB" placeholder="Date of Birth" value="<?php echo $userDetails[0]->DOB;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Identity number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="IdentityNo"
                                                required id="In" placeholder="Identity number" value="<?php echo $userDetails[0]->IdentityNo;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">PinCode<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PinCode"
                                                required id="PC" placeholder="Pin Code" value="<?php echo $userDetails[0]->Pincode;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Country<sup
                                                class="error">*</sup></label>
                                             <select class="form-control" id="Country" name="Country" disabled="true">
												 <?php if($userDetails[0]->Establishment_EstablishmentId == 1) { ?>
												  <option value="1" selected>India</option>
												<?php } ?>
                                             </select>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Driver Name<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="DriverName" required
                                                id="Pc" placeholder="Driver Name" value="<?php echo $userDetails[0]->DriverName;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Driver Licence No<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="DriverLicenseNo" required
                                                id="Pc" placeholder="Driver Licence No" value="<?php echo $userDetails[0]->DrivingLicense;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo
                                             Vehicle Permit:</label>	
                                             <div id="vehicle-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->VehiclePermit;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;"">
                                                <label for="vehicle-upload" id="vehicle-label"><a href="<?php echo base_url().'/'. $userDetails[0]->VehiclePermit;?>" target="new">View
                                                Image</a></label>
                                                <input type="file" id="vehicle-upload"
												   name="VehiclePermit" accept=".jpg,.jpeg,.png" value="<?php echo $userDetails[0]->VehiclePermit;?>" disabled/>
												   <input type="hidden" id="vehicle-upload"
												   name="VehiclePermitOld" value="<?php echo $userDetails[0]->VehiclePermit;?>"/>
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo Driver
                                             license:</label>
                                             <div id="driver-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->DrivingLicenseDocument;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="driver-upload" id="driver-label"><a href="<?php echo base_url().'/'. $userDetails[0]->DrivingLicenseDocument;?>" target="new">View
                                                Image</a></label>
                                                <input type="file" id="driver-upload"
												   name="DriverLicense" accept=".jpg,.jpeg,.png" disabled/>
												   <input type="hidden" id="driver-upload"
												   name="DriverLicenseOld" value="<?php echo $userDetails[0]->DrivingLicenseDocument;?>"/>
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo
                                             Profile:</label>
                                             <div id="profile-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->DisplayPicture;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="profile-upload" id="profile-label"><a href="<?php echo base_url().'/'. $userDetails[0]->DisplayPicture;?>" target="new">View
                                                Image</a></label>
                                                <input type="file" id="profile-upload"
												   name="DisplayPicture" accept=".jpg,.jpeg,.png" disabled />
												   <input type="hidden" id="profile-upload"
                                                   name="ProfileAvatarOld" value="<?php echo $userDetails[0]->DisplayPicture;?>" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Identity
                                             Document Upload:</label>
                                             <div id="identity-image-preview"  style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->IdentityDocument;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="identity-upload" id="identity-label"><a href="<?php echo base_url().'/'. $userDetails[0]->IdentityDocument;?>" target="new">View
                                                Image</a></label>
                                                <input type="file" id="identity-upload"
												   name="IdentityDocument" accept=".jpg,.jpeg,.png" disabled/>
												   <input type="hidden" id="identity-upload"
                                                   name="IdentityDocumentOld" value="<?php echo $userDetails[0]->IdentityDocument;?>" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:	
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
						   </div>
                           <div class="form-group col-sm-8">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Vehicle Data
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Brand:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleBrand"
                                                required id="VehicleBrand" placeholder="Vehicle Brand"
                                                value="<?php echo $userDetails[0]->VehicleBrand;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Type:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleType"
                                                required id="VehicleType" placeholder="Vehicle Type"
                                                value="<?php echo $userDetails[0]->VehicleType;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleNumber"
                                                required id="VehicleNumber" placeholder="Vehicle Number"
                                                value="<?php echo $userDetails[0]->VehicleNumber;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Color:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleColor"
                                                required id="VehicleColor" placeholder="Vehicle Color"
                                                value="<?php echo $userDetails[0]->VehicleColor;?>" readonly>
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-sm-4">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Terms and Services
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <ol>
                                             <li>Driver must provide their own car</li>
                                             <li>Driver must have a personal Android device</li>
                                             <li>Driver must have Driver license which is still active at
                                                least 6 months ahead
                                             </li>
                                          </ol>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
					 </form>
					 
												 <?php } else if(isset($_GET['update'])) {
                                        
                                        ?>
													<form id="main" method="post"
                        action="<?php echo site_url();?>admin/listdrivers/update"
                        enctype="multipart/form-data">
                        <input type="hidden" name="JobCategory" value="<?php echo $userDetails[0]->JobCategory_JobCategoryId;?>">
						<input type="hidden" name="redirectView" value="<?php echo $userDetails[0]->JobCategory_JobCategoryId;?>">
						<input type="hidden" name="UserId" value="<?php echo $userDetails[0]->UserId;?>">
						<input type="hidden" name="VehicleID" value="<?php echo $userDetails[0]->VehicleID;?>">
						<input type="hidden" name="DriverId" value="<?php echo $userDetails[0]->DriverId;?>">
                        <input type="hidden" name="_token" value="">
                        <div class="row">
                           <span id="service_category_append"></span>
                           <div class="form-group col-sm-12">
                              <div class="card">
                                 <!-- <div class="card-header">
                                    <h5>Add Driver of Bike Ride</h5>
                                    </div> -->
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label">First Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Firstname"
                                                required id="Firstname" placeholder="First Name"value="<?php echo $userDetails[0]->FirstName;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Last Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Lastname"
                                                required id="Lastname" placeholder="Last Name" value="<?php echo $userDetails[0]->LastName;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Email:<sup
                                                class="error">*</sup></label>
                                             <input type="email" class="form-control" name="Email"
                                                required id="email" placeholder="Unique Email" value="<?php echo $userDetails[0]->Email;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class=" col-form-label">Mobile Phone:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control"
                                                name="Phone" required id="Phone"
                                                placeholder="Unique Contact Number" value="<?php echo $userDetails[0]->Phone;?>">
                                             <span class="error"></span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Place of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PlaceOfBirth" required
                                                id="POB" placeholder="Place of Birth" value="<?php echo $userDetails[0]->PlaceOfBirth;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Date of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="Date" class="form-control" name="DOB" required
                                                id="POB" placeholder="Date of Birth" value="<?php echo $userDetails[0]->DOB;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Identity number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="IdentityNo"
                                                required id="In" placeholder="Identity number" value="<?php echo $userDetails[0]->IdentityNo;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">PinCode<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PinCode"
                                                required id="PC" placeholder="Pin Code" value="<?php echo $userDetails[0]->Pincode;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Country<sup
                                                class="error">*</sup></label>
                                             <select class="form-control" id="Country" name="Country">
												 <?php if($userDetails[0]->Establishment_EstablishmentId == 1) { ?>
												  <option value="1" selected>India</option>
												<?php } ?>
                                             </select>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Driver Name<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="DriverName" required
                                                id="Pc" placeholder="Driver Name" value="<?php echo $userDetails[0]->DriverName;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Driver Licence No<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="DriverLicenseNo" required
                                                id="Pc" placeholder="Driver Licence No" value="<?php echo $userDetails[0]->DrivingLicense;?>">
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo
                                             Vehicle Permit:</label>	
                                             <div id="vehicle-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->VehiclePermit;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;"">
                                                <label for="vehicle-upload" id="vehicle-label">Upload
                                                Image</label>
                                                <input type="file" id="vehicle-upload"
												   name="VehiclePermit" accept=".jpg,.jpeg,.png" value="<?php echo $userDetails[0]->VehiclePermit;?>"/>
												   <input type="hidden" id="vehicle-upload"
												   name="VehiclePermitOld" value="<?php echo $userDetails[0]->VehiclePermit;?>"/>
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo Driver
                                             license:</label>
                                             <div id="driver-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->DrivingLicenseDocument;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="driver-upload" id="driver-label">Upload
                                                Image</label>
                                                <input type="file" id="driver-upload"
												   name="DriverLicense" accept=".jpg,.jpeg,.png" />
												   <input type="hidden" id="driver-upload"
												   name="DriverLicenseOld" value="<?php echo $userDetails[0]->DrivingLicenseDocument;?>"/>
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo
                                             Profile:</label>
                                             <div id="profile-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->DisplayPicture;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="profile-upload" id="profile-label">Upload
                                                Image</label>
                                                <input type="file" id="profile-upload"
												   name="DisplayPicture" accept=".jpg,.jpeg,.png" />
												   <input type="hidden" id="profile-upload"
                                                   name="ProfileAvatarOld" value="<?php echo $userDetails[0]->DisplayPicture;?>" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Identity
                                             Document Upload:</label>
                                             <div id="identity-image-preview"  style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->IdentityDocument;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="identity-upload" id="identity-label">Upload
                                                Image</label>
                                                <input type="file" id="identity-upload"
												   name="IdentityDocument" accept=".jpg,.jpeg,.png" />
												   <input type="hidden" id="identity-upload"
                                                   name="IdentityDocumentOld" value="<?php echo $userDetails[0]->IdentityDocument;?>" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:	
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
						   </div>
                           <div class="form-group col-sm-8">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Vehicle Data
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Brand:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleBrand"
                                                required id="VehicleBrand" placeholder="Vehicle Brand"
                                                value="<?php echo $userDetails[0]->VehicleBrand;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Type:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleType"
                                                required id="VehicleType" placeholder="Vehicle Type"
                                                value="<?php echo $userDetails[0]->VehicleType;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleNumber"
                                                required id="VehicleNumber" placeholder="Vehicle Number"
                                                value="<?php echo $userDetails[0]->VehicleNumber;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Color:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleColor"
                                                required id="VehicleColor" placeholder="Vehicle Color"
                                                value="<?php echo $userDetails[0]->VehicleColor;?>">
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-sm-4">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Terms and Services
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <ol>
                                             <li>Driver must provide their own car</li>
                                             <li>Driver must have a personal Android device</li>
                                             <li>Driver must have Driver license which is still active at
                                                least 6 months ahead
                                             </li>
                                          </ol>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-sm-12">
                              <center>
                                 <button type="submit" class="btn btn-success m-b-0">Submit</button>
                              </center>
                           </div>
                        </div>
					 </form>
												 <?php } else if(isset($_GET['delete'])){?>
													<form id="main" method="post"
                        action="<?php echo site_url();?>admin/listdrivers/delete"
                        enctype="multipart/form-data">
                        <input type="hidden" name="JobCategory" value="1">
						<input type="hidden" name="redirectView" value="gocab">
						<input type="hidden" name="UserId" value="<?php echo $userDetails[0]->UserId;?>">
						<input type="hidden" name="VehicleID" value="<?php echo $userDetails[0]->VehicleID;?>">
						<input type="hidden" name="DriverId" value="<?php echo $userDetails[0]->DriverId;?>">
                        <input type="hidden" name="_token" value="">
                        <div class="row">
                           <span id="service_category_append"></span>
                           <div class="form-group col-sm-12">
						   <div class="form-group col-sm-12">
                              <center>
								 <button type="submit" class="btn btn-success m-b-0">Delete</button>
								 <a href="<?php echo base_url();?>admin/listdrivers/gocab" ?><button type="button" class="btn btn-success m-b-0">Cancel</button></a>
                              </center>
                           </div>
                              <div class="card">
                                 <!-- <div class="card-header">
                                    <h5>Add Driver of Bike Ride</h5>
                                    </div> -->
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label">First Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Firstname"
                                                required id="Firstname" placeholder="First Name"value="<?php echo $userDetails[0]->FirstName;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Last Name:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="Lastname"
                                                required id="Lastname" placeholder="Last Name" value="<?php echo $userDetails[0]->LastName;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Email:<sup
                                                class="error">*</sup></label>
                                             <input type="email" class="form-control" name="Email"
                                                required id="email" placeholder="Unique Email" value="<?php echo $userDetails[0]->Email;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class=" col-form-label">Mobile Phone:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control"
                                                name="Phone" required id="Phone"
                                                placeholder="Unique Contact Number" value="<?php echo $userDetails[0]->Phone;?>">
                                             <span class="error"></span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Place of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PlaceOfBirth" required
                                                id="POB" placeholder="Place of Birth" value="<?php echo $userDetails[0]->PlaceOfBirth;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Date of Birth:<sup
                                                class="error">*</sup></label>
                                             <input type="Date" class="form-control" name="DOB" required
                                                id="POB" placeholder="Date of Birth" value="<?php echo $userDetails[0]->DOB;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Identity number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="IdentityNo"
                                                required id="In" placeholder="Identity number" value="<?php echo $userDetails[0]->IdentityNo;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">PinCode<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="PinCode"
                                                required id="PC" placeholder="Pin Code" value="<?php echo $userDetails[0]->Pincode;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Country<sup
                                                class="error">*</sup></label>
                                             <select class="form-control" id="Country" name="Country">
												 <?php if($userDetails[0]->Establishment_EstablishmentId == 1) { ?>
												  <option value="1" selected>India</option>
												<?php } ?>
                                             </select>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Driver Name<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="DriverName" required
                                                id="Pc" placeholder="Driver Name" value="<?php echo $userDetails[0]->DriverName;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label">Driver Licence No<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="DriverLicenseNo" required
                                                id="Pc" placeholder="Driver Licence No" value="<?php echo $userDetails[0]->DrivingLicense;?>">
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo
                                             Vehicle Permit:</label>	
                                             <div id="vehicle-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->VehiclePermit;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;"">
                                                <label for="vehicle-upload" id="vehicle-label">Upload
                                                Image</label>
                                                <input type="file" id="vehicle-upload"
												   name="VehiclePermit" accept=".jpg,.jpeg,.png" value="<?php echo $userDetails[0]->VehiclePermit;?>"/>
												   <input type="hidden" id="vehicle-upload"
												   name="VehiclePermitOld" value="<?php echo $userDetails[0]->VehiclePermit;?>"/>
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo Driver
                                             license:</label>
                                             <div id="driver-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->DrivingLicenseDocument;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="driver-upload" id="driver-label">Upload
                                                Image</label>
                                                <input type="file" id="driver-upload"
												   name="DriverLicense" accept=".jpg,.jpeg,.png" />
												   <input type="hidden" id="driver-upload"
												   name="DriverLicenseOld" value="<?php echo $userDetails[0]->DrivingLicenseDocument;?>"/>
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Photo
                                             Profile:</label>
                                             <div id="profile-image-preview" style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->DisplayPicture;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="profile-upload" id="profile-label">Upload
                                                Image</label>
                                                <input type="file" id="profile-upload"
												   name="DisplayPicture" accept=".jpg,.jpeg,.png" />
												   <input type="hidden" id="profile-upload"
                                                   name="ProfileAvatarOld" value="<?php echo $userDetails[0]->DisplayPicture;?>" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-4">
                                             <label class="col-form-label image image-label">Identity
                                             Document Upload:</label>
                                             <div id="identity-image-preview"  style="background-image : url('<?php echo base_url().'/'. $userDetails[0]->IdentityDocument;?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                                                <label for="identity-upload" id="identity-label">Upload
                                                Image</label>
                                                <input type="file" id="identity-upload"
												   name="IdentityDocument" accept=".jpg,.jpeg,.png" />
												   <input type="hidden" id="identity-upload"
                                                   name="IdentityDocumentOld" value="<?php echo $userDetails[0]->IdentityDocument;?>" />
                                             </div>
                                             <span class="note">[Note: Format File jpg, jpeg, png Max:	
                                             1MB]</span>
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
						   </div>
                           <div class="form-group col-sm-8">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Vehicle Data
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Brand:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleBrand"
                                                required id="VehicleBrand" placeholder="Vehicle Brand"
                                                value="<?php echo $userDetails[0]->VehicleBrand;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Type:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleType"
                                                required id="VehicleType" placeholder="Vehicle Type"
                                                value="<?php echo $userDetails[0]->VehicleType;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Number:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleNumber"
                                                required id="VehicleNumber" placeholder="Vehicle Number"
                                                value="<?php echo $userDetails[0]->VehicleNumber;?>">
                                             <span class="error"></span>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="col-form-label">Vehicle Color:<sup
                                                class="error">*</sup></label>
                                             <input type="text" class="form-control" name="VehicleColor"
                                                required id="VehicleColor" placeholder="Vehicle Color"
                                                value="<?php echo $userDetails[0]->VehicleColor;?>">
                                             <span class="error"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-sm-4">
                              <div class="card">
                                 <div class="card-header">
                                    <h5>Terms and Services
                                    </h5>
                                 </div>
                                 <div class="card-content">
                                    <div class="card-body">
                                       <div class="row">
                                          <ol>
                                             <li>Driver must provide their own car</li>
                                             <li>Driver must have a personal Android device</li>
                                             <li>Driver must have Driver license which is still active at
                                                least 6 months ahead
                                             </li>
                                          </ol>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                          
                        </div>
					 </form>
												 <?php } ?>
									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
<style>
    #new-cons td, #new-cons th {display:table-cell !important;}
 </style>

