<style>
	/*datatable style*/
	table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before,
	table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
		background: #55d090;
	}

	.page-item.active .page-link {
		background: #55d090;
		border-color: #55d090;
	}

	.page-link {
		color: #55d090;
	}

	/*datatable td link*/
	.document a,
	.ride a,
	.ratings a {
		color: #55d090;
		font-weight: bold;
		font-size: 14px;
	}

	.document i,
	.ride i,
	.ratings i {
		font-size: 18px;
	}

	.ratings i {
		font-size: 16px;
	}

	.icon-list-demo i {
		height: auto;
		line-height: 10px;
		border: none;
		margin-right: 5px;
		color: #55d090;
	}

	/* Status style*/
	.toggle {
		display: inline-block;
	}

	.toggle input[type="checkbox"]:checked+.button-indecator:before {
		color: #55d090;
	}

	.toggle input[type="checkbox"]+.button-indecator:before {
		color: #55d090;
	}

	/* Vehicle type styles for the modal */
	.md-perspective,
	.md-perspective body {
		height: 100%;
		overflow: hidden;
	}

	.md-perspective body {
		background: #222;
		-webkit-perspective: 600px;
		-moz-perspective: 600px;
		perspective: 600px;
	}

	.md-modal {
		position: fixed;
		top: 50%;
		left: 50%;
		/*width: 50%;*/
		width: 30%;
		max-width: 630px;
		min-width: 300px;
		height: auto;
		z-index: 2000;
		visibility: hidden;
		-webkit-backface-visibility: hidden;
		-moz-backface-visibility: hidden;
		backface-visibility: hidden;
		-webkit-transform: translateX(-50%) translateY(-50%);
		-moz-transform: translateX(-50%) translateY(-50%);
		-ms-transform: translateX(-50%) translateY(-50%);
		transform: translateX(-50%) translateY(-50%);
	}

	.md-show {
		visibility: visible;
	}

	.md-overlay {
		position: fixed;
		width: 100%;
		height: 100%;
		visibility: hidden;
		top: 0;
		left: 0;
		z-index: 1000;
		opacity: 0;
		background: rgba(55, 58, 60, 0.65);
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
	}

	.md-show~.md-overlay {
		opacity: 1;
		visibility: visible;
	}

	/* Content styles */
	.md-content {
		color: #666666;
		background: #fff;
		position: relative;
		border-radius: 3px;
		margin: 0 auto;
	}

	.md-content h3 {
		color: #fff;
		margin: 0;
		/*padding: 0.4em;*/
		padding: 0.6em 0.4em 0.6em 1em;
		text-align: left;
		font-weight: 400;
		font-size: 1.5em;
		opacity: 0.8;
		border-radius: 3px 3px 0 0;
	}

	.md-content>div {
		padding: 15px 25px 30px 25px;
		margin: 0;
		font-size: 1em;
		/*font-weight: 300;*/
		/*font-size: 1.15em;*/
	}

	.md-content>div>div {
		width: 40%;
		margin: 0 auto;
		padding: 10px 0;
		justify-content: space-around;
		display: flex;
	}

	.md-content>div>div>img {
		border-radius: 50%;
		padding: 4px;
		border: 2px solid #2ed8b6;
	}

	.md-content>div ul {
		margin: 0;
		padding: 0 0 30px 0;
	}

	.md-content>div ul li {
		padding: 5px 0;
	}

	/* Individual modal styles with animations/transitions */
	.md-effect-1 .md-content {
		-webkit-transform: scale(0.7);
		-moz-transform: scale(0.7);
		-ms-transform: scale(0.7);
		transform: scale(0.7);
		opacity: 0;
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
	}

	.md-show.md-effect-1 .md-content {
		-webkit-transform: scale(1);
		-moz-transform: scale(1);
		-ms-transform: scale(1);
		transform: scale(1);
		opacity: 1;
	}

	.md-trigger:hover {
		color: #64b0f2;
		cursor: pointer;
	}

	.md-trigger img:hover {
		opacity: 0.7;
		cursor: pointer;
	}
</style>

<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">Commissions List</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
			</div>
		</div>
		<div class="content-body">
            <?php if($this->session->flashdata('successMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('successMessage'); ?>
                    </div>
            <?php } ?>
			<?php if($this->session->flashdata('errorMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('errorMessage'); ?>
                    </div>
            <?php } ?>
			<section>
				<div id="render-content">

					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h5> <?php if($status=='P') { echo 'Pending'; } elseif($status=='A') { echo 'Approved'; } elseif($status=='D') { echo 'Declined'; } ?> Commissions List </h5>
								</div>
								<div class="card-content">
									<div class="card-body">
										<?php  if($commission) { ?>
										<div class="table-responsive">
											<table id="new-cons"
												class="table table-striped table-bordered responsive nowrap"
												style="width: 100%!important;">
												<thead>
													<tr>
														<th>No</th>
														<th>Franchise Name</th>
                                                        <th>Category</th>
														<th>Basic Rate (Rs)</th>
                                                        <th>Minimum Rate (Rs)</th>
                                                        <th>Peak Hour Rate (Rs)</th>
														<th>Per Unit</th>
                                                        <th>Admin Charge (%)</th>
                                                        <th>Franchise Charge (%)</th>
                                                        <th>Gst Percentage (%) </th>
														<th data-priority="3">Actions</th>
													</tr>
												</thead>
												<tbody>
													<?php $i=1; foreach($commission as $row) { ?>
													<tr id="hide_66">
														<td><?php echo $i; ?></td>
														<td class="td-icon">
															<?php echo $row->FranchiseName; ?>
														</td>
                                                        <td class="td-icon">
															<?php echo $row->CategoryName; ?>
														</td>
														<td class="td-icon">
															<?php echo $row->BaseFare; ?>
														</td>
                                                        <td class="td-icon">
															<?php echo $row->MinimumFare; ?>
														</td>
                                                        <td class="td-icon">
															<?php echo $row->PeakHourBaseFare; ?>
														</td>
														<td><?php echo $row->PerUnit; ?></td>
                                                        <td class="td-icon">
															<?php echo $row->AdminCharge; ?>
														</td>
                                                        <td class="td-icon">
															<?php echo $row->FranchiseCharge; ?>
														</td>
                                                        <td class="td-icon">
															<?php echo $row->GstPercentage; ?>
														</td>
														<td class="action">
														
															<a href="<?php echo base_url(); ?>admin/view-commission/<?php echo $row->CommissionId; ?>" class="delete btn btn-outline-primary"
																data-toggle="tooltip" data-placement="top" title="View">
																<i class="fa fa-eye"></i>
                                                            </a>
														</td>
													</tr>
													<?php $i++; } ?>

													</tr>
												</tbody>
											</table>
										</div>
										<?php } else { echo 'No Records Found..'; } ?>
									</div>
								</div>
							</div>
						</div>
					</div>

			</section>
		</div>
	</div>
</div>