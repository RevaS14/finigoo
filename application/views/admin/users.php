
<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">Users List</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
			</div>
		</div>
		<div class="content-body">
            <?php if($this->session->flashdata('successMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('successMessage'); ?>
                    </div>
            <?php } ?>
			<?php if($this->session->flashdata('errorMessage')) { ?>
                <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('errorMessage'); ?>
                    </div>
            <?php } ?>

			<section>
				<div id="render-content">

					<div class="row">
						<div class="col-12">
							<div class="card">
								
								<div class="card-content">
									<div class="card-body">
										<?php  if($users) { ?>
										<div class="table-responsive">
											<table id="new-cons"
												class="table table-striped table-bordered responsive nowrap"
												style="width: 100%!important;">
												<thead>
													<tr>
														<th>No</th>
														<th>Name</th>
														<th>Email</th>
														<th>Contact No.</th>
														<th>Status</th>
														<th data-priority="3">Actions</th>
													</tr>
												</thead>
												<tbody>
													<?php $i=1; foreach($users as $allData) { ?>
													<tr id="hide_66">
														<td><?php echo $i; ?></td>
														<td class="td-icon">
															<?php echo $allData->FirstName; ?>
														</td>

														<td class="td-icon">
															<?php echo $allData->Email; ?>
														</td>

														<td><?php echo $allData->Phone; ?></td>

														<td><?php if($allData->Status=='A') { echo 'Active'; } elseif($allData->Status=='D') { echo 'Deactive'; } elseif($allData->Status=='P') { echo 'Deactive'; } ?></td>

														<td class="action">
															<span class="toggle"><label>
																	<input name="manual_assign"
																		class="form-control store_status <?php if($allData->Status=='A') { echo 'block'; } else { echo 'unblock'; } ?>"
																		type="checkbox" <?php if($allData->Status=='A') { echo 'checked'; } ?>
																		id="<?php echo $allData->UserId; ?>"><span
																		class="button-indecator" data-toggle="tooltip"
																		data-placement="top"
																		title="<?php if($allData->Status=='A') { echo 'Active'; }else { echo 'Deactive'; }; ?>"></span></label>
															</span>
															<a href="<?php echo base_url(); ?>admin/view-user/<?php echo $allData->UserId; ?>" class="delete btn btn-outline-primary"
																data-toggle="tooltip" data-placement="top" title="VIew">
																<i class="ft-eye"></i>
                                                            </a>
                                                            <a href="<?php echo base_url(); ?>admin/delete-user/<?php echo $allData->UserId; ?>" class="delete btn btn-outline-primary"
																data-toggle="tooltip" data-placement="top" title="Delete">
																<i class="ft-trash"></i>
															</a>
														</td>
													</tr>
													<?php $i++; } ?>

													</tr>
												</tbody>
											</table>
										</div>
										<?php } else { echo 'No Records Found..'; } ?>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

			</section>
		</div>
	</div>
</div>