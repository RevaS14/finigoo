
    <div class="container fluid">
      <div class="alert alert-info">
        <strong> Please wait.... </strong>
      </div>
    
      <form id="redirectForm" method="post" action="<?php echo base_url(); ?>admin/request">

          <input type="hidden" class="form-control" name="appId" value="7657941ffa88c2a0a358831f7567" placeholder="Enter App ID here (Ex. 123456a7890bc123defg4567)"/>

          <input type="hidden" class="form-control" name="orderId" value="<?php echo $orderId; ?>" placeholder="Enter Order ID here (Ex. order00001)"/>
 

          <input type="hidden" class="form-control" name="orderAmount" value="<?php echo $amount; ?>" placeholder="Enter Order Amount here (Ex. 100)"/>
       
          <input type="hidden" class="form-control" name="orderCurrency" value="INR" placeholder="Enter Currency here (Ex. INR)"/>

          <input type="hidden" class="form-control" name="orderNote" value="<?php echo $note; ?>" placeholder="Enter Order Note here (Ex. Test order)"/>
  

          <input type="hidden" class="form-control" name="customerName" value="Revathi" placeholder="Enter your name here (Ex. John Doe)"/>

          <input type="hidden" class="form-control" name="customerEmail" value="revathi@haqto.in" placeholder="Enter your email address here (Ex. Johndoe@test.com)"/>
     

          <input type="hidden" class="form-control" name="customerPhone" value="9994157428" placeholder="Enter your phone number here (Ex. 9999999999)"/>
     
          <input type="hidden" class="form-control" name="returnUrl" value="<?php echo base_url(); ?>admin/response" placeholder="Enter the URL to which customer will be redirected (Ex. www.example.com)"/>

          <input type="hidden" class="form-control" name="notifyUrl" value="<?php echo base_url(); ?>admin/notify" placeholder="Enter the URL to get server notificaitons (Ex. www.example.com)"/>
  
        <button type="submit" class="btn btn-primary btn-block" value="Pay">Submit</button>
        <br> 
        <br>
      </form>
    </div>
    <br>    
    <br>    
    <br>    
    <br>    
 