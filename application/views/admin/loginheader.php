<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
   <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title> Login </title>
    <link rel="icon" href="#" type="image/x-icon">
    <link   href="<?php echo base_url(); ?>assets/css/font.css"  rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/vendors.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/app.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/extensions/toastr.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sweetalert.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/style-changes.css">
    <!-- END Custom CSS-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/widget/widget.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/extensions/unslider.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/weather-icons/climacons.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/weather-icons/climacons.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/meteocons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/charts/morris.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/core/colors/palette-gradient.css">
      <!-- END Custom CSS-->
      <style>
   .card-header { padding: 0; }
   .login-block {
   margin: 4% auto 0;
   /*margin: 80px auto;*/
   min-height: 0;
   }
   /*admin role box radio button style*/
   input[type="radio"] {
   display: none;
   }
   input[type="radio"] + .label {
   position: relative;
   margin-left: 43%;
   display: block;
   padding-left: 20px;
   margin-right: 10px;
   cursor: pointer;
   line-height: 16px;
   transition: all .2s ease-in-out;
   margin-bottom: 10px;
   }
   input[type="radio"] + .label:before, input[type="radio"] > .label:after {
   content: '';
   position: absolute;
   top: -9px;
   left: 0;
   width: 20px;
   height: 20px;
   text-align: center;
   color: #f5f5f5;
   font-family: Times;
   border-radius: 50%;
   transition: all .3s ease;
   }
   input[type="radio"] + .label:before {
   /*box-shadow: inset 0 0 0 1px #666565, inset 0 0 0 16px #FFFFFF, inset 0 0 0 16px #00A5A8;*/
  /* box-shadow: 0 0 0 0 #91DEAC, inset 0 0 0 2px #FFFFFF, inset 0 0 0 3px #00A5A8, inset 0 0 0 16px #FFFFFF, inset 0 0 0 16px #00A5A8;*/
  box-shadow: 0 0 0 0 #f44336, inset 0 0 0 2px #FFFFFF, inset 0 0 0 3px #f44336, inset 0 0 0 16px #FFFFFF, inset 0 0 0 16px #f44336;
   }
   input[type="radio"] + .label:hover {
   color: #00A5A8;
   }
   input[type="radio"] + .label:hover:before {
   animation-duration: .5s;
   animation-name: change-size;
   animation-iteration-count: infinite;
   animation-direction: alternate;
   box-shadow: inset 0 0 0 1px #00A5A8, inset 0 0 0 16px #FFFFFF, inset 0 0 0 16px #00A5A8;
   }
   input[type="radio"]:checked + .label:hover {
   color: #333333;
   cursor: default;
   }
   input[type="radio"]:checked + .label:before {
   animation-duration: .2s;
   animation-name: select-radio;
   animation-iteration-count: 1;
   animation-direction: Normal;
   /*box-shadow: inset 0 0 0 1px #00A5A8, inset 0 0 0 3px #FFFFFF, inset 0 0 0 16px #00A5A8;*/
   box-shadow: inset 0 0 0 1px #f44336, inset 0 0 0 3px #FFFFFF, inset 0 0 0 16px #f44336;
   }
   @keyframes  change-size {
   from {
   box-shadow: 0 0 0 0 #00A5A8, inset 0 0 0 1px #00A5A8, inset 0 0 0 16px #FFFFFF, inset 0 0 0 16px #00A5A8;
   }
   to {
   box-shadow: 0 0 0 1px #00A5A8, inset 0 0 0 1px #00A5A8, inset 0 0 0 16px #FFFFFF, inset 0 0 0 16px #00A5A8;
   }
   }
   @keyframes  select-radio {
   0% {
   box-shadow: 0 0 0 0 #91DEAC, inset 0 0 0 2px #FFFFFF, inset 0 0 0 3px #00A5A8, inset 0 0 0 16px #FFFFFF, inset 0 0 0 16px #00A5A8;
   }
   90% {
   box-shadow: 0 0 0 10px #E8FFF0, inset 0 0 0 0 #FFFFFF, inset 0 0 0 1px #00A5A8, inset 0 0 0 2px #FFFFFF, inset 0 0 0 16px #00A5A8;
   }
   100% {
   box-shadow: 0 0 0 12px #E8FFF0, inset 0 0 0 0 #FFFFFF, inset 0 0 0 1px #00A5A8, inset 0 0 0 3px #FFFFFF, inset 0 0 0 16px #00A5A8;
   }
   }
   /*admin roles box style*/
   .roles-box {
   margin-bottom: 15px;
   }
   .roles-box-last-child {
   margin-bottom: 0;
   }
   .roles-wrapper-box {
   align-content: center;
   width: auto;
   height: 64px;
   cursor: pointer;
   }
   .roles-wrapper-box:hover .roles-label-box {
   color: white;
   cursor: pointer;
   /*background-color: #00A5A8;*/
   background-color: #f44336;
   animation-duration: .5s;
   animation-name: change-label-size;
   animation-iteration-count: infinite;
   animation-direction: alternate;
   /*box-shadow: 0 0 0 1px #00A5A8, 0 0 0 16px #FFFFFF, 0 0 0 16px #00A5A8;*/
   box-shadow: 0 0 0 1px #f44336, 0 0 0 16px #FFFFFF, 0 0 0 16px #f44336;
   }
   .roles-label-box {
   display: block;
   text-align: center;
   margin-top: -10px;
   padding: 15px 5px 5px 5px;
   border-radius: 7px;
   height: 100%;
   /*border: 1px solid #00A5A8;*/
   border: 1px solid #f44336;
   }
   @keyframes  change-label-size {
   from {
   box-shadow: 0 0 0 0 #00A5A8, inset 0 0 0 1px #00A5A8, inset 0 0 0 0 #FFFFFF, inset 0 0 0 0 #00A5A8;
   }
   to {
   box-shadow: 0 0 0 1px #00A5A8, inset 0 0 0 1px #00A5A8, inset 0 0 0 0 #FFFFFF, inset 0 0 0 0 #00A5A8;
   }
   }
   @media  screen and (max-width: 576px) {
   input[type="radio"] + .label {
   margin-left: 48%;
   display: block;
   }
   }
   /*button style*/
   .btn-success {
   background-color: #00A5A8;
   border-color: #00A5A8;
   }
   .btn-success:hover, .btn-success:active, .btn-success:focus {
   background-color: #00A5A8;
   border-color: #00A5A8;
   }
   .focus_style:focus {
    border-color: #FF9800 !important;
  }
</style>
   </head>
   <body class="vertical-layout vertical-menu 1-column   menu-expanded blank-page blank-page" data-open="click" data-menu="vertical-menu" data-col="1-column">