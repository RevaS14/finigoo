<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">
				Topup
				</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
				
			</div>
		</div>
		<br>
		<div class="content-body">
			<section>
				<div id="render-content">
					<div class="row">
						<div class="col-12">
							<div class="">
								<div class="">
								<?php if($this->session->flashdata('successMessage')) { ?>
										<div class="alert alert-success" role="alert">
												<?php echo $this->session->flashdata('successMessage'); ?>
											</div>
									<?php } ?>
									<div class="">
                                        <form id="" method="post" action="<?php if($type=='driver') { echo ''; } else { echo base_url().'admin/payment'; } ?>">
											<input type="hidden" name="_token" value="">
											<input type="hidden" name="userId" value="<?php echo $userId; ?>">
											<input type="hidden" name="type" value="<?php echo $type; ?>">
											<div class="form-group col-sm-12">
												<div class="card">
													<div class="card-content">
														<div class="card-body">
														
															<div class="row">
																
																	<div class="form-group row col-sm-12">
																		<label class="col-sm-12 col-form-label">Recharge
																			Amount (Rs): <sup class="error">*</sup></label>
																		<div class="col-sm-12">
																			<input type="text" class="form-control"
																				name="rechargeAmount" required id="name"
																				placeholder="Recharge Amount " value="">
																			<span class="error"></span>
																		</div>
																	</div>
  						
															</div>
														</div>
													</div>
												</div>
                                            </div>
                                            
											<div class="form-group row">
												<div class="col-sm-12">
													<center>
														<button type="submit" name="rechargeButton"
															class="btn btn-primary m-b-0">Recharge</button>
													</center>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>

			</section>
		</div>
	</div>
</div>