
<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">Transaction Details</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
			</div>
		</div>
		<div class="content-body">
            <?php if($data[0]->Status=='A') { ?>
                <div class="alert alert-success" role="alert">
                       Approved..
                    </div>
            <?php } ?>
			<?php if($data[0]->Status=='P') { ?>
                <div class="alert alert-danger" role="alert">
                        Pending for admin approval
                    </div>
            <?php } ?>
            <?php if($data[0]->Status=='C') { ?>
                <div class="alert alert-danger" role="alert">
                        Request Cancelled..
                    </div>
            <?php } ?>
            <style type="text/css">
            	.bank-table th, .bank-table td {
            		border-bottom: 1px solid #e3ebf3 !important;
            		text-align: left;
            		border-top: 1px solid #e3ebf3;
            	}
            	.table th, .table td {
            		border-top: 1px solid #e3ebf3;
            	}
            </style>
			<section>
				<div id="render-content">

					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-header">
								<?php if($data[0]->Status=='P') { ?>
								
							 <a name="C" id="<?php echo $data[0]->ManualTransactionId;?>" href="<?php echo base_url(); ?>admin/change-transaction-status"
											class="approve-status btn grey btn-outline-secondary float-right mr-1">Cancel</a>
                                 <a name="A" id="<?php echo $data[0]->ManualTransactionId;?>" href="<?php echo base_url(); ?>admin/change-transaction-status"
											class="approve-status btn grey btn-outline-secondary float-right mr-1">Approve</a>
										
									<?php } ?>
									 <a href="<?php echo base_url(); ?>/admin/manual-transactions/<?php if($data[0]->Status=='P') { echo 'pending'; } elseif($data[0]->Status=='A') { echo 'approved'; } elseif($data[0]->Status=='C') { echo 'cancelled'; } ?>"
											class=" btn grey btn-outline-secondary float-right mr-1">Back</a>
											</div>
										
								<div class="card-content">
									<div class="card-body">
										<div class="row">
										<div class="col-md-6">
											<h4> From Account Details</h4>
											<table class="table bank-table">
												<tr>
													<th>User Name :</th>
													<td><?php echo $data[0]->FirstName; ?></td>
												</tr>
												<tr>
													<th>Bank Name :</th>
													<td><?php echo $data[0]->FromBankName; ?></td>
												</tr>
												<tr>
													<th>Ifsc :</th>
													<td><?php echo $data[0]->FromIfsc; ?></td>
												</tr>
												<tr>
													<th>Branch Name :</th>
													<td><?php echo $data[0]->FromBranchName; ?></td>
												</tr>

												<tr>
													<th>Account Number :</th>
													<td><?php echo $data[0]->FromAccountNumber; ?></td>
												</tr>

												<tr>
													<th>Account Holder Name :</th>
													<td><?php echo $data[0]->FromAccountHolderName; ?></td>
												</tr>
												<tr>
													<th>Amount :</th>
													<td><?php echo $data[0]->Amount; ?></td>
												</tr>
												

											</table>

										</div>
										<div class="col-md-6">
											<h4> To Account Details</h4>
											<table class="table">
												
												<tr>
													<th>Bank Name :</th>
													<td><?php echo $data[0]->ToBankName; ?></td>
												</tr>
												<tr>
													<th>Ifsc :</th>
													<td><?php echo $data[0]->ToIfsc; ?></td>
												</tr>
												<tr>
													<th>Branch Name :</th>
													<td><?php echo $data[0]->ToBranchName; ?></td>
												</tr>

												<tr>
													<th>Account Number :</th>
													<td><?php echo $data[0]->ToAccountNumber; ?></td>
												</tr>

												<tr>
													<th>Account Holder Name :</th>
													<td><?php echo $data[0]->ToAccountHolderName; ?></td>
												</tr>
												

											</table>

									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<h4>Evidence Document</h4>
										<img style="display: block; width:100%" src="<?php echo base_url(); ?>/<?php echo $data[0]->EvidenceDocument; ?>">
									</div>
								</div>
								</div>
							</div>
						</div>
					</div>


					<div class="md-overlay"></div>
				</div>

			</section>
		</div>
	</div>
</div>