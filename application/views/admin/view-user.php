
<div class="app-content content">
	<?php $this->load->view('admin/topmenu'); ?>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0">User Details</h3>
			</div>
			<div class="content-header-right col-md-6 col-12">
			</div>
		</div>
		<div class="content-body">

            <style type="text/css">
            	.bank-table th, .bank-table td {
            		border-bottom: 1px solid #e3ebf3 !important;
            		text-align: left;
            		border-top: 1px solid #e3ebf3;
            	}
            	.table th, .table td {
            		border-top: 1px solid #e3ebf3;
            	}
            </style>
			<section>
				<div id="render-content">

					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-header">
									<a  href="<?php echo base_url(); ?>admin/alltransactions/<?php echo $data->UserId;?>"
											class="btn grey btn-outline-secondary float-right mr-1">View Transactions</a>
										<a  id="" href="<?php echo base_url(); ?>admin/users"
											class="btn grey btn-outline-secondary float-right mr-1">Back</a>
								</div>
								<div class="card-content">
									<div class="card-body">
										<div class="row">
										<div class="col-md-6">
											
											<table class="table bank-table">
												<tr>
													<th>First Name :</th>
													<td><?php echo $data->FirstName; ?></td>
												</tr>
												<tr>
													<th>Email :</th>
													<td><?php echo $data->Email; ?></td>
												</tr>
												<tr>
													<th>DOB :</th>
													<td><?php echo date('d-m-Y',strtotime($data->DOB)); ?></td>
												</tr>
												<tr>
													<th>Display Picture :</th>
													<td><img src="<?php echo $data->DisplayPicture; ?>" width="100"></td>
												</tr>

												<tr>
													<th>Country :</th>
													<td><?php echo $data->Establishment_EstablishmentId; ?></td>
												</tr>

												<tr>
													<th>City :</th>
													<td><?php echo $data->City; ?></td>
												</tr>
												<tr>
													<th>Status :</th>
													<td><?php if($data->Status=='A') { echo 'Active'; } elseif($data->Status=='D') { echo 'Deactive'; } elseif($data->Status=='P') { echo 'Deactive'; } ?></td>
												</tr>
												

											</table>

										</div>
										<div class="col-md-6">
											
											<table class="table">
												
												<tr>
													<th>Last Name :</th>
													<td><?php echo $data->LastName; ?></td>
												</tr>
												<tr>
													<th>Phone :</th>
													<td><?php echo $data->Phone; ?></td>
												</tr>
												<tr>
													<th>Place Of Birth :</th>
													<td><?php echo $data->PlaceOfBirth; ?></td>
												</tr>

												<tr>
													<th>Wallet Balance :</th>
													<td><?php echo $data->WalletBalance; ?></td>
												</tr>

												<tr>
													<th>State :</th>
													<td><?php echo $data->State; ?></td>
												</tr>
												<tr>
													<th>Pincode</th>
													<td><?php echo $data->Pincode; ?></td>
												</tr>
												<tr>
													<th>Registered On</th>
													<td><?php echo date('d-m-Y',strtotime($data->CreatedOn)); ?></td>
												</tr>
												

											</table>

									</div>
								</div>
								
								</div>
							</div>
						</div>
					</div>


					<div class="md-overlay"></div>
				</div>

			</section>
		</div>
	</div>
</div>