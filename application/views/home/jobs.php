<style >
    /*second nav */
    #myTab-2 li a{
         border: .8px solid #ad72056b;
         border-radius: 25px;
         color:black;
         padding: 6px 30px;
          font-size: 14px;
    }
    #myTab-2 li a.active {
    color: #fff;
    background-color: #E92C3C;
    font-size: 14px;
    display: inline-block;
    padding: 6px 30px;
    border-radius: 25px;
    /*border: .8px solid black;*/
}
/*footer subcribe*/
.rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
    font-size: 20px;
    font-size: 14px;
    font-weight: 600;
    position: absolute;
    top: 3px;
    right: 3px;
    height: 51px;
    padding: 0 40px;
    text-transform: uppercase;
    border-radius: 999px;
    /*position: relative;*/
    z-index: 1;
    color: #fff;
    border: none;
    background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
    background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
}
/*back to top button*/
#scrollUp {
    font-size: 30px;
    line-height: 48px;
    position: fixed;
    right: 30px;
    bottom: 30px;
    width: 45px;
    height: 45px;
    text-align: center;
    color: #fff;
    border-radius: 50%;
    background: #E92C3C;
}
/*services hover bg color*/
.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
    padding-left: 23px;
    color: #fff;
    background: #E92C3C;
}
.sticky {
  position: fixed;
  top: 16%;
  width: 100%;
  z-index: 99;
  animation-name: fadeInDown;
}
.rt{
	   background: linear-gradient(0deg, rgba(0, 0, 0, 0.20), rgba(0, 0, 0, 0.20)),url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/home-finigoo1.jpg);
    background-repeat: no-repeat;
    background-size: 100% 100%;
    height: 330px;
    position: relative;
    top: 93px;
    margin-bottom: 95px;
    background-attachment: fixed;
}
#rt_text{
	position: relative;
    top: 45%;
    color: white;
    font-weight: 600;
    width: 50%;
    margin: auto;
}
#rt_text h3{
line-height: 33px;
    text-align: center;
    font-weight: 900;
    font-size: 30px;
    font-family: sans-serif;
    color: #fff;
}
.rt:after{
	content:"";
	display:table;
	clear:both;
}
#dept{

	margin-bottom: 30px;

}
#roles h4 a{
	color:red!important;
	font-weight:bold;
}
#roles p{
	font-weight:bold;
	font-size: 14px;
    line-height: 10px;
}
#topic h2{
	color:red!important;
	font-weight:bold;
}
#jobs1{
	margin-bottom: 50px;
}
.roles1{
	margin-bottom:30px;
}
.select-drop{
    padding: 10px 26px;
    height: unset;
    font-size: 16px;
}
</style>

<!-- 
    ====== Services Start ==============
 -->
 
<div class="rt">
   <div id="rt_text">
				<h3>Current Job Openings at Finigoo</h3>
   </div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
	<div class="container" id="dept">
		<div class="row">
			<div class="col-lg-12">
				<form class="form-inline" action="">
						<div class="form-group">
							<select class="form-control select-drop" placeholder="All Departments">
								<option>All Departments</option>
								<option>Admin</option>
								<option>Brand & PR</option>
								<option>Bussiness</option>
								<option>Customer Experience</option>
								<option>Engineering</option>
								<option>Product</option>
								<option>Finance</option>
								<option>Human Resource</option>
								<option>Product Design</option>
								<option>Add Sales</option>
								<option>Marketing</option>
								<option>Media</option>
							</select>
						</div>
						<div class="form-group" style="margin-left: 8px;">
							<select class="form-control select-drop" placeholder="All Offices">
								<option>All Offices</option>
								<option>India</option>
							</select>
						</div>
				</form>
			</div>
		</div>
	</div>
		<div class="section-title-spacer"></div>
		<div class="section-title-spacer"></div>
														<!------------Jobs------------->
	<div class="container">
			<div class="row">
			
					<div class="col-12" id="jobs1">
            
                        <div class="row">
						<div class="offset-lg-2 col-lg-5 col-md-6 col-sm-6 col-12" id="topic">
						
							<h2>Android Developer</h2>
							
						</div>
						<div class="col-lg-5 col-md-6 col-sm-6 col-12" id="roles">
						
							<div class="roles1">
								<h4><a href="android.html">Senior Android Developer</a></h4>
								<p>Mumbai, Bengaluru</p>
							</div>
							
							<div class="roles1">
								<h4><a href="android.html">Junior Android Developer</a></h4>
								<p>Mumbai, Bengaluru</p>
							</div>
							
							<div class="roles1">
								<h4><a href="android.html">Android Developer(Freshers)</a></h4>
								<p>Mumbai, Bengaluru</p>
							</div>
							
						</div>
                    </div>
					</div>
					<div class="col-12" id="jobs1">
			<div class="row">
						<div class="offset-lg-2 col-lg-5 col-md-6 col-sm-6 col-12" id="topic">
						
							<h2>Zonal Manager</h2>
							
						</div>
						<div class="col-lg-5 col-md-6 col-sm-6 col-12" id="roles">
						
							<div class="roles1">
								<h4><a href="">Zonal Manager-Franchisee Development</a></h4>
								<p>Delhi,Mumbai,Kolkata</p>
							</div>
							
						</div>
                    </div>
					</div>
					<div class="col-12" id="jobs1">
			<div class="row">
						<div class="offset-lg-2 col-lg-5 col-md-6 col-sm-6 col-12" id="topic">
						
							<h2>Marketing</h2>
							
						</div>
						<div class="col-lg-5 col-md-6 col-sm-6 col-12" id="roles">
						
							<div class="roles1">
								<h4><a href="">Marketing Executive</a></h4>
								<p>Mumbai,Kolkata</p>
							</div>
							
							<div class="roles1">
								<h4><a href="">Digital Marketing</a></h4>
								<p>Mumbai,Bengaluru</p>
							</div>
							
						</div>
                    </div>
                    </div>
                    

					<div class="col-12" id="jobs1">
			<div class="row">
						<div class="offset-lg-2 col-lg-5 col-md-6 col-sm-6 col-12" id="topic">
						
							<h2>Admin</h2>
							
						</div>
						<div class="col-lg-5 col-md-6 col-sm-6 col-12" id="roles">
						
							<div class="roles1">
								<h4><a href="">System Admin</a></h4>
								<p>Delhi</p>
							</div>
							
							<div class="roles1">
								<h4><a href="">Office Admin</a></h4>
								<p>Mumbai</p>
							</div>
							
							<div class="roles1">
								<h4><a href="">Admin Manager</a></h4>
								<p>Bengaluru</p>
							</div>
							
						</div>
                    </div>
					</div>
					<div class="col-12" id="jobs1">
			<div class="row">
						<div class="offset-lg-2 col-lg-5 col-md-6 col-sm-6 col-xs-12" id="topic">
						
							<h2>Android Developer</h2>
							
						</div>
						<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12" id="roles">
						
							<div class="roles1">
								<h4><a href="">Senior Android Developer</a></h4>
								<p>Mumbai, Bengaluru</p>
							</div>
							
							<div class="roles1">
								<h4><a href="">Junior Android Developer</a></h4>
								<p>Mumbai, Bengaluru</p>
							</div>
							
							<div class="roles1">
								<h4><a href="">Android Developer(Freshers)</a></h4>
								<p>Mumbai, Bengaluru</p>
							</div>
							
						</div>
                    </div>
					</div>
			</div>
</div>