<style>
    /*second nav */
    #myTab-2 li a {
        border: .8px solid #ad72056b;
        border-radius: 25px;
        color: black;
        padding: 6px 30px;
        font-size: 14px;
    }

    #myTab-2 li a.active {
        color: #fff;
        background-color: #01AEF3;
        font-size: 14px;
        display: inline-block;
        padding: 6px 30px;
        border-radius: 25px;
        /*border: .8px solid black;*/
    }

    /*footer subcribe*/
    .rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
        font-size: 20px;
        font-size: 14px;
        font-weight: 600;
        position: absolute;
        top: 3px;
        right: 3px;
        height: 51px;
        padding: 0 40px;
        text-transform: uppercase;
        border-radius: 999px;
        /*position: relative;*/
        z-index: 99999;
        color: #fff;
        border: none;
        background: -webkit-linear-gradient(55deg, #01AEF3 0%, #01AEF3 53%, #01AEF3 70%, #01AEF3 100%);
        background: linear-gradient(35deg, #01AEF3 0%, #01AEF3 53%, #01AEF3 70%, #01AEF3 100%);
    }

    /*back to top button*/
    #scrollUp {
        font-size: 30px;
        line-height: 48px;
        position: fixed;
        right: 30px;
        bottom: 30px;
        width: 45px;
        height: 45px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
        background: #01AEF3;
    }

    /*services hover bg color*/
    .main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
    .main-menu ul ul>li:hover>a {
        padding-left: 23px;
        color: #fff;
        background: #01AEF3;
    }

    .rt-section-title span {
        font-family: 'Lato', sans-serif;
        font-size: 18px;
        display: block;
        margin: 0 0 0;
        text-transform: uppercase;
        color: #01aef3;
    }

    .icofont-check-circled:before {
        content: "\eed7";
        color: #01aef3;
    }

    .flipInX {
        color: #01AEF3;
    }

    .flipInX:hover {
        color: red;
    }

    .works3-area .rt-inner-overlay {
        height: 439px;
        background-image: -webkit-linear-gradient(82deg, #01AEF3 0%, #01AEF3 53%, #01AEF3 70%, #01AEF3 100%);
        background-image: linear-gradient(8deg, #01AEF3 0%, #01AEF3 53%, #01AEF3 70%, #01AEF3 100%);
    }

    /*..flipInX{
    -webkit-transition: transform .2s linear;
    -moz-transition: transform .2s linear;
    -o-transition: transform .2s linear;
    transition: transform .2s linear;
}

.flipInX:hover {
    -webkit-transform: translateY(20px);
    -moz-transform: translateY(20px);
    -o-transform: translateY(20px);
    transform: translateY(20px);
}*/
    .submenufix.show {
        background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.38)), url(<?php echo base_url(); ?>landing-assets/assets/images/extra/money.jpg);
        background-size: 100% 100%;
    }

    @media (max-width:1200px) {
        .submenufix.show {
            background: white;
        }
    }


    .brand_card li img {
        width: 70px;
        margin-bottom: 20px;
    }
</style>

<div class="rt-breadcump rt-breadcump-height">
    <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/goupi.jpg)">
    </div><!-- /.rt-page-bg -->
    <div class="container">
        <div class="row rt-breadcump-height">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h3>Go UPI</h3>
                    <p style="font-size: 30px;"> India's Payment App</p>
                </div><!-- /.breadcrumbs-content -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.rt-bredcump -->
<!-- <div class="container" id="tabb" >
<div class="flight-list-box rt-mb-40">
                        <ul class="nav pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="#rt-itm_1" role="tab"
                                    aria-controls="rt-itm_1" aria-selected="true">GoUPI</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">blog</a>
                            </li>
                            <li class="nav-item">
                               
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                               
                            </li>
                        </ul>
                    </div>
</div> -->
<!-- 
    ====== Services Start ==============
 -->

<section class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 mx-auto text-center">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title"><span style="color:#01AEF3">Go UPI</span>
                        Get started now!</h2>
                    <p>Enable UPI payments via apps like BHIM, PhonePe, WhatsApp, etc. by updating to our latest Android App.</p>
                </div><!-- /.rt-section-title-wrapper -->
            </div><!-- /.col-lg-8 -->
        </div><!-- ./row -->
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row align-items-center">
            <div class="col-lg-5 mb-0 mb-md-4">
                <div class="icon-thumb rt-mb-30 wow fadeInDown" data-wow-duration="1s">
                    <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/9.png" alt="icon-image" draggable="false" width="80">
                </div><!-- /.icon-thumb -->
                <h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInDown" data-wow-duration="1s">
                    Now accept UPI payments using PayTM, PhonePe,WhatsApp or any banking App
                </h4>
                <div class="text-424 wow fadeInUp" data-wow-duration="1s">
                    <ul class="rt-list f-size-14">

                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span> Accept payments via UPI and get paid instantly.</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>
                            What's interesting is, you get onboarded instantly by just downloading our App and creating your profile.</li>

                    </ul>
                </div><!-- /.text-424 -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6 offset-lg-1">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/Go Upi-01.png" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->




    </div><!-- ./ copntainer -->
</section>

<!-- 
    ======== call to action start====
 -->



<div class="spacer-top"></div><!-- /.spacer-top -->
<section class="brands-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 mx-auto text-center">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title">
                        <span> Accept payments anytime, anywhere through your mobile</span>
                        Payment App For Merchants


                    </h2><!-- /.rt-section-title -->

                </div><!-- /.rt-section-title-wrapper -->
            </div><!-- /.col-lg-9 -->
        </div><!-- /.row -->
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row">
            <div class="col-lg-9 mx-auto">
                <ul class="rt-border-brands brand_card" style="background-color: #01aef30d; padding: 10px;">
                    <li class="single-border-brands">
                        <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/OneTimeSignUp.png" />
                        <a href="#" class="wow flipInX d-block">
                            One-time sign-up
                        </a>
                    </li><!-- /.single-border-brands -->
                    <li class="single-border-brands">
                        <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/ExcitingBank.png" />
                        <a href="#" class="wow flipInX d-block" data-wow-duration="1s">
                            Link with your existing bank account
                        </a>
                    </li><!-- /.single-border-brands -->
                    <li class="single-border-brands">
                        <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/NoStep.png" />
                        <a href="#" class="wow flipInX d-block" data-wow-duration="1.5s">
                            No setup & operational costs
                        </a>
                    </li><!-- /.single-border-brands -->
                    <li class="single-border-brands">
                        <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/24H7Support.png" />
                        <a href="#" class="wow flipInX d-block" data-wow-duration="2s">
                            24/7 dedicated Support

                        </a>
                    </li><!-- /.single-border-brands -->
                    <li class="single-border-brands">
                        <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/BusinessTypes.png" />
                        <a href="#" class="wow flipInX d-block" data-wow-duration="2.5s">
                            Suitable for all business type
                        </a>
                    </li><!-- /.single-border-brands -->
                    <li class="single-border-brands">
                        <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/SecureInfo.png" />
                        <a href="#" class="wow flipInX d-block" data-wow-duration="3s">
                            More Secure Customers share only their virtual address and no other sensitive information.

                        </a>
                    </li><!-- /.single-border-brands -->
                    <li class="single-border-brands">
                        <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/MultiCashOnDelivery.png" />
                        <a href="#" class="wow flipInX d-block" data-wow-duration="3.5s">
                            Multi-utility Can be used for split sharing with cash on delivery,**
                        </a>
                    </li><!-- /.single-border-brands -->
                    <li class="single-border-brands">
                        <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/MerchantPayments.png" />
                        <a href="#" class="wow flipInX d-block" data-wow-duration="4s">
                            ** merchant payments and remittances.

                        </a>
                    </li><!-- /.single-border-brands -->
                </ul><!-- /.rt-border-brands -->
            </div><!-- /.col-lg-7 -->
        </div><!-- /.row -->
    </div><!-- /.containe -->
</section>
<div class="spacer-bottom"></div><!-- /.spacer-bottom -->
<section class="works3-area">
    <div class="rt-inner-overlay"></div><!-- /.rt-overlay -->
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="rt-section-title-wrapper">
                    <h3></h3>
                </div><!-- /.rt-section-title-wrapper -->
                <div class="rt-box-style-1">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom">
                                <div class="icon-thumb">
                                    <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/9.png" alt="box-icon" draggable="false" width="70">
                                </div><!-- /.icon-thumb -->
                                <div class="iconbox-content">
                                    <h5>Unique Virtual account for each payer</h5>
                                    <p>Use a unique account and virtual UPI ID for each payer. Opt for fully numeric or branded virtual a/c numbers or UPI IDs-easy to remember, easy to share.</p>
                                </div><!-- /.iconbox-content -->
                            </div><!-- /.rt-single-icon-box -->
                        </div><!-- /.col-lg-4 -->
                        <div class="col-lg-4 col-md-6">
                            <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="1s" style="height: 340px;">
                                <div class="icon-thumb">
                                    <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/9.png" alt="box-icon" draggable="false" width="70">
                                </div><!-- /.icon-thumb -->
                                <div class="iconbox-content">
                                    <h5>All bank and UPI transfers</h5>
                                    <p>Accept payments via IMPS, RTGS, NEFT, and cheques. Create virtual UPI IDs for fast and simple payment collection from any UPI App.</p>
                                </div><!-- /.iconbox-content -->
                            </div><!-- /.rt-single-icon-box -->
                        </div><!-- /.col-lg-4 -->
                        <div class="col-lg-4 col-md-6">
                            <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="1.5s" style="height: 340px;">
                                <div class="icon-thumb">
                                    <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/9.png" alt="box-icon" draggable="false" width="70">
                                </div><!-- /.icon-thumb -->
                                <div class="iconbox-content">
                                    <h5>Auto bank reconciliation</h5>
                                    <p>Freedom from manual reconciliation: Know who paid and when</p>
                                </div><!-- /.iconbox-content -->
                            </div><!-- /.rt-single-icon-box -->
                        </div><!-- /.col-lg-4 -->

                    </div><!-- /.row -->
                </div><!-- /.rt-box-style-1 -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>


<div class="spacer-bottom"></div><!-- /.spacer-bottom -->
<section class="caltoaction-4 rt-dim-light rt-pt-100 rt-pb-100">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h4 class="f-size-40 rt-semibold rt-mb-40 f-size-md-32 f-size-xs-26">Earning rewards is easy, simple, and fun.</h4>
                <p>Now, your customers can earn rewards and redeem it whenever they want..</p>
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>