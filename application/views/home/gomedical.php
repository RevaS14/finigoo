<style>
    /*second nav */
    #myTab-2 li a {
        border: .8px solid #ad72056b;
        border-radius: 25px;
        color: black;
        padding: 6px 30px;
        font-size: 14px;
    }

    #myTab-2 li a.active {
        color: #fff;
        background-color: #E92C3C;
        font-size: 14px;
        display: inline-block;
        padding: 6px 30px;
        border-radius: 25px;
        /*border: .8px solid black;*/
    }

    /*footer subcribe*/
    .rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
        font-size: 20px;
        font-size: 14px;
        font-weight: 600;
        position: absolute;
        top: 3px;
        right: 3px;
        height: 51px;
        padding: 0 40px;
        text-transform: uppercase;
        border-radius: 999px;
        /*position: relative;*/
        z-index: 1;
        color: #fff;
        border: none;
        background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
        background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
    }

    /*back to top button*/
    #scrollUp {
        font-size: 30px;
        line-height: 48px;
        position: fixed;
        right: 30px;
        bottom: 30px;
        width: 45px;
        height: 45px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
        background: #E92C3C;
    }

    /*services hover bg color*/
    .main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
    .main-menu ul ul>li:hover>a {
        padding-left: 23px;
        color: #fff;
        background: #E92C3C;
    }

    .sticky {
        position: fixed;
        top: 16%;
        width: 100%;
        z-index: 99;
        animation-name: fadeInDown;
    }

    .rt {
        background: linear-gradient(0deg, rgba(0, 0, 0, 0.20), rgba(0, 0, 0, 0.20)), url(<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/home-finigoo1.jpg);
        background-repeat: no-repeat;
        background-size: 100% 100%;
        height: 80%;
        position: relative;
        top: 93px;
        margin-bottom: 95px;
    }

    .rt_text1 h1 {
        font-weight: 900;
    }

    .rt_text1 {
        width: 40%;
        margin: auto;
        text-align: center;
        position: relative;
        top: 70%;
        color: white;
    }

    .rt_text1 button {
        color: white;
        background-color: #ec1d23;
        border: none;
        font-size: 15px;
    }

    #service p {
        text-align: justify;
    }

    #service1 img {
        width: 60%;
        height: 300px;
        margin: auto;
        display: block;
    }

    .may_seprater {
        width: 80px;
        height: 4px;
        margin: 20px 0;
        background-color: red;
    }

    .may_seprater1 {
        width: 80px;
        height: 4px;
        margin: 20px auto;
        background-color: red;
    }

    #tech {
        background-color: #212121;
        color: aliceblue;
        padding: 40px;
        font-weight: 600;
        letter-spacing: 1px;
    }

    #tech h2 {
        font-weight: bold;
    }

    #join_us {
        font-weight: 600;
        letter-spacing: 1px;
    }

    #join_us h2 {
        font-weight: bold;
    }

    .formaction {
        width: 60%;
        margin: auto;
        box-shadow: 0px 0px 4px 0px;
    }

    .form-inline label {
        justify-content: left;
    }

    .form-inline .form-group {
        display: inline-block;
        margin-bottom: 0;
        vertical-align: middle;
        padding: 30px;
        width: 50%;
        border-left: 1px solid black;
        text-align: left;
    }

    .form-inline .form-group:first-child {
        border-left: 0px;
    }

    .form-inline .radio input[type=radio] {
        margin-right: 10px;
        vertical-align: middle;
        align-items: center;
    }

    .radio label {
        font-size: 14px;
        align-items: baseline;
        padding-left: 20px;
        font-weight: bold;
        margin-top: 30px;
    }

    .form-inline .form-control {
        margin-top: 30px;
    }

    .formaction button {

        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;
        align-items: center;
        justify-content: center;
        width: 30%;
        margin: auto;
        margin-bottom: -15px;
    }

    #join_us {
        background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/all-img/pharma_joinus.png);
        height: 100%;
    }
</style>
<!-- 
    ====== Services Start ==============
 -->

<div class="rt">


    <div class="rt_text1">
        <h1>PHARMACY DELIVERY</h1>
        <button class="btn btn-default" type="button">Get estimate</button>
    </div>

</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>

<div class="container">
    <div class="row">
        <div class="col-lg-6" id="service">
            <h2><b>How our service works</b></h2>

            <div class="may_seprater"></div>
            <p>We understand how important it is to deliver medicines to customers on time.<b>Finigoo</b> provides 90-minutes deliveries for pharma products.<b>Finigoo</b> aims to make medicine delivery hassle-free and convenient for our clients and their customers with innovative logistics solutions. Our vast network of well-trained delivery partners and innovative technology solutions ensure that pharma orders reach their destinations correctly and at the quickest time possible.</p>
        </div>
        <div class="col-lg-6" id="service1">
            <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/Pharma.png">
        </div>
    </div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container-fluid" id="tech">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Technology</h2>
                <div class="may_seprater1"></div>
                <p>Connecting businesses to customers,<b>Finigoo</b> offers 90-minute hyperlocal delivery. We are fast, reliable, and affordable. We offer live tracking, 24/7 support, user-friendly technology, most reasonable pricing, and the fastest delivery.</p>
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/work-img-2.png" style="height: 300px;">
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" id="join_us">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Ready to join us?</h2>
                <div class="may_seprater1"></div>
                <p>Deliver with us and give your business the<b>Finigoo</b> advantage!</p>

                </br>
                </br>

                <div class="formaction">
                    <div id="form1">
                        <form class="form-inline" action="">
                            <div class="form-group">
                                <label for="delivery" style="color:#565656;">What do you want to deliver?</label>
                                <select class="form-control" name="company_size" placeholder="">
                                    <option class="form-control" selected>Select*</option>
                                    <option class="form-control" value="">Food & Beverages</option>
                                    <option class="form-control" value="">Grocery</option>
                                    <option class="form-control" value="">Pharmaceuticals</option>
                                    <option class="form-control" value="">ECommerce shipments</option>
                                </select>

                            </div>
                            <div class="form-group">


                                <label for="delivery" style="color:#565656;">Where do you want to deliver?</label>
                                <div class="radio">
                                    <label><input type="radio" name="optradio" checked>Same City</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="optradio">Different City</label>
                                </div>
                            </div>

                            <button type="button" class="btn btn-primary">GET ESTIMATE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2>Ready to join us?</h2>
            <div class="may_seprater1"></div>
            <p>Hundreds of esteemed clients from the food, pharma, grocery and other businesses trust <b>Finigoo</b> for last-mile delivery services in India.</p>

            </br>
            </br>
        </div>

    </div>
    <div class="row row justify-content-md-center">
        <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/Pharma_Logo.png">
    </div>
</div>