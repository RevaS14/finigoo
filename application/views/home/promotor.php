<!DOCTYPE html>
<html>

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <title> </title>

    <meta charset="UTF-8">
    <link rel="icon" type="image/x-icon" href="" />
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <meta name="author" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <meta name="viewport" content="width=device-width, user-scalable=no">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>landing-assets/assets/promoter/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>landing-assets/assets/promoter/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>landing-assets/assets/promoter/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>landing-assets/assets/promoter/css/main.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>landing-assets/assets/promoter/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>landing-assets/assets/promoter/css/slick-theme.css">

    <style type="text/css">
        @import url(http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300ita‌​lic,400italic,500,500italic,700,700italic,900italic,900);

        html,
        body,
        html * {
            font-family: 'Roboto', sans-serif;
        }

        .join-btn-main.header-fixed {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 99999;

        }
    </style>
</head>

<body style=" user-select: none;">

    <!-- <div class="loader-wrapper">
        <div class="preloader">
            <div class="spinner"></div>
            <img class="preloader-logo" src="assets/promoter/images/cubber_logo.svg" alt="Finigoo Logo"/>
        </div>

    </div> -->


    <div class="container-fluid">

        <div class="row">
            <div class="w-100">
                <div class="refer-earn-wrapper">

                    <div class="swing-wrapper">
                        <div class="swing">
                            <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/visitors-lable.svg" class="img-fluid">
                            <div class="visitors-counter">
                                Our Visitors
                                <span>348 K</span>
                            </div>
                        </div>
                    </div>

                    <div class="top-animation-mns-prt">
                        <div class="row m-0">
                            <div class="col-12 p-0">
                                <div class="hrd-top-banner">
                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/promoters1.png" width="100%" class="img-fluid" alt="Cubber Promoter" />
                                    <!-- <div class="banner-txt">
                                        <p class="wow bounceInDown animated">become Finigoo</p>
                                        <h1 class="wow lightSpeedIn animated">promoters</h1>
                                    </div> -->
                                </div>
                                <div class="bnr-ntm-text pro-text-top" style="margin-top: 25px !important;">
                                    <h2 class="wow slideInDown animated">Refer Friends and
                                        <br />
                                        Earn <span>₹ 20,000</span> to <span>₹ 1,05,900</span></h2>
                                </div>
                                <div class="join-btn-main text-center wow slideInUp animated">
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#Contact-us-popup">Get Promoter Code</a>
                                </div>
                            </div>
                        </div>

                        <div class="row m-0" style="padding: 20px 0 0 0;">
                            <div class="col-12 p-0">
                                <div class="cbr-top-side text-center">
                                    <div class="posin-wrapper animtin-img-1">
                                        <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/bag-top-money-note.png" class="img-fluid" alt="Currency Icon">
                                    </div>
                                    <div class="cbr-promoter-men-animation">
                                        <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/cubber-promoter-men.png" class="img-fluid bike-promoter-men" alt="Promoter Icon">
                                    </div>
                                    <div class="posin-wrapper animtin-img-2">
                                        <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/bag-top-right-image.png" class="img-fluid" alt="Rupee Icon">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="center-animation-mns-prt">
                        <div class="row m-0">
                            <div class="col-12 p-0">
                                <div class="inner-ptn-wrapper">

                                    <div id="scroll-top">
                                        <div class="scroll-btm-arrow">
                                            <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/bag-btm-row-track.png" class="img-fluid" alt="Rupee Icon">
                                        </div>
                                    </div>

                                    <div class="inner-ptn-left-space">
                                        <div class="top-two-line">
                                            <p><i class="fa fa-circle" aria-hidden="true"></i>FILL THE FORM AND BECOME PROMOTER.</p>
                                            <p><i class="fa fa-circle" aria-hidden="true"></i>SHARE CUBBER STORE APPLICATION to YOUR friends / relatives with your REFFERAL CODE AND START EARNING.</p>
                                            <div class="posin-wrapper animtin-img-3">
                                                <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/rupee-bg-img.png" class="img-fluid" alt="Rupee Icon" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="earn-month-name-row">
                                        <div class="inner-ptn-left-space">
                                            <div class="month-name-bg first-name-bg text-center">
                                                <span class="pulse red"></span>
                                                <span>1st Month</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="month-inr-earn-img-row">
                                        <div class="posin-wrapper animtin-img-4">
                                            <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/money-bg-note.png" class="img-fluid" alt="Currency Icon" />
                                        </div>
                                        <div class="inner-ptn-left-space">
                                            <div class="row m-0">
                                                <div class="col-md-6">
                                                    <div class="month-inr-info">
                                                        <ul class="monthly-incom-row">
                                                            <li>
                                                                <h3 class="first-amount-clr">₹9,000</h3>
                                                                <p><span>Registration Income</span></p>
                                                            </li>
                                                        </ul>
                                                        <ul class="monthly-incom-row">
                                                            <li>
                                                                <h3 class="first-amount-clr">₹5,325</h3>
                                                                <p><span>Recharge + Booking + Money Transfer Income</span></p>
                                                            </li>
                                                        </ul>
                                                        <!-- <ul class="monthly-incom-row">
														<li>
															<h3 class="first-amount-clr">₹14,325</h3>
															<p><span>Recharge + Booking Income</span></p>
														</li>
													</ul> -->
                                                        <div class="moth-total-income-main">
                                                            <div class="moth-total-income-btn first-month-incm-btn-bg d-inline-block text-center">
                                                                <h2>₹<span class="counter">14,325</span></h2>
                                                                <p>Total Earning</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-center">
                                                    <div class="month-inr-img">
                                                        <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/10store-right-img.png" class="img-fluid" alt="Promoter Icon" />
                                                        <div class="image-store-count" style="right: 55px;">
                                                            <h2 class="store-contr" style="font-size: 25px;">30</h2>
                                                            <h5>New Store</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="earn-month-name-row">
                                        <div class="inner-ptn-left-space">
                                            <div class="month-name-bg second-name-bg text-center">
                                                <span class="pulse green"></span>
                                                <span>2nd Month</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="month-inr-earn-img-row">
                                        <div class="posin-wrapper animtin-img-5">
                                            <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/rupee-bg-img.png" class="img-fluid" alt="Rupee Icon" />
                                        </div>
                                        <div class="inner-ptn-left-space">
                                            <div class="row m-0">
                                                <div class="col-md-6">
                                                    <div class="month-inr-info">
                                                        <ul class="monthly-incom-row">
                                                            <li>
                                                                <h3 class="second-amount-clr">₹9,000</h3>
                                                                <p><span>Registration Income</span></p>
                                                            </li>
                                                        </ul>
                                                        <ul class="monthly-incom-row">
                                                            <li>
                                                                <h3 class="second-amount-clr">₹3,000</h3>
                                                                <!-- <p style="margin-right: 10px;"> -->
                                                                <p class="inner-p">
                                                                    <span>Monthly Fixed Income</span>
                                                                    <span class="valid">100*30(1 store / 1 day of previous month)</span>
                                                                </p>
                                                            </li>
                                                        </ul>
                                                        <ul class="monthly-incom-row">
                                                            <li>
                                                                <h3 class="second-amount-clr">₹10,650</h3>
                                                                <p><span>Recharge + Booking + Money Transfer Income</span></p>
                                                            </li>
                                                        </ul>
                                                        <div class="moth-total-income-main">
                                                            <div class="moth-total-income-btn second-month-incm-btn-bg d-inline-block text-center">
                                                                <h2>₹<span class="counter">22,650</span></h2>
                                                                <p>Total Earning</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-center">
                                                    <div class="month-inr-img">
                                                        <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/25store-right-img.png" class="img-fluid" alt="Promoter Icon" />
                                                        <div class="image-store-count" style="right: 55px;">
                                                            <h2 class="store-contr" style="font-size: 25px;">60</h2>
                                                            <h5>New Store</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="month-inr-earn-img-row">
                                        <!-- <div class="posin-wrapper animtin-img-6">
										<img src="images/rupee-bg-img.png" class="img-fluid" alt="" />
									</div> -->
                                        <div class="inner-ptn-left-space">
                                            <div class="row m-0 pt-4 pb-4">
                                                <div class="col-md-6">
                                                    <div class="brackets-main d-inline-block">
                                                        <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/brackets-month.png" class="img-fluid" alt="Burly Brackets Icon" />
                                                    </div>
                                                    <div class="month-name-bg brackets-name-bg text-center">
                                                        <span>3 to 11 Month</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-center">
                                                    <div class="month-inr-img">
                                                        <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/promoters3.png" style="width: 160px;" width="100%" class="img-fluid" alt="Promoter Earning Icon" />
                                                        <!-- <div class="month-inr-text">
                                                            <p>NEW STORE 330</p>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="posin-wrapper animtin-img-7">
										<img src="images/money-bg-note.png" class="img-fluid" alt="">
									</div> -->
                                    </div>
                                    <div class="earn-month-name-row">
                                        <div class="inner-ptn-left-space">
                                            <div class="month-name-bg twel-name-bg text-center">
                                                <span class="pulse purple"></span>
                                                <span>12th Month</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="earn-month-name-row mb-3">
                                        <div class="inner-ptn-left-space">
                                            <div class="twel-mnt-cplit-info-text">
                                                <span>You will get up to ₹1,05,900/
                                                    <br />
                                                    Month within 12 month</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="month-inr-earn-img-row">
                                        <div class="inner-ptn-left-space">
                                            <div class="row m-0">
                                                <div class="col-md-6">
                                                    <div class="month-inr-info">
                                                        <ul class="monthly-incom-row">
                                                            <li>
                                                                <h3 class="twel-amount-clr">₹9,000</h3>
                                                                <p><span>Registration Income</span></p>
                                                            </li>
                                                        </ul>
                                                        <ul class="monthly-incom-row">
                                                            <li>
                                                                <h3 class="twel-amount-clr">₹33,000</h3>
                                                                <!-- <p style="margin-right: 10px;"> -->
                                                                <p class="inner-p">
                                                                    <span>Monthly Fixed Income</span>
                                                                    <span class="valid">100*330(330 store / 11 month)</span>
                                                                </p>
                                                            </li>
                                                        </ul>
                                                        <ul class="monthly-incom-row">
                                                            <li>
                                                                <h3 class="twel-amount-clr">₹63,900</h3>
                                                                <p><span>Recharge + Booking + Money Transfer Income</span></p>
                                                            </li>
                                                        </ul>
                                                        <div class="moth-total-income-main">
                                                            <div class="moth-total-income-btn twel-month-incm-btn-bg d-inline-block text-center">
                                                                <h2>₹<span class="counter">1,05,900</span></h2>
                                                                <p>Total Earning</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-center">
                                                    <div class="month-inr-img">
                                                        <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/125store-right-img.png" class="img-fluid" alt="Promoter Icon" />
                                                        <div class="image-store-count" style="right: 55px;">
                                                            <h2 class="store-contr" style="font-size: 25px;">360</h2>
                                                            <h5>New Store</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="graph-wrapper">
                                        <div class="posin-wrapper animtin-img-8">
                                            <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/money-bg-note.png" class="img-fluid" alt="Currency Icon">
                                        </div>
                                        <div class="text-center">
                                            <img src="<?php echo base_url();?>landing-assets/assets/promoter/images/graph-image.png" class="img-fluid" alt="Earning Chart" />
                                            <ul class="grph-lable-wrapper">
                                                <li class="graph-earn-lbl-1">
                                                    <div class="graph-lbl-inner">
                                                        <!-- <img src="images/graph-lable-1.png" class="img-fluid" alt="" /> -->
                                                        <span>₹30,975/
                                                            <p>per month</p>
                                                        </span>
                                                    </div>
                                                </li>
                                                <li class="graph-earn-lbl-2">
                                                    <div class="graph-lbl-inner">
                                                        <!-- <img src="images/graph-lable-2.png" class="img-fluid" alt="" /> -->
                                                        <span>₹55,950/
                                                            <p>per month</p>
                                                        </span>
                                                    </div>
                                                </li>
                                                <li class="graph-earn-lbl-3">
                                                    <div class="graph-lbl-inner">
                                                        <!-- <img src="images/graph-lable-3.png" class="img-fluid" alt="" /> -->
                                                        <span>₹80,925/
                                                            <p>per month</p>
                                                        </span>
                                                    </div>
                                                </li>
                                                <li class="graph-earn-lbl-4">
                                                    <div class="graph-lbl-inner">
                                                        <!-- <img src="images/graph-lable-4.png" class="img-fluid" alt="" /> -->
                                                        <span>₹1,05,900/
                                                            <p>per month</p>
                                                        </span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="posin-wrapper animtin-img-9">
                                            <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/rupee-bg-img.png" class="img-fluid" alt="Rupee Icon">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 p-0">
                                <div class="cbr-bottom-side text-center"></div>
                                <div class="posin-wrapper animtin-img-11">
                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/rupee-bg-img.png" class="img-fluid" alt="Rupee Icon">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-12">
                            <div class="cubr-str-srvice-heading">
                                Finigoo Services
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-12">
                            <div class="cubr-str-sevn-img text-center">
                                <ul class="d-flex flex-wrap promoter-info-row-1 justify-content-center">
                                    <li class="col-6 col-md wow bounceIn animated">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/1.png" class="img-fluid" alt="Promoter Icon">
                                                    <div class="promoter-info-inner-1">
                                                        <!-- <div class="promoter-info-title">
                                                            <h6 class="mb-2">Recharge</h6>
                                                        </div> -->
                                                        <!-- <div class="promoter-info-img">
                                                            <img src="assets/promoter/images/promoter-recharge-icon.png" class="img-fluid" alt="Recharge Icon">
                                                        </div> -->
                                                    </div>
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/1-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-logo">
                                                        <li>
                                                            <img src="assets/promoter/images/recharge-jio.png" class="img-fluid" alt="Jio Icon"/>
                                                        </li>
                                                        <li>
                                                            <img src="assets/promoter/images/recharge-idea.png" class="img-fluid" alt="Idea Icon"/>
                                                        </li>
                                                        <li>
                                                            <img src="assets/promoter/images/recharge-vodafone.png" class="img-fluid" alt="Vodafone Icon"/>
                                                        </li>
                                                        <li>
                                                            <img src="assets/promoter/images/recharge-airtel.png" class="img-fluid" alt="Airtel Icon"/>
                                                        </li>

                                                    </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="col-6 col-md wow bounceIn animated">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url();?>landing-assets/assets/promoter/icons/2.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-1">
                                                        <div class="promoter-info-title">
                                                            <h6 class="mb-2">Utility Bill</h6>
                                                        </div>
                                                        <div class="promoter-info-img">
                                                            <img src="assets/promoter/images/promoter-utility-icon.png" class="img-fluid" alt="Bill payment icon">
                                                        </div>
                                                    </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url();?>landing-assets/assets/promoter/icons/2-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-logo">
                                                        <li>
                                                            <img src="assets/promoter/images/electricity-icon.png" class="img-fluid" alt="Electricity Icon" />
                                                        </li>
                                                        <li>
                                                            <img src="assets/promoter/images/gas-icon.png" class="img-fluid" alt="Gas Bill Icon"/>
                                                        </li>
                                                        <li>
                                                            <img src="assets/promoter/images/water.png" class="img-fluid" alt="Water Bill Icon"/>
                                                        </li>
                                                        <li>
                                                            <img src="assets/promoter/images/insurance.png" class="img-fluid" alt="Insurance Icon"/>
                                                        </li>

                                                    </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-6 col-md wow bounceIn animated">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url();?>landing-assets/assets/promoter/icons/3.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-1">
                                                        <div class="promoter-info-title">
                                                            <h6 class="mb-2">Bus Booking</h6>
                                                        </div>
                                                        <div class="promoter-info-img">
                                                            <img src="assets/promoter/images/promoter-bus-booking-icon.png" class="img-fluid" alt="bus booking Icon">
                                                        </div>
                                                    </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url();?>landing-assets/assets/promoter/icons/3-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-lg-logo">
                                                        <li>
                                                            <img src="assets/promoter/images/promoter-bus-booking-icon.png" class="img-fluid" alt="bus booking Icon">
                                                        </li>
                                                    </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <ul class="d-flex flex-wrap promoter-info-row-2 justify-content-center">
                                    <li class="col-6 col-md wow bounceIn animated">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url();?>landing-assets/assets/promoter/icons/4.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-2">
                                                        <div class="promoter-info-img">
                                                            <img src="assets/promoter/images/promoter-hotel-booking-icon.png" class="img-fluid" alt="Hotel booking icon">
                                                        </div>
                                                        <div class="promoter-info-title">
                                                            <h6 class="mb-2">Hotel Booking</h6>
                                                        </div>
                                                    </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url();?>landing-assets/assets/promoter/icons/4-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-lg-logo">
                                                        <li class="mt-0 mb-0">
                                                            <img src="assets/promoter/images/promoter-hotel-booking-icon.png" class="img-fluid" alt="Hotel booking icon">
                                                        </li>
                                                    </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-6 col-md wow bounceIn animated">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url();?>landing-assets/assets/promoter/icons/5.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-2">
                                                        <div class="promoter-info-img">
                                                            <img src="assets/promoter/images/promoter-money-transfer-icon.png" class="img-fluid" alt="Rupee Icon">
                                                        </div>
                                                        <div class="promoter-info-title">
                                                            <h6 class="mb-2">Money Transfer</h6>
                                                        </div>
                                                    </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url();?>landing-assets/assets/promoter/icons/5-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-lg-logo">
                                                        <li class="mt-0 mb-0">
                                                            <img src="assets/promoter/images/promoter-money-transfer-icon.png" class="img-fluid" alt="Rupee Icon">
                                                        </li>
                                                    </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-6 col-md wow bounceIn animated">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url();?>landing-assets/assets/promoter/icons/6.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-2">
                                                        <div class="promoter-info-img">
                                                            <img src="assets/promoter/images/promoter-flightr-booking-icon.png" class="img-fluid" alt="Flight Icon">
                                                        </div>
                                                        <div class="promoter-info-title">
                                                            <h6 class="mb-2">Flight Booking</h6>
                                                        </div>
                                                    </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url();?>landing-assets/assets/promoter/icons/6-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-logo services-back-logo-2">
                                                        <li>
                                                            <img src="assets/promoter/images/air-india.png" class="img-fluid" alt="Air India Icon"/>
                                                        </li>
                                                        <li>
                                                            <img src="assets/promoter/images/jet-airways.png" class="img-fluid" alt="Jet Airways Icon"/>
                                                        </li>
                                                        <li>
                                                            <img src="assets/promoter/images/indigo.png" class="img-fluid" alt="Indigo Icon"/>
                                                        </li>
                                                        <li>
                                                            <img src="assets/promoter/images/Air-Asia.png" class="img-fluid" alt="Air Asia Icon"/>
                                                        </li>
                                                    </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="col-6 col-md wow bounceIn animated">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url();?>landing-assets/assets/promoter/icons/7.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-2">
                                                        <div class="promoter-info-img">
                                                            <img src="assets/promoter/images/promoter-adhar-atm-icon.png" class="img-fluid" alt="AEPS">
                                                        </div>
                                                        <div class="promoter-info-title">
                                                            <h6 class="mb-2">Aadhar  ATM</h6>
                                                        </div>
                                                    </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url();?>landing-assets/assets/promoter/icons/7-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-lg-logo">
                                                        <li class="mt-0 mb-0">
                                                            <img src="assets/promoter/images/promoter-adhar-atm-icon.png" class="img-fluid" alt="AEPS">
                                                        </li>
                                                    </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>





                                <ul class="d-flex flex-wrap promoter-info-row-2 justify-content-center">
                                    <li class="col-6 col-md wow bounceIn  animated" style="visibility: visible; animation-name: bounceIn;">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/8.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-2">
                                                    <div class="promoter-info-img">
                                                        <img src="assets/promoter/images/promoter-hotel-booking-icon.png" class="img-fluid" alt="Hotel booking icon">
                                                    </div>
                                                    <div class="promoter-info-title">
                                                        <h6 class="mb-2">Hotel Booking</h6>
                                                    </div>
                                                </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/8-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-lg-logo">
                                                    <li class="mt-0 mb-0">
                                                        <img src="assets/promoter/images/promoter-hotel-booking-icon.png" class="img-fluid" alt="Hotel booking icon">
                                                    </li>
                                                </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-6 col-md wow bounceIn  animated" style="visibility: visible; animation-name: bounceIn;">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/9.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-2">
                                                    <div class="promoter-info-img">
                                                        <img src="assets/promoter/images/promoter-money-transfer-icon.png" class="img-fluid" alt="Rupee Icon">
                                                    </div>
                                                    <div class="promoter-info-title">
                                                        <h6 class="mb-2">Money Transfer</h6>
                                                    </div>
                                                </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/9-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-lg-logo">
                                                    <li class="mt-0 mb-0">
                                                        <img src="assets/promoter/images/promoter-money-transfer-icon.png" class="img-fluid" alt="Rupee Icon">
                                                    </li>
                                                </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-6 col-md wow bounceIn  animated" style="visibility: visible; animation-name: bounceIn;">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/10.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-2">
                                                    <div class="promoter-info-img">
                                                        <img src="assets/promoter/images/promoter-flightr-booking-icon.png" class="img-fluid" alt="Flight Icon">
                                                    </div>
                                                    <div class="promoter-info-title">
                                                        <h6 class="mb-2">Flight Booking</h6>
                                                    </div>
                                                </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/10-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-logo services-back-logo-2">
                                                    <li>
                                                        <img src="assets/promoter/images/air-india.png" class="img-fluid" alt="Air India Icon"/>
                                                    </li>
                                                    <li>
                                                        <img src="assets/promoter/images/jet-airways.png" class="img-fluid" alt="Jet Airways Icon"/>
                                                    </li>
                                                    <li>
                                                        <img src="assets/promoter/images/indigo.png" class="img-fluid" alt="Indigo Icon"/>
                                                    </li>
                                                    <li>
                                                        <img src="assets/promoter/images/Air-Asia.png" class="img-fluid" alt="Air Asia Icon"/>
                                                    </li>
                                                </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                </ul>



                                <ul class="d-flex flex-wrap promoter-info-row-2 justify-content-center">
                                    <li class="col-6 col-md wow bounceIn   animated" style="visibility: visible; animation-name: bounceIn;">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/11.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-2">
                                                    <div class="promoter-info-img">
                                                        <img src="assets/promoter/images/promoter-hotel-booking-icon.png" class="img-fluid" alt="Hotel booking icon">
                                                    </div>
                                                    <div class="promoter-info-title">
                                                        <h6 class="mb-2">Hotel Booking</h6>
                                                    </div>
                                                </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/11-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-lg-logo">
                                                    <li class="mt-0 mb-0">
                                                        <img src="assets/promoter/images/promoter-hotel-booking-icon.png" class="img-fluid" alt="Hotel booking icon">
                                                    </li>
                                                </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-6 col-md wow bounceIn   animated" style="visibility: visible; animation-name: bounceIn;">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/12.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-2">
                                                    <div class="promoter-info-img">
                                                        <img src="assets/promoter/images/promoter-money-transfer-icon.png" class="img-fluid" alt="Rupee Icon">
                                                    </div>
                                                    <div class="promoter-info-title">
                                                        <h6 class="mb-2">Money Transfer</h6>
                                                    </div>
                                                </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/12-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-lg-logo">
                                                    <li class="mt-0 mb-0">
                                                        <img src="assets/promoter/images/promoter-money-transfer-icon.png" class="img-fluid" alt="Rupee Icon">
                                                    </li>
                                                </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-6 col-md wow bounceIn   animated" style="visibility: visible; animation-name: bounceIn;">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/13.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-2">
                                                    <div class="promoter-info-img">
                                                        <img src="assets/promoter/images/promoter-flightr-booking-icon.png" class="img-fluid" alt="Flight Icon">
                                                    </div>
                                                    <div class="promoter-info-title">
                                                        <h6 class="mb-2">Flight Booking</h6>
                                                    </div>
                                                </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/13-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-logo services-back-logo-2">
                                                    <li>
                                                        <img src="assets/promoter/images/air-india.png" class="img-fluid" alt="Air India Icon"/>
                                                    </li>
                                                    <li>
                                                        <img src="assets/promoter/images/jet-airways.png" class="img-fluid" alt="Jet Airways Icon"/>
                                                    </li>
                                                    <li>
                                                        <img src="assets/promoter/images/indigo.png" class="img-fluid" alt="Indigo Icon"/>
                                                    </li>
                                                    <li>
                                                        <img src="assets/promoter/images/Air-Asia.png" class="img-fluid" alt="Air Asia Icon"/>
                                                    </li>
                                                </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="col-6 col-md wow bounceIn   animated" style="visibility: visible; animation-name: bounceIn;">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/14.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-2">
                                                    <div class="promoter-info-img">
                                                        <img src="assets/promoter/images/promoter-flightr-booking-icon.png" class="img-fluid" alt="Flight Icon">
                                                    </div>
                                                    <div class="promoter-info-title">
                                                        <h6 class="mb-2">Flight Booking</h6>
                                                    </div>
                                                </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/14-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-logo services-back-logo-2">
                                                    <li>
                                                        <img src="assets/promoter/images/air-india.png" class="img-fluid" alt="Air India Icon"/>
                                                    </li>
                                                    <li>
                                                        <img src="assets/promoter/images/jet-airways.png" class="img-fluid" alt="Jet Airways Icon"/>
                                                    </li>
                                                    <li>
                                                        <img src="assets/promoter/images/indigo.png" class="img-fluid" alt="Indigo Icon"/>
                                                    </li>
                                                    <li>
                                                        <img src="assets/promoter/images/Air-Asia.png" class="img-fluid" alt="Air Asia Icon"/>
                                                    </li>
                                                </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                </ul>



                                <ul class="d-flex flex-wrap promoter-info-row-2 justify-content-center">
                                    <li class="col-6 col-md wow bounceIn   animated" style="visibility: visible; animation-name: bounceIn;">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/15.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-2">
                                                    <div class="promoter-info-img">
                                                        <img src="assets/promoter/images/promoter-hotel-booking-icon.png" class="img-fluid" alt="Hotel booking icon">
                                                    </div>
                                                    <div class="promoter-info-title">
                                                        <h6 class="mb-2">Hotel Booking</h6>
                                                    </div>
                                                </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="assets/promoter/icons/15-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-lg-logo">
                                                    <li class="mt-0 mb-0">
                                                        <img src="assets/promoter/images/promoter-hotel-booking-icon.png" class="img-fluid" alt="Hotel booking icon">
                                                    </li>
                                                </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="col-6 col-md wow bounceIn   animated" style="visibility: visible; animation-name: bounceIn;">
                                        <div class="services-flipper-wrapper">
                                            <div class="services-flipper-inner">
                                                <div class="services-front-side">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/16.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <div class="promoter-info-inner-2">
                                                    <div class="promoter-info-img">
                                                        <img src="assets/promoter/images/promoter-hotel-booking-icon.png" class="img-fluid" alt="Hotel booking icon">
                                                    </div>
                                                    <div class="promoter-info-title">
                                                        <h6 class="mb-2">Hotel Booking</h6>
                                                    </div>
                                                </div> -->
                                                </div>
                                                <div class="services-front-back">
                                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/icons/16-back.png" class="img-fluid" alt="Promoter Icon">
                                                    <!-- <ul class="services-back-lg-logo">
                                                    <li class="mt-0 mb-0">
                                                        <img src="assets/promoter/images/promoter-hotel-booking-icon.png" class="img-fluid" alt="Hotel booking icon">
                                                    </li>
                                                </ul> -->
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row m-0">
                        <div class="col-12">
                            <div class="cbr-center-pr-text">
                                <h4>Exclusive
                                <br>Shopping Partners</h4>
                            </div>
                        </div>
                        <div class="col-12 text-center" style="display: inline-flex;">
                            <div class="col-xs-12 col-md-12 col-lg-12">
                                <img src="assets/promoter/images/katie-black.png">
                            </div>
                        </div>
                    </div> -->
                    <div class="row m-0">
                        <div class="col-12 top-promoter">
                            <div class="cbr-center-pr-text">
                                <h4>Top Promoters</h4>
                            </div>
                        </div>
                        <div class="container">
                            <div class="slider center">
                                <div class="testimonial-slider">
                                    <div class="sld-box">
                                        <div class="col-12 text-center">
                                            <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/index2.png" alt="Los Angeles" style="width: 50px; height: 50px; margin-top: 15px; margin-left: 41%;">
                                        </div>
                                        <div class="col-12 text-center">
                                            <p class="testimonial-name text-center">Jashvant Malik</p>
                                            <table class="table table-bordered table-text">
                                                <tr>
                                                    <th><b>Total Refferal</b></th>
                                                    <td><b>221</b></td>
                                                </tr>
                                                <tr>
                                                    <th><b>Total Earning</b></th>
                                                    <td><b>Rs. 66300</b></td>
                                                </tr>
                                                <tr>
                                                    <th><b>Promoter Since</b></th>
                                                    <td><b>15-07-2019 04:15 PM</b></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial-slider">
                                    <div class="sld-box">
                                        <div class="col-12 text-center">
                                            <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/index2.png" alt="Los Angeles" style="width: 50px; height: 50px; margin-top: 15px; margin-left: 41%;">
                                        </div>
                                        <div class="col-12 text-center">
                                            <p class="testimonial-name text-center">Sadasiva Nayak</p>
                                            <table class="table table-bordered table-text">
                                                <tr>
                                                    <th><b>Total Refferal</b></th>
                                                    <td><b>208</b></td>
                                                </tr>
                                                <tr>
                                                    <th><b>Total Earning</b></th>
                                                    <td><b>Rs. 62400</b></td>
                                                </tr>
                                                <tr>
                                                    <th><b>Promoter Since</b></th>
                                                    <td><b>15-07-2019 10:30 AM</b></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial-slider">
                                    <div class="sld-box">
                                        <div class="col-12 text-center">
                                            <img src="../s3.ap-south-1.amazonaws.com/cbrcdn/images/user_profile/index2.png" alt="Los Angeles" style="width: 50px; height: 50px; margin-top: 15px; margin-left: 41%;">
                                        </div>
                                        <div class="col-12 text-center">
                                            <p class="testimonial-name text-center">Eyaz Tarique</p>
                                            <table class="table table-bordered table-text">
                                                <tr>
                                                    <th><b>Total Refferal</b></th>
                                                    <td><b>199</b></td>
                                                </tr>
                                                <tr>
                                                    <th><b>Total Earning</b></th>
                                                    <td><b>Rs. 59700</b></td>
                                                </tr>
                                                <tr>
                                                    <th><b>Promoter Since</b></th>
                                                    <td><b>20-07-2019 08:25 PM</b></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row m-0">
                        <div class="col-12">
                            <div class="ftr-money-mn-wrapper">
                                <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/promoters2.png" class="img-fluid" alt="Promoter Earning Icon" />
                                <!-- <div class="posin-wrapper animtin-img-10">
									<img src="images/bag-bottom-note-image.png" class="img-fluid" alt="">
								</div> -->
                                <div class="posin-wrapper animtin-img-12">
                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/rupee-bg-pro-img.png" class="img-fluid" alt="Rupee Icon">
                                </div>
                                <div class="posin-wrapper animtin-img-13">
                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/rupee-bg-pro-img.png" class="img-fluid" alt="Rupee Icon">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0">
                            <div class="tem-footer-wrapper">
                                <div class="row m-0">
                                    <div class="col-6 col-md">
                                        <ul>
                                            <li>
                                                <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/call-icon.png" class="img-fluid" alt="Call Icon" />
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">+91 123456789</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- <div class="col-6 col-md">
                                        <ul>
                                            <li>
                                                <img src="assets/promoter/images/mail-icon.png" class="img-fluid" alt="Mail Icon" />
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">store@haqto.in</a>
                                            </li>
                                        </ul>
                                    </div> -->
                                    <div class="col-6 col-md">
                                        <ul>
                                            <li>
                                                <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/website-icon.png" class="img-fluid" alt="Browser Icon" />
                                            </li>
                                            <li>
                                                <a href="http://finigoo.com/" target="_blank">finigoo.com</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- <div class="col-6 col-md">
                                        <div class="contact-us-main">
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#Contact-us-popup">Contact us</a>
                                        </div>
                                    </div> -->
                                </div>
                                <!-- <div class="row mt-2 m-0 align-items-center">
                                    <div class="col termncon">
                                        <div class="footer-social-media">
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-pinterest" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-4 termncon">
                                        <p>T&C Apply</p>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade contactus-wrapper" id="Contact-us-popup" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="modal-body">
                        <h3 class="get-tch-heading">Become Finigoo Promotor</h3>
                        <div class="row">
                            <div class="d-none order-lg-1 col-md-8 offset-md-2 col-lg-6 offset-lg-0 pr-0 justify-content-center align-self-end">
                                <div class="">
                                    <img src="<?php echo base_url(); ?>landing-assets/assets/promoter/images/contact-bg-ptn.png" class="img-fluid" alt="Promoter Earning Icon" />
                                </div>
                            </div>
                            <div class="order-lg-0 col-lg-12">
                                <div class="contc-fild-main">
                                    <form>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>First Name</label>
                                                    <input type="text" name="fname" id="fname" class="form-control" placeholder="Enter your first name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Last Name</label>
                                                    <input type="text" name="lname" id="lname" class="form-control" placeholder="Enter your last name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Phone No.</label>
                                                    <input type="text" name="mobileNo" id="mobileNo" class="form-control" placeholder="Enter your phone no" onkeypress="return isNumberKeynum(event);" maxlength="10">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Email Address</label>
                                                    <input type="text" name="emailID" id="emailID" class="form-control" placeholder="Enter your email address">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Select Occupation</label>
                                                    <select name="currentWork" id="currentWork" class="form-control" placeholder="Select Occupation">
                                                        <option value="0">Select Occupation</option>
                                                        <option value="1">Youtuber</option>
                                                        <option value="2">Private Job</option>
                                                        <option value="3">Job Seeker</option>
                                                        <option value="4">Shop Owner</option>
                                                        <option value="5">Student</option>
                                                        <option value="6">Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 hidden otr-wrk">
                                                <div class="form-group">
                                                    <label>Enter Occupation</label>
                                                    <input type="text" name="otherWork" id="otherWork" class="form-control" placeholder="Enter Occupation">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Message</label>
                                                    <textarea rows="3" name="msg" id="msg" class="form-control" placeholder="Enter your message"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="contc-sumt-btn text-center">
                                                    <button type="button" name="conBtn" id="conBtn" class="contc-sumt-btn">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>



    </div>

    <script type="text/javascript" src="<?php echo base_url(); ?>landing-assets/assets/promoter/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>landing-assets/assets/promoter/js/waypoints.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>landing-assets/assets/promoter/js/jquery.counterup.min.js"></script>
    <!-- <script type="text/javascript" src="assets/promoter/js/popper.min.js"></script> -->
    <script type="text/javascript" src="<?php echo base_url(); ?>landing-assets/assets/promoter/js/wow.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>landing-assets/assets/promoter/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>landing-assets/assets/promoter/js/slick.min.js"></script>



    <script type="text/javascript">
        var yourNavigation = $(".join-btn-main ");
        stickyDiv = "header-fixed";
        yourHeader = $('.top-content-part').height();

        $(window).scroll(function() {
            if ($(this).scrollTop() > 400) {
                yourNavigation.addClass(stickyDiv);
            } else {
                yourNavigation.removeClass(stickyDiv);
            }
        });

        $(document).ready(function() {
            $(".loader-wrapper").hide();

            $('#currentWork').on('change', function() {
                var selVal = $(this).children("option:selected").val();
                if (selVal == '6') {
                    $('.otr-wrk').removeClass('hidden');
                } else {
                    $('.otr-wrk').addClass('hidden');
                }
            });
        });

        function isNumberKeynum(event) {
            if (event.which == 8 || event.which == 0) {
                return true;
            }
            if (event.which < 46 || event.which > 59) {
                return false;
            }
            if (event.which == 46 && $(this).val().indexOf('.') != -1) {
                return false;
            }
        }

        $('.center').slick({
            speed: 500,
            centerMode: true,
            centerPadding: '150px',
            autoplay: true,
            autoplaySpeed: 3000,
            lazyLoad: 'ondemand',
            slidesToShow: 1,
            slidesToScroll: 1,
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '50px',
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '0px',
                        slidesToShow: 1
                    }
                }
            ]
        });


        jQuery(document).ready(function($) {
            $('.counter').counterUp({
                delay: 10,
                time: 1500,
            });
            $('.store-contr').counterUp({
                delay: 10,
                time: 2000,
            });
        });

        $(function() {
            $(window).scroll(function() {
                var FirstDiv = $(".top-animation-mns-prt").height() - 300; //1100; 
                var ScrollDiv = $(".center-animation-mns-prt").height(); //2540;
                var Px = $(this).scrollTop() - FirstDiv;
                if ($(this).scrollTop() + 300 > parseInt(FirstDiv + ScrollDiv))
                    document.querySelector("#scroll-top").style.height = parseInt(ScrollDiv - 200) + "px";
                else if ($(this).scrollTop() > 0)
                    document.querySelector("#scroll-top").style.height = Px + "px";
                else
                    document.querySelector("#scroll-top").style.height = "0px";
            });

            $(".alert__close").click(function() {
                const alertClose = document.querySelectorAll('.alert__close');
                alertClose.forEach(item => {
                    item.addEventListener('click', function() {
                        item.parentNode.style.transform = 'translateX(3rem)';
                        item.parentNode.style.opacity = '0';
                        setTimeout(() => {
                            $(".alert").hide()
                            item.parentNode.remove();
                        }, 100)
                    })
                });
            });
        });

        wow = new WOW({
            animateClass: 'animated',
            offset: 100,
        });
        wow.init();

        // setTimeout(() => {
        //     $(".alert--success").hide();
        //     $(".alert--danger").hide();
        // }, 2000);
    </script>

</body>


<!-- Mirrored from www.cubber.in/cubber-promoter?utm_source=cubber_web&utm_medium=cubber_site&utm_campaign=cubber_in%20Jii%20start%20this%20page by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Mar 2020 16:21:42 GMT -->

</html>