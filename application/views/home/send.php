<style >
    /*second nav */
    #myTab-2 li a{
         border: .8px solid #ad72056b;
         border-radius: 25px;
         color:black;
         padding: 6px 30px;
          font-size: 14px;
    }
    #myTab-2 li a.active {
    color: #fff;
    background-color: #E92C3C;
    font-size: 14px;
    display: inline-block;
    padding: 6px 30px;
    border-radius: 25px;
    /*border: .8px solid black;*/
}
/*footer subcribe*/
.rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
    font-size: 20px;
    font-size: 14px;
    font-weight: 600;
    position: absolute;
    top: 3px;
    right: 3px;
    height: 51px;
    padding: 0 40px;
    text-transform: uppercase;
    border-radius: 999px;
    /*position: relative;*/
    z-index: 1;
    color: #fff;
    border: none;
    background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
    background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
}
/*back to top button*/
#scrollUp {
    font-size: 30px;
    line-height: 48px;
    position: fixed;
    right: 30px;
    bottom: 30px;
    width: 45px;
    height: 45px;
    text-align: center;
    color: #fff;
    border-radius: 50%;
    background: #00AFF0;
}
/*services hover bg color*/
.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
    padding-left: 23px;
    color: #fff;
    background: #E92C3C;
}
.sticky {
  position: fixed;
  top: 16%;
  width: 100%;
  z-index: 99;
  animation-name: fadeInDown;
}
.send_rt{
	   background: linear-gradient(0deg, rgba(0, 0, 0, 0.20), rgba(0, 0, 0, 0.20)),url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/gopack.jpg);
    background-repeat: no-repeat;
    background-size: 100% 100%;
    height: 100%;
    position: relative;
    top:40px;
    margin-bottom: 95px;
}

#send_bnr_txt h2{
	font-size: 55px;
    line-height: 65px;
    margin: 0 0 14px 0;
	color:white;
    text-transform: none;
	font-family: 'Baloo Thambi 2', cursive;
}
#send_bnr_txt p{
	font-size: 22px;
    color: white;
    line-height: 1.4em;
	margin: 0 0 95px 0;
}
#send_bnr_txt{
	position:relative;
	top:160px;
}
#send_bnr_txt button{
	position: absolute;
    background-color: #fee685;
    color: #ed1d24;
    border: none;
    font-weight: lighter;
    height: 60px;
    width: 60%;
}
#send_bnr_txt button:focus{
	outline: #fee685;
    box-shadow: none;
}
#send_bnr_txt button i{
	padding-left: 45px;
    margin: 0px 10px 0px 10px;
    position: relative;
}
#send_bnr_txt button:hover i{
	left: 10px;
	transition: inherit;
}
#snd_bnr img{
    height: 100%;
}
@media (max-width:1390px){
	#send_bnr_txt p{
		margin: 0 0 30px 0;
	}
}
#send_part ul li{
	list-style-type: none;
    display: inline-block;
    height: 80px;
    width: 80px;
	}
#send_part ul li img{
	width: 50%;
}
#snd_bnr {
   width: 60%;
    margin: auto;
    height: 200px;
    display: flex;
}
#snd_bnr1{
	width: 25%;
    height: 20%;
    padding-left: 20px;
	
}
#cnt_two{
	font-family: 'Baloo Thambi 2', cursive;
}
#join{
	font-family: 'Baloo Thambi 2', cursive;
	padding-top:70px;
}
#snd_bnr1 img{
	width:100%;
	height:100%;
}
#cnt_two1 h2{
	font-size: 21px;
	font-family: 'Baloo Thambi 2', cursive;
}
@media (max-width:1330px){
	#snd_bnr {
	height: 150px;
	width: fit-content;
}
}
</style>
<!-- 
    ====== Services Start ==============
 -->
 

<div class="rt-breadcump rt-breadcump-height">
    <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/gopack.jpg)">
    </div><!-- /.rt-page-bg -->
    <div class="container">
        <div class="row rt-breadcump-height">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h3>Go Send</h3>
                    <p style="font-size: 30px;">Deliveries for Your Business</p>
                  
                    <!-- /.breadcrumbs -->
                </div><!-- /.breadcrumbs-content -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>

	<!-- <div class="send_rt">
		<div class="container">
			
				<div class="col-lg-5" id="send_bnr_txt">
					<p><strong>Finigoo Courier</strong></p>
					<h2>Reliable Delivery </br> for Your Business.</h2>
					<p>Finigoo Courier guarantees reliable delivery of your product to your customer, at the right location in the right time through its efficient distribution management.	</p>
					<button class="btn btn-default btn-lg">Become a Merchant<i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
				</div>
			
		</div>
	</div> -->
	<div class="section-title-spacer"></div>
			<div class="section-title-spacer"></div>
			<!-- <div class="container-fluid">
				<div class="container" id="send_part">
					<div class="row row justify-content-md-center">
							
							<div class="col-lg-6">
								<h2 class="text-center">Trusted by 3000+ Businesses</h2>
								</br>
								</br>
								<ul>
									<li>
										<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/flt-logo-1.png">
									</li>
									<li>
										<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/2.png">
									</li>
									<li>
										<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/4.png">
									</li>
									<li>
										<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/1.png">
									</li>
									<li>
										<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/3.jpg">
									</li>
									<li>
										<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/5.png">
									</li>
								</ul>
							</div>
							
					</div>
				</div>
			</div> -->
			
			
            			
	<div class="container" id="cnt_two">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="text-center">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/LiveProduct.png" style="width: 100px;">
				</div>
				</br>
				</br>
				<div class="col-lg-12">
					<h2 class="text-center"><b>Live Product</br>Status</b></h2>
					</br>
					<p class="text-center">We provide you the option of real time delivery state with which you can know the current status of product delivery.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="text-center">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/CallCenter.png" style="width: 100px;">
				</div>
				</br>
				</br>
				<div class="col-lg-12">
					<h2 class="text-center"><b>Call Center</br>Support</b></h2>
					</br>
					<p class="text-center">Call center support and key account managers are provided for all sorts of queries and needs of the clients.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="text-center">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/100PerInsurance.png" style="width: 100px;">	
				</div>
				</br>
				</br>
				<div class="col-lg-12">
					<h2 class="text-center"><b>100% Insurance</br>Coverage</b></h2>
					</br>
					<p class="text-center">We take full responsibility of the deliveries by providing 100% insurance coverage.</p>
				</div>
			</div>
		</div>
	</div>
	
	
			<div class="section-title-spacer"></div>
			<div class="section-title-spacer"></div>
			
	
	<!----------------------------------------------------------------------->
	
	
	
	<div class="container" id="cnt_two1">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div id="snd_bnr1">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/download-16.png">
				</div>
				
				<div class="col-lg-12">
					<h2 class=""><b>Standard Delivery & </br>Express Delivery</b></h2>
					</br>
					<p class="">Delivery within Metropolitan cities of Dhaka, Chittagong, Sylhet, Khulna, Rajshahi within 24-72 HRS On-demand/Express Delivery within 4-6 HRS from point of pick-up to the customer (Dhaka Only)</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div id="snd_bnr1">
						<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/download-17.png">
				</div>
				
				<div class="col-lg-12">
					<h2 class=""><b>Nationwide</br>Delivery</b></h2>
					</br>
					<p class="">Offering home delivery to 50+ Districts Nationwide where your packages will be delivered to your customers’ doorstep within 48-72 HRS<p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div id="snd_bnr1">
						<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/download-18.png">	
				</div>
				
				<div class="col-lg-12">
					<h2 class=""><b>Fulfillment </br> solution</b></h2>
					</br>
					<p class="">Through warehousing and fulfillment solution, offers customized service packages with inventory management support, order processing, packaging, after sales support.</p>
				</div>
			</div>
		</div>
		
	</div>
	
			<div class="section-title-spacer"></div>
			
	<div class="container" id="cnt_two1">
	
	<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div id="snd_bnr1">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/download-19.png">
				</div>
				
				<div class="col-lg-12">
					<h2 class=""><b>Corporate Service</br>/Contract Logistics</b></h2>
					</br>
					<p class="">We offer customized service packages with inventory management support, order processing, packaging, after sales support.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div id="snd_bnr1">
						<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/download-20.png">
				</div>
				
				<div class="col-lg-12">
					<h2 class=""><b>Cash on </br> Delivery</b></h2>
					</br>
					<p class="">We provide the option of cash on delivery by collecting the payment in cash at the time of delivery</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div id="snd_bnr1">
						<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/download-21.png">	
				</div>
				<div class="col-lg-12">
					<h2 class=""><b>Reverse</br>Logistics</b></h2>
					</br>
					<p class="">Reverse logistics is provided that allows customers to return their products</p>
				</div>
			</div>
		</div>
	
	</div>
			
			
			<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
	<div class="container-fluid" id="share">
		<div class="container">
			<div class="row">
				<div class="col-lg-6" id="join">
					<h1>Reliability is Our First Priority</h1>
					<p>We offer the lowest price with the highest value through our innovative logistics design. We deliver your courier at the right location at the right time, currently in 50+ districts throughout the country.</p>
					</br>
					</br>
				</div>
				<div class="col-lg-6">
					<img src="<?php echo base_url();?>landing-assets/assets/images/backgrounds/tab1.jpg">
				</div>
			</div>
		</div>
	</div>
