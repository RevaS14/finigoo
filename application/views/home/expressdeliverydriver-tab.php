<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/driver-register.css">
<style>
	.works3-area .rt-inner-overlay {
		height: 439px;
		background-image: -webkit-linear-gradient(82deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
		background-image: linear-gradient(8deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
	}

	/*second nav */

	#myTab-2 li a {
		border: .8px solid #ad72056b;
		border-radius: 25px;
		color: black;
		padding: 6px 30px;
		font-size: 14px;
	}

	#myTab-2 li a.active {
		color: #fff;
		background-color: #01AC00;
		font-size: 14px;
		display: inline-block;
		padding: 6px 30px;
		border-radius: 25px;
		/*border: .8px solid black;*/
	}

	/*footer subcribe*/

	.rt-site-footer .footer-top .footer-subscripbe-box .btn {
		font-family: 'Poppins', sans-serifs;
		font-size: 20px;
		font-size: 14px;
		font-weight: 600;
		position: absolute;
		top: 3px;
		right: 3px;
		height: 51px;
		padding: 0 40px;
		text-transform: uppercase;
		border-radius: 999px;
		/*position: relative;*/
		z-index: 99999;
		color: #fff;
		border: none;
		background: -webkit-linear-gradient(55deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
		background: linear-gradient(35deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
	}

	/*back to top button*/

	#scrollUp {
		font-size: 30px;
		line-height: 48px;
		position: fixed;
		right: 30px;
		bottom: 30px;
		width: 45px;
		height: 45px;
		text-align: center;
		color: #fff;
		border-radius: 50%;
		background: #ED1D24;
	}

	/*services hover bg color*/

	.main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
	.main-menu ul ul>li:hover>a {
		padding-left: 23px;
		color: #fff;
		background: #ED1D24;
	}

	#aa a {
		color: black;
	}

	#aa a:hover {
		color: #b6b1b1;
	}

	@media only screen and (max-width:767px) {
		.related-post .blog-grid {
			margin-bottom: 15px;
		}

		.related-post .archive-link {
			display: none;
		}

		.faq-section .archive-link .blogall-btn {
			width: 199px;
		}
	}

	.work-step-section .card-header .btn-link {
		outline: none;
	}

	.earnway-menu-item li a {
		font-size: 14px;
	}

	.cstform {
		height: 45px !important;
		font-size: 14px !important;
	}

	.title_submenu {
		font-size: 20px;
		color: #e82801 !important;
	}
</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- start earn way section -->
<section class="pt60 earn-way-section pt150">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title text-center">
					<h2 class="mb20">Earn with Your Bike or Car</h2>
					<p>Become a captain, rider or foodman on the highest earning platform!</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="pb100 bg-light earn-way-content">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-6 left-side-form">
				<div class="theiaStickySidebar">
					<div class="earnway-single-menu-item">
						<ul id="menu-earn-menu" class="earnway-menu-item">
							<li id="menu-item-238" class="bike menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item"><a href="<?php echo base_url(); ?>godriver-tab" aria-current="page">Go Driver</a>
							</li>
							<li id="menu-item-249" class="car menu-item menu-item-type-post_type menu-item-object-page"><a href="<?php echo base_url(); ?>food-deliverydriver-tab">Food Delivery Driver</a>
							</li>
							<li id="menu-item-259" class="cycle menu-item menu-item-type-post_type menu-item-object-page current_page_item"><a href="<?php echo base_url(); ?>express-deliverydriver-tab">Express Delivery Driver</a>
							</li>
						</ul>
					</div>
					<div class="earn-bike-firststep-form firststep-form earn-form form-box">
						<form id="earnBikeFirstStepForm">
							<h3 style="color:#01AC00">Sign up with Express Delivery Driver</h3>
							<p>Enter your information below to get started</p>
							<div class="row">
								<div class="col-12 col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control cstform" id="firstname" placeholder="First Name">
									</div>
								</div>
								<div class="col-12 col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control cstform" id="firstname" placeholder="Last Name">
									</div>
								</div>
							</div>
							<div class="row cstrow">
								<div class="col-12 cstcol">
									<div class="form-group">
										<input type="text" class="form-control cstform" id="firstname" placeholder="+91      Phone Number">
									</div>
								</div>
							</div>
							<!-- <div class="row cstrow">
            <div class="col-xs-12 cstcol">
                <div class="form-group">
                    <input type="email" class="form-control cstform" id="firstname" placeholder="Email Address">
                </div>
            </div>
        </div> -->
							<div class="row cstrow">
								<div class="col-12 cstcol">
									<div class="form-group">
										<select class="form-control cstform">
											<option value="" selected="selected">What is your vechile type?</option>
											<option value="MB">Motorbike</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row cstrow">
								<div class="col-12 cstcol">
									<div class="form-group">
										<select class="form-control cstform">
											<option value="" selected="selected">How did you hear about GrabExpress?</option>
											<option value="Outdoor">Outdoor</option>
											<option value="TV">TV</option>
											<option value="Radio">Radio</option>
											<option value="Newspaper / Print">Newspaper / Print</option>
											<option value="Friends &amp; Family">Friends &amp; Family</option>
											<option value="Grab Driver">Grab Driver</option>
											<option value="Grab Staff">Grab Staff</option>
											<option value="Event">Event</option>
											<option value="Forum / Blog">Forum / Blog</option>
											<option value="Internet Ad &amp; Search">Internet Ad &amp; Search</option>
											<option value="Facebook">Facebook</option>
											<option value="Twitter">Twitter</option>
											<option value="Instagram">Instagram</option>
											<option value="Others">Others</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row cstrow">
								<div class="col-12 cstcol">
									<div class="form-group">
										<input type="text" class="form-control cstform" id="firstname" placeholder="Referral Code">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-12" style="text-align: center;">
									<button type="button" style="padding: 8px 14px;" class="btn btn-success submitcstbtn">SIGN UP NOW</button>
								</div>
							</div>
							<div class="row cstrow">
								<div class="col-12 cstcol">
									<p style="font-size: 12px;text-align: center;margin-top: 10px;color: gray;">By proceeding, I agree that you can collect, use and disclose the information provided by me in accordance with your <a href="#">Privacy Policy</a> which I have read and understand.</p>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-6 right-side-content">
				<div class="theiaStickySidebar">
					<div class="service-area-section">
						<div class="section-title alt mt50">
							<h2 class="mb10">Got a bike? </h2>
							<p class="mb50">These are the services you can be a part of!</p>
						</div>
						<ul class="service-area-list">
							<li>
								<img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/icon-bike.jpg" alt="Bike Rider">Bike Rider</li>
							<li>
								<img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/icon-food.jpg" alt="Food Man">Food Man</li>
							<li>
								<img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/icon-delivery.jpg" alt="Parcel Delivery ">Parcel Delivery</li>
						</ul>
					</div>
					<div class="job-part-section bdt">
						<div class="section-title alt">
							<h2 class="mb20">Why Join the finigoo Family?</h2>
							<p class="mb50i">Being with finigoo means being on the highest earning platform! So, what are you waiting for? Join us to earn the most!</p>
						</div>
						<ul class="job-part-list">
							<li> <span class="job-part-image">
									<img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/icon-insured.png" alt="Your Ride is Insured">
								</span>
								<span class="job-part-info">
									<span class="job-part-title">
										Your Ride is Insured </span>
									<span class="job-part-details">
										finigoo cares about your safety. And to keep you safe, finigoo is giving you insurance coverage. </span>
								</span>
							</li>
							<li> <span class="job-part-image">
									<img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/icon-bonus.png" alt="Earn More with Bonus">
								</span>
								<span class="job-part-info">
									<span class="job-part-title">
										Earn More with Bonus </span>
									<span class="job-part-details">
										With finigoo’s daily quests and attractive special offers, you can extra regularly. </span>
								</span>
							</li>
							<li> <span class="job-part-image">
									<img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/icon-time.png" alt="Get Your Payment on Time">
								</span>
								<span class="job-part-info">
									<span class="job-part-title">
										Get Your Payment on Time </span>
									<span class="job-part-details">
										With finigoo, you will never face a delay in payment. Get your payment in the shortest time! </span>
								</span>
							</li>
						</ul>
					</div>
					<div class="join-part-section bdt">
						<div class="section-title alt">
							<h2 class="mb20">Know if You’re Eligible to Join Us</h2>
							<p class="mb50i">Not sure if you’re eligible to be a rider? If you have the following, you can join us!</p>
						</div>
						<ul class="join-part-list">
							<li> <span class="join-number-area">
									<span class="join-number">
										1 </span>
								</span>
								<div class="join-part-info"> <span class="join-part-title">
										You must have an Android phone </span>
									<span class="join-part-details">
										With a smartphone, you can easily download and work with the finigoo Drive app. </span>
								</div>
							</li>
							<li> <span class="join-number-area">
									<span class="join-number">
										2 </span>
								</span>
								<div class="join-part-info"> <span class="join-part-title">
										You must be at least 18 years old </span>
									<span class="join-part-details">
										To be a part of finigoo, you must be an adult. </span>
								</div>
							</li>
							<li> <span class="join-number-area">
									<span class="join-number">
										3 </span>
								</span>
								<div class="join-part-info"> <span class="join-part-title">
										You must have a driving license </span>
									<span class="join-part-details">
										To be a rider of finigoo, you must have a valid driver’s license. </span>
								</div>
							</li>
							<li> <span class="join-number-area">
									<span class="join-number">
										4 </span>
								</span>
								<div class="join-part-info"> <span class="join-part-title">
										You must have bike registration documents </span>
									<span class="join-part-details">
										To register your bike with finigoo, you must have your bike’s valid registration documents. </span>
								</div>
							</li>
							<li> <span class="join-number-area">
									<span class="join-number">
										5 </span>
								</span>
								<div class="join-part-info"> <span class="join-part-title">
										National ID/Passport </span>
									<span class="join-part-details">
										In order to register as a rider, you either have an updated national ID or passport. </span>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end earn way section -->
<!-- start work step section -->
<section class="pt150 work-step-section">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-12 col-md-7">
				<div class="section-title">
					<h2 class="mb50">How you can give a ride?</h2>
				</div>
				<div class="accordion" id="accordionbike">
					<div class="card">
						<div class="card-header" id="bikeOne">
							<h5 class="mb-0">
								<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapsebikeOne" aria-expanded="true" aria-controls="collapsebikeOne">
									<span class="work-step-number">1</span>
									Download the Drive App
									<i class="collapse-arrow-direction"></i>
								</button>
							</h5>
						</div>
						<div id="collapsebikeOne" class="collapse show" aria-labelledby="bikeOne" data-parent="#accordionbike">
							<div class="card-body">
								<p>Be sure to download the finigoo Drive app.</p> <a class="app-image" href="#"><img width="183" height="55" src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/Google-Play-ds.png" title="Google-Play-ds"></a>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" id="bikeTwo">
							<h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsebikeTwo" aria-expanded="false" aria-controls="collapsebikeTwo">
									<span class="work-step-number">2</span>
									Sign up and keep your status active
									<i class="collapse-arrow-direction"></i>
								</button>
							</h5>
						</div>
						<div id="collapsebikeTwo" class="collapse" aria-labelledby="bikeTwo" data-parent="#accordionbike">
							<div class="card-body">
								<p>Sign up on the drive app and keep your status online.</p>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" id="bikeThree">
							<h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsebikeThree" aria-expanded="false" aria-controls="collapsebikeThree">
									<span class="work-step-number">3</span>
									Get ride requests
									<i class="collapse-arrow-direction"></i>
								</button>
							</h5>
						</div>
						<div id="collapsebikeThree" class="collapse" aria-labelledby="bikeThree" data-parent="#accordionbike">
							<div class="card-body">
								<p>Make sure your GPS is at high accuracy and wait for a ride request.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-5">
				<div class="earn-step-image bg-image-alt" style="background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/all-img/mobile-screen-2.png);">
					<img width="343" height="610" src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/Bike-Request-Screen-.png" title="Bike-Request-Screen-">
				</div>
			</div>
		</div>
	</div>
</section>