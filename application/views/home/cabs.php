<style>
	.works3-area .rt-inner-overlay {
	    height: 439px;
	    background-image: -webkit-linear-gradient(82deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
	    background-image: linear-gradient(8deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
	}
	    /*second nav */
	    #myTab-2 li a{
	         border: .8px solid #ad72056b;
	         border-radius: 25px;
	         color:black;
	         padding: 6px 30px;
	          font-size: 14px;
	    }
	    #myTab-2 li a.active {
	    color: #fff;
	    background-color: #01AC00;
	    font-size: 14px;
	    display: inline-block;
	    padding: 6px 30px;
	    border-radius: 25px;
	    /*border: .8px solid black;*/
	}
	/*footer subcribe*/
	.rt-site-footer .footer-top .footer-subscripbe-box .btn {
	        font-family: 'Poppins', sans-serifs;
	    font-size: 20px;
	    font-size: 14px;
	    font-weight: 600;
	    position: absolute;
	    top: 3px;
	    right: 3px;
	    height: 51px;
	    padding: 0 40px;
	    text-transform: uppercase;
	    border-radius: 999px;
	    /*position: relative;*/
	    z-index: 999;
	    color: #fff;
	    border: none;
	    background: -webkit-linear-gradient(55deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
	    background: linear-gradient(35deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
	}
	/*back to top button*/
	#scrollUp {
	    font-size: 30px;
	    line-height: 48px;
	    position: fixed;
	    right: 30px;
	    bottom: 30px;
	    width: 45px;
	    height: 45px;
	    text-align: center;
	    color: #fff;
	    border-radius: 50%;
	    background: #01AC00;
	}
	/*services hover bg color*/
	.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
	    padding-left: 23px;
	    color: #fff;
	    background: #01AC00;
	}
	.icofont-check-circled:before {
	    content: "\eed7";
	    color: #01ac00;
	}
	.submenufix.show{
		background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.38)), url(?php echo base_url(); ?>landing-assets/assets/images/extra/car.jpg);
	    background-size:100% 100%;
	}
</style>
<div class="rt-breadcump rt-breadcump-height">
	<div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/gocab1.jpg)"></div>
	<!-- /.rt-page-bg -->
	<div class="container">
		<div class="row rt-breadcump-height">
			<div class="col-12">
				<div class="breadcrumbs-content">
					<h3>Go Cab</h3>
					<p style="font-size: 30px;">Go Mart Delivery at Your Doorstep</p>
					<!--<h3>Go Cab</h3>-->
					<!--  <div class="breadcrumbs">
                        <span class="divider"><i class="icofont-home"></i></span>
                        <a href="#" title="Home">Home</a>
                        <span class="divider"><i class="icofont-simple-right"></i></span>
                        <a href="#">China</a>
                        <span class="divider"><i class="icofont-simple-right"></i></span>
                        <a href="#">Hotels</a>
                        <span class="divider"><i class="icofont-simple-right"></i></span>
                        <a href="#" title="Home">Hong Kong Hotels</a>
                        <span class="divider"><i class="icofont-simple-right"></i></span>
                        Empire Hotel Kowloon - Tsim Sha Tsui

                    </div> -->
					<!-- /.breadcrumbs -->
				</div>
				<!-- /.breadcrumbs-content -->
			</div>
			<!-- /.col-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</div>
<!-- /.rt-bredcump -->
<!-- <div class="container" id="tabb" >
<div class="flight-list-box rt-mb-40">
                        <ul class="nav pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                              <li class="nav-item">
                                <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="#rt-itm_1" role="tab"
                                    aria-controls="rt-itm_1" aria-selected="true">Go Cab</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_3-tab" data-toggle="tab" href="#rt-itm_3" role="tab"
                                    aria-controls="rt-itm_3" aria-selected="false">Contact Us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_4-tab" data-toggle="tab" href="#rt-itm_4" role="tab"
                                    aria-controls="rt-itm_4" aria-selected="false">Driver Join</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_5-tab" data-toggle="tab" href="#rt-itm_5" role="tab"
                                    aria-controls="rt-itm_5" aria-selected="false">Policies</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_6-tab" data-toggle="tab" href="#rt-itm_6" role="tab"
                                    aria-controls="rt-itm_6" aria-selected="false">Reviews</a>
                            </li>
                        </ul>
                    </div>
</div> -->
<section class="content-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 mx-auto text-center">
				<div class="rt-section-title-wrapper">
					<h2 class="rt-section-title"><span style="color:#01AC00">Go Cab</span>
                   
Your ride is just a click away!</h2>
				</div>
				<!-- /.rt-section-title-wrapper -->
			</div>
			<!-- /.col-lg-8 -->
		</div>
		<!-- ./row -->
		<div class="section-title-spacer"></div>
		<!-- /.section-title-spacer -->
		<div class="row align-items-center">
			<div class="col-lg-5 mb-0 mb-md-4">
				<h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInDown" data-wow-duration="1s">Our app makes ordering a ride easier and more convenient:</h4>
				<div class="text-424 wow fadeInUp" data-wow-duration="1s">
					<ul class="rt-list f-size-14">
						<li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>ride price is known in advance, helping you to plan out your expenses;</li>
						<li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>scheduled orders ensure that your car will arrive on time;</li>
						<li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>online map shows you your driver's current location;</li>
						<li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>ride templates allow you to place your order in a couple of taps.</li>
					</ul>
				</div>
				<!-- /.text-424 -->
			</div>
			<!-- /.col-lg-6 -->
			<div class="col-lg-6 offset-lg-1">
				<img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/gocarr.png" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
			</div>
			<!-- /.col-lg-6 -->
		</div>
		<!-- /.row -->
		<div class="section-title-spacer"></div>
		<!-- /.section-title-spacer -->
		<div class="row align-items-center">
			<div class="col-lg-6 mb-0 mb-md-4">
				<img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/work-img-7.jpeg" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
			</div>
			<!-- /.col-lg-6 -->
			<div class="col-lg-5 offset-lg-1">
				<h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInUp" data-wow-duration="1s">Why Go Cab?</h4>
				<div class="text-424 wow fadeInDown" data-wow-duration="1s">
					<p class="f-size-14 text-424"><i class="icofont-check-circled"></i>
						Because we believe in investing extensively in ensuring that each and every journey is a special one - from the time of booking to the end of your journey.</p>
				</div>
				<!-- /.text-424 -->
			</div>
			<!-- /.col-lg-6 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- ./ copntainer -->
</section>
<!-- 
    ======== call to action start====
 -->
<section class="caltoaction-4 rt-dim-light rt-pt-100 rt-pb-100">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h4 class="f-size-40 rt-semibold rt-mb-40 f-size-md-32 f-size-xs-26">India's Largest Intercity and Local Cab Services</h4>
				<p>We are Go Cab, an online cab booking aggregator, providing customers with reliable and premium Intercity and Local car rental services. we are uniquely placed as the largest chauffeur driven car rental company in India in terms of geographical reach.</p>
			</div>
			<!-- /.col-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>