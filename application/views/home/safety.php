<style>
    /*second nav */
    #myTab-2 li a {
        border: .8px solid #ad72056b;
        border-radius: 25px;
        color: black;
        padding: 6px 30px;
        font-size: 14px;
    }

    #myTab-2 li a.active {
        color: #fff;
        background-color: #E92C3C;
        font-size: 14px;
        display: inline-block;
        padding: 6px 30px;
        border-radius: 25px;
        /*border: .8px solid black;*/
    }

    /*footer subcribe*/
    .rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
        font-size: 20px;
        font-size: 14px;
        font-weight: 600;
        position: absolute;
        top: 3px;
        right: 3px;
        height: 51px;
        padding: 0 40px;
        text-transform: uppercase;
        border-radius: 999px;
        /*position: relative;*/
        z-index: 1;
        color: #fff;
        border: none;
        background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
        background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
    }

    /*back to top button*/
    #scrollUp {
        font-size: 30px;
        line-height: 48px;
        position: fixed;
        right: 30px;
        bottom: 30px;
        width: 45px;
        height: 45px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
        background: #E92C3C;
    }

    /*services hover bg color*/
    .main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
    .main-menu ul ul>li:hover>a {
        padding-left: 23px;
        color: #fff;
        background: #E92C3C;
    }

    .sticky {
        position: fixed;
        top: 16%;
        width: 100%;
        z-index: 99;
        animation-name: fadeInDown;
    }

    .rt_bnd {

        position: relative;
        top: 93px;
        margin-bottom: 95px;
        height: 100%;
        width: 100%;
    }

    #slider {
        width: 100%;
        margin: 0 auto;
    }

    #slider img {
        width: 100%;
        height: 700px;
    }

    #light button {
        border-radius: 50px;
    }

    #light h1 {
        line-height: 50px;
    }

    #light_txt {
        padding-left: 95px;
    }

    #light img {
        height: 400px;
        width: 80%;
        margin: auto;
        display: flex;
    }

    #txt_para {
        padding: 50px;
    }

    #caro .carousel-inner {
        border-radius: 50px;
        height: 400px;
    }

    #sliderz {
        padding: 0px;
    }

    #slider1 {
        padding: 0px;
    }

    #slider1 .carousel-caption {

        top: 30%;
        left: 10%;
        text-align: left;

    }
</style>
<!-- 
    ====== Services Start ==============
 -->

<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container-fluid" id="sliderz">

    <div class="row" id="slider">
        <div class="col-lg-12" id="slider1">
            <div id="carousel" class="carousel slide " data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel" data-slide-to="1"></li>
                    <li data-target="#carousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/ads3.jpg" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h1><b>Mau tau cara #AmanBersamafinigoo ?</b></h1>
                            <h2><b>Cari tau sekarang!</b></h2>
                            <button class="btn btn-primary" type="button" tabindex="0">Klik di sini</button>
                        </div>


                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/ads2.jpg" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/ads1.jpg" alt="Third slide">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h2><b>We are committed to your safety and security</b></h2>
            </br>
            <p><b>We take the initiative to equip you with knowledge, technology advancement and protection to keep you safe with finigoo. Check out our three safe steps below:</b></p>


        </div>
    </div>
</div>

<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container" id="light">
    <div class="row">
        <div class="col-lg-6">
            <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/light1.jpg" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
        </div>
        <div class="col-lg-6" id="light_txt">
            <h1><b>#1 We equip you with knowledge</b></h1>
            <p>We believe that knowledge is power, so we provide you with information related to safety and security measures. Apply these steps to avoid various types of fraud.
                </br>
                </br>
                <b>Don't pay anything if it’s not through finigoo app</br>
                    Secure your personal data, do not share the OTP code</br>
                    Use a PIN, and</br>
                    File any suspicious action through finigoo’s official email and help page that you can access from finigoo app</p>
            </br>
            <button type="button" class="btn btn-primary"><b>Learn More</b></button>
        </div>
    </div>
    </br>
    </br>
    </br>
    <div class="row">
        <div class="col-lg-6">
            <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/sequre.jpg" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
        </div>
        <div class="col-lg-6" id="light_txt">
            <h1><b>#2 We facilitate you with the latest technology advancement</b></h1>
            <p>finigoo SHIELD is a series of finigoo’s technology advancement that we have prepared to maintain your safety and security. Starting from providing notification feature when potential fraud is detected, to emergency button we hope you will never use.</p>
            </br>
            </br>
            <button type="button" class="btn btn-primary"><b>Learn More</b></button>
        </div>
    </div>
    </br>
    </br>
    </br>
    <div class="row">
        <div class="col-lg-6">
            <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/light.jpeg" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
        </div>
        <div class="col-lg-6" id="light_txt">
            <h1><b>#3 We protect with you with insurance coverage</b></h1>
            <p>We always ensure that you get the best services from finigoo’s reliable partners with insurance coverage to minimize risks.</p>
            </br>
            </br>
            <button type="button" class="btn btn-primary"><b>Learn More</b></button>
        </div>
    </div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>

<div class="container">
    <h1><b>finigoo and Komkominfo Collaboration</b></h1>
    </br>
    <div class="row">
        <div class="col-lg-7">
            <iframe width="100%" height="400px" src="https://www.youtube.com/embed/RLXctHmSBI8?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-lg-5" id="txt_para">
            <h2><b>Practice our safe steps</b></h2>
            <p>Be aware of various types of fraud, including psychological manipulation that is commonly used. Through a collaboration video with the Ministry of Communication and Information, we hope you will fully understand how psychological manipulation can occur and know how to avoid it.
                </br>
                Don't forget, always be vigilant and follow our safe steps.</p>
        </div>
    </div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container" id="caro">
    <div class="row">
        <div class="col-lg-12">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/bike1.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/tab.jpg" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/tab1.jpg" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>