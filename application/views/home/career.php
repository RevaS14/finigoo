<style>
	/*second nav */
	    #myTab-2 li a{
	         border: .8px solid #ad72056b;
	         border-radius: 25px;
	         color:black;
	         padding: 6px 30px;
	          font-size: 14px;
	    }
	    #myTab-2 li a.active {
	    color: #fff;
	    background-color: #E92C3C;
	    font-size: 14px;
	    display: inline-block;
	    padding: 6px 30px;
	    border-radius: 25px;
	    /*border: .8px solid black;*/
	}
	/*footer subcribe*/
	.rt-site-footer .footer-top .footer-subscripbe-box .btn {
	        font-family: 'Poppins', sans-serifs;
	    font-size: 20px;
	    font-size: 14px;
	    font-weight: 600;
	    position: absolute;
	    top: 3px;
	    right: 3px;
	    height: 51px;
	    padding: 0 40px;
	    text-transform: uppercase;
	    border-radius: 999px;
	    /*position: relative;*/
	    z-index: 1;
	    color: #fff;
	    border: none;
	    background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
	    background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
	}
	/*back to top button*/
	#scrollUp {
	    font-size: 30px;
	    line-height: 48px;
	    position: fixed;
	    right: 30px;
	    bottom: 30px;
	    width: 45px;
	    height: 45px;
	    text-align: center;
	    color: #fff;
	    border-radius: 50%;
	    background: #E92C3C;
	}
	/*services hover bg color*/
	.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
	    padding-left: 23px;
	    color: #fff;
	    background: #E92C3C;
	}
	.sticky {
	  position: fixed;
	  top: 16%;
	  width: 100%;
	  z-index: 99;
	  animation-name: fadeInDown;
	}
	.rt{
	    background: url(<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/home-finigoo1.jpg);
	    background-repeat: no-repeat;
	    background-size: 100% 100%;
	    height: 100%;
		position:relative;
		top:93px;
		margin-bottom:95px;
	}
	#rt_text{
		position:relative;
		top:30%;
		color:white;
		font-weight:600;
	}
	#rt_text h3{
	line-height: 33px;
	    text-align: initial;
	    font-weight: bold;
	    font-size: 22px;
	    color: #fff;
	}
	#rt_text button{
		color: black;
	    background-color: #fee685;
	    border-color: transparent;
		padding: 10px 23px;
	    font-size: 16px;
		font-weight:bold;
	}
	.rt:after{
		content:"";
		display:table;
		clear:both;
	}
	#bnr1{
		background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/all-img/career1.jpg);
	    height: 400px;
	    background-size: cover;
		background-position: center;
	}
	#txt1 h3{
		font-weight:700; 
	}
	#txt1 p{
		font-weight:700; 
		color: #747474;
	}
	#team{
	    background-color:#f5f8f9;
	    padding: 10px 15px;
	}
	.align{
		height: 150px;
	    background-color: #ec1d23;
	    margin-bottom: 30px!important;
		color:white;
		display: table;
	}
	.align1{
		width:80%;
		margin:auto;
	}
	.align h3{
		text-align: center;
	    vertical-align: middle;
	    display: table-cell;
		font-size: medium;
	}
	.align a{
		text-decoration:none;
		color:white;
	}
	#team button{
		width: auto;
	    margin: auto;
		color:white;
		font-weight:700;
	    background-color:#ec1d23;
	    padding: 12px 20px;
	    font-size: 16px;
	}
	#ser{
		margin-bottom:50px;
	}
	#team1{
	margin-top:30px;
		margin-bottom:30px;
	}
	#team1 h3{
		text-align:center;
		font-weight:700;
	}
	#txt2 h3{
		font-weight:700; 
	}
	#txt2 p{
		font-weight:700; 
		color: #747474;
	}
	.image{
		height: 150px;
	    width: 50%;
	    margin: auto;
		margin-bottom:20px;
		background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/all-img/career1.png);
		background-size: contain;
	    background-position: center;
	    background-repeat: no-repeat;
	}
	.image1{
		height: 150px;
	    width: 50%;
	    margin: auto;
		margin-bottom:20px;
		background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/all-img/career2.png);
		background-size: contain;
	    background-position: center;
	    background-repeat: no-repeat;
	}
	.image2{
		height: 150px;
	    width: 50%;
	    margin: auto;
		margin-bottom:20px;
		background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/all-img/work-img-5.png);
		background-size: contain;
	    background-position: center;
	    background-repeat: no-repeat;
	}
	#end{
		text-align:center;
		font-weight:bold;
	}
	#end h4{
		text-align:center;
		font-weight:bold;
	}
	#end p{
		text-align:center;
		font-weight:bold;
		color: #747474;
		font-size:13px;
	}
	#end button{
		width: auto;
	    margin: auto;
		color:white;
		font-weight:700;
	    background-color:#ec1d23;
	    padding: 12px 20px;
	    font-size: 16px;
	}
	.thumbnail {
	    margin-bottom: 6px;
	    height: 300px;
	    width: 100%;
	}
	
	.carousel-control.left,.carousel-control.right{
	  background-image:none;
	  margin-top:10%;
	  width:5%;
	}
</style>
<!-- 
    ====== Services Start ==============
 -->
<div class="rt">
	<div class="container" id="rt_text">
		<div class="row">
			<div class="col-lg-4">
				<h3>We are driven by our mission to simplify and improve the lives of people and build an awesome organisation that inspire</h3>
				<button type="btn" class="btn btn-default btn-lg">Join Our Team</button>
			</div>
		</div>
	</div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-6" id="bnr1" style="padding-right:30px;"></div>
				<div class="col-lg-6 align-items-center" id="txt1" style="padding-top:100px;padding-left:30px;">
					<h3>Join our Journey</h3>
					<p>Be Part of FiniGoo to bring a world-class financial & Multi products for the new-age India. Working at FiniGoo is a rewarding experience! Great people, a work environment that thrives on creativity, the opportunity to take on roles beyond a defined job description are just some reasons you should work with us.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- /.col-12 -->
</div>
<!-- /.row -->
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container-fluid" id="team">
	<div class="container" id="team1">
		<h3>Browse by team</h3>
		</br>
		</br>
		<div class="row" id="ser">
			<div class="col-6 col-sm-4 col-md-3">
				<div class="col-lg-12 align" style="margin:auto;">
					<h3><a href="jobs.html" target="_blank">Admin</a></h3>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-3">
				<div class="col-lg-12 align" style="margin:auto;">
					<h3><a href="jobs.html" target="_blank">Brand & PR</a></h3>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-3">
				<div class="col-lg-12 align" style="margin:auto;">
					<h3><a href="jobs.html" target="_blank">Bussiness</a></h3>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-3">
				<div class="col-lg-12 align" style="margin:auto;">
					<h3><a href="jobs.html" target="_blank">Customer Experience</a></h3>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-3">
				<div class="col-lg-12 align" style="margin:auto;">
					<h3><a href="jobs.html" target="_blank">Engineering</a></h3>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-3">
				<div class="col-lg-12 align" style="margin:auto;">
					<h3><a href="jobs.html" target="_blank">Product</a></h3>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-3">
				<div class="col-lg-12 align" style="margin:auto;">
					<h3><a href="jobs.html" target="_blank">Finance</a></h3>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-3">
				<div class="col-lg-12 align" style="margin:auto;">
					<h3><a href="jobs.html" target="_blank">Human Resource</a></h3>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-3">
				<div class="col-lg-12 align" style="margin:auto;">
					<h3><a href="jobs.html" target="_blank">Product Design</a></h3>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-3">
				<div class="col-lg-12 align" style="margin:auto;">
					<h3><a href="jobs.html" target="_blank">Add Sales</a></h3>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-3">
				<div class="col-lg-12 align" style="margin:auto;">
					<h3><a href="jobs.html" target="_blank">Marketing</a></h3>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-3">
				<div class="col-lg-12 align" style="margin:auto;">
					<h3><a href="jobs.html" target="_blank">Media</a></h3>
				</div>
			</div>
			</br>
			</br>
			<button type="btn" class="btn btn-default btn-lg">Click here to explore jobs and apply</button>
		</div>
	</div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-6" id="txt2">
				<h3><b>Our Culture</b></h3>
				<p>We think that you can do your best with a freedom to experiment, sense of ownership of your work, license to fail and when you have peers who care just as much as you about their art.</p>
			</div>
			<div class="col-lg-6">
				<iframe style="width:100%; height: 250px;" src="https://www.youtube.com/embed/RLXctHmSBI8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container">
	<div class="col-lg-12" id="end">
		<div class="row">
			<div class="col-lg-4">
				<div class="image"></div>
				<h4>Equality</h4>
				<p>We embrace simplicity, diversity and inclusion. We believe that great ideas can come from anyone and anywhere in the organisation.</p>
			</div>
			<div class="col-lg-4">
				<div class="image1"></div>
				<h4>Our Service</h4>
				<p>We think that you can do your best with a freedom to experiment, sense of ownership of your work.</p>
			</div>
			<div class="col-lg-4">
				<div class="image2"></div>
				<h4>Part of FiniGoo</h4>
				<p>Working at FiniGoo is a rewarding experience! Great people, a work environment that thrives on creativity</p>
			</div>
			</br>
			</br>
			</br>
			<div class="section-title-spacer"></div>
			<button type="btn" class="btn btn-default btn-lg">Explore engineering at Careem</button>
		</div>
	</div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="modal" id="modal-gallery" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body">
				<div id="modal-carousel" class="carousel">
					<div class="carousel-inner"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>