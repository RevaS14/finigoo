<style>
    /*second nav */
    #myTab-2 li a {
        border: .8px solid #ad72056b;
        border-radius: 25px;
        color: black;
        padding: 6px 30px;
        font-size: 14px;
    }

    #myTab-2 li a.active {
        color: #fff;
        background-color: #B121B4;
        font-size: 14px;
        display: inline-block;
        padding: 6px 30px;
        border-radius: 25px;
        /*border: .8px solid black;*/
    }

    /*footer subcribe*/
    .rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
        font-size: 20px;
        font-size: 14px;
        font-weight: 600;
        position: absolute;
        top: 3px;
        right: 3px;
        height: 51px;
        padding: 0 40px;
        text-transform: uppercase;
        border-radius: 999px;
        /*position: relative;*/
        z-index: 999999;
        color: #fff;
        border: none;
        background: -webkit-linear-gradient(55deg, #B121B4 0%, #B121B4 53%, #B121B4 70%, #B121B4 100%);
        background: linear-gradient(35deg, #B121B4 0%, #B121B4 53%, #B121B4 70%, #B121B4 100%);
    }

    /*back to top button*/
    #scrollUp {
        font-size: 30px;
        line-height: 48px;
        position: fixed;
        right: 30px;
        bottom: 30px;
        width: 45px;
        height: 45px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
        background: #B121B4;
    }

    /*services hover bg color*/
    .main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
    .main-menu ul ul>li:hover>a {
        padding-left: 23px;
        color: #fff;
        background: #B121B4;
    }

    .rt-section-title span {
        font-family: 'Lato', sans-serif;
        font-size: 18px;
        display: block;
        margin: 0 0 0;
        text-transform: uppercase;
        color: #B121B4;
    }

    .submenufix.show {
        background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.38)), url(<?php echo base_url(); ?>landing-assets/assets/images/extra/demand.jpg);
        background-size: cover;
    }

    @media (max-width:1200px) {
        .submenufix.show {
            background: white;
        }
    }
</style>
<div class="rt-breadcump rt-breadcump-height">
    <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/godemand.jpg)">
    </div><!-- /.rt-page-bg -->
    <div class="container">
        <div class="row rt-breadcump-height">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h3>Go Demand</h3>
                    <p style="font-size: 30px;">Your Service Experts</p>
                    <!-- /.breadcrumbs -->
                </div><!-- /.breadcrumbs-content -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.rt-bredcump -->

<!-- <div class="container" id="tabb" >
<div class="flight-list-box rt-mb-40">
                        <ul class="nav  pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="#rt-itm_1" role="tab"
                                    aria-controls="rt-itm_1" aria-selected="true">Go Demand</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">Blog</a>
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                                
                            </li>
                        </ul>
                    </div>
</div> -->
<!-- 
    ========works start========
 -->
<section class="works-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 text-center mx-auto text-center">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title"><span>Go Demand</span>
                        RELIABLE SERVICE </h2><!-- /.rt-section-title -->
                    <p> The easiest way to hire reliable service professionals for your home.</p>
                </div><!-- /.rt-section-title-wrapper- -->
            </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
    </div><!-- /.container -->
</section>
<!-- 
    ========works start========
 -->
<div class="spacer-bottom"></div><!-- /.spacer-bottom -->
<section class="caltoaction-4 rt-dim-light rt-pt-100 rt-pb-100">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h4 class="f-size-40 rt-semibold rt-mb-40 f-size-md-32 f-size-xs-26">Trusted Professionals for all your home service needs</h4>
                <p>Go Demand is on a mission to improve the service industry in Southeast Asia. We are dedicated to raising service standards and lifting income levels for thousands of service professionals and small business owners. We’ll still deliver the same great experience, whether you typed in Go Demand, We are on-demand mobile app and an evolution of the traditional directory service, yellow pages or local business listing. We take the best of mobile technology to make finding local businesses easier, more convenient and safer.</p>

            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>



<div class="spacer-top"></div><!-- /.spacer-bottom -->
<div class="container">
    <h4 class="f-size-40 rt-semibold rt-mb-40 f-size-md-32 f-size-xs-26">We deliver on five key principles to ensure that 100% satisfaction is guaranteed.</h4>
</div>
<section class="about-area2">

    <div class="rt-design-elmnts  rtbgprefix-contain">
        <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/Go Demand.png">
    </div><!-- /.rt-design-elmnts -->

    <div class="container">

        <div class="row">
            <div class="col-lg-7">
                <div id="accordion">
                    <div class="card wow fade-in-bottom">
                        <div class="card-header card-primary" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white" style="background-color:#B121B4"><i class="icofont-question" style="background-color:#B121B4"></i></span> Quality & Reliability
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Only the most trusted and skilled service providers are found on Go Demand. Professionals are thoroughly vetted and trained before servicing customers.
                            </div>
                        </div>
                    </div><!-- end single accrodain -->
                    <div class="card wow fade-in-bottom" data-wow-duration="1.0s">
                        <div class="card-header card-primary" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white" style="background-color:#B121B4"><i class="icofont-question"></i></span> Security
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                We use market leading digital payment infrastructure to make sure your payment and info is safe with us.
                            </div>
                        </div>
                    </div><!-- end single accrodain -->
                    <div class="card wow fade-in-bottom" data-wow-duration="1.5s">
                        <div class="card-header card-primary" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white" style="background-color:#B121B4"><i class="icofont-question"></i></span> Customer Service
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Our 100% response rate ensures that all support requests are answered quickly and resolved efficiently.
                            </div>
                        </div>
                    </div><!-- end single accrodian -->
                    <div class="card wow fade-in-bottom" data-wow-duration="1.5s">
                        <div class="card-header card-primary" id="headingThree2">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree2" aria-expanded="false" aria-controls="collapseThree2">
                                    <span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white" style="background-color:#B121B4"><i class="icofont-question"></i></span> Convenience
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree2" class="collapse" aria-labelledby="headingThree2" data-parent="#accordion">
                            <div class="card-body">
                                Save hours by finding professionals instantly, manage your jobs in one place and review and rate professionals on the app after the job is done.
                            </div>
                        </div>
                    </div><!-- end single accrodian -->
                    <div class="card wow fade-in-bottom" data-wow-duration="1.5s">
                        <div class="card-header card-primary" id="headingThree21">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree21" aria-expanded="false" aria-controls="collapseThree21">
                                    <span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white" style="background-color:#B121B4"><i class="icofont-question"></i></span> Fair Pricing
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree21" class="collapse" aria-labelledby="headingThree21" data-parent="#accordion">
                            <div class="card-body">
                                All service prices are competitive and set by experienced providers.
                            </div>
                        </div>
                    </div><!-- end single accrodian -->

                </div><!-- end accrodian group -->


            </div><!-- /.col-lg-7 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
<div class="spacer-top"></div><!-- /.spacer-bottom -->

<div class="container" style="background-color:#B121B4;border-radius: 50px;">
    <div class="row">
        <div class="col-lg-12 align-items-center justify-content-center" style="text-align: center;padding: 50px;margin-top: 30px;">
            <h3>Let us make your home or office perfect today! </h3><!-- /.rt-section-title -->
            <p style="color: #fff;">Go Demand is the leading mobile application to find local service providers like cleaners, part time maids, electricians, air conditioning experts, plumbers and other home pros. Use our app to book reliable Go Demand Heroes at the tap of a button and use cashless options to pay.</p>
            <div class="section-title-spacer"></div><!-- /.section-title-spacer -->

        </div><!-- /.col-12 -->
    </div><!-- /.row -->
</div>