<style>
    /*footer subcribe*/
    .rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
        font-size: 20px;
        font-size: 14px;
        font-weight: 600;
        position: absolute;
        top: 3px;
        right: 3px;
        height: 51px;
        padding: 0 40px;
        text-transform: uppercase;
        border-radius: 999px;
        /*position: relative;*/
        z-index: 1;
        color: #fff;
        border: none;
        background: -webkit-linear-gradient(55deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
        background: linear-gradient(35deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
    }

    /*back to top button*/
    #scrollUp {
        font-size: 30px;
        line-height: 42px;
        position: fixed;
        right: 30px;
        bottom: 30px;
        width: 45px;
        height: 45px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
        background: #01AC00;
    }

    /*services hover bg color*/
    .main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
    .main-menu ul ul>li:hover>a {
        padding-left: 23px;
        color: #fff;
        background: #01AC00;
    }

    .icofont-check-circled:before {
        content: "\eed7";
        color: #01ac00;
    }

    #ban {
        background: linear-gradient(0deg, rgba(0, 0, 0, 0.52), rgba(0, 0, 0, 0.58)),
            url(<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/franchise-bg.jpg);
        position: relative;
        background-repeat: no-repeat;
        background-size: cover;
        width: 100%;
        height: auto;
        background-attachment: fixed;
    }

    #nav-top .rt-site-header.rt-fixed-top {
        position: unset;
        z-index: auto;
        top: 0;
        left: 0;
        width: 100%;
    }

    #rht_txt h2,
    h5 {
        color: white;
        line-height: 40px;
    }

    #rht_txt {
        height: auto;
        padding: 100px 0px 0px 0px;
    }

    #register {
        background-color: white;
        padding: 40px 20px 40px 20px;
    }

    .form-group input {
        font-size: 15px;
    }

    .form-group select {
        font-size: 15px;
    }

    .form-group option {
        font-size: 15px;
    }

    .form-horizontal button {
        font-size: 16px;
        background-color: #ed1d24;
        border: none;
    }

    .form-horizontal button:hover {
        border: 1px solid red;
        background-color: transparent;
        color: black;
    }

    #form_para p {
        font-size: 12px;
        text-align: center;
        margin-top: 10px;
        color: gray;
    }

    #code {
        float: left;
        padding-right: 0px;
    }

    #num {
        float: left;
        padding-left: 0px;
    }

    @media screen and(max-width:700px; ) {

        #rht_txt {
            height: auto !important;
            padding: 0px 0px 0px 0px !important;
        }
    }

    #nav-top li a {
        color: black;
    }

    .text-424 p {
        text-align: justify;
    }

    .nav-link {
        display: block;
        padding: .5rem 1rem;
        width: 160px;
    }

    #iconiz #myTab-2 img {
        animation-name: iconic;
        animation-duration: 0.4s;
        animation-iteration-count: 1;
        animation-timing-function: linear;

    }

    @keyframes iconic {
        0% {
            width: 0px;
        }

        25% {
            width: 10px;
        }

        50% {
            width: 15px;
        }

        75% {
            width: 25px;
        }

        100% {
            width: 35px;
        }
    }

    #myTab-2 li a:hover {
        font-size: 15px;
        transition-duration: 0.1s;
    }

    .we_bl {
        width: 25%;
        margin: auto;
        text-align: left;

    }

    #iconiz .nav-item {
        margin: 0px;
    }
</style>


<div class="container-fluid" id="ban">
    <div class="row" id="ban_form">

        <div class="col-md-7" id="rht_txt" style="padding-top: 25%;">
            <h2>Become A Franchise Owner</h2>
            <h5>Partner With Finigoo To Launch Your Own Taxi , Auto & </br> Delivery Business In Your City</h5>
        </div>
        <div class="col-md-5" id="register" style="margin-top: 86px;">
            <h4 class="text-center">Register Now In Finigoo</h4>
            <p class="text-center">Start Auto | Taxi | Delivery Bussiness Now</p>
            <form class="form-horizontal" role="form">
                <div class="form-group form-group-lg">
                    <label class="control-label col-md-2"><b>Name</b></label>
                    <div class="col-md-12">
                        <input type="text" name="user" class="form-control col-md-12" id="name" placeholder="Name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2"><b>Email</b></label>
                    <div class="col-md-12">
                        <input type="text" name="email" class="form-control col-md-12" id="email" placeholder="Email">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-2"><b>Phone</b></label>
                    <div class="col-md-12">

                        <div>
                            <div id="code">
                                <input type="tel" class="form-control " placeholder="+91" style="/* width: 40px; */">
                            </div>
                            <div id="num">
                                <input type="tel" class="form-control col-md-12" placeholder="Phone" style="">
                            </div>
                        </div>
                    </div>







                </div>
                <div class="form-group">
                    <label class="control-label col-md-2"><b>City</b></label>
                    <div class="col-md-12">
                        <input type="text" name="city" class="form-control col-md-12" id="city" placeholder="City">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-2"><b>Investment</b></label>
                    <div class="col-md-12">
                        <select class="form-control">
                            <option class="form-control">Select Inverstment</option>
                            <option class="form-control">&#8377;50k - 2lak</option>
                            <option class="form-control">&#8377;2lak - 5lak</option>
                            <option class="form-control">&#8377;5lak - 10lak</option>
                            <option class="form-control">&#8377;10lk - 20lak</option>
                            <option class="form-control">&#8377;20lak - 30lak</option>
                            <option class="form-control">&#8377;30lak - 50lak</option>
                            <option class="form-control">&#8377;50lak - 1cr</option>
                            <option class="form-control">&#8377;1cr above</option>
                        </select>
                    </div>
                </div>
                </br>
                <div class="form-group">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-success col-md-12 btn-lg"><b>SUBMIT YOUR APPLICATION</b></button>
                    </div>
                </div>
                <div class="col-md-12" id="form_para">
                    <p>By proceeding, I agree that you can collect, use and disclose the information provided by me in accordance with your <a href="#">Privacy Policy</a> which I have read and understand.</p>
                </div>

            </form>
        </div>

    </div>
</div>




<!--------End--------->
<div class="section-title-spacer"></div><!-- /.section-title-spacer -->
<div class="section-title-spacer"></div><!-- /.section-title-spacer -->
<section class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 mx-auto text-center">
                <div class="rt-section-title-wrapper">
                    <h3 class="rt-section-title" style="color:#ed1d24; font-size:40px;">FiniGoo Franchise Opportunities</h3>

                </div><!-- /.rt-section-title-wrapper -->
            </div><!-- /.col-lg-8 -->
        </div><!-- ./row -->
        <!-- /.section-title-spacer -->
        <div class="row align-items-center">
            <div class="col-lg-5 mb-0 mb-md-4">

                <h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInDown" data-wow-duration="1s">Become our Partner in Growth</h4>
                <div class="text-424 wow fadeInUp" data-wow-duration="1s">
                    <!-- <ul class="rt-list f-size-14">
                       
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>ride price is known in advance, helping you to plan out your expenses;</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>scheduled orders ensure that your car will arrive on time;</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>online map shows you your driver's current location;</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>ride templates allow you to place your order in a couple of taps.</li>
                        
                    </ul>-->
                    <p>At FiniGoo, we believe in efficient & simple processes and this portal is a reflection of our promise to making things easier. This industry first online portal is your first step towards owning FiniGoo India’s franchise and we have designed it to be a one-click solution for making the entire application process easier and faster for you.</br>



                        We hope this initiative will result in more fruitful and seamless partnerships.</p>
                </div><!-- /.text-424 -->
            </div><!-- /.col-lg-6 -->


            <div class="col-lg-6 offset-lg-1">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/Store_Images/Store_Image_3.jpeg" alt="work image" draggable="false" class="wow zoomIn img-circle" data-wow-duration="2.5s">
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->


        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="col-lg-12 col-md-6">
            <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="1.5s" style="height:auto; line-height:30px;">

                <div class="iconbox-content" id="iconiz">
                    <h4 class="f-size-20 rt-semibold rt-mb-20 f-size-md-15 f-size-xs-10">Features</h4>


                    <ul class="nav pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="" role="tab" aria-controls="rt-itm_1" aria-selected="true" disabled>
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/4.png" alt="box-icon" draggable="false" style="max-width: 35px;padding: 5px;">
                                Go Cab</a>
                        </li>
                        <li class="nav-item">

                            <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" id="2" href="" role="tab" aria-controls="rt-itm_2" aria-selected="false" disabled>
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/8.png" alt="box-icon" draggable="false" style="max-width: 35px;padding: 5px;">
                                Go Ride</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" id="3" href="" role="tab" aria-controls="rt-itm_1" aria-selected="true" disabled>
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/3.png" alt="box-icon" draggable="false" style="max-width: 35px;padding: 5px;">
                                Go Food</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="" role="tab" aria-controls="rt-itm_2" aria-selected="false" disabled>
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/11.png" alt="box-icon" draggable="false" style="max-width: 35px;padding: 5px;">
                                Go Bills</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="" role="tab" aria-controls="rt-itm_1" aria-selected="true" disabled>
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/2.png" alt="box-icon" draggable="false" style="max-width: 35px;padding: 5px;">
                                Go Pay</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="" role="tab" aria-controls="rt-itm_2" aria-selected="false" disabled>
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/9.png" alt="box-icon" draggable="false" style="max-width: 35px;padding: 5px;">
                                Go UPI</a>
                        </li>
                    </ul>
                    <ul class="nav pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="" role="tab" aria-controls="rt-itm_1" aria-selected="true" disabled>
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/new/box.png" alt="box-icon" draggable="false" style="max-width: 35px;padding: 5px;">
                                Go Box</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="" role="tab" aria-controls="rt-itm_2" aria-selected="false" disabled>
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/15.png" alt="box-icon" draggable="false" style="max-width: 35px;padding: 5px;">
                                Go Grocery </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="" role="tab" aria-controls="rt-itm_2" aria-selected="false" disabled>
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/box-icon-3.png" alt="box-icon" draggable="false" style="max-width: 35px;padding: 5px;">
                                Go Mall </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="" role="tab" aria-controls="rt-itm_2" aria-selected="false" disabled>
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/7.png" alt="box-icon" draggable="false" style="max-width: 35px;padding: 5px; ">
                                Go Beauty </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="" role="tab" aria-controls="rt-itm_2" aria-selected="false" disabled>
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/14.png" alt="box-icon" draggable="false" style="max-width: 35px;padding: 5px; margin-left:10px;">
                                Go Demand</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="" role="tab" aria-controls="rt-itm_2" aria-selected="false" disabled>
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/con-1.png" alt="box-icon" draggable="false" style="max-width: 35px;padding: 5px; margin-left:10px;">
                                Go Travel</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="" role="tab" aria-controls="rt-itm_2" aria-selected="false" disabled>
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/12.png" alt="box-icon" draggable="false" style="max-width: 30px;padding: 5px; margin-left:10px;">
                                Go Points</a>
                        </li>

                    </ul>
                </div><!-- /.iconbox-content -->
            </div><!-- /.rt-single-icon-box -->
        </div><!-- /.col-lg-4 -->


        <div class="section-title-spacer"></div>

        <div class="row align-items-center">
            <div class="col-lg-6 mb-0 mb-md-4">

                <img src="<?php echo base_url(); ?>landing-assets/assets/images/Store_Images/Store_Image_5.jpg" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-5 offset-lg-1">

                <h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInUp" data-wow-duration="1s">Freedom of Operations & Proven Technology</h4>
                <div class="text-424 wow fadeInDown" data-wow-duration="1s">
                    <p class="f-size-14 text-424"><i class="fa fa-check-circle" aria-hidden="true" style="color: red;padding-right: 10px;"></i>
                        Are just a few reasons why our partners love us
                    </p>
                    <p class="f-size-14 text-424"><i class="fa fa-check-circle" aria-hidden="true" style="color: red;padding-right: 10px;"></i>
                        India's first Online Multi Services aggregator
                    </p>
                    <p class="f-size-14 text-424"><i class="fa fa-check-circle" aria-hidden="true" style="color: red; padding-right: 10px;"></i>
                        Transparent payment & franchise agreement
                    </p>
                    <p class="f-size-14 text-424"><i class="fa fa-check-circle" aria-hidden="true" style="color: red; padding-right: 10px;"></i>
                        24/7 helpline & centralized training support
                    </p>
                    <p class="f-size-14 text-424"><i class="fa fa-check-circle" aria-hidden="true" style="color: red; padding-right: 10px;"></i>
                        Get more rides & deliveries via FiniGoo mobile app & grow
                    </p></br>
                </div><!-- /.text-424 -->


            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        <div class="section-title-spacer"></div>
        <div class="container">
            <div class="col-lg-12 col-md-6">
                <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="1.5s" style="height:auto; line-height:30px;">

                    <div class="iconbox-content">
                        <h4 class="f-size-20 rt-semibold rt-mb-20 f-size-md-15 f-size-xs-10">We believe that the key differentiators in the highly dynamic Indian telecom market are:</h4>
                        <div class="we_bl">
                            <p class="f-size-14 text-424"><i class="fa fa-check-circle" aria-hidden="true" style="color: red; padding-right: 10px;"></i>
                                Enhanced customer experience

                            </p>
                            <p class="f-size-14 text-424"><i class="fa fa-check-circle" aria-hidden="true" style="color: red; padding-right: 10px;"></i>
                                Consistent brand experience
                            </p></br>
                        </div>


                    </div><!-- /.iconbox-content -->
                </div><!-- /.rt-single-icon-box -->
            </div><!-- /.col-lg-4 -->
        </div>
        <div class="section-title-spacer"></div>
        <div class="row align-items-center">
            <div class="col-lg-6 mb-0 mb-md-4">

                <h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInDown" data-wow-duration="1s">Benefits</h4>
                <div class="text-424 wow fadeInUp" data-wow-duration="1s">
                    <ul class="rt-list f-size-14">

                        <li><span class="rt-pr-4"><i class="fa fa-check-circle" aria-hidden="true" style="color: red; padding-right: 10px;"></i></span>Our solutions boost productivity, eliminating all the redundant activities.</li>
                        <li><span class="rt-pr-4"><i class="fa fa-check-circle" aria-hidden="true" style="color: red; padding-right: 10px;"></i></span>Easily deployable, we ensure that our solutions enable you to improve the process,<i class="fa fa-check-circle" aria-hidden="true" style="color: red; padding-right: 10px; visibility:hidden;"></i> without essentially altering them.</li>
                        <li><span class="rt-pr-4"><i class="fa fa-check-circle" aria-hidden="true" style="color: red; padding-right: 10px;"></i></span>Accessible through smartphones, our solutions can be managed remotely as well.</li>
                        <li><span class="rt-pr-4"><i class="fa fa-check-circle" aria-hidden="true" style="color: red; padding-right: 10px;"></i></span>Ensuring real-time analysis, we make sure that the operation time is saved.</li>


                    </ul>

                </div><!-- /.text-424 -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-5 offset-lg-1">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/Store_Images/Store_Image_1.jpg" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->

        <h4 class="f-size-20 rt-semibold rt-mb-20 f-size-md-15 f-size-xs-10 text-center">Start Your Franchise With Us & Watch Your Business Grow</h4>
    </div><!-- ./ copntainer -->
</section>



<!-- 
    ======== call to action start====
 -->
<section class="caltoaction-4 rt-dim-light rt-pt-100 rt-pb-100">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h4 class="f-size-40 rt-semibold rt-mb-40 f-size-md-32 f-size-xs-26">India's Largest Intercity and Local Cab Services</h4>
                <p>We are Go Cab, an online cab booking aggregator, providing customers with reliable and premium Intercity and Local car rental services. we are uniquely placed as the largest chauffeur driven car rental company in India in terms of geographical reach.</p>
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
<!-------End---------->