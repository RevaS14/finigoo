<style>
	/*second nav */
	#myTab-2 li a {
		border: .8px solid #ad72056b;
		border-radius: 25px;
		color: black;
		padding: 6px 30px;
		font-size: 14px;
	}

	#myTab-2 li a.active {
		color: #fff;
		background-color: #E92C3C;
		font-size: 14px;
		display: inline-block;
		padding: 6px 30px;
		border-radius: 25px;
		/*border: .8px solid black;*/
	}

	/*footer subcribe*/
	.rt-site-footer .footer-top .footer-subscripbe-box .btn {
		font-family: 'Poppins', sans-serifs;
		font-size: 20px;
		font-size: 14px;
		font-weight: 600;
		position: absolute;
		top: 3px;
		right: 3px;
		height: 51px;
		padding: 0 40px;
		text-transform: uppercase;
		border-radius: 999px;
		/*position: relative;*/
		z-index: 1;
		color: #fff;
		border: none;
		background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
		background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
	}

	/*back to top button*/
	#scrollUp {
		font-size: 30px;
		line-height: 48px;
		position: fixed;
		right: 30px;
		bottom: 30px;
		width: 45px;
		height: 45px;
		text-align: center;
		color: #fff;
		border-radius: 50%;
		background: #E92C3C;
	}

	/*services hover bg color*/
	.main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
	.main-menu ul ul>li:hover>a {
		padding-left: 23px;
		color: #fff;
		background: #E92C3C;
	}

	.sticky {
		position: fixed;
		top: 16%;
		width: 100%;
		z-index: 99;
		animation-name: fadeInDown;
	}

	.rt_bnd {

		position: relative;
		top: 93px;
		margin-bottom: 95px;
		height: 100%;
		width: 100%;
	}

	.slider {
		width: 100%;
		margin: 0 auto;
	}

	.slider img {
		width: 100%;
		height: 100%;
	}

	#sliderz {
		padding-right: 0px;
		padding-left: 0px;
	}

	#sliderz1 {
		padding-top: 35px;
		padding-bottom: 0px;
		background-color: #ec1c22;
		color: white;
	}

	#end_brand {
		padding: 120px 120px 120px 120px;
	}

	#end_brand2 {
		padding: 0px 70px 0px 70px;
	}

	#end_brand1 {
		border-right: 1px solid black;
	}

	#end_brand1 .image {
		width: 40%;
		margin: auto;
	}

	@media (max-width:991px) {
		#end_brand1 {
			border-right: 0px;
		}
	}
</style>
<!-- 
    ====== Services Start ==============
 -->
<div class="rt_bnd">
	<div class="container">
		<iframe width="100%" height="600px" src="https://www.youtube.com/embed/RLXctHmSBI8?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-3">
			<h2><b> A Multi-Service Application.</b></h2>
		</div>
		<div class="offset-lg-1 col-lg-7">
			<p>	<q><i>Our team of leaders with a vision to reach people from every walk of life</i></q>
				</br>
				</br>
				</br>FiniGoo providing a multi-service application with solutions for connecting four-wheel and two-wheeler transportation, ordering food, delivery,Mobile Payments and many other services to serve the needs.</br>
				</br>Daily demand of Indian users, in world-leading technology platform. we wishes to improve the quality of life of users, creating many useful values for partners and the community.</br>
				</br>
			</p>
		</div>
	</div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container">
	<div class="row">
		<div class="col-lg-8" id="sliderz">
			<div id="demo" class="carousel slide" data-ride="carousel">
				<!-- The slideshow -->
				<div class="carousel-inner" style="height: 100%;">
					<div class="carousel-item active">
						<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/PANA7454.jpg" />
					</div>
					<div class="carousel-item">
						<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/story2.jpg" />
					</div>
					<div class="carousel-item">
						<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/story3.jpg" />
					</div>
				</div>
			</div>
			<!-- <div class="slider">
                   <div>
                     <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/PANA7454.jpg" />
                   </div>
                   <div>
                     <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/story2.jpg" />
                   </div>
                   <div>
                     <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/story3.jpg" />
                   </div>
             </div> --></div>
		<div class="col-lg-4" id="sliderz1">
			<h2 style="font-size: 30px;color: #fff;">FiniGoo Franchise Store</h2>
			</br>
			<p>FiniGoo is a team of leaders with a vision to reach people from every walk of life by easy transaction solutions in banking and e-commerce, with the end goal of becoming a leading transaction service provider for all financial requirements across India & Southeast Asia.</br>We, at FiniGoo, aspire to empower 10,00,000 stores across Tier I, II and rural towns in India. Using the power of Aadhaar & Mobility, we are motivated to transform the FiniGoo Franchise stores into Fintech & Hyper local Marts.</p>
		</div>
	</div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container-fluid">
	<h2 class="text-center"><b>Technologies and you needs</b></h2>
	<div class="row" id="end_brand">
		<div class="col-lg-6" id="end_brand1">
			<div class="image">
				<p>FiniGoo is built and developed on the basis of world advanced technology has been improving the quality of life for millions of people in Southeast Asia.</p>
			</div>
		</div>
		<div class="col-lg-6" id="end_brand2">
			<p>FiniGoo with a team of dedicated partners in large numbers always gives you the best quality service regardless of weather or time.</p>
		</div>
	</div>
</div>
<style>
	.closedicon {
		display: block;
		position: absolute;
		right: 25px;
		color: red;
		font-weight: bold;
		font-size: 20px;
		border: 1px solid red;
		width: 28px;
		height: 28px;
		top: 30px;
		z-index: 999999999;
		text-align: center;
	}

	.show {
		display: block !important;
	}

	.hide {
		display: none !important;
	}

	/* .submenufix {
                position: fixed;
                top: 90px;
                background: #ffffff;
                width: 100%;
                overflow-y: auto;
	            z-index: 9999;
                display: none;
                height: calc(100% - 200px);
            } */

	.submenufix .sub-menu li {
		list-style: none;
		color: black;
		line-height: 2.5;
	}

	.submenufix .sub-menu li a {
		color: black;
	}

	.title_submenu {
		font-size: 20px;
		color: #e82801 !important;

	}
</style>