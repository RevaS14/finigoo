<style>
    /*second nav */
    #myTab-2 li a {
        border: .8px solid #ad72056b;
        border-radius: 25px;
        color: black;
        padding: 6px 30px;
        font-size: 14px;
    }

    #myTab-2 li a.active {
        color: #fff;
        background-color: #01AEF3;
        font-size: 14px;
        display: inline-block;
        padding: 6px 30px;
        border-radius: 25px;
        /*border: .8px solid black;*/
    }

    /*footer subcribe*/
    .rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
        font-size: 20px;
        font-size: 14px;
        font-weight: 600;
        position: absolute;
        top: 3px;
        right: 3px;
        height: 51px;
        padding: 0 40px;
        text-transform: uppercase;
        border-radius: 999px;
        /*position: relative;*/
        z-index: 999999;
        color: #fff;
        border: none;
        background: -webkit-linear-gradient(55deg, #01AEF3 0%, #01AEF3 53%, #01AEF3 70%, #01AEF3 100%);
        background: linear-gradient(35deg, #01AEF3 0%, #01AEF3 53%, #01AEF3 70%, #01AEF3 100%);
    }

    /*back to top button*/
    #scrollUp {
        font-size: 30px;
        line-height: 48px;
        position: fixed;
        right: 30px;
        bottom: 30px;
        width: 45px;
        height: 45px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
        background: #01AEF3;
    }

    /*services hover bg color*/
    .main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
    .main-menu ul ul>li:hover>a {
        padding-left: 23px;
        color: #fff;
        background: #01AEF3;
    }

    .rt-section-title span {
        font-family: 'Lato', sans-serif;
        font-size: 18px;
        display: block;
        margin: 0 0 0;
        text-transform: uppercase;
        color: #01aef3;
    }

    .icofont-check-circled:before {
        content: "\eed7";
        color: #01AEF3;
    }
</style>
<div class="rt-breadcump rt-breadcump-height">
    <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/contact-banner.jpg)">
    </div><!-- /.rt-page-bg -->
    <div class="container">
        <div class="row rt-breadcump-height">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h3>Contact Us</h3>
                    <!-- /.breadcrumbs -->
                </div><!-- /.breadcrumbs-content -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.rt-bredcump -->
<section class="works-area">
    <div class="container">

        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row">
            <div class="col-lg-3 col-md-6 mx-auto text-left">
                <div class="services-box-2 wow fade-in-bottom animated" style="visibility: visible; animation-name: fade-in-bottom;">
                    <h4>Malaysia</h4>
                    <p>Level 8, Tower 8 Avenue 5,<br />
                        The Horizon, Phase 2,<br />
                        Bangsar South, 59200 Kuala Lumpur,<br />
                        Federal Territory of Kuala Lumpur,<br /> Malaysia</p>
                </div><!-- /.services-box-2 -->
            </div><!-- /.col-lg-3 -->
            <div class="col-lg-3 col-md-6 mx-auto text-left">
                <div class="services-box-2 wow fade-in-bottom animated" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fade-in-bottom;">
                    <h4>Singapore </h4>
                    <p>77 Robinson Rd,<br /> 34 Floor,<br /> Singapore 068896</p>
                </div><!-- /.services-box-2 -->
            </div><!-- /.col-lg-3 -->
            <div class="col-lg-3 col-md-6 mx-auto text-left">
                <div class="services-box-2 wow fade-in-bottom animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
                    <h4>Thailand</h4>
                    <p>Unit 2901-2904, <br />Level 29 388 Exchange Tower,<br /> Sukhumvit Rd,<br /> Klongtoey, Bangkok 10110,<br /> Thailand</p>
                </div><!-- /.services-box-2 -->
            </div><!-- /.col-lg-3 -->
            <div class="col-lg-3 col-md-6 mx-auto text-left">
                <div class="services-box-2 wow fade-in-bottom animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
                    <h4>India </h4>
                    <p>
                        Finigoo Technologies,<br />
                        #677, 1st Floor, 27th Main, 13th Cross<br />
                        HSR Layout, Sector 1, Bangalore<br />
                        Karnataka, India 560 102.

                    </p>
                </div><!-- /.services-box-2 -->
            </div><!-- /.col-lg-3 -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</section>