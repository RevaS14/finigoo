<style>
        /*second nav */
    #myTab-2 li a{
         border: .8px solid #ad72056b;
         border-radius: 25px;
         color:black;
         padding: 6px 30px;
          font-size: 14px;
    }
    #myTab-2 li a.active {
    color: #fff;
    background-color: #01aef3;
    font-size: 14px;
    display: inline-block;
    padding: 6px 30px;
    border-radius: 25px;
    /*border: .8px solid black;*/
}
/*footer subcribe*/
.rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
    font-size: 20px;
    font-size: 14px;
    font-weight: 600;
    position: absolute;
    top: 3px;
    right: 3px;
    height: 51px;
    padding: 0 40px;
    text-transform: uppercase;
    border-radius: 999px;
    /*position: relative;*/
    z-index: 999999;
    color: #fff;
    border: none;
    background: -webkit-linear-gradient(55deg, #01aef3 0%, #01aef3 53%, #01aef3 70%, #01aef3 100%);
    background: linear-gradient(35deg, #01aef3 0%, #01aef3 53%, #01aef3 70%, #01aef3 100%);
}
/*back to top button*/
#scrollUp {
    font-size: 30px;
    line-height: 48px;
    position: fixed;
    right: 30px;
    bottom: 30px;
    width: 45px;
    height: 45px;
    text-align: center;
    color: #fff;
    border-radius: 50%;
    background: #01aef3;
}
/*services hover bg color*/
.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
    padding-left: 23px;
    color: #fff;
    background: #01aef3;
}
.rt-section-title span {
    font-family: 'Lato', sans-serif;
    font-size: 18px;
    display: block;
    margin: 0 0 0;
    text-transform: uppercase;
    color: #01aef3;
}
.icofont-check-circled:before {
    content: "\eed7";
    color: #01aef3;
}
.submenufix.show{
	background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.38)), url(<?php echo base_url();?>landing-assets/assets/images/extra/pay.jpg);
    background-size: cover;
}
@media (max-width:1200px){
.submenufix.show{
		background:white;
	}
}
</style>

<div class="rt-breadcump rt-breadcump-height">
    <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/gopay.jpg)">
    </div><!-- /.rt-page-bg -->
    <div class="container">
        <div class="row rt-breadcump-height">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h3>Go Pay</h3>
                    <p style="font-size: 30px;"></p>
                   <!--  <div class="breadcrumbs">
                        <span class="divider"><i class="icofont-home"></i></span>
                        <a href="#" title="Home">Home</a>
                        <span class="divider"><i class="icofont-simple-right"></i></span>
                        <a href="#">China</a>
                        <span class="divider"><i class="icofont-simple-right"></i></span>
                        <a href="#">Hotels</a>
                        <span class="divider"><i class="icofont-simple-right"></i></span>
                        <a href="#" title="Home">Hong Kong Hotels</a>
                        <span class="divider"><i class="icofont-simple-right"></i></span>
                        Empire Hotel Kowloon - Tsim Sha Tsui

                    </div> --><!-- /.breadcrumbs -->
                </div><!-- /.breadcrumbs-content -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.rt-bredcump -->
<!-- <div class="container" id="tabb" >
<div class="flight-list-box rt-mb-40">
                        <ul class="nav  pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="#rt-itm_1" role="tab"
                                    aria-controls="rt-itm_1" aria-selected="true">Go Pay</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">Benefit & Convenience</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_3-tab" data-toggle="tab" href="#rt-itm_3" role="tab"
                                    aria-controls="rt-itm_3" aria-selected="false">How To Top Up GoPay</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_4-tab" data-toggle="tab" href="#rt-itm_4" role="tab"
                                    aria-controls="rt-itm_4" aria-selected="false">Safety</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_5-tab" data-toggle="tab" href="#rt-itm_5" role="tab"
                                    aria-controls="rt-itm_5" aria-selected="false">GoPay Everywhere</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_6-tab" data-toggle="tab" href="#rt-itm_6" role="tab"
                                    aria-controls="rt-itm_6" aria-selected="false">Terms & Conditions</a>
                            </li>
                        </ul>
                    </div>
</div> -->
<section class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 mx-auto text-center">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title"><span>GoPay</span>
                 Why You Should Upgrade GoPay   </h2>
                    <p>In compliance to Bank Indonesia's regulation, follow these steps to upgrade your GoPay account.</p>
                </div><!-- /.rt-section-title-wrapper -->
            </div><!-- /.col-lg-8 -->
        </div><!-- ./row -->
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row align-items-center">
            <div class="col-lg-5 mb-0 mb-md-4">
                
                <h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInDown" data-wow-duration="1s">What affiliate benefits are offered?</h4>
                <div class="text-424 wow fadeInUp" data-wow-duration="1s">
                    <ul class="rt-list f-size-14">
                       
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span> Earn up to 5% commission per sale</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Average order of $650+</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Variety of text links and banners.</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Reliable third-party tracking</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Accessible and dynamic reporting</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Monthly commission checks</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Exciting incentive programs</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Dedicated affiliate team</li>
                    </ul>
                </div><!-- /.text-424 -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6 offset-lg-1">
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/Go Pay-02.png" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
     </div><!-- ./ copntainer -->
</section>

<!-- 
    ======== call to action start====
 -->
<div class="spacer-top"></div><!-- /.spacer-bottom -->
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="cta-box-2 d-flex flex-column align-items-center justify-content-center bg-gradient-primary" style="background-color:#01AEF3">
                <div class="row">
                    <div class="col-lg-7 mx-auto">
                        <div class="rt-section-title-wrapper text-white text-center">
                            <h2 class="rt-section-title">
                                <span>Save Time, Save Money</span>
                                Let’s Get Started
                            </h2><!-- /.rt-section-title -->
                            <p>We have the knowledge, experience, and expertise to take care of all your travel needs.
                                Sign up and we'll send the best deals to you </p>
                                <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
                            <a href="#" class="rt-btn rt-light pill rt-xl rt-Bshadow-1 text-uppercase">Sign UP</a>
                        </div><!-- /.rt-section-title-wrapper- -->
                    </div><!-- /.col-lg-7 -->
                </div><!-- /.row -->
            </div><!-- /.inner-content -->
        </div><!-- /.col-12 -->
    </div><!-- /.row -->
</div><!-- /.container -->