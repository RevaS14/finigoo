<style>
	/*second nav */
	    #myTab-2 li a{
	         border: .8px solid #ad72056b;
	         border-radius: 25px;
	         color:black;
	         padding: 6px 30px;
	          font-size: 14px;
	    }
	    #myTab-2 li a.active {
	    color: #fff;
	    background-color: #01AEF3;
	    font-size: 14px;
	    display: inline-block;
	    padding: 6px 30px;
	    border-radius: 25px;
	    /*border: .8px solid black;*/
	}
	/*footer subcribe*/
	.rt-site-footer .footer-top .footer-subscripbe-box .btn {
	        font-family: 'Poppins', sans-serifs;
	    font-size: 20px;
	    font-size: 14px;
	    font-weight: 600;
	    position: absolute;
	    top: 3px;
	    right: 3px;
	    height: 51px;
	    padding: 0 40px;
	    text-transform: uppercase;
	    border-radius: 999px;
	    /*position: relative;*/
	    z-index: 999999;
	    color: #fff;
	    border: none;
	    background: -webkit-linear-gradient(55deg, #01AEF3 0%, #01AEF3 53%, #01AEF3 70%, #01AEF3 100%);
	    background: linear-gradient(35deg, #01AEF3 0%, #01AEF3 53%, #01AEF3 70%, #01AEF3 100%);
	}
	/*back to top button*/
	#scrollUp {
	    font-size: 30px;
	    line-height: 48px;
	    position: fixed;
	    right: 30px;
	    bottom: 30px;
	    width: 45px;
	    height: 45px;
	    text-align: center;
	    color: #fff;
	    border-radius: 50%;
	    background: #01AEF3;
	}
	/*services hover bg color*/
	.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
	    padding-left: 23px;
	    color: #fff;
	    background: #01AEF3;
	}
	.rt-section-title span {
	    font-family: 'Lato', sans-serif;
	    font-size: 18px;
	    display: block;
	    margin: 0 0 0;
	    text-transform: uppercase;
	    color: #01aef3;
	}
	.icofont-check-circled:before {
	    content: "\eed7";
	    color: #01AEF3;
	}
	.submenufix.show{
		background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.49)), url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/goride.jpg);
	    background-size: cover;
	}
	@media (max-width:1200px){
	.submenufix.show{
			background:white;
		}
	}
</style>
<div class="rt-breadcump rt-breadcump-height">
	<div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/gobills.jpg)"></div>
	<!-- /.rt-page-bg -->
	<div class="container">
		<div class="row rt-breadcump-height">
			<div class="col-12">
				<div class="breadcrumbs-content">
					<h3>Go Bills</h3>
					<p style="font-size: 30px;">Fastest & Simplest Way for all you Bills.</p>
					<!-- /.breadcrumbs -->
				</div>
				<!-- /.breadcrumbs-content -->
			</div>
			<!-- /.col-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</div>
<!-- /.rt-bredcump -->
<div class="container" id="tabb">
	<!-- <div class="flight-list-box rt-mb-40">
                        <ul class="nav  pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="#rt-itm_1" role="tab"
                                    aria-controls="rt-itm_1" aria-selected="true">Go Bills</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">How To Use</a>
                            </li>
                            <li class="nav-item">
                               <a class="nav-link" id="rt-itm_3-tab" data-toggle="tab" href="#rt-itm_3" role="tab"
                                    aria-controls="rt-itm_3" aria-selected="false">Go Bills FAQ</a> 
                            </li>
                           
                        </ul>
                    </div>
</div> -->
	<section class="content-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 mx-auto text-center">
					<div class="rt-section-title-wrapper">
						<h2 class="rt-section-title"><span>Fastest & Simplest way for</span>
                     Online Recharge</h2>
						<p>Paying your utility bills was never so easy!</p>
						<p>You can pay all your Recharge and bills including prepaid, postpaid, electricity, phone, internet, etc.</p>
					</div>
					<!-- /.rt-section-title-wrapper -->
				</div>
				<!-- /.col-lg-8 -->
			</div>
			<!-- ./row -->
			<div class="section-title-spacer"></div>
			<!-- /.section-title-spacer -->
			<div class="row align-items-center">
				<div class="col-lg-6 mb-0 mb-md-4">
					<!-- <div class="rt-icon rt-hw-45 rt-circle icon-glow-1 icon-gradinet-1 f-size-20">
                    
                </div> -->
					<!-- /.rt-icon -->
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/Go Bills-01.png" alt="work image" draggable="false">
				</div>
				<!-- /.col-lg-6 -->
				<div class="col-lg-5 offset-lg-1 mb-0 mb-md-4">
					<!-- <div class="rt-icon rt-hw-65 f-size-45  rt-dotted-primary text-gradinet-primary rt-mb-30">
                        <i class="icofont-hand-drag1"></i>
                </div> -->
					<!-- /.rt-icon -->
					<h4 class="f-size-24 rt-semiblod rt-mb-20">Why GoBills?</h4>
					<div class="text-424">
						<p class="f-size-14 "><i class="icofont-check-circled"></i>Your electricity bill payment gets easier with FiniGoo!. Trusted by over millions of users, FiniGoo is your one-stop shop for online recharges and bill payments.</p>
						<p class="f-size-14 "><i class="icofont-check-circled"></i>There are many online bill payment facilities provided by FiniGoo. Some of these include, scheduling payments on time, save the customer information for transaction in the future, and other different paying alternatives.</p>
					</div>
					<!-- /.text-424 -->
				</div>
				<!-- /.col-lg-6 -->
			</div>
			<!-- /.row -->
			<div class="row align-items-center">
				<div class="col-lg-5 mb-0 mb-md-4">
					<!-- <div class="rt-icon rt-hw-65 f-size-45  rt-dotted-primary text-gradinet-primary rt-mb-30">
                    <i class="icofont-hand-drag1"></i>
                </div> -->
					<!-- /.rt-icon -->
					<h4 class="f-size-24 rt-semiblod rt-mb-20">FiniGoo Franchise Store</h4>
					<div class="text-424">
						<p class="f-size-14 "><i class="icofont-check-circled"></i>Break the long queues and pay your bills in no time with our Bill Payment Service at your nearest FiniGoo Franchise Store. FiniGoo portal allows its Franchise to pay utility bills such as Electricity, Water, Gas, Fixed-line, Municipal Taxes etc. on behalf of the customers.</p>
					</div>
					<!-- /.text-424 -->
				</div>
				<!-- /.col-lg-6 -->
				<div class="col-lg-6 offset-lg-1">
					<!-- <div class="rt-icon rt-hw-45 rt-circle icon-glow-1 icon-gradinet-1 f-size-20">
                    02
                </div> -->
					<!-- /.rt-icon -->
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/work-img-1.png" alt="work image" draggable="false">
				</div>
				<!-- /.col-lg-6 -->
			</div>
			<!-- /.row -->
			<!-- /.col-lg-6 -->
		</div>
		<!-- /.row -->
</div>
<!-- ./ copntainer -->
</section>
<section class="works-area">
	<div class="container">
		<div class="row">
			<div class="col-xl-8 text-center mx-auto text-center">
				<div class="rt-section-title-wrapper">
					<h2 class="rt-section-title">
                        <!-- <span>Welcome to</span>
                        Our Help Center
                    </h2> -->
					<!-- /.rt-section-title -->
					<!--  <p>We're here to help you 24/7.Check out our help guides for information and answers.</p> -->
				</div>
				<!-- /.rt-section-title-wrapper- -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="section-title-spacer"></div>
		<!-- /.section-title-spacer -->
		<div class="row">
			<div class="col-lg-3 col-md-6 mx-auto text-center">
				<div class="services-box-2 wow fade-in-bottom">
					<div class="services-thumb">
						<img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/secure.png" style="width: 120px;" alt="service-icons" draggable="false">
					</div>
					<!-- /.services-thumb -->
					<h4>Secure</h4>
				</div>
				<!-- /.services-box-2 -->
			</div>
			<!-- /.col-lg-3 -->
			<div class="col-lg-3 col-md-6 mx-auto text-center">
				<div class="services-box-2 wow fade-in-bottom" data-wow-duration="1s">
					<div class="services-thumb">
						<img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/easysteps.png" style="width: 120px;" alt="service-icons" draggable="false">
					</div>
					<!-- /.services-thumb -->
					<h4>Convenient</h4>
				</div>
				<!-- /.services-box-2 -->
			</div>
			<!-- /.col-lg-3 -->
			<div class="col-lg-3 col-md-6 mx-auto text-center">
				<div class="services-box-2 wow fade-in-bottom" data-wow-duration="1.5s">
					<div class="services-thumb">
						<img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/fee.png" style="width: 120px;" alt="service-icons" draggable="false">
					</div>
					<!-- /.services-thumb -->
					<h4>Support 24/7</h4>
				</div>
				<!-- /.services-box-2 -->
			</div>
			<!-- /.col-lg-3 -->
			<div class="col-lg-3 col-md-6 mx-auto text-center">
				<div class="services-box-2 wow fade-in-bottom" data-wow-duration="2s">
					<div class="services-thumb">
						<img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/easyreturn.png" style="width: 120px;" alt="service-icons" draggable="false">
					</div>
					<!-- /.services-thumb -->
					<h4>Rewarding</h4>
				</div>
				<!-- /.services-box-2 -->
			</div>
			<!-- /.col-lg-3 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>