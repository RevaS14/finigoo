<style>
    /*second nav */
    #myTab-2 li a {
        border: .8px solid #ad72056b;
        border-radius: 25px;
        color: black;
        padding: 6px 30px;
        font-size: 14px;
    }

    #myTab-2 li a.active {
        color: #fff;
        background-color: #E01894;
        font-size: 14px;
        display: inline-block;
        padding: 6px 30px;
        border-radius: 25px;
        /*border: .8px solid black;*/
    }

    .works3-area .rt-inner-overlay {
        height: 439px;
        background-image: -webkit-linear-gradient(82deg, #E01894 0%, #E01894 53%, #E01894 70%, #E01894 100%);
        background-image: linear-gradient(8deg, #E01894 0%, #E01894 53%, #E01894 70%, #E01894 100%);
    }

    /*footer subcribe*/
    .rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
        font-size: 20px;
        font-size: 14px;
        font-weight: 600;
        position: absolute;
        top: 3px;
        right: 3px;
        height: 51px;
        padding: 0 40px;
        text-transform: uppercase;
        border-radius: 999px;
        /*position: relative;*/
        z-index: 99999;
        color: #fff;
        border: none;
        background: -webkit-linear-gradient(55deg, #E01894 0%, #E01894 53%, #E01894 70%, #E01894 100%);
        background: linear-gradient(35deg, #E01894 0%, #E01894 53%, #E01894 70%, #E01894 100%);
    }

    /*back to top button*/
    #scrollUp {
        font-size: 30px;
        line-height: 48px;
        position: fixed;
        right: 30px;
        bottom: 30px;
        width: 45px;
        height: 45px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
        background: #E01894;
    }

    /*services hover bg color*/
    .main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
    .main-menu ul ul>li:hover>a {
        padding-left: 23px;
        color: #fff;
        background: #E01894;
    }

    .submenufix.show {
        background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.38)), url(<?php echo base_url(); ?>landing-assets/assets/images/extra/beauty1.jpg);
        background-size: 100% 100%;
    }

    @media (max-width:1200px) {
        .submenufix.show {
            background: white;
        }
    }
</style>


    <!-- 
    ====== Services Start ==============
 -->
    <div class="rt-breadcump rt-breadcump-height">
        <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/gobeauty.jpg)">
        </div><!-- /.rt-page-bg -->
        <div class="container">
            <div class="row rt-breadcump-height">
                <div class="col-12">
                    <div class="breadcrumbs-content">
                        <h3>Go Beauty</h3>
                        <p style="font-size: 30px;">Smart Salon at Home & Wellness</p>
                    </div><!-- /.breadcrumbs-content -->
                </div><!-- /.col-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.rt-bredcump -->
    <!-- <div class="container" id="tabb" >
<div class="flight-list-box rt-mb-40">
                        <ul class="nav pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="#rt-itm_1" role="tab"
                                    aria-controls="rt-itm_1" aria-selected="true">Go Beauty</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">blog</a>
                            </li>
                            <li class="nav-item">
                               
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                               
                            </li>
                        </ul>
                    </div>
</div> -->

    <!-- 
    ====== Services Start ==============
 -->

    <section class="svcPage-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 text-center mx-auto">
                    <div class="rt-section-title-wrapper">
                        <h2 class="rt-section-title">
                            <span style="color:#E01894;">GoBeauty</span>
                            We are India's largest at-home services app.
                        </h2><!-- /.rt-section-title -->
                        <p>You can book at home services - from beauty & wellness for women and men</p>
                    </div><!-- /.rt-section-title-wrapper- -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
            <div class="section-title-spacer"></div><!-- /.section-title-spacer -->

        </div><!-- /.container -->
    </section>

    <!-- 
    ====== Works Start ==============
 -->
    <div class="spacer-bottom"></div><!-- /.spacer-bottom -->
    <section class="works3-area">
        <div class="rt-inner-overlay"></div><!-- /.rt-overlay -->
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="rt-section-title-wrapper">
                        <h3>Beauty & Wellness for women and men</h3>
                    </div><!-- /.rt-section-title-wrapper -->
                    <div class="rt-box-style-1">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom">
                                    <div class="icon-thumb">
                                        <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/7.png" style="width: 120px;" alt="box-icon" draggable="false">
                                    </div><!-- /.icon-thumb -->
                                    <div class="iconbox-content">
                                        <h5>Beauty and Wellness</h5>
                                        <p>Salon at home, Spa at Home, Party Make-up, Parlour at home, massage at home, Haircut for men</p>
                                    </div><!-- /.iconbox-content -->
                                </div><!-- /.rt-single-icon-box -->
                            </div><!-- /.col-lg-4 -->
                            <div class="col-lg-6 col-md-6">
                                <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="1s">
                                    <div class="icon-thumb">
                                        <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/7.png" style="width: 120px;" alt="box-icon" draggable="false">
                                    </div><!-- /.icon-thumb -->
                                    <div class="iconbox-content">
                                        <h5>Health at Home</h5>
                                        <p>Yoga Trainers, Fitness Trainers</p>
                                    </div><!-- /.iconbox-content -->
                                </div><!-- /.rt-single-icon-box -->
                            </div><!-- /.col-lg-4 -->


                        </div><!-- /.row -->
                    </div><!-- /.rt-box-style-1 -->
                </div><!-- /.col-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    <!-- 
    ======About Area Start ==============
 -->
    <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
    <section class="about-area2">

        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 mb-0 mb-md-4">

                    <h2 class="rt-section-title">
                        <span>Extraordinary experiences, Satisfaction guaranteed</span></br>
                        Why Choose Us?
                    </h2>
                    <div class="text-424 wow fadeInUp" data-wow-duration="1s">
                        <p>
                            Brands - L'Oréal Seasoul O3+ Nutribios, Lotus Specifix
                            Rework guarantee We provide a rework guarantee within a limit in accordance with the applicable provisions.
                        </p>
                    </div><!-- /.text-424 -->
                </div><!-- /.col-lg-6 -->
                <div class="col-lg-6 offset-lg-1">
                    <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/Go Beauty-01.JPEG" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
                </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->

        </div><!-- /.container -->
    </section>

    <section class="book-area">
        <div class="rt-design-elmnts rtbgprefix-contain" style="background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/all-img/abt_vec_1.png)">

        </div><!-- /.rt-design-elmnts -->
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="rt-section-title-wrapper">
                        <h2 class="rt-section-title">
                            How It Works?

                        </h2><!-- /.rt-section-title -->

                    </div><!-- /.rt-section-title-wrapper -->
                    <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
                    <div class="rt-single-icon-box wow fade-in-bottom" style="background-color:#E01894;color:#ffffff;">

                        <div class="iconbox-content">
                            <h5 style="color:#ffffff;">Book the Service after selecting required service</h5>

                        </div><!-- /.iconbox-content -->
                    </div><!-- /.rt-single-icon-box wow fade-in-bottom -->
                    <div class="rt-single-icon-box wow fade-in-bottom" style="background-color:#E01894;color:#ffffff" data-wow-duration="1.5s">

                        <div class="iconbox-content">
                            <h5 style="color:#ffffff;">We send Beautician to your place</h5>

                        </div><!-- /.iconbox-content -->
                    </div><!-- /.rt-single-icon-box wow fade-in-bottom -->
                    <div class="rt-single-icon-box wow fade-in-bottom" style="background-color:#E01894;color:#ffffff" data-wow-duration="2s">

                        <div class="iconbox-content">
                            <h5 style="color:#ffffff;">Service is completed</h5>

                        </div><!-- /.iconbox-content -->
                    </div><!-- /.rt-single-icon-box wow fade-in-bottom -->
                    <div class="rt-single-icon-box wow fade-in-bottom" style="background-color:#E01894;color:#ffffff" data-wow-duration="2.5s">

                        <div class="iconbox-content">
                            <h5 style="color:#ffffff;">Payment is collected</h5>

                        </div><!-- /.iconbox-content -->
                    </div><!-- /.rt-single-icon-box wow fade-in-bottom -->
                    <div class="rt-single-icon-box wow fade-in-bottom" style="background-color:#E01894;color:#ffffff;" data-wow-duration="2.5s">

                        <div class="iconbox-content">
                            <h5 style="color:#ffffff;">Rate the service</h5>

                        </div><!-- /.iconbox-content -->
                    </div>
                </div><!-- /.col-lg-9 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    <div class="spacer-top"></div><!-- /.spacer-top -->
    <section class="svcPage-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 text-center mx-auto">
                    <div class="rt-section-title-wrapper">
                        <h2 class="rt-section-title">
                            <span style="color:#E01994">Great looks, just an App away!</span>

                        </h2><!-- /.rt-section-title -->

                    </div><!-- /.rt-section-title-wrapper- -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
            <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="0.5s" style="height: 313px;">
                        <div class="icon-thumb">
                            <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/7.png" alt="box-icon" draggable="false" style="width: 50px;">
                        </div><!-- /.icon-thumb -->
                        <div class="iconbox-content">
                            <h5>Genuine Products</h5>
                            <p>All our beauticians use only branded and genuine products because you deserve the best!</p>

                        </div><!-- /.iconbox-content -->
                    </div><!-- /.rt-single-icon-box -->
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4 col-md-6">
                    <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="1s">
                        <div class="icon-thumb">
                            <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/7.png" alt="box-icon" draggable="false" style="width: 50px;">
                        </div><!-- /.icon-thumb -->
                        <div class="iconbox-content">
                            <h5>Trained Beauticians </h5>
                            <p>We have very high bar on allowing beauty professionals to list on FiniGoo Go Beauty. All beauticians need to pass our standard of service and cosmetics test.</p>

                        </div><!-- /.iconbox-content -->
                    </div><!-- /.rt-single-icon-box -->
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4 col-md-6">
                    <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="1.5s" style="height: 310px;">
                        <div class="icon-thumb">
                            <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/7.png" alt="box-icon" draggable="false" style="width: 50px;">
                        </div><!-- /.icon-thumb -->
                        <div class="iconbox-content">
                            <h5>Background Checked</h5>
                            <p>All our beauty professionals are vetted and background checked before they become part of FiniGoo Go Beauty family.</p>

                        </div><!-- /.iconbox-content -->
                    </div><!-- /.rt-single-icon-box -->
                </div><!-- /.col-lg-4 -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>