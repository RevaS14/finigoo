<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tab</title>
    <!-- ==================Start Css Link===================== -->
    <link href="https://fonts.googleapis.com/css?family=Lato&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/datepicker.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/OverlayScrollbars.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/fontawesome.css">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/icofont.css">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/slick.css">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/slick-theme.css">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/slider-range.css">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/select2.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/tippy.css">
    <link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/app.css">
    <!-- ==================End Css Link===================== -->

</head>
<style>
    .works3-area .rt-inner-overlay {
        height: 439px;
        background-image: -webkit-linear-gradient(82deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
        background-image: linear-gradient(8deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
    }
    /*second nav */
    
    #myTab-2 li a {
        border: .8px solid #ad72056b;
        border-radius: 25px;
        color: black;
        padding: 6px 30px;
        font-size: 14px;
    }
    
    #myTab-2 li a.active {
        color: #fff;
        background-color: #01AC00;
        font-size: 14px;
        display: inline-block;
        padding: 6px 30px;
        border-radius: 25px;
        /*border: .8px solid black;*/
    }
    /*footer subcribe*/
    
    .rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
        font-size: 20px;
        font-size: 14px;
        font-weight: 600;
        position: absolute;
        top: 3px;
        right: 3px;
        height: 51px;
        padding: 0 40px;
        text-transform: uppercase;
        border-radius: 999px;
        /*position: relative;*/
        z-index: 99999;
        color: #fff;
        border: none;
        background: -webkit-linear-gradient(55deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
        background: linear-gradient(35deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
    }
    /*back to top button*/
    
    #scrollUp {
        font-size: 30px;
        line-height: 48px;
        position: fixed;
        right: 30px;
        bottom: 30px;
        width: 45px;
        height: 45px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
        background: #01AC00;
    }
    /*services hover bg color*/
    
    .main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
    .main-menu ul ul>li:hover>a {
        padding-left: 23px;
        color: #fff;
        background: #ED1D24;
    }
    #aa a{
        color: black;
    }
    #aa a:hover{
        color:  #b6b1b1;
    }
</style>

<body>


    <!-- 
        ================== Header Area Start===================
     -->
    
    <!-- /.rt-preloder -->

   <!--  <header class="rt-site-header  rt-fixed-top white-menu">



        <div class="main-header rt-sticky  rt-sticky-active">
            <nav class="navbar">
                <div class="container">
                    <a href="index.html" class="brand-logo "><img src="<?php echo base_url();?>landing-assets/assets/images/logo/finigoo.png" alt=""></a>
                    <a href="index.html" class="sticky-logo "><img src="<?php echo base_url();?>landing-assets/assets/images/logo/finigoo.png" alt=""></a>
                    <div class="ml-auto d-flex align-items-center">

                        <div class="main-menu" id="aa">
                            <ul>
                                <li class="current-menu-item"><a href="aboutus.html">About Us</a></li>

                                <li class="menu-item-has-children"><a href="#">Services</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item-has-children"><a href="#">Transport & Logistics</a>
                                            <ul class="sub-menu">
                                                <li><a href="bike.html">Go Bike</a></li>
                                                <li><a href="cabs.html">Go Cabs</a></li>
                                                <li><a href="GoPack.html">Go Pack</a></li>
                                                <li><a href="GoAuto.html">Go Auto</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item-has-children"><a href="#">Food & FMCG</a>
                                            <ul class="sub-menu">
                                                <li><a href="GoFood.html">Go Food</a></li>
                                                <li><a href="grocery.html">Go Grocery</a></li>
                                                <li><a href="GoBeauty.html">Go Beauty</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item-has-children"><a href="#">Payments</a>
                                            <ul class="sub-menu">
                                                <li><a href="money.html">Go Money</a></li>
                                                <li><a href="bills.html">Go Bills</a></li>
                                                <li><a href="GoCashback.html">Go Cashback</a></li>
                                                <li><a href="GoPay.html">Go Pay</a></li>
                                                <li><a href="GoUPI.html">Go UPI</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item-has-children"><a href="#">Business</a>
                                            <ul class="sub-menu">
                                                <li><a href="GoDemand.html">Go Demand</a></li>
                                                <li><a href="GoMart.html">Go Mart</a></li>
                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                                <li> <a href="#">Help Centre</a></li>
                                <li> <a href="#">Join Us</a></li>
                            </ul>
                            <div class="mobile-menu">
                                <div class="menu-click">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        end main menu
                        <div class="rt-nav-tolls d-flex align-items-center">
                            <span class="d-md-inline d-none"><a href="#" class="rt-btn rt-gradient2 rt-rounded text-uppercase rt-Bshadow-1">Contact
            Us</a></span>
                        </div>

                    </div>
                </div>
            </nav>
        </div>

    </header> -->


    <!-- TAB -->
    <div class="container-fluid cst-header">
        <header>
             <a href="index.html" class="sticky-logo "><img src="<?php echo base_url();?>landing-assets/assets/images/logo/finigoo.png" alt="" 
            ></a>
                  
            <div id="material-tabs">
                <a id="tab1-tab" href="#tab1partner" class="active">Finigoo Driver</a>
                <a id="tab2-tab" href="#tab2partner">Food Delivery Driver</a>
               <!-- <a id="tab3-tab" href="#tab3partner">Express Delivery Driver</a>
                <a id="tab4-tab" href="#tab4partner">Food Merchant</a>-->
                <a id="tab5-tab" href="#tab5partner">Pay Merchant</a>
                <span class="yellow-bar"></span>
            </div>
        </header>

        <div class="tab-content">
            <div id="tab1partner"><!-------tab1--------->
                <div class="cardparrent">
                    <h3 style="color:red">Drive with Finigoo</h3>
                    <p>Enter your basic information to get started</p>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="First Name">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="Last Name">
                            </div>
                        </div>
                    </div>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="+91       Phone Number">
                            </div>
                        </div>
                    </div>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <input type="email" class="form-control cstform" id="firstname" placeholder="Email Address">
                            </div>
                        </div>
                    </div>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <select class="form-control cstform">
                                    <option value="" selected="selected">Type of Driver</option>
                                    <option value="GC">FinigooCar</option><option value="GT">FinigooTaxi – Standard</option>
                                    <option value="GT_LIMO">FinigooTaxi – Limo</option>
                                    <option value="-GH">FinigooHitch</option>
                                    <option value="GCO">FinogooCoach</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <select class="form-control cstform">
                                        <option value="" selected="selected">How did you hear about Finigoo?</option>
                                        <option value="Outdoor">Outdoor</option>
                                        <option value="TV">TV</option>
                                        <option value="Radio">Radio</option>
                                        <option value="Newspaper / Print">Newspaper / Print</option>
                                        <option value="Friends &amp; Family">Friends &amp; Family</option>
                                        <option value="Finigoo Driver">Finigoo Driver</option>
                                        <option value="Finigoo Staff">Finigoo Staff</option>
                                        <option value="Event">Event</option>
                                        <option value="Forum / Blog">Forum / Blog</option>
                                        <option value="Internet Ad &amp; Search">Internet Ad &amp; Search</option>
                                        <option value="Facebook">Facebook</option>
                                        <option value="Twitter">Twitter</option>
                                        <option value="Instagram">Instagram</option>
                                        <option value="Others">Others</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="Referral Code">
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-success submitcstbtn">SIGN UP NOW</button>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <p style="font-size: 12px;text-align: center;margin-top: 10px;color: gray;">By proceeding, I agree that you can collect, use and disclose the information provided by me in accordance with your <a href="#">Privacy Policy</a> which I have read and understand.</p>
                        </div>
                    </div>

                </div>
            </div><!----tab1 end----->


            <div id="tab2partner"><!--------tab2---------->
                 
                <div class="cardparrent">
                    <h3 style="color:red">Sign up with FinigooFood</h3>
                    <p>Enter your basic information to get started</p>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="First Name">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="Last Name">
                            </div>
                        </div>
                    </div>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="+91      Phone Number">
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <input type="email" class="form-control cstform" id="firstname" placeholder="Email Address">
                            </div>
                        </div>
                    </div> -->
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <select class="form-control cstform">
                                    <option value="" selected="selected">What is your vechile type?</option>
                                    <option value="MB">Motorbike</option>
                                    <option value="BC">Bicycle</option>
                                    <option value="D0F">Delivery on Foot</option>
                                    <option value="PAB">Power Assisted Bicycle</option>
                                 </select>
                            </div>
                        </div>
                    </div>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <select class="form-control cstform">
                                        <option value="" selected="selected">How did you hear about Finigoo?</option>
                                        <option value="Outdoor">Outdoor</option>
                                        <option value="TV">TV</option>
                                        <option value="Radio">Radio</option>
                                        <option value="Newspaper / Print">Newspaper / Print</option>
                                        <option value="Friends &amp; Family">Friends &amp; Family</option>
                                        <option value="Finigoo Driver">Finigoo Driver</option>
                                        <option value="Finigoo Staff">Finigoo Staff</option>
                                        <option value="Event">Event</option>
                                        <option value="Forum / Blog">Forum / Blog</option>
                                        <option value="Internet Ad &amp; Search">Internet Ad &amp; Search</option>
                                        <option value="Facebook">Facebook</option>
                                        <option value="Twitter">Twitter</option>
                                        <option value="Instagram">Instagram</option>
                                        <option value="Others">Others</option>
                                </select>
                            </div>
                        </div>
                   </div>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="Referral Code">
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-success submitcstbtn">SIGN UP NOW</button>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <p style="font-size: 12px;text-align: center;margin-top: 10px;color: gray;">By proceeding, I agree that you can collect, use and disclose the information provided by me in accordance with your <a href="#">Privacy Policy</a> which I have read and understand.</p>
                        </div>
                    </div>

                </div>
            </div><!------tab2 ends------->
            
          <!-- <div id="tab3partner">
                 
                <div class="cardparrent">
                    <h3 style="color:red">Sign up with GrabExpress</h3>
                    <p>Enter your information below to get started</p>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="First Name">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="Last Name">
                            </div>
                        </div>
                    </div>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="+91      Phone Number">
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <input type="email" class="form-control cstform" id="firstname" placeholder="Email Address">
                            </div>
                        </div>
                    </div> 
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <select class="form-control cstform">
                                    <option value="" selected="selected">What is your vechile type?</option>
                                    <option value="MB">Motorbike</option>
                                    
                                 </select>
                            </div>
                        </div>
                    </div>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <select class="form-control cstform">
                                        <option value="" selected="selected">How did you hear about GrabExpress?</option>
                                        <option value="Outdoor">Outdoor</option>
                                        <option value="TV">TV</option>
                                        <option value="Radio">Radio</option>
                                        <option value="Newspaper / Print">Newspaper / Print</option>
                                        <option value="Friends &amp; Family">Friends &amp; Family</option>
                                        <option value="Grab Driver">Grab Driver</option>
                                        <option value="Grab Staff">Grab Staff</option>
                                        <option value="Event">Event</option>
                                        <option value="Forum / Blog">Forum / Blog</option>
                                        <option value="Internet Ad &amp; Search">Internet Ad &amp; Search</option>
                                        <option value="Facebook">Facebook</option>
                                        <option value="Twitter">Twitter</option>
                                        <option value="Instagram">Instagram</option>
                                        <option value="Others">Others</option>
                                </select>
                            </div>
                        </div>
                   </div>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="Referral Code">
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-success submitcstbtn">SIGN UP NOW</button>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <p style="font-size: 12px;text-align: center;margin-top: 10px;color: gray;">By proceeding, I agree that you can collect, use and disclose the information provided by me in accordance with your <a href="#">Privacy Policy</a> which I have read and understand.</p>
                        </div>
                    </div>

                </div>
            </div>


            <div id="tab4partner">
                <div class="cardparrent">
                    <h3 style="color:red">Partner with us</h3>
                    <p>Enter your basic information to get started</p>
                          <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="businessname" placeholder="Business Name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="First Name">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="Last Name">
                            </div>
                        </div>
                    </div>

                      <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="+91      Phone Number">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                               
                                <input type="email" class="form-control cstform" id="firstname" placeholder="Email Address">
                            </div>
                        </div>
                    </div>

                      <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="address" placeholder="Address">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="postalcode" placeholder="Postal Code">
                            </div>
                        </div>
                    </div>

                      <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                 <select class="form-control cstform">
                                    <option value="" selected="selected">Type of Business</option>
                                    <option value="SP">Sole proprietor</option>
                                    <option value="CP">Companies and Partnerships</option>
                                    <option value="T_F_NGO">Trust Foundations and NGOs</option>
                                    <option value="PSG">Public Sector bodies, State-owned companies, Government</option>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="outlets" placeholder="Number of Outlets">
                            </div>
                        </div>
                    </div>

                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <select class="form-control cstform">
                                    <option value="" selected="selected">Type of Partnership</option>
                                    <option value="GM">GrapPay Merchant</option>
                                    <option value="GM">GrabFood Merchant</option>
                                    <option value="GP_GM">GrabPay and GrabFood Merchant</option>
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="Job Title">
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-success submitcstbtn">SUBMIT</button>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <p style="font-size: 12px;text-align: center;margin-top: 10px;color: gray;">By proceeding, I agree that you can collect, use and disclose the information provided by me in accordance with your <a href="#">Privacy Policy</a> which I have read and understand.</p>
                        </div>
                    </div>

                </div>
            </div><!----tab4 end----->

             <div id="tab5partner"><!-------tab5--------->
                <div class="cardparrent">
                    <h3 style="color:red">Want to be a FinigooPay merchant?</h3>
                    <p>Leave your details here and we will be in touch shortly.</p>
                          <div class="row cstrow"><!---BUSINESSS NAME-->
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="businessname" placeholder="Business Name">
                            </div>
                        </div>
                    </div>
                    <div class="row"><!--- NAME-->
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="First Name">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="firstname" placeholder="Last Name">
                            </div>
                        </div>
                    </div>

                      <!---MOBILE MAIL-->
                        <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="number" placeholder="+91      Phone Number">
                            </div>
                        </div>
                        <div class="col-xs-12 cstcol"><!---mail-->
                            <div class="form-group">
                               
                                <input type="email" class="form-control cstform" id="mail" placeholder="Email Address">
                            </div>
                        </div>
                        <!----- address code-->
                   <div class="col-xs-12 cstcol">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="address" placeholder="Business Address">
                            </div>
                        </div>

                      <div class="row">
                        <div class="col-xs-12 col-sm-7">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="city" placeholder="City">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <div class="form-group">
                                <input type="text" class="form-control cstform" id="postcode" placeholder="Post Code">
                            </div>
                        </div>
                    </div><!----add & portal code--->

                    
                        <div class="col-xs-12 cstco">
                            <div class="form-group">
                                 <select class="form-control cstform">
                                    <option value="" selected="selected">Business Type</option>
                                    <option value="SP">Sole proprietor</option>
                                    <option value="CP">Companies and Partnerships</option>
                                    <option value="T_F_NGO">Trust Foundations and NGOs</option>
                                    <option value="PSG">Public Sector bodies, State-owned companies, Government</option>
                                    
                                </select>
                            </div>
                        </div>
                <div class="col-xs-12 cstco">
                            <div class="form-group">
                                 <select class="form-control cstform">
                                    <option value="" selected="selected">Type of Partnership</option>
                                    <option value="GM">FinigooPay Merchant</option>
                                    <option value="GM">FinigooFood Merchant</option>
                                    <option value="GP_GM">FinigooPay and FinigooFood Merchant</option>
                                    
                                </select>
                            </div>
                        </div>

              
                
                    <button type="button" class="btn btn-success submitcstbtn">SUBMIT</button>
                    <div class="row cstrow">
                        <div class="col-xs-12 cstcol">
                            <p style="font-size: 12px;text-align: center;margin-top: 10px;color: gray;">By proceeding, I agree that you can collect, use and disclose the information provided by me in accordance with your <a href="#">Privacy Policy</a> which I have read and understand.</p>
                        </div>
                    </div>

                </div>
            </div><!----tab5 end----->
        </div>
    </div>
    <!-- end container -->
    <!-- TAB -->


    <!-- ==================Start Js Link===================== -->
    <script src="<?php echo base_url();?>landing-assets/assets/js/jquery-2.2.4.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/moment.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/instafeed.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/jquery.scrollUp.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/TweenMax.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/scrollax.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3&amp;key=AIzaSyCy7becgYuLwns3uumNm6WdBYkBpLfy44k"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/wow.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/jquery.overlayScrollbars.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/jquery.appear.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/slick.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/slider-range.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/vivus.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/tippy.all.min.js"></script>
    <script src="<?php echo base_url();?>landing-assets/assets/js/app.js"></script>

    <!-- ==================End Js Link===================== -->
    <!-- Tab Css Start -->
    <style>
        .cst-header .submitcstbtn {
            width: 100%;
            padding: 7px;
            font-size: 16px;
            background-color: #ed1d24;
            border-color: #ed1d24;
        }
        
        .cst-header .cstrow {
            padding: 5px 14px;
        }
        
        .cst-header .cstcol {
            width: 100%;
        }
        
        .cardparrent {
            max-width: 500px;
            padding: 25px;
            background-color: #fcfcfc;
            margin: 25px;
            position: absolute;
            width: 100%;
            right: 100px;
            top: 50px;
        }
        
        .cstform {
            height: 45px !important;
            font-size: 14px !important;
        }
        
        .cst-header {
            height: 750px;
            width: 100%;
            padding: 0px;
            margin: 0px;
            border-radius: 5px;
            box-shadow: 0 2px 3px rgba(0, 0, 0, .3);
            
        }
        
        .cst-header header {
            position: relative;
        }
        
        .cst-header .hide {
            display: none;
        }
        
        .cst-header .tab-content {
            padding: 0px;
            height: 100%;
            display: grid;
            width: 100%;
        }
        
        .cst-header #material-tabs {
            position: relative;
            display: block;
            padding: 0;
             float: right;
        }
        
        .cst-header #material-tabs>a {
            position: relative;
            display: inline-block;
            text-decoration: none;
            padding: 22px;
            text-transform: uppercase;
            font-size: 14px;
            font-weight: 600;
            color: #424f5a;
            text-align: center;
        }
        
        .cst-header #material-tabs>a.active {
            font-weight: 700;
            outline: none;
        }
        
        .cst-header #material-tabs>a:not(.active):hover {
            background-color: inherit;
            color: #7c848a;
        }
        
        #tab1partner {
            background: url("<?php echo base_url();?>landing-assets/assets/images/backgrounds/tab2.jpg");
            position: relative;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }
        
        #tab2partner {
            background:url("<?php echo base_url();?>landing-assets/assets/images/backgrounds/tab3.jpg");
            position: relative;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }
        
        #tab3partner {
            background:url("<?php echo base_url();?>landing-assets/assets/images/backgrounds/tab.jpg");
            position: relative;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }
        
        #tab4partner {
            background: url("<?php echo base_url();?>landing-assets/assets/images/backgrounds/tab.jpg");
            position: relative;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
			background-attachment: fixed;
        }
        
        #tab5partner {
            background:url("<?php echo base_url();?>landing-assets/assets/images/backgrounds/tab1.jpg");
            position: relative;
            background-repeat: no-repeat;
            background-size: cover;
             background-position: center;
        }
        
        @media only screen and (max-width: 520px) {
            .cst-header .nav-tabs#material-tabs>li>a {
                font-size: 11px;
            }
        }
        
        .cst-header #material-tabs a {
            border: 3px solid white;
            border-radius: 5px !important;
        }
        
        .cst-header #material-tabs a.active {
            border-bottom: 3px solid red;
            color: red;
        }
    </style>
    <!-- Tab Css Start -->
    <!-- Tab Script Start -->
    <script>
        $(document).ready(function() {
            $('.cst-header #material-tabs').each(function() {

                var $active, $content, $links = $(this).find('a');

                $active = $($links[0]);
                $active.addClass('active');

                $content = $($active[0].hash);

                $links.not($active).each(function() {
                    $(this.hash).hide();
                });

                $(this).on('click', 'a', function(e) {

                    $active.removeClass('active');
                    $content.hide();

                    $active = $(this);
                    $content = $(this.hash);

                    $active.addClass('active');
                    $content.show();

                    e.preventDefault();
                });
            });
        });
    </script>

<script src="<?php echo base_url();?>landing-assets/assets/js/jquery.flagstrap.js"></script>
<script>
       $('#options').flagStrap({
        countries: {
            "IN": "India",
            "TH": "Thailand",
            "SG": "Singapore",
            "ID": "Indonesia",
        },
        buttonSize: "btn-sm",
        buttonType: "btn-info",
        labelMargin: "10px",
        scrollable: false,
        scrollableHeight: "350px"
    });
</script>
    <!-- Tab Script Start -->
</body>

</html>