<style >
    /*second nav */
    #myTab-2 li a{
         border: .8px solid #ad72056b;
         border-radius: 25px;
         color:black;
         padding: 6px 30px;
          font-size: 14px;
    }
    #myTab-2 li a.active {
    color: #fff;
    background-color: #E92C3C;
    font-size: 14px;
    display: inline-block;
    padding: 6px 30px;
    border-radius: 25px;
    /*border: .8px solid black;*/
}
/*footer subcribe*/
.rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
    font-size: 20px;
    font-size: 14px;
    font-weight: 600;
    position: absolute;
    top: 3px;
    right: 3px;
    height: 51px;
    padding: 0 40px;
    text-transform: uppercase;
    border-radius: 999px;
    /*position: relative;*/
    z-index: 1;
    color: #fff;
    border: none;
    background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
    background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
}
/*back to top button*/
#scrollUp {
    font-size: 30px;
    line-height: 48px;
    position: fixed;
    right: 30px;
    bottom: 30px;
    width: 45px;
    height: 45px;
    text-align: center;
    color: #fff;
    border-radius: 50%;
    background: #E92C3C;
}
/*services hover bg color*/
.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
    padding-left: 23px;
    color: #fff;
    background: #E92C3C;
}
.sticky {
  position: fixed;
  top: 16%;
  width: 100%;
  z-index: 99;
  animation-name: fadeInDown;
}
#bnr_press {
    margin-top: 90px;
    padding-top: 30px;
    background-color: #ffffff;
	font-family: 'Baloo Thambi 2', cursive;
}
.col-9 .show{
	opacity:1;
}
#sld{
	padding: 20px;
    border-radius: 6px;
    margin-bottom: 40px;
    border: 1px solid lightgrey;
	}
.side-pills .nav-link{
    color: black;
    background-color: transparent;
    padding: 10px 26px;
    text-align: left;
    text-decoration: none;
    color: #000;
    border-radius: 0;
    border-radius: 999px;
    background: #fff;
    -webkit-box-shadow: 0 0 10px 0 rgba(51, 55, 69, .2);
    box-shadow: 0 0 10px 0 rgba(51, 55, 69, .2);
    margin-bottom: 20px;
}
</style>
<!-- 
    ====== Services Start ==============
 -->
 

   <div class="container-fluid" id="bnr_press">
	<div class="container">
		<div class="row"> 
		
			<div class="col-lg-8">
				<h1><b>Finigoo Press</h1>
<p>Our press release, coverage and press kit</b></p>
				
			</div>
			<div class="col-lg-4">
				<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/processed.png">
			</div>
		</div>
   </div>
   </div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
    
   <div class="container">
		<div class="row">
  <div class="col-3">
    <div class="nav flex-column nav-pills side-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Press Release</a>
      <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Press Coverage</a>
      <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Press Kit</a>
      <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Contact</a>
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
			<div class=" offset-lg-1 col-lg-11" id="sld">
				<div class="col-lg-8">
					<p>December 6, 2018</p>
					<h2><b>Mashrafe Moving Bangladesh!</b></h2>
				</div>
				<div class="col-lg-4">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/career1.jpg">
				</div>
			</div>
	  </div>
      <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
			<div class="offset-lg-1 col-lg-11" id="sld">
				<div class="col-lg-8">
					<p>December 6, 2018</p>
					<h2><b>Mashrafe Moving Bangladesh!</b></h2>
				</div>
				<div class="col-lg-4">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/career1.jpg">
				</div>
			</div>
			</br>
			</br>
			<div class="offset-lg-1 col-lg-11" id="sld">
				<div class="col-lg-8">
					<p>December 6, 2018</p>
					<h2><b>Mashrafe Moving Bangladesh!</b></h2>
				</div>
				<div class="col-lg-4">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/career1.jpg">
				</div>
			</div>
			</br>
			</br>
			<div class="offset-lg-1 col-lg-11" id="sld">
				<div class="col-lg-8">
					<p>December 6, 2018</p>
					<h2><b>Mashrafe Moving Bangladesh!</b></h2>
				</div>
				<div class="col-lg-4">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/career1.jpg">
				</div>
			</div>
			
			
			
	  </div>
      <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
			<div class="offset-lg-1 col-lg-11" id="sld">
				<div class="col-lg-8">
					<p>December 6, 2018</p>
					<h2><b>Mashrafe Moving Bangladesh!</b></h2>
				</div>
				<div class="col-lg-4">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/career1.jpg">
				</div>
			</div>
	  </div>
      <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
		<div class="offset-lg-1 col-lg-11" id="sld">
				<div class="col-lg-8">
					<p>December 6, 2018</p>
					<h2><b>Mashrafe Moving Bangladesh!</b></h2>
				</div>
				<div class="col-lg-4">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/career1.jpg">
				</div>
			</div>
	  </div>
    </div>
  </div>
		</div>
	</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>