<style >
    /* .works3-area .rt-inner-overlay {
    height: 439px;
    background-image: -webkit-linear-gradient(82deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
    background-image: linear-gradient(8deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
} */
    /*second nav */
    #myTab-2 li a{
         border: .8px solid #ad72056b;
         border-radius: 25px;
         color:black;
         padding: 6px 30px;
          font-size: 14px;
    }
    #myTab-2 li a.active {
    color: #fff;
    background-color: #01AC00;
    font-size: 14px;
    display: inline-block;
    padding: 6px 30px;
    border-radius: 25px;
    /*border: .8px solid black;*/
}
/*footer subcribe*/
.rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
    font-size: 20px;
    font-size: 14px;
    font-weight: 600;
    position: absolute;
    top: 3px;
    right: 3px;
    height: 51px;
    padding: 0 40px;
    text-transform: uppercase;
    border-radius: 999px;
    /*position: relative;*/
    z-index: 99999;
    color: #fff;
    border: none;
    background: -webkit-linear-gradient(55deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
    background: linear-gradient(35deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
}
/*back to top button*/
#scrollUp {
    font-size: 30px;
    line-height: 48px;
    position: fixed;
    right: 30px;
    bottom: 30px;
    width: 45px;
    height: 45px;
    text-align: center;
    color: #fff;
    border-radius: 50%;
    background: #01AC00;
}
/*services hover bg color*/
.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
    padding-left: 23px;
    color: #fff;
    background: #01AC00;
}
.submenufix.show{
	background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.38)), url(<?php echo base_url();?>landing-assets/assets/images/extra/autos1.jpg);
    background-size: cover;
}
@media (max-width:1200px){
.submenufix.show{
		background:white;
	}
}

.works3-area .rt-inner-overlay {
    height: 439px;
    background-image: -webkit-linear-gradient(82deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
    background-image: linear-gradient(8deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
}
</style>

<div class="rt-breadcump rt-breadcump-height">
    <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/goauto.png)">
    </div><!-- /.rt-page-bg -->
    <div class="container">
        <div class="row rt-breadcump-height">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h3>Go Auto</h3>
                    <p style="font-size: 30px;">Local.. Reliable.. Safe !</p>
                
                </div><!-- /.breadcrumbs-content -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.rt-bredcump -->

<!-- <div class="container" id="tabb" >
<div class="flight-list-box rt-mb-40">
                        <ul class="nav pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="#rt-itm_1" role="tab"
                                    aria-controls="rt-itm_1" aria-selected="true">Go Auto</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">blog</a>
                            </li>
                            <li class="nav-item">
                               
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                               
                            </li>
                        </ul>
                    </div>
</div> -->

<section class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 mx-auto text-center">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title"><span style="color:#01AC00">Go Auto</span>     
 Local.. Reliable.. Safe.!</h2>
                    
                </div><!-- /.rt-section-title-wrapper -->
            </div><!-- /.col-lg-8 -->
        </div><!-- ./row -->
    </div>
</section>


<section class="about-area2">
    <div class="rt-design-elmnts  rtbgprefix-contain" style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/all-img/Go Auto-01.png)">
    </div><!-- /.rt-design-elmnts -->
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 mb-0 mb-md-4">
                
              <h2 class="rt-section-title">
                        <span style="color:#01AC00">Go Auto</span>
                    </h2>
                <div class="text-424 wow fadeInUp" data-wow-duration="1s">
                        <p>
                    Go Auto is a online Autorickshaw booking application for Across Pan India. This application helps to get nearest auto Autorickshaw quick and easy. Application also helps get all details of driver, vehicle information. Go Auto app is featured with Communication through calls, chat and location sharing.
                    This application helps the riders security with travel history and without any online payment.
                    </p>
                </div><!-- /.text-424 -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6 offset-lg-1">
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/Go Auto-01.png" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>

<section class="works-area">
    <div class="container">

        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row">
            <div class="col-lg-3 col-md-6 mx-auto text-center">
                <div class="services-box-2 wow fade-in-bottom">
                    <div class="services-thumb">
                        <img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/easysteps.png" style="width: 120px;" alt="service-icons" draggable="false">
                    </div><!-- /.services-thumb -->
                    <h4>Easy</h4>
                    <p>Go Auto at just a click of a button</p>
                </div><!-- /.services-box-2 -->
            </div><!-- /.col-lg-3 -->
            <div class="col-lg-3 col-md-6 mx-auto text-center">
                <div class="services-box-2 wow fade-in-bottom" data-wow-duration="1s">
                    <div class="services-thumb">
                        <img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/easyreturn.png" style="width: 120px;" alt="service-icons" draggable="false">
                    </div><!-- /.services-thumb -->
                    <h4>Fast</h4>
                    <p>Dedicated vehicles to serve you with real-time tracking.</p>
                </div><!-- /.services-box-2 -->
            </div><!-- /.col-lg-3 -->
            <div class="col-lg-3 col-md-6 mx-auto text-center">
                <div class="services-box-2 wow fade-in-bottom" data-wow-duration="1.5s">
                    <div class="services-thumb">
                        <img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/secure.png" style="width: 120px;" alt="service-icons" draggable="false">
                    </div><!-- /.services-thumb -->
                    <h4>Safe</h4>
                    <p>Verified drivers and SOS safety feature for all our riders.</p>
                </div><!-- /.services-box-2 -->
            </div><!-- /.col-lg-3 -->
            <div class="col-lg-3 col-md-6 mx-auto text-center">
                <div class="services-box-2 wow fade-in-bottom" data-wow-duration="2s">
                    <div class="services-thumb">
                        <img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/fee.png" style="width: 120px;" alt="service-icons" draggable="false">
                    </div><!-- /.services-thumb -->
                    <h4>Eco Friendly</h4>
                    <p>No pollution vehicles which leads to a greener environment</p>
                </div><!-- /.services-box-2 -->
            </div><!-- /.col-lg-3 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
<div class="spacer-bottom"></div><!-- /.spacer-bottom -->
<section class="works3-area">
    <div class="rt-inner-overlay"></div><!-- /.rt-overlay -->
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="rt-section-title-wrapper">
                    <h3>Go Auto eco-friendly ride begins with a single Tap.</h3>
                </div><!-- /.rt-section-title-wrapper -->
                <div class="rt-box-style-1">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" style="height: 280px;">
                               
                                <div class="iconbox-content">
                                    <img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/1.png" style="width: 50px;"/>
                                    <h5>Go Auto</h5>
                                    <p>Now the Autorickshaw travel booking is your finger tips. Go Auto - world is the first, easy and most efficient application for booking for Autorickshaw passengers in Kerala
Within this app you can book your nearest [closest] Autorickshaw in seconds</p>
                                </div><!-- /.iconbox-content -->
                            </div><!-- /.rt-single-icon-box -->
                        </div><!-- /.col-lg-4 -->
                        <div class="col-lg-4 col-md-6">
                            <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="1s" >
                              
                                <div class="iconbox-content">
                                    <img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/1.png" style="width: 50px;"/>
                                    <h5>Safety for Women and Children</h5>
                                    <p>Passengers, including women and children, can ride in any Autorickshaw all the time without fear. In emergency situations you can inform the police through the Emergency Alert Button in the app and send an alert messages to any of the ten relevant numbers.</p>
                                </div><!-- /.iconbox-content -->
                            </div><!-- /.rt-single-icon-box -->
                        </div><!-- /.col-lg-4 -->
                        <div class="col-lg-4 col-md-6">
                            <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="1.5s">
                              
                                <div class="iconbox-content">
                                    <img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/1.png" style="width: 50px;"/>
                                    <h5>More Income for Drivers</h5>
                                    <p>Drivers can earn more Income to using this app. You can get more passengers in this app. Drivers can easily locate places where most of the passengers are. This app helps to communicate between drivers and passengers, and find their space.</p>
                                </div><!-- /.iconbox-content -->
                            </div><!-- /.rt-single-icon-box -->
                        </div><!-- /.col-lg-4 -->
                     
                    </div><!-- /.row -->
                </div><!-- /.rt-box-style-1 -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
<div class="spacer-top"></div><!-- /.spacer-bottom -->
<section class="about-area">
    <div class="rt-design-elmnts rtbgprefix-contain"  style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/all-img/auto.png)">

    </div><!-- /.rt-design-elmnts -->
    <div class="container">
        <div class="row">
            <div class="col-xl-7 offset-xl-5">
               <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div id="accordion">
    <div class="card wow fade-in-bottom">
        <div class="card-header card-primary" id="headingOne">
            <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                    aria-controls="collapseOne"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i
                        class="icofont-question"></i></span>
                    Cashless
                </button>
            </h5>
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
               Pay from the multiple payment methods available..
            </div>
        </div>
    </div><!-- end single accrodain -->
    <div class="card wow fade-in-bottom" data-wow-duration="1.0s">
        <div class="card-header card-primary" id="headingTwo">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="false" aria-controls="collapseTwo"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i
                        class="icofont-question"></i></span>
                Dedicated Customer Support


                </button>
            </h5>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
            <div class="card-body">
                Reach out to us anytime. Good service our top priority.


            </div>
        </div>
    </div><!-- end single accrodain -->
    <div class="card wow fade-in-bottom" data-wow-duration="1.5s">
        <div class="card-header card-primary" id="headingThree">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree"
                    aria-expanded="false" aria-controls="collapseThree"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i
                        class="icofont-question"></i></span>
                SOS
                </button>
            </h5>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
            <div class="card-body">
               Your safety our first priority. call emergency services when in need.
            </div>
        </div>
    </div><!-- end single accrodian -->
    <div class="card wow fade-in-bottom" data-wow-duration="1.5s">
        <div class="card-header card-primary" id="headingThree2">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree2"
                    aria-expanded="false" aria-controls="collapseThree2"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i
                        class="icofont-question"></i></span>
               Top Rated Drivers


                </button>
            </h5>
        </div>
        <div id="collapseThree2" class="collapse" aria-labelledby="headingThree2" data-parent="#accordion">
            <div class="card-body">
               Ride with the verified and trained drivers to deliver only the best experience.
            </div>
        </div>
    </div><!-- end single accrodian -->
 

</div><!-- end accrodian group -->


            </div><!-- /.col-lg-7 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
            </div><!-- /.col-lg-9 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
<div class="spacer-top"></div><!-- /.spacer-bottom -->
