<!DOCTYPE html>
<html lang="en">

<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>landing-assets/login_css//main.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>





	<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Lato&amp;display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/datepicker.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/OverlayScrollbars.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/fontawesome.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/icofont.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/owl.theme.default.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/slick.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/slick-theme.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/slider-range.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/select2.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/tippy.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/app.css">
	<!--===============================================================================================-->
	<style>
		body {
			/* background-image: url("<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/login-bg.jpg"); */
			background: #fff;

		}

		.flip-container {
			perspective: 1000;
			position: absolute;
			right: 60px;
			top: 20%;
		}

		@media (max-width: 576px) {
			.flip-container {
				right: unset;

			}

			.flipper {
				max-width: 74% !important;
				margin: 0 auto;
			}
		}

		.backgroud-oval {
			right: -5%;
		}

		.flipper {
			padding: 30px;
			/* height: 400px; */
			width: 450px;
			height: 525px;
			position: relative;
			background: #f5f5f5;
			-webkit-border-radius: 20px;
			-moz-border-radius: 20px;
			border-radius: 20px;

			transition: 0.6s;
			transform-style: preserve-3d;
			box-shadow: 0 10px 15px 0 rgba(0, 0, 0, .1) !important;
			background: #fff;
		}

		.front,
		.back {
			/*background-color: rgba(0,0,0,.3);*/
			position: absolute;
			padding: 40px 30px;
			top: 0;
			left: 0;
			right: 0;

			backface-visibility: hidden;
		}

		.front {
			z-index: 2;
			/* for firefox 31 */
			transform: rotateY(0deg);
		}

		.back {
			transform: rotateY(180deg);
		}

		.flip {
			transform: rotateY(180deg);
		}







		form {
			margin-top: 45px;
			/* width: 380px;
	margin: 4em auto;
	padding: 3em 2em 2em 2em;
	background: #fafafa;
	border: 1px solid #ebebeb;
	box-shadow: rgba(0,0,0,0.14902) 0px 1px 1px 0px,rgba(0,0,0,0.09804) 0px 1px 2px 0px; */
		}

		.group {
			position: relative;
			margin-bottom: 45px;
		}

		input {
			font-size: 18px;
			padding: 10px 10px 10px 5px;
			-webkit-appearance: none;
			display: block;
			background: #fff;
			color: #636363;
			width: 100%;
			border: none;
			border-radius: 0;
			border-bottom: 1px solid #757575;
		}

		input:focus {
			outline: none;
		}


		/* Label */

		.group label {
			color: #999;
			font-size: 16px;
			font-weight: normal;
			position: absolute;
			pointer-events: none;
			left: 5px;
			top: 10px;
			transition: all 0.2s ease;
			width: 100%;
		}


		/* active */

		input:focus~label,
		input.used~label {
			top: -20px;
			transform: scale(.75);
			left: -45px;
			/* font-size: 14px; */
			color: #4a89dc;
		}


		/* Underline */

		.bar {
			position: relative;
			display: block;
			width: 100%;
		}

		.bar:before,
		.bar:after {
			content: '';
			height: 2px;
			width: 0;
			bottom: 1px;
			position: absolute;
			background: #4a89dc;
			transition: all 0.2s ease;
		}

		.bar:before {
			left: 50%;
		}

		.bar:after {
			right: 50%;
		}


		/* active */

		input:focus~.bar:before,
		input:focus~.bar:after {
			width: 50%;
		}


		/* Highlight */

		.highlight {
			position: absolute;
			height: 60%;
			width: 100px;
			top: 25%;
			left: 0;
			pointer-events: none;
			opacity: 0.5;
		}


		/* active */

		input:focus~.highlight {
			animation: inputHighlighter 0.3s ease;
		}


		/* Animations */

		@keyframes inputHighlighter {
			from {
				background: #4a89dc;
			}

			to {
				width: 0;
				background: transparent;
			}
		}

		.title {
			font-weight: 400;
		}

		.forgot {
			cursor: pointer;
			width: 100%;
			text-align: right;
		}

		.forgot label {
			cursor: pointer;
		}

		.button {
			outline: none !important;
			border: none;
			padding: 20px 50px;
			border-radius: 50px;
			color: rgba(0, 0, 0, .26);
			background-color: rgba(0, 0, 0, .12);
			font-size: 18px;
		}

		.logn_btn {
			text-align: center;
			margin: 40px 0px;
		}

		.signin {
			font-size: 12px;
			margin-top: 10px;
		}

		.signin label a {
			font-size: 12px;
		}

		.signin label a:hover {
			color: #000;
		}

		.field-icon {
			position: absolute;
			right: 10px;
			top: 15px;
			cursor: pointer;
		}

		.f-logo {
			width: 200px;
			/* position: absolute;
    z-index: 2; */
			padding: 15px 0px 15px 0px;
		}

		.nav-head {
			height: 85px;
			width: 100%;
			background: #fff;
			position: fixed;
			top: 0;
			text-align: center;
			z-index: 99;
		}

		.backgroud-oval {
			position: absolute;

		}

		.bg-section {
			background-image: url("<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/login-bg.jpg");
			background-size: cover;
		}
	</style>
</head>

<body>



	<nav class="nav-head">
		<a href="<?php echo base_url(); ?>"> <img src="<?php echo base_url(); ?>landing-assets/assets/logo/finogoo-logo.png" class="f-logo"></a>
	</nav>




	<section class="bg-section">


		<!-- <div class="col-12 col-sm-6"></div> -->

		<div class="">

		</div>
		<div class="limiter">
			<div class="container-login100">
				<div _ngcontent-bdw-c2="" class="backgroud-oval">
					<img _ngcontent-bdw-c2="" class="oval-back" src="<?php echo base_url(); ?>landing-assets/login_svg/back.svg">
					<img _ngcontent-bdw-c2="" class="oval-middle" src="<?php echo base_url(); ?>landing-assets/login_svg/middle.svg">
					<img _ngcontent-bdw-c2="" class="oval-front" src="<?php echo base_url(); ?>landing-assets/login_svg/front.svg">
				</div>




				<div class="flip-container">
					<div class="flipper" id="flipper">
						<div class="front">
							<h1 class="title">Login</h1>
							<label style="font-size: 12px;">Enter your details below here</label>
							<form>
								<div class="group">
									<input type="text"><span class="highlight"></span><span class="bar"></span>
									<label>Your Mobile Number</label>
								</div>
								<div class="group" style="margin-bottom: 24px;">
									<input type="password" name="password" id="password-field" style="position: relative;"><span class="highlight"></span><span class="bar"></span>
									<label>Enter Password</label>

									<span toggle="#password-field" class="fa fa-eye field-icon toggle-password"></span>
								</div>

								<div class="forgot">
									<label id="loginButton">Forgot Password</label>
								</div>

								<div class="logn_btn">
									<button type="button" class="button buttonBlue">
										LOGIN
									</button>
									<div class="signin">
										<label>New User? <a href="#">Sign Up</a></label>
									</div>
								</div>
							</form>
						</div>

						<div class="back">
							<h1 class="title" style="font-size: 20px;cursor: pointer;" id="registerButton"> <span style="float: left;margin-right: 8px;"> <img src="login_svg/backarrow.svg"></span>Login</h1>

							<br />
							<h1 class="title">Forgot Password</h1>
							<label style="font-size: 12px;">Enter your registered email address / mobile number.</label>
							<form>
								<div class="group" style="margin-top: 75px;">
									<input type="text"><span class="highlight"></span><span class="bar"></span>
									<label>Your Email or Mobile Number</label>
								</div>

								<div class="logn_btn" style="margin-top: 75px;">
									<button type="button" class="button buttonBlue">
										SEND
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>


			</div>
		</div>


	</section>

	<section class="rt-site-footer" data-scrollax-parent="true">
		<div class="rt-shape-emenetns-1" style="background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/shape-elements/shape-4.png); transform: translateZ(0px) translateY(-126.188px);" data-scrollax="properties: { translateY: '340px' }"></div><!-- /.rt-shape-emenetns-1 -->
		<div class="footer-top rtbgprefix-cover">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6">
						<div class="rt-single-widget wow fade-in-bottom animated" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fade-in-bottom;">
							<h3 class="rt-footer-title"><img src="<?php echo base_url(); ?>landing-assets/assets/logo/finogoo-logo.png"></h3><!-- /.rt-footer-title -->
						</div><!-- /.rt-single-widge -->
					</div><!-- /.col-lg-3-->
					<div class="col-lg-3 col-md-6">
						<div class="rt-single-widget wow fade-in-bottom animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
							<!-- <h3 class="rt-footer-title">Work With Us</h3> -->
							<ul class="rt-usefulllinks">
								<li><a href="<?php echo base_url(); ?>aboutus" style="list-style-type: none;">About Us</a></li>
								<!-- <li><a href="#" style="list-style-type: none;">Services</a></li> -->
								<li><a href="<?php echo base_url(); ?>career" style="list-style-type: none;">Careers</a></li>
								<li><a href="<?php echo base_url(); ?>goads" style="list-style-type: none;">Go Ads</a></li>
								<li><a href="<?php echo base_url(); ?>press-meet">Press Release</a></li>
								<!-- <li><a href="#">Retirement Plan</a></li>
								<li><a href="#">Travel APIs</a></li> -->
							</ul>
						</div><!-- /.rt-single-widget -->
					</div><!-- /.col-lg-3-->
					<div class="col-lg-3 col-md-6">
						<div class="rt-single-widget wow fade-in-bottom animated" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fade-in-bottom;">
							<!-- <h3 class="rt-footer-title">
								My Account
							</h3> -->
							<ul class="rt-usefulllinks">

								<!-- <li><a href="#">Help Centre</a></li> -->
								<li><a href="<?php echo base_url(); ?>contact-us">Contact Us</a></li>
								<!-- <li><a href="#">Driver Help Centre</a></li> -->
								<!-- <li><a href="#">Terms & Conditions</a></li> -->
								<li><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li>
								<li><a href="<?php echo base_url(); ?>safety">Safety</a></li>
							</ul><!-- /.rt-usefulllinks -->
						</div><!-- end single widget -->
					</div><!-- /.col-lg-3-->
					<div class="col-lg-3 col-md-6" id="social_icon">
						<div class="rt-single-widget wow fade-in-bottom animated" data-wow-duration="2.5s" style="visibility: visible; animation-duration: 2.5s; animation-name: fade-in-bottom;">

							<h3 class="rt-footer-title" id="follow_us">Follow Us On</h3>
							<a href="#"><img src="<?php echo base_url(); ?>landing-assets/assets/logo/fb.jpg"></a>
							<a href="#"><img src="<?php echo base_url(); ?>landing-assets/assets/logo/twitter.jpg"></a>
							<a href="#"><img src="<?php echo base_url(); ?>landing-assets/assets/logo/youtube.jpg"></a>

						</div>
					</div>
				</div>
				<hr>
				<div class="col-sm-12" id="fin_info">
					<p>© 2020 Finigoo</p>
				</div>
			</div>



			<div class="container-fluid " id="ftr_1">

				<div class="col-md-12" id="ftr_2">
					<img src="<?php echo base_url(); ?>landing-assets/assets/images/flights/brush_banner.png">
				</div>

			</div>
		</div>
	</section>

	<script>
		var loginButton = document.getElementById("loginButton");
		var registerButton = document.getElementById("registerButton");

		loginButton.onclick = function() {
			document.querySelector("#flipper").classList.toggle("flip");
		}

		registerButton.onclick = function() {
			document.querySelector("#flipper").classList.toggle("flip");
		}
	</script>

	<script>
		$(window, document, undefined).ready(function() {

			$('input').blur(function() {
				var $this = $(this);
				if ($this.val())
					$this.addClass('used');
				else
					$this.removeClass('used');
			});

			var $ripples = $('.ripples');

			$ripples.on('click.Ripples', function(e) {

				var $this = $(this);
				var $offset = $this.parent().offset();
				var $circle = $this.find('.ripplesCircle');

				var x = e.pageX - $offset.left;
				var y = e.pageY - $offset.top;

				$circle.css({
					top: y + 'px',
					left: x + 'px'
				});

				$this.addClass('is-active');

			});

			$ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
				$(this).removeClass('is-active');
			});

		});
	</script>

	<script>
		$(".toggle-password").click(function() {

			$(this).toggleClass("fa-eye fa-eye-slash");
			var input = $($(this).attr("toggle"));
			if (input.attr("type") == "password") {
				input.attr("type", "text");
			} else {
				input.attr("type", "password");
			}
		});
	</script>
</body>

</html>