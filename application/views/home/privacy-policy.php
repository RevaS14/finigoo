<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<style>
    /*second nav */
    #myTab-2 li a {
        border: .8px solid #ad72056b;
        border-radius: 25px;
        color: black;
        padding: 6px 30px;
        font-size: 14px;
    }

    #myTab-2 li a.active {
        color: #fff;
        background-color: #E92C3C;
        font-size: 14px;
        display: inline-block;
        padding: 6px 30px;
        border-radius: 25px;
        /*border: .8px solid black;*/
    }

    /*footer subcribe*/
    .rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
        font-size: 20px;
        font-size: 14px;
        font-weight: 600;
        position: absolute;
        top: 3px;
        right: 3px;
        height: 51px;
        padding: 0 40px;
        text-transform: uppercase;
        border-radius: 999px;
        /*position: relative;*/
        z-index: 1;
        color: #fff;
        border: none;
        background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
        background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
    }

    /*back to top button*/
    #scrollUp {
        font-size: 30px;
        line-height: 48px;
        position: fixed;
        right: 30px;
        bottom: 30px;
        width: 45px;
        height: 45px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
        background: #E92C3C;
    }

    /*services hover bg color*/
    .main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
    .main-menu ul ul>li:hover>a {
        padding-left: 23px;
        color: #fff;
        background: #E92C3C;
    }

    .sticky {
        position: fixed;
        top: 16%;
        width: 100%;
        z-index: 99;
        animation-name: fadeInDown;
    }

    #first {
        position: relative;
        top: 93px;
        height: 400px;
        margin-bottom: 50px;
    }

    #inner {
        height: 100%;
    }

    #inner .row {
        height: 100%;
    }

    #fst_bnr {
        background-image: url(<?php echo base_url();?>landing-assets/assets/images/all-img/smoke.png);
        background-position: center;
        background-size: cover;
        padding-left: 0px;
    }

    #snd_bnr {
        background: linear-gradient(0deg, rgba(224, 23, 23, 0.5), rgba(224, 23, 23, 0.5)), url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/tab1.jpg);
        background-position: center;
        background-size: cover;
        padding-left: 0px;
    }

    .txt {
        width: 80%;
        box-shadow: -12px 12px 0px 0px red;
        margin: auto;
        position: relative;
        height: 150px;
        background-color: white;
        text-align: center;
        top: 80px;
    }

    .txt h2 {
        padding: 60px;
        font-weight: bold;
    }

    @media (max-width:567px) {
        #fst_bnr {
            display: none;
        }
    }

    @media (min-width:568px) and (max-width:780px) {
        #snd_bnr {
            display: none;
        }
    }

    #list ul {
        list-style: none;
        padding: 0;
        margin: 0;
        background-color: #fcfcfe;
        box-shadow: 0 3px 6px 0 rgba(0, 0, 0, .13);
        padding-top: 20px;
        padding-bottom: 20px;
    }

    /*#list ul li{
    display: inline-block;
    padding: 10px 0;
    margin: 20px 0;
    border-left: 1px solid #e8ebf2;
    padding-left: 60px;
    padding-right: 60px;
}
@media (max-width:740px){
#list ul li{
    
    width:100%;
}
}
@media (min-width:741px) and (max-width:948px){
#list ul li{
    
    padding: 10px 20px 10px 20px;
}
}*/
    #list ul li a {
        color: #000;
        display: block;
        text-align: center;
        letter-spacing: 4px;
        border-left: 1px solid #adadad;
        border-radius: 0;
    }

    #list ul li a:first-child {
        border-left: none;
    }

    .sub_menu {
        background-color: #e8ebf2;
    }

    .content {
        background-color: #e8ebf2;
        padding-bottom: 100px;
        margin-bottom: 5%;
    }

    .content .head {
        margin-top: 50px;
    }

    .content .head h1 {
        border-top: 2px solid red;
        font-family: Lato, Helvetica, sans-serif;
        padding-top: 30px;
        font-size: 2.25em;
        font-weight: 300;
        line-height: 1.28;
        letter-spacing: 2px;
    }

    .content .text {
        margin-top: 50px;
        letter-spacing: 2px;
        text-align: justify;
    }

    .content .text h5 {
        font-weight: 800;
        text-align: center;
        line-height: 30px;
    }
     .content .text h4 {
        text-align: center;
     }

    .content .text li {
        letter-spacing: 2px;
        font-size: 14px;
    }
</style>


<!-- 
    ====== Services Start ==============
-->

<div class="section-title-spacer"></div>
<section id="first">
    <div class="container-fluid" id="inner">
        <div class="row">
            <div class="col-lg-6 col-md-6" id="fst_bnr">
                <div class="txt">
                    <h2>Privacy Policy</h2>
                </div>
            </div>
            <div class="col-lg-6 col-md-6" id="snd_bnr"></div>
        </div>
    </div>
</section>
<div class="container-fluid sub_menu">
    <div class="row justify-content-md-center">
        <div class="col-lg-10 col-md-12 col-sm-12" id="list">
            <ul class="nav nav-pills nav-fill">
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>help">FAQ</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>/terms-and-condition">Terms and Conditions</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>privacy-policy">Privacy Policy</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>contact-us">Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid content">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-4 head">
                <h1>Quicklar</br><span style="color:red;">Privacy Policy</span></h1>
            </div>
            <div class="col-lg-8 text">
                <h4>PRIVACY POLICY</h4>
                <h5>Introduction</h5>
                <p>Our privacy policy will help you understand what information we collect at Quicklar, how Quicklar uses it, and what choices you have. Quicklar built the Quicklar app as a free app. This SERVICE is provided by Quicklar at no cost and is intended for use as is. If you choose to use our Service, then you agree to the collection and use of information in relation with this policy. The Personal Information that we collect are used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.<br><br>
                The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible in our website, unless otherwise defined in this Privacy Policy.</p>
               
                <h4>Information Collection and Use</h4>
                <p>For a better experience while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to users name, email address, gender, location, pictures. The information that we request will be retained by us and used as described in this privacy policy.<br><br>
                The app does use third party services that may collect information used to identify you.</p>
                <h4>Cookies</h4>
                <p>Cookies are files with small amount of data that is commonly used an anonymous unique identifier. These are sent to your browser from the website that you visit and are stored on your devices’s internal memory.
                    <br><br>
                This Services does not uses these “cookies” explicitly. However, the app may use third party code and libraries that use “cookies” to collection information and to improve their services. You have the option to either accept or refuse these cookies, and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.</p>
                <h4>Location Information</h4>
                <p>Some of the services may use location information transmitted from users' mobile phones. We only use this information within the scope necessary for the designated service.</p><br>
                <h4>Device Information</h4>
                <p>We collect information from your device in some cases. The information will be utilized for the provision of better service and to prevent fraudulent acts. Additionally, such information will not include that which will identify the individual user.</p><br>
                <h4>Service Providers</h4>
                <p><strong>We may employ third-party companies and individuals due to the following reasons:</strong></p>
                <ul>
                    <li>To facilitate our Service;</li>
                    <li>To provide the Service on our behalf;</li>
                    <li>To perform Service-related services; or</li>
                    <li>To assist us in analyzing how our Service is used.</li>
                </ul>
                <p>
                We want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.

                </p><br>
                <h4>Security</h4>
                <p>We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.</p><br>
                <h4>Children’s Privacy</h4>
                <p>This Services do not address anyone under the age of 13. We do not knowingly collect personal identifiable information from children under 13. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.</p><br>
                <h4>Changes to This Privacy Policy</h4>
                <p>We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately, after they are posted on this page.</p>
                <h4>Contact Us</h4>
                <p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us.
Contact Information:
Email: info@quiclar.com</p>
                
            </div>
        </div>
    </div>
</div>