<style >
    /*second nav */
    #myTab-2 li a{
         border: .8px solid #ad72056b;
         border-radius: 25px;
         color:black;
         padding: 6px 30px;
          font-size: 14px;
    }
    #myTab-2 li a.active {
    color: #fff;
    background-color: #E92C3C;
    font-size: 14px;
    display: inline-block;
    padding: 6px 30px;
    border-radius: 25px;
    /*border: .8px solid black;*/
}
/*footer subcribe*/
.rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
    font-size: 20px;
    font-size: 14px;
    font-weight: 600;
    position: absolute;
    top: 3px;
    right: 3px;
    height: 51px;
    padding: 0 40px;
    text-transform: uppercase;
    border-radius: 999px;
    /*position: relative;*/
    z-index: 1;
    color: #fff;
    border: none;
    background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
    background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
}
/*back to top button*/
#scrollUp {
    font-size: 30px;
    line-height: 48px;
    position: fixed;
    right: 30px;
    bottom: 30px;
    width: 45px;
    height: 45px;
    text-align: center;
    color: #fff;
    border-radius: 50%;
    background: #E92C3C;
}
/*services hover bg color*/
.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
    padding-left: 23px;
    color: #fff;
    background: #E92C3C;
}
.sticky {
  position: fixed;
  top: 16%;
  width: 100%;
  z-index: 99;
  animation-name: fadeInDown;
}
/*.rt_pricing{
	    background: linear-gradient(0deg, rgba(0, 0, 0, 0.20), rgba(0, 0, 0, 0.20)),url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/home-finigoo1.jpg);
    background-repeat: no-repeat;
    background-size: 100% 100%;
    height: 100%;
	position:relative;
	top:93px;
	margin-bottom:95px;
}
#rt_pricing_text{
	position:relative;
	top:30%;
	color:white;
	font-weight:600;
}
#rt_pricing_text h3{
line-height: 33px;
    text-align: initial;
    font-weight: bold;
    font-size: 22px;
}
#rt_pricing_text button{
	color: black;
    background-color: #fee685;
    border-color: transparent;
	padding: 10px 23px;
    font-size: 16px;
	font-weight:bold;
}
.rt_pricing:after{
	content:"";
	display:table;
	clear:both;
	}*/
	.flexibleRowFT {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
}
.pricing ul li {
    display: inline-block;
    width: 33.333%;
    box-shadow: 0 3px 32px 0 rgba(0, 0, 0, 0.1);
    padding: 30px 25px 30px;
    transition: 0.4s;
    transform: scale(1);
    background-color: #fff;
}
.pricing ul li:hover{
    transform: scale(1.05);
    z-index: 2;
}
.pricing ul li.hover{
    transform: scale(1.05);
    z-index: 1;
}
.pricing ul {
    width:100%;
    padding: 0px;
    margin: auto;
}
.flexible {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
}
.pricing .sub-menu li img{
	width: 90px;
    margin: auto;
    display: block;
}
.pricing ul li p:before {
    content: '';
    background-image: url(<?php echo base_url();?>landing-assets/assets/images/all-img/iconfinder_tick.png);
    width: 17px;
    height: 17px;
    position: absolute;
    background-size: contain;
    background-repeat: no-repeat;
    margin: auto;
    top: 0px;
    bottom: 0px;
    left: 0px;
}
.pricing ul li p {
    position: relative;
    font-size: 14px;
    letter-spacing: 0.1px;
    margin: 0px;
    color: #333;
    padding: 6px 0px;
    text-align: left;
    padding-left: 30px;
    line-height: 1.4;
    font-weight: 300;
}
#pricing_logos img{
	height: 60px;
    display: flex;
    margin: auto;
}
#pricing_logos{
	text-align:center;
}

#pricing_para p{
	line-height:40px;
}
#pricing_para{
    background-color: #fafafa;
    padding: 40px 15px;
}
#para2 span{
	color: red;
}

@media(max-width: 768px){
    .pricing ul li {

    width: 100%;
   
}
}
</style>
<!-- 
    ====== Services Start ==============
 -->
 
<!--<div class="rt_pricing">
   <div class="container" id="rt_pricing_text">
		<div class="row"> 
			<div class="col-lg-6">
				<h1><b><b>Finigoo</b>Kios by Kudo</b></h1>
<h3>All-round Digital Warung</h3>
				<button type="btn" class="btn btn-default btn-lg">Own a Warung? Be a FinigooKios Partner</button>
			</div>
		</div>
   </div>
</div>-->
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
		 <div class="container">
			<h2 class="text-center"><b>Simple Pricing That Works At Any Scale</b></h2>	
			</br>
			</br>
			<div class="row">
				<div class="col-12 text-center pricing">
					<ul class="flexible">
                <li class="">
                   <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/premium.png" alt="">
				   </br>
				   </br>
                    <strong><h4>Starter</h4></strong>
                    <h3>₹<b>50,000</b></h3>
                    <h4>Inclusive of all taxes</h4>
					<hr>
                    <p>Franchise Fee</p>
                    <p><b>100</b> Branding Sets</p>
                    <p><b>2000</b> Leaflets</p>
                    <p><b>5</b> T-Shirts</p>
                    <p><b>3</b> Bags</p>
                    <p><b>10</b> Tapes</p>
                    <p><b>2 Months Of Digital Marketing</b> (Facebook/ Instagram)</p></br>
                    <button class="btn btn-primary btn-block" style="padding: 12px 0px;font-size: 16px;">Get Started</button>
                </li>
                <li class="hover">
                    <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/premium.png" alt="">
					</br>
				   </br>
                    <strong><h4>Pro</h4></strong>
                    <h3>₹<b>80,000</b></h3>
                    <h4>Inclusive of all taxes</h4>
					<hr>
                    <p>Franchise Fee</p>
                    <p><b>200</b> Branding Sets</p>
                    <p><b>5000</b> Leaflets</p>
                    <p><b>10</b> T-Shirts</p>
                    <p><b>5</b> Bags</p>
                    <p><b>50</b> Tapes</p>
                    <p><b>6 Months Of Digital Marketing</b> (Facebook/ Instagram)</p></br>
                     <button class="btn btn-primary btn-block" style="padding: 12px 0px;font-size: 16px;">Get Started</button>
                </li>
                <li>
                    <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/premium.png" alt="">
					</br>
				   </br>
                    <strong><h4>Enterprise</h4></strong>
                    <h3>₹<b>1,25,000</b></h3>
                    <h4>Inclusive of all taxes</h4>
					<hr>
                    <p>Franchise Fee</p>
                    <p><b>300</b> Branding Sets</p>
                    <p><b>7500</b> Leaflets</p>
                    <p><b>15</b> T-Shirts</p>
                    <p><b>10</b> Bags</p>
                    <p><b>100</b> Tapes</p>
                    <p><b>12 Months Of Digital Marketing</b> (Facebook/ Instagram)</p></br>
                  <button class="btn btn-primary btn-block" style="padding: 12px 0px;font-size: 16px;">Get Started</button>
                </li>
            </ul>
				</div>
			</div>
		</div>
		
		<div class="section-title-spacer"></div>
		<div class="section-title-spacer"></div>	
	
			<div class="container-fluid" id="pricing_para">
				<div class="container">
                    <div class="row">
					<div class="col-lg-6" id="para1">
						<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/download-15.png">
					</div>
					<div class="offset-lg-1 col-lg-5" id="para2">
						<h2><b>City Launch Package</b></h2>
						
						<p>1 Employee and branding material for 100 stores</br>
<span>Starter Plan </span> + <b> ₹59,000 </b> = ₹1,09,000</br>

<span>Pro Plan </span> + <b> ₹59,000 </b> = ₹1,39,000</br>

<span>Enterprise Plan </span> + <b> ₹59,000 </b> = ₹1,84,000</br></p>
                    </div>
                </div>
				</div>
			</div>

		
		<div class="section-title-spacer"></div>