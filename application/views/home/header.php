<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo APPNAME; ?></title>
    <!-- ==================Start Css Link===================== -->
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/datepicker.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/OverlayScrollbars.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/fontawesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/icofont.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/slick.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/slick-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/slider-range.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/select2.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/tippy.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/app.css">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- ==================End Css Link===================== -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/component.css">
    <link rel='stylesheet' href='<?php echo base_url(); ?>landing-assets/assets/css/menu.css' />
    <!-- <link href="<?php echo base_url(); ?>landing-assets/assets/css/flag/flags.css" rel="stylesheet"> -->
    <!--[if lt IE 9]>
<script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<style>
    @import url(http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300ita‌​lic,400italic,500,500italic,700,700italic,900italic,900);

    html,
    body,
    html * {
        font-family: 'Roboto', sans-serif;
    }

    .icon-thumb {
        width: 80px;
        margin-left: 100px;
    }

    a {
        text-decoration: none !important;
    }
</style>
<style>
    .wrapper {
        margin-top: 68px;
    }

    #top .navbar-default .navbar-collapse {
        padding-left: 6px;
        padding-right: 6px;
    }

    #top .navbar-default {
        min-height: 66px;
    }

    #top .navbar-brand {
        margin-top: 16px;
    }

    @media only screen and (max-width: 767px) {
        .wrapper {
            margin-top: 50px;
        }
    }


    .menu_icon {
        margin-right: 16px !important;
        font-size: 30px !important;
        color: #121212 !important;
        float: left;
        width: 48px;
        padding: 0px !important;
    }

    .menu_icon .bars {
        margin-top: -4px;
        float: left;
        font-size: 10px;
        font-family: 'Sanomat Grab Web Medium', sans-serif;
        color: #363a45;
    }

    .display_none {
        display: none;
    }

    .slider_curve {
        background-color: #564545;
        height: auto;
        padding: 0;
        overflow: hidden;
        border-radius: 30px;
    }

    .effect-ruby-red:hover {
        background: #ed1d24;
    }

    .effect-ruby-org:hover {
        background: #FD6000;
    }

    .effect-ruby-skyblue:hover {
        background: #01ADD1;
    }

    .effect-ruby-blue:hover {
        background: #771FEA;
    }

    .effect-ruby-green:hover {
        background: #0FA214;
    }

    .effect-ruby-violet:hover {
        background: #D51E92;
    }
</style>

<body>
    <!-- 
        ================== Header Area Start===================
     -->
    <header class="rt-site-header  rt-fixed-top white-menu">
        <div class="main-header" id="sr">
            <nav class="navbar">
                <div class="container navbar_container">
                    <div class="main-menu">
                        <ul style="display: inline;float:left;">
                            <li class="serviceclick current-menu-item">
                                <a href="javascript:void(0);" class="menu_icon" data-toggle="collapse" data-target="#navbarSupportedContent">
                                    <!-- <i class="fa fa-bars" aria-hidden="true"></i> -->
                                    <svg class="ham hamRotate ham7" viewBox="0 0 100 100" width="80" onclick="this.classList.toggle('active')">
                                        <path class="line top" d="m 70,33 h -40 c 0,0 -6,1.368796 -6,8.5 0,7.131204 6,8.5013 6,8.5013 l 20,-0.0013" />
                                        <path class="line middle" d="m 70,50 h -40" />
                                        <path class="line bottom" d="m 69.575405,67.073826 h -40 c -5.592752,0 -6.873604,-9.348582 1.371031,-9.348582 8.244634,0 19.053564,21.797129 19.053564,12.274756 l 0,-40" />
                                        <text x="30" y="90" fill="#000" style="font-size: 14px;font-weight: 600;">MENU</text>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </div> <a href="<?php echo base_url(); ?>" class="brand-logo "><img src="<?php echo base_url(); ?>landing-assets/assets/images/logo/finigoo.png" alt=""></a>
                    <a href="<?php echo base_url(); ?>about-us" class="sticky-logo "><img src="<?php echo base_url(); ?>landing-assets/assets/images/logo/finigoo.png" alt=""></a>
                    <div class="ml-auto d-flex align-items-center"></div>
                    <div>
                        <form class="float-left">
                            <div class="form-group country_list">
                                <div id="options" data-input-name="country2" data-selected-country="IN"></div>
                            </div>
                        </form>
                        <div class="login_btn">
                            <a href="<?php echo base_url(); ?>login">
                                <!-- <i class="fa fa-user-circle" aria-hidden="true"></i> -->
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/user.svg">
                            </a>
                        </div>
                        <div class="login_btn">
                            <a href="<?php echo base_url(); ?>tracking">
                                <!-- <i class="fa fa-user-circle" aria-hidden="true"></i> -->
                                <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/tracking.svg">
                            </a>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- mobile menu -->
            <nav class="navbar navbar-expand-md navbar-light mobile_menu">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto py-4 py-md-0">
                        <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4 active"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Services</a>
                            <div class="dropdown-menu">
                                <label class="submenu_title">Transport & Logistics</label> <a class="dropdown-item" href="<?php echo base_url(); ?>bike">Go Bike </a>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>cabs">Go Cabs </a>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>goauto"> Go Auto </a>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>gopack">Go Pack</a>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>">Go Cargo </a>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>">Go Send </a>
                                <label class="submenu_title">Payments</label> <a class="dropdown-item" href="<?php echo base_url(); ?>kiosk">Go Kiosk</a>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>gobills">Go Bills</a>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>gopoint">Go Point</a>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>gopay"> Go Pay</a>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>goupi">Go UPI</a>
                                <label class="submenu_title">Food & FMCG</label> <a class="dropdown-item" href="<?php echo base_url(); ?>gofood"> Go Food </a>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>gomart">Go Mart</a>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>gobeauty"> Go Beauty </a>
                                <label class="submenu_title">Business</label> <a class="dropdown-item" href="<?php echo base_url(); ?>godemand">Go Demand </a>
                                <label class="submenu_title">Loan</label> <a class="dropdown-item" href="<?php echo base_url(); ?>goloan">Go Loan </a>
                                <!-- <label class="submenu_title">Go Ads</label>
                             <a class="dropdown-item" href="Goads.html">  Go Ads   </a> -->
                            </div>
                        </li>
                        <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4"> <a class="nav-link" href="<?php echo base_url(); ?>aboutus">About Us</a>
                        </li>
                        <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4"> <a class="nav-link" href="<?php echo base_url(); ?>help">Help Centre</a>
                        </li>
                        <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4"> <a class="nav-link" href="<?php echo base_url(); ?>godriver-tab">Join Us</a>
                        </li>
                        <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4"> <a class="nav-link" href="<?php echo base_url(); ?>career">Career</a>
                        </li>
                        <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Franchise Login
                            </a>
                            <div class="dropdown-menu"> <a class="dropdown-item" href="<?php echo base_url(); ?>franchise">Franchise Login</a>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>pricing">Go Pricing</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!-- Modal -->
    <div class="modal fade" id="rtmodal-1" tabindex="-1" role="dialog" aria-labelledby="rtmodal-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered rt-lgoinmodal " role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="rt-modal-headr rt-mb-20 one">
                        <img src="<?php echo base_url(); ?>landing-assets/assets/images/logo/Logo-icon.png" alt="modal logo" draggable="false">
                        <h4>Login in to Emigrar</h4>
                        <p>Log in to get in the moment updates on the things that interest you.</p>
                    </div>
                    <!-- /.rt-modal-headr -->
                    <div class="rt-modal-headr rt-mb-20 two">
                        <img src="<?php echo base_url(); ?>landing-assets/assets/images/logo/Logo-icon.png" alt="modal logo" draggable="false">
                        <h4>Create your Account</h4>
                        <p>Log in to get in the moment updates on the things that interest you.</p>
                    </div>
                    <!-- /.rt-modal-headr -->
                    <div class="rt-modal-input one">
                        <form action="#" class="rt-form">
                            <input type="text" class="form-control pill rt-mb-15" placeholder="User name">
                            <input type="password" class="form-control pill rt-mb-15" placeholder="Password">
                            <div class="form-group forgot-pass">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="gridCheckss">
                                    <label class="form-check-label" for="gridCheckss">Remember Password</label>
                                </div>
                                <!-- ./orm-group -->
                            </div>
                            <input type="submit" class="rt-btn rt-gradient pill d-block text-uppercase " value="Log In">
                        </form>
                        <div class="ac-register"> <span>Don’t have an account? <a href="#" class="open-creatac">Sign Up Now <i class="icofont-double-right"></i></a></span>
                        </div>
                        <!-- /.ac-register -->
                    </div>
                    <!-- /.rt-modal-input -->
                    <div class="rt-modal-input two">
                        <form action="#" class="rt-form">
                            <input type="text" class="form-control pill rt-mb-15" placeholder="User name">
                            <input type="password" class="form-control pill rt-mb-15" placeholder="Enter your mail address">
                            <input type="password" class="form-control pill rt-mb-15" placeholder="Password">
                            <div class="form-group forgot-pass">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="gridCheck333">
                                    <label class="form-check-label" for="gridCheck333">I'd like to hear about promos, new products,and much more!</label>
                                </div>
                                <!-- ./orm-group -->
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="gridCheck222">
                                    <label class="form-check-label" for="gridCheck222">By clicking "Sign up" you agree to our Terms of Service and Privacy Policy</label>
                                </div>
                                <!-- ./orm-group -->
                            </div>
                            <input type="submit" class="rt-btn rt-gradient pill d-block text-uppercase " value="Log In">
                        </form>
                        <div class="ac-register"> <span>Already have an account?<a href="#">LOGIN <i class="icofont-double-right"></i></a></span>
                        </div>
                        <!-- /.ac-register -->
                    </div>
                    <!-- /.rt-modal-input -->
                    <div class="rt-modal-footer"> <span>Or</span>
                        <h4>Sign Up with social media</h4>
                        <ul class="rt-social rt-circle-style2">
                            <li><a href="#"><i class="icofont-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="icofont-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="icofont-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.rt-modal-footer -->
                </div>
            </div>
        </div>
    </div>