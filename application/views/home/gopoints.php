<style >
    /*second nav */
    #myTab-2 li a{
         border: .8px solid #ad72056b;
         border-radius: 25px;
         color:black;
         padding: 6px 30px;
          font-size: 14px;
    }
    #myTab-2 li a.active {
    color: #fff;
    background-color: #E92C3C;
    font-size: 14px;
    display: inline-block;
    padding: 6px 30px;
    border-radius: 25px;
    /*border: .8px solid black;*/
}
/*footer subcribe*/
.rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
    font-size: 20px;
    font-size: 14px;
    font-weight: 600;
    position: absolute;
    top: 3px;
    right: 3px;
    height: 51px;
    padding: 0 40px;
    text-transform: uppercase;
    border-radius: 999px;
    /*position: relative;*/
    z-index: 1;
    color: #fff;
    border: none;
    background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
    background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
}
/*back to top button*/
#scrollUp {
    font-size: 30px;
    line-height: 48px;
    position: fixed;
    right: 30px;
    bottom: 30px;
    width: 45px;
    height: 45px;
    text-align: center;
    color: #fff;
    border-radius: 50%;
    background: #00AFF0;
}
/*services hover bg color*/
.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
    padding-left: 23px;
    color: #fff;
    background: #E92C3C;
}
.sticky {
  position: fixed;
  top: 16%;
  width: 100%;
  z-index: 99;
  animation-name: fadeInDown;
}
.rt{
	   background: linear-gradient(0deg, rgba(0, 0, 0, 0.20), rgba(0, 0, 0, 0.20)),
	   url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/gopoint.jpeg);
    background-repeat: no-repeat;
    background-size: 100% 100%;
    height: 100%;
    position: relative;
    top:40px;
    margin-bottom: 95px;
}

#bnr_txt h2{
	font-size: 55px;
    line-height: 65px;
    margin: 0 0 14px 0;
	color:white;
    text-transform: none;
	/* font-family: 'Baloo Thambi 2', cursive; */
}
#bnr_txt p{
	font-size: 22px;
    color: white;
    line-height: 1.4em;
	margin: 0 0 95px 0;
}
#bnr_txt{
	position:relative;
	top:160px;
}
#bnr_txt button{
	position: absolute;
    background-color: #fee685;
    color: #ed1d24;
    border: none;
    font-weight: lighter;
    height: 60px;
    width: 60%;
}
#bnr_txt button:focus{
	outline: #fee685;
    box-shadow: none;
}
#bnr_txt button i{
	padding-left: 45px;
    margin: 0px 10px 0px 10px;
    position: relative;
}
#bnr_txt button:hover i{
	left: 10px;
	transition: inherit;
}
#snd_bnr img{
    height: 100%;
}
@media (max-width:1390px){
	#bnr_txt p{
		margin: 0 0 30px 0;
	}
}
#cnt_two{
	margin-bottom:30px;
}
#cnt_two h2{
	/* font-family: 'Baloo Thambi 2', cursive; */
}
#share{
	background-color: #FBFBFB;
}
#join{
	padding: 40px;
}
#join h1{
	/* font-family: 'Baloo Thambi 2', cursive; */
	font-weight:bold;
}
#join p{
	font-size:22px;
	line-height: 35px;
    letter-spacing: 1px;
}
#join img{
	width: 50%;
}
#join1 h1{
	/* font-family: 'Baloo Thambi 2', cursive; */
	font-weight:bold;
	text-align:center;
}
#join1 p{
	font-size:22px;
	line-height: 35px;
    letter-spacing: 1px;
	text-align:center;
}
#join1_inr{
	width:50%;
	margin:auto;
	padding-top: 50px;
    padding-bottom: 50px;
}
#join_two{
	/* font-family: 'Baloo Thambi 2', cursive; */
	font-weight:bold;
}
.table th{
	width: 10%;
    text-align: center;
}
.gld{
	width:100%;
}
.gld img{
	width: 30%;
    margin: auto;
}
td img{
	width: 20%;
    display: flex;
    margin: auto;
}
tbody td{
	font-size:20px;
}
.table td{
	padding-top: 30px!important;
    padding-bottom: 30px!important;
}
#join_two p{
	padding: 30px;
    box-shadow: -1px 0px 8px 0px grey;
	font-size: 22px;
	line-height: 35px;
	border-radius: 20px;
}
#join_two h1{
	font-size:40px;
	margin-bottom:20px;
}
#join_three h1{
	font-size:40px;
	margin-bottom:20px;
	/* font-family: 'Baloo Thambi 2', cursive; */
	font-weight:bold;
}
#join_three ul{
    list-style-type: none;
    padding-left: 0;
}
#join_three ul li {
    position: relative;
	/* font-family: 'Baloo Thambi 2', cursive; */
    padding-left: 50px;
    font-size: 20px;
    margin-bottom: 30px;
}
#join_three ul li:before {
    content: '';
        width: 30px;
    height: 30px;
    position: absolute;
    background-image: url('<?php echo base_url();?>landing-assets/assets/images/all-img/accept.png');
    background-size: cover;
    background-position: center;
    left: 0;
    top: 50%;
    transform: translateY(-50%);
}
#join_four h2{
	font-size:40px;

	font-family: 'Baloo Thambi 2', cursive;
	font-weight:bold;
}
#join_five h1{
	font-size:40px;
	text-align:center;
	/* font-family: 'Baloo Thambi 2', cursive; */
	font-weight:bold;
}
#join_four ul li {
    display: inline-flex;
    width: 14%;
    float: left;
    border: 1px solid lightgray;
    border-radius: 13px;
    padding: 10px;
    margin-left: 10px;
}
#join_four img{
	height: 70px;
    width: 60%;
    margin: auto;
}
#join_five .panel-primary>.panel-heading {
    color: #000;
    background-color: transparent;
    border-color: transparent;
}
#join_five p{
	font-size: 22px;
    line-height: 35px;
	font-family: 'Baloo Thambi 2', cursive;
}
#join_five a{
	font-size: 22px;
    line-height: 35px;
	/* font-family: 'Baloo Thambi 2', cursive; */
}
/* #join_five i{
	padding-right: 20px;
} */
#snd_bnr {
    width: 50%;
    margin: auto;
	height: 200px;
}
@media (max-width:1330px){
	#snd_bnr {
	height: 150px;
	width: fit-content;
}
}
#expand .show{
	overflow:hidden;
}
</style>


<!-- 
    ====== Services Start ==============
 -->
 
<div class="rt-breadcump rt-breadcump-height">
    <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/gopoint.jpeg)">
    </div><!-- /.rt-page-bg -->
    <div class="container">
        <div class="row rt-breadcump-height">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h3>Go Point</h3>
                    <p style="font-size: 30px;">Earn Points for Every Order</p>
                  
                    <!-- /.breadcrumbs -->
                </div><!-- /.breadcrumbs-content -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>




	<!-- <div class="rt">
		<div class="container">
			
				<div class="col-lg-5" id="bnr_txt">
					<h2>Introducing </br> Finigoo Points</h2>
					<p>Finigoo Points lets you earn points toward benefits and discounts, so the more you order with us, the better it gets.	</p>
					<button class="btn btn-default btn-lg">See the Benefits <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
				</div>
			
		</div>
	</div> -->




<div class="section-title-spacer"></div>
	<div class="container" id="cnt_two">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="text-center">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/EarnPoints.png" style="width: 120px;">
				</div>
				</br>
				</br>
				<div class="col-lg-12">
					<h2 class="text-center"><b>Earn Points for</br>Every Order</b></h2>
					</br>
					<p class="text-center">You earn 1 point for every 10 taka you spend on Finigoo Ride or Food.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="text-center">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/RedeemExciting.png" style="width: 120px;">
				</div>
				</br>
				</br>
				<div class="col-lg-12">
					<h2 class="text-center"><b>Redeem Exciting</br>Deals</b></h2>
					</br>
					<p class="text-center">Use your available points to redeem exciting deals of your choice.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="text-center">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/UnlockBenefit.png" style="width: 120px;">
				</div>
				</br>
				</br>
				<div class="col-lg-12">
					<h2 class="text-center"><b>Unlock Benefits at</br>Every Level</b></h2>
					</br>
					<p class="text-center">Higher points mean higher levels and more exclusive benefits.</p>
				</div>
			</div>
		</div>
	</div>
<div class="section-title-spacer"></div>
	<div class="container-fluid" id="share">
		<div class="container">
			<div class="row">
				<div class="col-lg-6" id="join">
					<h1>How to Join</h1>
					<p>Update your app to join Finigoo Points. You’ll be added at no extra cost starting at the Bronze level.</p>
					</br>
					</br>
					<img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/play.png" style="float:left;">
					<img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/app.png" style="float:left;">
				</div>
				<div class="col-lg-6">
					<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/app-mbl2.png">
				</div>
			</div>
		</div>
	</div>
	<div class="section-title-spacer"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12" id="join1">
				<div id="join1_inr">
					<h1>Benefits of Finigoo Points</h1>
					</br>
					</br>
					<p>You'll automatically earn points every time you make an order. To access Finigoo Points, tap on the point widget at Home screen or from More tab.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="section-title-spacer"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 table-responsive">
				<table class="table table-striped">
					<thead>
						<th style="text-align: left;">BENEFITS</th>
						<th>
							<div class="gld">
								<img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/medal.png">
								<p><b>0 PTS</b></p>
								<h4><b>BRONZE</b></h4>
							</div>
						</th>
						<th>
							<div class="gld">
								<img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/medal (1).png">
								<p><b>200 PTS</b></p>
								<h4><b>SILVER</b></h4>
							</div>
						</th>
						<th>
							<div class="gld">
								<img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/gold-medal.png">
								<p><b>1000 PTS</b></p>
								<h4><b>GOLD</b></h4>
							</div>
						</th>
						<th>
							<div class="gld">
								<img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/medal.png">
								<p><b>7000 PTS</b></p>
								<h4><b>PLATINUM</b></h4>
							</div>
						</th>
					</thead>
					<tbody>
						<tr>
							<td>Earn points with every eligible ride and food order</td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/checkmark.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/checkmark.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/checkmark.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/checkmark.png"></td>
							
						</tr>
						<tr>
							<td>Access to exclusive deals and discounts	</td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/remove.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/checkmark.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/checkmark.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/checkmark.png"></td>
							
						</tr>
						<tr>
							<td>Priority Support</td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/remove.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/remove.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/checkmark.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/checkmark.png"></td>
							
						</tr>
						<tr>
							<td>Free deliveries on select food orders	</td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/remove.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/remove.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/checkmark.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/checkmark.png"></td>
							
						</tr>
						<tr>
							<td>Premium Support Hotline</td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/remove.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/remove.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/remove.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/checkmark.png"></td>
							
						</tr>
						<tr>
							<td>Special rates for your two favourite destinations*</td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/remove.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/remove.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/remove.png"></td>
							<td><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/checkmark.png"></td>
							
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<p>* Features coming soon</p>
	</div>
		<div class="section-title-spacer"></div>
			<div class="section-title-spacer"></div>
			<div class="container-fluid">
				<div class="container">
					<div class="row">
							<div class="col-lg-7" id="join_two">
								<h1>How to Join</h1>
								</br>
								</br>
								<p> Update your app to join Finigoo Points. You’ll be added at no extra cost starting at the Bronze level.</p>
								<p> Use your available points to redeem exciting deals of your choice.</p>
								<p> Congratulations.</p>
								
								
							</div>
							<div class="col-lg-5">
								<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/app-mbl.jpg">
							</div>
					</div>
				</div>
			</div>
			<div class="section-title-spacer"></div>
			<div class="section-title-spacer"></div>
			<div class="container-fluid">
				<div class="container">
					<div class="row">
							<div class="col-lg-7">
								<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/gopoint-sm-2.JPEG">
							</div>
							<div class="col-lg-5" id="join_three">
								<h1>How to Avail Benefit</h1>
								</br>
								</br>
								<ul>
										<li>Avail the specific service from our partners.</li>
										<li>Open the Finigoo App and tap on Points Widget on Home</li>
										<li>During billing or preparation of invoice, show your Finigoo Points Card to the counter or corresponding representative.</li>
										<li>Congratulations! You have availed an Exclusive benefit package from Finigoo Points </li>
								</ul>
								
								
							</div>
					</div>
				</div>
			</div>
			
			<div class="section-title-spacer"></div>
			<div class="section-title-spacer"></div>
		<!--	<div class="container-fluid">
				<div class="container" id="join_four">
					<div class="row">
							
							<div class="col-lg-12">
								<h2>Our Loyalty Partners</h2>
								</br>
								</br>
								<ul>
									<li>
										<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/flt-logo-1.png">
									</li>
									<li>
										<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/2.png">
									</li>
									<li>
										<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/4.png">
									</li>
									<li>
										<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/1.png">
									</li>
									<li>
										<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/3.jpg">
									</li>
									<li>
										<img src="<?php echo base_url();?>landing-assets/assets/images/all-img/5.png">
									</li>
								</ul>
							</div>
							
					</div>
				</div>
			</div>-->
			
			
			<!---------------------------------------------------------------------------------->
			
			
			
			<div class="section-title-spacer"></div>
			<div class="section-title-spacer"></div>
			
			
			<div class="container">
				<div class="row">
					<div class="col-lg-12" id="join_five">
						<h1>Frequently Asked Questions</h1>
						<div id="accordion">
							<div class="card wow fade-in-bottom animated" style="visibility: visible; animation-name: fade-in-bottom;">
								<div class="card-header card-primary" id="headingOne">
									<h5 class="mb-0">
										<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
											What is Finigoo Points?
										</button>
									</h5>
								</div>
						
								<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
									<div class="card-body">
										Finogoo Points allows you to earn points by taking our services and receive rewards based on your usage.
									</div>
								</div>
							</div><!-- end single accrodain -->
							<div class="card wow fade-in-bottom animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1s; animation-name: fade-in-bottom;">
								<div class="card-header card-primary" id="headingTwo">
									<h5 class="mb-0">
										<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
											will I benefit as a Finigoo Points user?
						
						
										</button>
									</h5>
								</div>
								<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion" style="">
									<div class="card-body">
										As a Finigoo Points user, you will receive rewards such as exciting Deal packages and Benefits. The more you use Finigoo, the greater the benefits you will earn.
						
						
									</div>
								</div>
							</div><!-- end single accrodain -->
							<div class="card wow fade-in-bottom animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
								<div class="card-header card-primary" id="headingThree">
									<h5 class="mb-0">
										<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
											How can I earn points?
										</button>
									</h5>
								</div>
								<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion" style="">
									<div class="card-body">
										You can earn points by taking a ride or ordering food on our platform. You will be awarded 1 point for every 10 BDT you spend on our services.
									</div>
								</div>
							</div><!-- end single accrodian -->
							<div class="card wow fade-in-bottom animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
								<div class="card-header card-primary" id="headingThree2">
									<h5 class="mb-0">
										<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree2" aria-expanded="false" aria-controls="collapseThree2"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
											You can earn points by taking a ride or ordering food on our platform. You will be awarded 1 point for every 10 BDT you spend on our services.
						
						
										</button>
									</h5>
								</div>
								<div id="collapseThree2" class="collapse" aria-labelledby="headingThree2" data-parent="#accordion">
									<div class="card-body">
										Finigoo Points has 4 levels - Bronze (0 - 199), Silver(200 - 999), Gold (1000 - 6999) and Platinum (7000+). You will be a Bronze user and have 0 points, to begin with. As soon as you earn enough points to get to the next level, your level status will change immediately.
									</div>
								</div>
							</div><!-- end single accrodian -->
						 
						
						</div>

					</div>
				</div>
			</div>