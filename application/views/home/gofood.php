<style>
    /*second nav */
    #myTab-2 li a {
        border: .8px solid #ad72056b;
        border-radius: 25px;
        color: black;
        padding: 6px 30px;
        font-size: 14px;
    }

    #myTab-2 li a.active {
        color: #fff;
        background-color: #EE2737;
        font-size: 14px;
        display: inline-block;
        padding: 6px 30px;
        border-radius: 25px;
        /*border: .8px solid black;*/
    }

    /*footer subcribe*/
    .rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
        font-size: 20px;
        font-size: 14px;
        font-weight: 600;
        position: absolute;
        top: 3px;
        right: 3px;
        height: 51px;
        padding: 0 40px;
        text-transform: uppercase;
        border-radius: 999px;
        /*position: relative;*/
        z-index: 99999;
        color: #fff;
        border: none;
        background: -webkit-linear-gradient(55deg, #EE2737 0%, #EE2737 53%, #EE2737 70%, #EE2737 100%);
        background: linear-gradient(35deg, #EE2737 0%, #EE2737 53%, #EE2737 70%, #EE2737 100%);
    }

    /*back to top button*/
    #scrollUp {
        font-size: 30px;
        line-height: 48px;
        position: fixed;
        right: 30px;
        bottom: 30px;
        width: 45px;
        height: 45px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
        background: #EE2737;
    }

    /*services hover bg color*/
    .main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
    .main-menu ul ul>li:hover>a {
        padding-left: 23px;
        color: #fff;
        background: #EE2737;
    }

    .icofont-check-circled:before {
        content: "\eed7";
        color: #ee2737;
    }

    .submenufix.show {
        background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.38)), url(<?php echo base_url(); ?>landing-assets/assets/images/extra/ridez.jpg);
        background-size: 100% 100%;
    }

    @media (max-width:1200px) {
        .submenufix.show {
            background: white;
        }
    }

    .rt-dim-light {
        background-color: #ee2737;
        border-radius: 20px;
    }

    .rt-dim-light h4 {
        color: #fff;
    }
</style>
<div class="rt-breadcump rt-breadcump-height">
    <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/gofood1.jpg)">
    </div><!-- /.rt-page-bg -->
    <div class="container">
        <div class="row rt-breadcump-height">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h3>Go Food </h3>
                    <!-- <p style="font-size: 30px;">Go Mart Delivery at Your Doorstep</p> -->

                </div><!-- /.breadcrumbs-content -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.rt-bredcump -->
<!-- <div class="container" id="tabb" >
<div class="flight-list-box rt-mb-40">
                        <ul class="nav pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="#rt-itm_1" role="tab"
                                    aria-controls="rt-itm_1" aria-selected="true">Go food</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">blog</a>
                            </li>
                            <li class="nav-item">
                               <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">GoFood Partners</a>
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                               
                            </li>
                        </ul>
                    </div>
</div> -->
<section class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title"><span>Go Food</span>
                        Instant Food Delivery Service</h2>
                    <p>A marketplace that connects food hunters with restaurants and food vendors of their choice with a robust instant delivery.Go Food is now in the FIniGoo app, creating a more seamless experience with your daily needs in the everyday everything app.</p>
                </div><!-- /.rt-section-title-wrapper -->
            </div><!-- /.col-lg-8 -->
        </div><!-- ./row -->

        <div class="spacer-bottom"></div><!-- /.spacer-bottom -->
        <section class="caltoaction-4 rt-dim-light rt-pt-100 rt-pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h4 class="f-size-40 rt-semibold rt-mb-40 f-size-md-32 f-size-xs-26"><i>Your favourite food delivered hot and fresh!!!</i></h4>
                    </div><!-- /.col-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section>
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->

        <div class="row align-items-center">
            <div class="col-lg-5 mb-0 mb-md-4">

                <div class="text-424">
                    <h2>Why order Go Food?</h2>
                    <p class="f-size-14 ">
                        Have your tea time snacks sent to you.<br>No more skipping lunch,<br>sno matter how busy.</p>
                </div><!-- /.text-424 -->

            </div><!-- /.col-lg-6 -->

            <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/Go Food-01.png" alt="work image" draggable="false" width="530">
        </div><!-- /.col-lg-6 -->
    </div><!-- /.row -->

    </div><!-- ./ copntainer -->
</section>

<!-- 
    ======== call to action start====
 -->


<div class="spacer-top"></div><!-- /.spacer-bottom -->

<section class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 mx-auto text-center">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title"> Select a dish the menu is available

                    </h2>

                </div><!-- /.rt-section-title-wrapper -->
            </div><!-- /.col-lg-8 -->
        </div>
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->

        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row align-items-center">
            <div class="col-lg-6 mb-0 mb-md-4">

                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/work-img-4.png" alt="work image" draggable="false">
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-5 offset-lg-1">

                <h4 class="f-size-24 rt-semiblod rt-mb-20">Open the app and select a dish for lunch and dinner</h4>
                <div class="text-424">
                    <ul class="rt-list">
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span> You can also choose tasty dishes, drinks and delicious desserts</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span> Enter your delivery address</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Select a delivery time that perfectly fits your schedule</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span> Pay cash to the rider or conveniently with credit card, UPI, Online Banking and more directly on the app</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Track your delivery on the app know exactly when your food will arrive</li>
                    </ul>
                </div><!-- /.text-424 -->


            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->


    </div><!-- ./ copntainer -->
</section>