<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sign up</title>
    <!-- ==================Start Css Link===================== -->
<link href="https://fonts.googleapis.com/css?family=Lato&amp;display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/animate.css">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/datepicker.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/OverlayScrollbars.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/fontawesome.css">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/icofont.css">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/magnific-popup.css">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/owl.theme.default.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/slick.css">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/slick-theme.css">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/slider-range.css">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/tippy.css">
<link rel="stylesheet" href="<?php echo base_url();?>landing-assets/assets/css/app.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- ==================End Css Link===================== -->




    <!--[if lt IE 9]>
<script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<style >
    /*second nav */
    #myTab-2 li a{
         border: .8px solid #ad72056b;
         border-radius: 25px;
         color:black;
         padding: 6px 30px;
          font-size: 14px;
    }
    #myTab-2 li a.active {
    color: #fff;
    background-color: #E92C3C;
    font-size: 14px;
    display: inline-block;
    padding: 6px 30px;
    border-radius: 25px;
    /*border: .8px solid black;*/
}
/*footer subcribe*/
.rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
    font-size: 20px;
    font-size: 14px;
    font-weight: 600;
    position: absolute;
    top: 3px;
    right: 3px;
    height: 51px;
    padding: 0 40px;
    text-transform: uppercase;
    border-radius: 999px;
    /*position: relative;*/
    z-index: 1;
    color: #fff;
    border: none;
    background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
    background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
}
/*back to top button*/
#scrollUp {
    font-size: 30px;
    line-height: 48px;
    position: fixed;
    right: 30px;
    bottom: 30px;
    width: 45px;
    height: 45px;
    text-align: center;
    color: #fff;
    border-radius: 50%;
    background: #E92C3C;
}
/*services hover bg color*/
.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
    padding-left: 23px;
    color: #fff;
    background: #E92C3C;
}
.sticky {
  position: fixed;
  top: 16%;
  width: 100%;
  z-index: 99;
  animation-name: fadeInDown;
}
#ban{
    background-repeat: no-repeat;
    position: relative;
    border-radius: 8px;
    background-image: url(<?php echo base_url();?>landing-assets/assets/images/testimonials/t_12.jpg);
    display: flex;
    background-position: center center;
	background-size:cover;
}
.carousel-caption {
    text-align: left;
    right: 7%;
    left: 10%;
    padding-bottom: 0px;
	}
.carousel-caption h4{
	font-weight: 700;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #fff;
    font-size: 1.8em;
}
.carousel-caption p{
line-height: 16px;
    font-size: 14px;
}
#ban1 label{
    display: inline-block;
    max-width: 100%;
    color: rgba(0,0,0,.65);
    font-size: 13px;
    font-weight: 100;
	text-align: start;
}
.carousel-caption {
    z-index: 99999;
	text-shadow: none;
}
#ban1 h1{
	margin-top:0px;
}
.x{
	font-family: sans-serif;
    color: #000000ba;
    font-size: 13px;
    font-weight: 600;
}
.x::before{
	border-left:3px solid red;
	content:"";
}
.x1{
	font-family: sans-serif;
    color:#9e9e9e;
	
	font-size:13px;
}

label::before{
	content:"*";
	color:red;
	align-item:center;
	padding-right:5px;
}
.input-lg{
	font-size:14px;
}
.btn-lg {
    display: block;
    background-color: #ed1d24;
    color: aliceblue;
    font-weight: 600;
}
#para p{
	
	line-height:14px;
    color: #9e9e9e;
	font-family: sans-serif;
    font-size: 14px;
}
#code {
    float: left;
    padding-right: 0px;
    width: 50px;
}
@media (max-width:1000px){
	#ban{
		display:none!important;
	}
}
</style>

<body>
<header class="rt-site-header  rt-fixed-top white-menu">

        

        <div class="main-header rt-sticky">
            <nav class="navbar">
                <div class="container">
                           <a href="<?php echo base_url();?>" class="brand-logo " style="width: 100%;"><img src="<?php echo base_url();?>landing-assets/assets/images/logo/finigoo.png" alt="" 
            ></a>
			<a href="<?php echo base_url();?>" class="sticky-logo "><img src="<?php echo base_url();?>landing-assets/assets/images/logo/finigoo.png" alt="" 
            ></a>
        </div>
      </nav>
    </div><!-- /.bootom-header -->

    </header>	

 <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
  
				<div class="container-fluid" style="padding-right:30px; padding-left: 30px; margin-top: 90px;">
						
						<div class="row">
								
								<div class="col-xl-4 col-lg-5 col-md-6 col-sm-6" id="ban">
								
								
								
          
									<div class="carousel-caption">
										<h4>WE ARE HERE TO MOVE YOUR BUSINESS FORWARD</h4>
												<hr>
										<p>A one stop platform for you to manage employees, set policies digitally and gain insights on all Finigoo Services regionally.</p>
									</div>
								</div>
								<div class="offset-xl-1 col-xl-6  offset-lg-1 col-lg-6 col-md-12 col-sm-12" id="ban1" style="padding-left:20px;">
									
									<h1><b>Create Company Account</b></h1>
									
									</br>
									<p class="x">&nbsp;Company Information</p>
<form class="form-horizontal" action="">
	<div class="form-group form-group-md">
		<label class="control-label col-lg-3" for="name">Company Name</label>
			<div class="col-lg-9">
				<input type="text" class="form-control" id="company_name" value="" placeholder="Enter Your Company's Name">
			</div>
	</div>
	<div class="form-group form-group-md">
		<label class="control-label col-lg-3" for="size">Company Size</label>
			<div class="col-lg-9">
				<select class="form-control" placeholder="Company Size">
					<option class="form-control" selected>Select Your Company Size</option>
					<option class="form-control" value=""><50</option>
					<option class="form-control" value="">51-250</option>
					<option class="form-control" value="">251-500</option>
					<option class="form-control" value="">501-1000</option>
					<option class="form-control" value="">>1001</option>
				</select>
			</div>
	</div>
	<div class="form-group form-group-md">
		<label class="control-label col-lg-3" for="country">Country</label>
			<div class="col-lg-9">
				<input type="text" class="form-control" id="Country" Value="India" placeholder="Country" disabled>
			</div>
	</div>
	<div class="form-group form-group-md">
		<label class="control-label col-lg-3" for="country">State</label>
			<div class="col-lg-9">
				<select class="form-control" placeholder="Select State">
					<option class="form-control" selected>Select Your State</option>
					<option class="form-control" value="">Tamil Nadu</option>
					<option class="form-control" value="">Kerala</option>
					<option class="form-control" value="">Karnadaka</option>
				</select>
			</div>
	</div>
	<div class="form-group form-group-md">
		<label class="control-label col-lg-3" for="country">City</label>
			<div class="col-lg-9">
				<select class="form-control" placeholder="Select City">
					<option class="form-control" selected>Select Your City</option>
					<option class="form-control" value="">Chennai</option>
					<option class="form-control" value="">Coimbatore</option>
					<option class="form-control" value="">Trichy</option>
					<option class="form-control" value="">Salem</option>
				</select>
			</div>
	</div>
			<p class="x">&nbsp;Company Admin</p>

	<div class="form-group form-group-md">
		<label class="control-label col-lg-3" for="full-name">Full Name</label>
			<div class="col-lg-9">
				<input type="text" class="form-control" value="" id="admin_name" placeholder="Enter Company Admin Full Name">
			</div>
	</div>
	<div class="form-group form-group-md">
		<label class="control-label col-lg-3" for="name">Email</label>
			<div class="col-lg-9">
				<input type="text" class="form-control" value="" id="email" placeholder="Enter Company Admin Email Address">
			</div>
	</div>
	<div class="form-group form-group-md">
		<label class="control-label col-lg-3" for="name">Phone Number</label>
			<div class="col-lg-9">
				<div>
							<div id="code">
									<input type="tel" class="form-control" placeholder="+91" style="/* width: 40px; */">
							</div>
							<div id="num">
									<input type="tel" class="form-control col-md-12" placeholder="Phone" style="">
							</div>
						</div>
			</div>
	</div>
	<hr>
	<p class="x1">Have a Referral Code? (Optional)
	<a role="button" id="hide" style="color:red;">Hide</a>
<a role="button" id="show" style="color:red;">Show</a>
</p>
	<div class="form-group form-group-md">
			<div class="col-lg-12" style="height:20px; margin-bottom:10px;">
				<input type="text" class="form-control" value="" id="ref1" placeholder="Enter Code">
			</div>
	</div>
	
  <button type="submit" id="submit" class="btn btn-default btn-lg">Sign Up</button>
</form>
</br>
</br>
								</div>
						</div>
						
				</div>
  
 
  <div class="section-title-spacer"></div><!-- /.section-title-spacer -->







<!-- 
    ====== Services Start ==============
 -->


<div class="section-title-spacer"></div>
  <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
 
<!---
    !============= Footer Area Start ===========!
 -->

 <section class="rt-site-footer" data-scrollax-parent="true">
    <div class="rt-shape-emenetns-1" style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/shape-elements/shape-4.png)" data-scrollax="properties: { translateY: '340px' }"></div><!-- /.rt-shape-emenetns-1 -->   
    <div class="footer-top rtbgprefix-cover">
          <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="rt-single-widget wow fade-in-bottom" data-wow-duration="1s">
                        <h3 class="rt-footer-title"><img src="<?php echo base_url();?>landing-assets/assets/logo/finogoo-logo.png"></h3><!-- /.rt-footer-title -->
                        <!-- <ul class="rt-usefulllinks">
                            <li>
                                <a href="#">About Us</a>
                            </li>
                            <li>
                                <a href="#">Contact Us</a>
                            </li>
                            <li>
                                <a href="#">Authenticity Guarantee</a>
                            </li>
                            <li>
                                <a href="#">Customer Reviews</a>
                            </li>
                            <li>
                                <a href="#">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="#">Business License</a>
                            </li>
                            
                        </ul> -->

                    </div><!-- /.rt-single-widge -->
                </div><!-- /.col-lg-3-->
                <div class="col-lg-3 col-md-6">
                    <div class="rt-single-widget wow fade-in-bottom" data-wow-duration="1.5s">
                        <!-- <h3 class="rt-footer-title">Work With Us</h3> -->
                        <ul class="rt-usefulllinks">
                            <li><a href="<?php echo base_url();?>aboutus" style="list-style-type: none;">About Us</a></li>
                            <li><a href="<?php echo base_url();?>careers" style="list-style-type: none;">Careers</a></li>
                            <li><a href="<?php echo base_url();?>goads" style="list-style-type: none;">Go Ads</a></li>
                            <li><a href="<?php echo base_url();?>press-meet" style="list-style-type: none;">Press Release</a></li> 
                            <!-- <li><a href="#">Advertise with us</a></li>
                            <li><a href="#">Retirement Plan</a></li>
                            <li><a href="#">Travel APIs</a></li> -->
                        </ul>
                    </div><!-- /.rt-single-widget -->
                </div><!-- /.col-lg-3-->
                <div class="col-lg-3 col-md-6">
                    <div class="rt-single-widget wow fade-in-bottom" data-wow-duration="2s">
                        <!-- <h3 class="rt-footer-title">
                            My Account
                        </h3> -->
                        <ul class="rt-usefulllinks">
                        
                            <!-- <li><a href="#">Help Centre</a></li> -->
                            <li><a href="<?php echo base_url();?>contact-us">Contact Us</a></li>
                            <!-- <li><a href="#">Driver Help Centre</a></li> -->
                            <!-- <li><a href="#">Terms & Conditions</a></li> -->
                            <li><a href="<?php echo base_url();?>privacy-policy">Privacy Policy</a></li>
                            <li><a href="<?php echo base_url();?>safety">Safety</a></li>
                        </ul><!-- /.rt-usefulllinks -->
                    </div><!-- end single widget -->
                </div><!-- /.col-lg-3-->
                <div class="col-lg-3 col-md-6" id="social_icon">
                    <div class="rt-single-widget wow fade-in-bottom" data-wow-duration="2.5s">
                       
                <h3 class="rt-footer-title" id="follow_us">Follow Us On</h3>
                                 <a href="#"><img src="<?php echo base_url();?>landing-assets/assets/logo/fb.jpg"/></a>
                                 <a href="#"><img src="<?php echo base_url();?>landing-assets/assets/logo/twitter.jpg" src="#"/></a>
                                 <a href="#"><img src="<?php echo base_url();?>landing-assets/assets/logo/youtube.jpg" src="#"/></a>
								 
								 </div>
</div></div>
<hr>
<div class="col-sm-12" id="fin_info"><p>© 2020 Finigoo</p></div>
</div> 



<div class="container-fluid " id="ftr_1" >
	
		<div class="col-md-12" id="ftr_2">
				<img src="<?php echo base_url();?>landing-assets/assets/images/flights/brush_banner.png">
		</div>
	
</div> 
</section>
  
  <!--------------------ROLE 2------------------>

<!-- ==================Start Js Link===================== -->
<script src="<?php echo base_url();?>landing-assets/assets/js/jquery-2.2.4.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/moment.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/imagesloaded.pkgd.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/instafeed.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/waypoints.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/jquery.scrollUp.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/TweenMax.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/scrollax.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3&amp;key=AIzaSyCy7becgYuLwns3uumNm6WdBYkBpLfy44k"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/wow.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/jquery.overlayScrollbars.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/jquery.appear.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/select2.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/slick.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/slider-range.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/vivus.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/tippy.all.min.js"></script>
<script src="<?php echo base_url();?>landing-assets/assets/js/app.js"></script>
<!-- ==================End Js Link===================== -->
 <script>
$(document).ready(function(){
	$("#hide").hide();
	$("#ref1").hide();
  $("#hide").click(function(){
    $("#ref1").hide();
	$("#hide").hide();
	$("#show").show();
  });
  $("#show").click(function(){
    $("#ref1").show();
	$("#show").hide();
	$("#hide").show();
	$("ref1").show();
  });
});
</script>

<script src="<?php echo base_url();?>landing-assets/assets/js/jquery.flagstrap.js"></script>
<script>
       $('#options').flagStrap({
        countries: {
            "IN": "India",
            "TH": "Thailand",
            "SG": "Singapore",
            "ID": "Indonesia",
        },
        buttonSize: "btn-sm",
        buttonType: "btn-info",
        labelMargin: "10px",
        scrollable: false,
        scrollableHeight: "350px"
    });
</script>
        <style>
		.closedicon{
		display: block;
    position: absolute;
    right: 25px;
    color: red;
    font-weight: bold;
    font-size: 20px;
    border: 1px solid red;
    width: 28px;
    height: 28px; 
	top:30px;
	z-index: 999999999;
    text-align: center;
	}
            .show {
                display: block !important;
            }
            .hide{
			    display: none !important; 
			}
            .submenufix {
                position: fixed;
                top: 90px;
                background: #ffffff;
                width: 100%;
                overflow-y: auto;
	            z-index: 9999;
                display: none;
                height: calc(100% - 200px);
            }
            
            .submenufix .sub-menu li {
                list-style: none;
                color: black;
                line-height: 2.5;
            }
            
            .submenufix .sub-menu li a {
                color: black;
            }
            
            .title_submenu {
                font-size: 20px;
                color: #e82801 !important;
             
            }
        </style>

</body>

</html>