<style >
    /*second nav */
  #myTab-2 li a{
         border: .8px solid #ad72056b;
         border-radius: 25px;
         color:black;
         padding: 6px 30px;
          font-size: 14px;
    }
    #myTab-2 li a.active {
    color: #fff;
    background-color: #01AEF3;
    font-size: 14px;
    display: inline-block;
    padding: 6px 30px;
    border-radius: 25px;
    /*border: .8px solid black;*/
}
/*footer subcribe*/
.rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
    font-size: 20px;
    font-size: 14px;
    font-weight: 600;
    position: absolute;
    top: 3px;
    right: 3px;
    height: 51px;
    padding: 0 40px;
    text-transform: uppercase;
    border-radius: 999px;
    /*position: relative;*/
    z-index: 99999;
    color: #fff;
    border: none;
    background: -webkit-linear-gradient(55deg, #01AEF3 0%, #01AEF3 53%, #01AEF3 70%, #01AEF3 100%);
    background: linear-gradient(35deg, #01AEF3 0%, #01AEF3 53%, #01AEF3 70%, #01AEF3 100%);
}
/*back to top button*/
#scrollUp {
    font-size: 30px;
    line-height: 48px;
    position: fixed;
    right: 30px;
    bottom: 30px;
    width: 45px;
    height: 45px;
    text-align: center;
    color: #fff;
    border-radius: 50%;
    background: #01AEF3;
}
/*services hover bg color*/
.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
    padding-left: 23px;
    color: #fff;
    background: #01AEF3;
}
.submenufix.show{
	background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.38)), url(<?php echo base_url();?>landing-assets/assets/images/extra/cash.jpg);
    background-size: cover;
}
@media (max-width:1200px){
.submenufix.show{
		background:white;
	}
}
</style>
<!-- 
    ====== Services Start ==============
 -->
<div class="rt-breadcump rt-breadcump-height">
    <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/gomoney.jpg)">
    </div><!-- /.rt-page-bg -->
    <div class="container">
        <div class="row rt-breadcump-height">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h3>  Go Point</h3>
                <p style="font-size: 30px;">Earn Points for Every Order</p>
                </div><!-- /.breadcrumbs-content -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.rt-bredcump -->
<!-- <div class="container" id="tabb" >
<div class="flight-list-box rt-mb-40">
                        <ul class="nav pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="#rt-itm_1" role="tab"
                                    aria-controls="rt-itm_1" aria-selected="true">Go cashback</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">blog</a>
                            </li>
                            <li class="nav-item">
                               
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                               
                            </li>
                        </ul>
                    </div>
</div> -->

<!-- 
    ====== Services Start ==============
 -->

<section class="svcPage-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 text-center mx-auto">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title">
                        <span style="color: #01AEF3;">Go Point</span>
                       India’s Most Generous Point.
                    </h2><!-- /.rt-section-title -->
                    <p><b>Join our millions of members worldwide and earn money back when you shop or pay online.</b></p>
                </div><!-- /.rt-section-title-wrapper- -->
            </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->
        <section class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
               
            </div><!-- /.col-lg-8 -->
        </div><!-- ./row -->
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row align-items-center">
            <div class="col-lg-6 mb-0 mb-md-4">
              
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/work-img-1.png" alt="work image" draggable="false">
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-5 offset-lg-1 mb-0 mb-md-4">
                
                <h4 class="f-size-24 rt-semiblod rt-mb-20">What is GO Point ?</h4>
                <div class="text-424">
                    <p class="f-size-14 ">Go Point can be used for recharges, bill payments and payments on FiniGoo partner platforms/stores. The Point amount credited to FiniGoo wallet.</p>
                </div><!-- /.text-424 -->
               
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->


    </div><!-- ./ copntainer -->
</section>
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="0.5s">
                    <div class="icon-thumb">
                        <img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/12.png" style="width: 120px;" alt="box-icon" draggable="false">
                    </div><!-- /.icon-thumb -->
                    <div class="iconbox-content">
                        <h5>When will I get my cash back?</h5>
                        <p>It can take up to 10 working days for your Point to show in your account as ‘pending’. We then need to wait until the retailer confirms your purchase and for them to pay us. This can take on average between 15 working days. Once we have your Point, it'll become ‘payable’ in your account.</p>
                        
                    </div><!-- /.iconbox-content -->
                </div><!-- /.rt-single-icon-box -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4 col-md-6">
                <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="1s">
                    <div class="icon-thumb">
                        <img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/12.png" style="width: 120px;" alt="box-icon" draggable="false">
                    </div><!-- /.icon-thumb -->
                    <div class="iconbox-content">
                        <h5>How do I withdraw my cash back?</h5>
                        <p>When your Point shows as 'payable' in your account, you can withdraw and choose from a wide range of payout methods such as bank transfer, FiniGoo Wallet.
                            <br>
                            <br>
                            <br>
                            <br></p>
                        
                    </div><!-- /.iconbox-content -->
                </div><!-- /.rt-single-icon-box -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4 col-md-6">
                <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="1.5s">
                    <div class="icon-thumb">
                        <img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/12.png" style="width: 120px;" alt="box-icon" draggable="false">
                    </div><!-- /.icon-thumb -->
                    <div class="iconbox-content">
                        <h5>How to save more on Go Point?</h5>
                        <p>Go Point is a one stop solution for all your extra saving woes. Here are some of the best tried and tested hacks that can help you save even more on your online shopping via FiniGoo Services.
                            <br>
                            <br>
                            <br>
                        </p>
                        
                    </div><!-- /.iconbox-content -->
                </div><!-- /.rt-single-icon-box -->
            </div><!-- /.col-lg-4 -->
            
           
           
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>

<!-- 
    ====== Works Start ==============
 -->

<!-- 
    ======About Area Start ==============
 -->
<div class="spacer-top"></div><!-- /.spacer-top -->
<section class="about-area">
     <div class="container">
    <div class="rt-design-elmnts rtbgprefix-contain"  style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/all-img/abt_vec_2.png)">

    </div><!-- /.rt-design-elmnts -->
   
        <div class="row">
            <div class="col-xl-6 offset-xl-6">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title" style="padding:14px;">
                        <span>FiniGoo (Go Point)Offers and Coupons</span>
                        
                    </h2><!-- /.rt-section-title -->
                    <p style="padding:14px; text-align:justify;">
                       Go back offers you a catalogue of retailer discounts and coupon codes that are clubbed with exclusive Go Point. Our Go Point ninjas update these coupons in real-time, offering the best Point on each deal that users wish to avail.
                    </p>
                </div><!-- /.rt-section-title-wrapper -->
                <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
               
            </div><!-- /.col-lg-9 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>