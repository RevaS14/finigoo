<style >
    /*second nav */
    #myTab-2 li a{
         border: .8px solid #ad72056b;
         border-radius: 25px;
         color:black;
         padding: 6px 30px;
          font-size: 14px;
    }
    #myTab-2 li a.active {
    color: #fff;
    background-color: #E92C3C;
    font-size: 14px;
    display: inline-block;
    padding: 6px 30px;
    border-radius: 25px;
    /*border: .8px solid black;*/
}
/*footer subcribe*/
.rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
    font-size: 20px;
    font-size: 14px;
    font-weight: 600;
    position: absolute;
    top: 3px;
    right: 3px;
    height: 51px;
    padding: 0 40px;
    text-transform: uppercase;
    border-radius: 999px;
    /*position: relative;*/
    z-index: 1;
    color: #fff;
    border: none;
    background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
    background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
}
/*back to top button*/
#scrollUp {
    font-size: 30px;
    line-height: 48px;
    position: fixed;
    right: 30px;
    bottom: 30px;
    width: 45px;
    height: 45px;
    text-align: center;
    color: #fff;
    border-radius: 50%;
    background: #00AFF0;
}
/*services hover bg color*/
.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
    padding-left: 23px;
    color: #fff;
    background: #E92C3C;
}
.sticky {
  position: fixed;
  top: 16%;
  width: 100%;
  z-index: 99;
  animation-name: fadeInDown;
}
.rt{
	background: linear-gradient(0deg, rgba(0, 0, 0, 0.20), rgba(0, 0, 0, 0.20)),url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/home-finigoo1.jpg);
    background-repeat: no-repeat;
    background-size: 100% 100%;
    height: 400px;
    position: relative;
    top: 93px;
    margin-bottom: 40px;
    background-attachment: fixed;
}
#rt_text{
	position:relative;
	top:22%;
	color:white;
	font-weight:600;
}
#rt_text h2{
	border-bottom: 1px dotted;
    height: 50%;
}

#rt_text span{

	line-height: 25px;
    text-align: initial;
    font-weight: bold;
    font-size: 22px;
	border-bottom: 2px solid #72c02c;
}
#rt_text button{
	color: black;
    background-color: #fee685;
    border-color: transparent;
	padding: 10px 23px;
    font-size: 16px;
	font-weight:bold;
}
.rt:after{
	content:"";
	display:table;
	clear:both;
}
#two p{
	font-size: 16px;
    line-height: 30px;
}
#three p{
	font-size: 16px;
    line-height: 30px;
}
.sm_bnr{
	width: 50%;
    margin: auto;
	margin-bottom: 50px;
}
.sm_bnr img{
	width: 70%;
    height: auto;
}
#box:hover{
	box-shadow: 0px 0px 5px 1px lightgrey;
}
#box{
	padding:30px;
}
</style>


<div class="rt-breadcump rt-breadcump-height">
        <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/goloan-banner.jpg)">
        </div><!-- /.rt-page-bg -->
        <div class="container">
            <div class="row rt-breadcump-height">
                <div class="col-12">
                    <div class="breadcrumbs-content">
                        <h3>Go Loan</h3>
                        <p style="font-size: 30px;"></p>
                      
                        <!-- /.breadcrumbs -->
                    </div><!-- /.breadcrumbs-content -->
                </div><!-- /.col-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>


<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>

<div class="container" id="two">
	<div class="row">
		<div class="col-lg-12 text-center">
			<p>Whether driving with <b>Finigoo serves</b> as your main or side income, life’s always easier with better cash flow. Our Upfront Cash Program is offered to select group of <b>Finigoo</b> Driver Partners who can choose to request for a loan. This program aims to assist our driver partners in situations like upgrading their motorbike, repairing or maintaining their motorbike or smartphone, family emergencies, paying for their children’s education or to augment their learning skills.</p>
		</div>
	</div>
</div>
<div class="section-title-spacer"></div>

<div class="container" id="three">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="text-center"><b>EASY STEPS FOR SELLERS</b></h2>
			</br>
            </br>
            <div class="row">
			<div class="col-lg-6 text-center" id="box">
					<div class="sm_bnr">
						<img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/easysteps.png" style="width: 120px;">
					</div>
				 <h4><b>Easy application</b></h4>
					<p>Qualified driver partners will become eligible after an ascertained time on the platform. All Documentation will be done digitally.</br>
					Upon successful application, <b>Finigoo</b> will disburse the amount into your Wallet that can be withdrawn via regular banking channels.</p>
			</div>
			<div class="col-lg-6 text-center" id="box">
					<div class="sm_bnr">
						<img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/easyreturn.png" style="width: 120px;">
					</div>
				 <h4><b>Flexible options to return</b></h4>
					<p>Depending on which arrangement is more convenient for applicants, you will have upto 24 months to return the loan.
					</br>
					If you are unable to drive for <b>Finigoo</b> (for whatever reason), alternative options to return the loan will also be available.</p>
			</div>
        </div>
		</div>
	</div>
</div>
<div class="section-title-spacer"></div>

<div class="container" id="three">
	<div class="row">
		<div class="col-lg-12">
            <div class="row">
			<div class="col-lg-6 text-center" id="box">
					<div class="sm_bnr">
						<img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/secure.png" style="width: 120px;">
					</div>
				 <h4><b>Hassle-free, auto deductions</b></h4>
					<p>No need to worry about payment due dates anymore.
						</br>
						Automated deductions from your Cash Wallet will be made over a period of time until your Upfront Cash balance is settled.</p>
			</div>
			<div class="col-lg-6 text-center" id="box">
					<div class="sm_bnr">
						<img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/fee.png" style="width: 120px;">
					</div>
				 <h4><b>One-time admin fee</b></h4>
					<p>Just a one-time admin fee. No other fees or charges are associated with this program.
						</br>
						However, we will inform you directly in case there are any changes to our fee structure.</p>
			</div>
        </div>
		</div>
	</div>
</div>
<div class="container" id="four">
	<div class="row">
		<div class="col-lg-12">
            <h2 class="text-center"><b>FAQs</b></h2></br></br>
            

            <div id="accordion">
                <div class="card wow fade-in-bottom animated animated" style="visibility: visible; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                How will the loan be disbursed?
                            </button>
                        </h5>
                    </div>
            
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                        <div class="card-body">
                            We will disburse the loan into your Finigoo Wallet that you can withdraw into any bank or branchless banking account to advance the cash

                        </div>
                    </div>
                </div><!-- end single accrodain -->
                <div class="card wow fade-in-bottom animated animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1s; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                Why are other Driver-Partners offered a higher or lower Upfront Cash amount?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion" style="">
                        <div class="card-body">
                            The Upfront Cash amount offered to each Driver-Partner under the program is determined based on multiple factors including the partner’s past incentive earnings and driving or delivery history.
                        </div>
                    </div>
                </div><!-- end single accrodain -->
                <div class="card wow fade-in-bottom animated animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                My Upfront Cash application was rejected. Can I apply again if my application for the Upfront Cash Program was unsuccessful?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion" style="">
                        <div class="card-body">
                            Yes, you may apply again in the future if you qualify for the program.

                        </div>
                    </div>
                </div><!-- end single accrodian -->
                <div class="card wow fade-in-bottom animated animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingThree2">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree2" aria-expanded="false" aria-controls="collapseThree2"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                I changed my mind. How can I cancel my Upfront Cash application?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree2" class="collapse" aria-labelledby="headingThree2" data-parent="#accordion" style="">
                        <div class="card-body">
                            Finigoo-Partners who wish to cancel their Upfront Cash Program and return their loan for any reasons are encouraged to call customer service hotline at +91 0422 765895432 or write in via the Help Center, as we have a dedicated team who can help them on a case-by-case basis.
                        </div>
                    </div>
                </div><!-- end single accrodian -->

                <div class="card wow fade-in-bottom animated animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingThree2">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree3" aria-expanded="false" aria-controls="collapseThree2"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                Can Driver-Partners who were not invited request or appeal to join?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree3" class="collapse" aria-labelledby="headingThree2" data-parent="#accordion" style="">
                        <div class="card-body">
                            No. At the moment, we are only offering the Upfront Cash Program to a small group of driver-partners and/or delivery partners who were selected for their historical incentive and other earnings and overall driving pattern.
                        </div>
                    </div>
                </div><!-- end single accrodian -->

                <div class="card wow fade-in-bottom animated animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingThree2">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree4" aria-expanded="false" aria-controls="collapseThree2"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                When and how will the Upfront Cash be transferred to me?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree4" class="collapse" aria-labelledby="headingThree2" data-parent="#accordion" style="">
                        <div class="card-body">
                            The Upfront Cash amount will be topped up into your Cash Wallet shortly after applying to the program successfully. You can then transfer this amount to your bank or branchless banking account.
                        </div>
                    </div>
                </div><!-- end single accrodian -->

                <div class="card wow fade-in-bottom animated animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingThree2">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree5" aria-expanded="false" aria-controls="collapseThree2"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                What if I cannot pay back my loan on time?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree5" class="collapse" aria-labelledby="headingThree2" data-parent="#accordion" style="">
                        <div class="card-body">
                            Finigoo will send you a notice to break your payments up into smaller denominations so that paying these off is easier for you. However, if you fail to pay the loan back, we will ban your membership to the Finigoo platform and engage a recovery team to collect payment from you. If you fail to still pay, a collections team may be sent to your house and to your references to ensure the loan amount is recovered.
                        </div>
                    </div>
                </div><!-- end single accrodian -->
             
            
            </div>

			
		</div>
	</div>
</div>