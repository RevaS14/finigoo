<style>
    /*second nav */
    #myTab-2 li a {
        border: .8px solid #ad72056b;
        border-radius: 25px;
        color: black;
        padding: 6px 30px;
        font-size: 14px;
    }

    #myTab-2 li a.active {
        color: #fff;
        background-color: #E92C3C;
        font-size: 14px;
        display: inline-block;
        padding: 6px 30px;
        border-radius: 25px;
        /*border: .8px solid black;*/
    }

    /*footer subcribe*/
    .rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
        font-size: 20px;
        font-size: 14px;
        font-weight: 600;
        position: absolute;
        top: 3px;
        right: 3px;
        height: 51px;
        padding: 0 40px;
        text-transform: uppercase;
        border-radius: 999px;
        /*position: relative;*/
        z-index: 1;
        color: #fff;
        border: none;
        background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
        background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
    }

    /*back to top button*/
    #scrollUp {
        font-size: 30px;
        line-height: 48px;
        position: fixed;
        right: 30px;
        bottom: 30px;
        width: 45px;
        height: 45px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
        background: #00AFF0;
    }

    /*services hover bg color*/
    .main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
    .main-menu ul ul>li:hover>a {
        padding-left: 23px;
        color: #fff;
        background: #E92C3C;
    }

    .sticky {
        position: fixed;
        top: 16%;
        width: 100%;
        z-index: 99;
        animation-name: fadeInDown;
    }

    .rt_kiosk {
        background: linear-gradient(0deg, rgba(0, 0, 0, 0.20), rgba(0, 0, 0, 0.20)), url(<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/gokiosk.jpg);
        background-repeat: no-repeat;
        background-size: 100% 100%;
        height: 100%;
        position: relative;
        top: 93px;
        margin-bottom: 95px;
    }

    #rt_kiosk_text {
        position: relative;
        top: 30%;
        color: white;
        font-weight: 600;
    }

    #rt_kiosk_text h3 {
        line-height: 33px;
        text-align: initial;
        font-weight: bold;
        font-size: 22px;
    }

    #rt_kiosk_text button {
        color: #fff;
        background-color: #ed1d24;
        border-color: transparent;
        padding: 10px 23px;
        font-size: 16px;
        font-weight: bold;
    }

    .rt_kiosk:after {
        content: "";
        display: table;
        clear: both;
    }

    #billboards_kiosk img {
        width: 80%;
        height: 190px;
        display: flex;
        margin: auto;
        border-radius: 50%;
    }

    #billboards_kiosk {
        text-align: center;
    }

    .container h3,
    h4 {
        font-weight: bold;
    }

    #txt {
        background-color: #00AFF0;
        padding-top: 30px;
        padding-bottom: 45px;
        color: white;
    }

    #join_five .panel-primary>.panel-heading {
        color: black;
        background-color: transparent;
        border-color: transparent;
    }

    #join_five p {
        font-size: 22px;
        line-height: 35px;
    }

    #join_five a {
        font-size: 22px;
        line-height: 35px;
    }

    #expand .show {
        overflow: hidden;
    }
</style>
<!-- 
    ====== Services Start ==============
 -->

<div class="rt-breadcump rt-breadcump-height">
    <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/gokiosk.jpg)">
    </div><!-- /.rt-page-bg -->
    <div class="container">
        <div class="row rt-breadcump-height">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h3>Go Kiosk</h3>
                    <p style="font-size: 30px;">All-round Digital Shop</p>

                    <!-- /.breadcrumbs -->
                </div><!-- /.breadcrumbs-content -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>



<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2><b>Whatever your need is, get it sorted at Warung FinigooKios</b></h2>
            <p>Gone are the days where traditional warung are less capable than modern minimarkets.</br>
                At Warung FinigooKios, there are various products and services that you can access at any time.</br>
                Be it buying goods or transferring money, you can do all of them at your nearest Warung FinigooKios.</p>
        </div>
    </div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container" id="billboards_kiosk">
    <h3>What can you get at Warung FinigooKios?</h3>
    </br>
    </br>
    <div class="row">

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">

            <div class="img_cls">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/BillingPayment.png">
                <h4>Mobile Top Up & Billing Payment</h4>
                <p>Can buy phone credit & make payments. Too busy to go to a payment counter? You can pay it at the nearest FinigooKios warung.</p>
            </div>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">

            <div class="img-cls">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/MoneyTransfer.png">
                <h4>Money Transfer</h4>
                <p>Can send money to any bank.No more queuing at the cashpoint, you can now transfer or deposit money from warung.</p>
            </div>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">

            <div class="img-cls">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/DriverRegistration.png">
                <h4>Finigoo Driver Registration</h4>
                <p>Can register to become Finigoo Driver. Need extra income? Register at Warung FinigooKios to drive with Finigoo.</p>
            </div>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">

            <div class="img-cls">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/Groceries.png">
                <h4>Groceries</h4>
                <p>Can buy groceries. Not only phone data plan, you can also get your everyday grocery needs from Warung FinigooKios.</p>
            </div>
        </div>
    </div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container" id="billboards_kiosk">
    <div class="row justify-content-md-center">

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">

            <div class="img_cls">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/CellphoneProtection.png">
                <h4>Cellphone Protection</h4>
                <p>Get cellphone protection. Every time you buy mobile top-ups at Warung FinigooKios, you can get protection for your mobile phone.</p>
            </div>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">

            <div class="img-cls">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/PackageDelivery.png">
                <h4>Package Delivery*</h4>
                <p>Can deliver packages. Cut the hassle and queue at the delivery courier. Simply take your package to nearest Warung FinigooKios, fill in the shipping data and select the shipping courier.</p>
            </div>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">

            <div class="img-cls">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/GoldSaving.png">
                <h4>Gold Saving*</h4>
                <p>Can do gold-saving. Now you can save gold safely, reliably and sharia-compliant from only 10.000 Rupiah at Warung FinigooKios.</p>
            </div>
        </div>
    </div>
</div>

<div class="section-title-spacer"></div>

<div class="container">
    <p class="text-center">*Only available in selected cities</p>
</div>

<div class="section-title-spacer"></div>

<div class="container-fluid text-center" id="txt">
    <div class="row">
        <div class="col-lg-12">
            <h2><b>Become a Warung Warrior!</b></h2>
            <p>Haven’t spotted any Warung FinigooKios around you? Help warungs around your area to go digital by signing them up with FinigooKios (it’s easy!).</br> Not only benefiting them, you will also get OVO Cash reward for each warung registered. Check the full info here Warung Warrior</p>
        </div>
    </div>
</div>

<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>

<div class="container">
    <h2 class="text-center"><b>Find Warung FinigooKios easily!</b></h2>
    </br>
    </br>
    <div class="row">

        <div class="col-lg-8" id="bnr1">
            </br>
            <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/GoKiosk-sm.png" class="img-responsive" alt="">
        </div>
        <div class="col-lg-4 align-items-center" id="txt1" style="padding-top:100px;padding-left:30px;">
            <h3>Look for one or more of these signs.</h3>
            <ol>
                <li><span style="font-weight: 400">Signboard</span></li>
                <li><span style="font-weight: 400">Store Flag</span></li>
                <li><span style="font-weight: 400">Info Board</span></li>
                <li><span style="font-weight: 400">Shopblind</span></li>
                <li style="list-style-type: none">
                    <ol>
                        <li style="list-style-type: none">&nbsp;</li>
                    </ol>
                </li>
            </ol>
        </div>
    </div>
</div><!-- /.row -->

<!--------------------------------------------------------------------------------->




<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>


<div class="container">
    <div class="row">
        <div class="col-lg-12" id="join_five">
            <h1>Frequently Asked Questions</h1>


            <div id="accordion">
                <div class="card wow fade-in-bottom animated" style="visibility: visible; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                will I benefit as a Finigoo Points user?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                        <div class="card-body">
                            As a Finigoo Points user, you will receive rewards such as exciting Deal packages and Benefits. The more you use Finigoo, the greater the benefits you will earn.
                        </div>
                    </div>
                </div><!-- end single accrodain -->
                <div class="card wow fade-in-bottom animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1s; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                How can I earn points?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion" style="">
                        <div class="card-body">
                            You can earn points by taking a ride or ordering food on our platform. You will be awarded 1 point for every 10 BDT you spend on our services.
                        </div>
                    </div>
                </div><!-- end single accrodain -->
                <div class="card wow fade-in-bottom animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                How many points do I need to get to the next level?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion" style="">
                        <div class="card-body">
                            Finigoo Points has 4 levels - Bronze (0 - 199), Silver(200 - 999), Gold (1000 - 6999) and Platinum (7000+). You will be a Bronze user and have 0 points, to begin with. As soon as you earn enough points to get to the next level, your level status will change immediately.
                        </div>
                    </div>
                </div><!-- end single accrodian -->
                <div class="card wow fade-in-bottom animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingThree2">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree2" aria-expanded="false" aria-controls="collapseThree2"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                What is Finigoo Points?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree2" class="collapse" aria-labelledby="headingThree2" data-parent="#accordion">
                        <div class="card-body">
                            Finogoo Points allows you to earn points by taking our services and receive rewards based on your usage.
                        </div>
                    </div>
                </div><!-- end single accrodian -->


            </div>

        </div>
    </div>
</div>