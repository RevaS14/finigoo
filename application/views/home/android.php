<style>
	/*second nav */
	    #myTab-2 li a{
	         border: .8px solid #ad72056b;
	         border-radius: 25px;
	         color:black;
	         padding: 6px 30px;
	          font-size: 14px;
	    }
	    #myTab-2 li a.active {
	    color: #fff;
	    background-color: #E92C3C;
	    font-size: 14px;
	    display: inline-block;
	    padding: 6px 30px;
	    border-radius: 25px;
	    /*border: .8px solid black;*/
	}
	/*footer subcribe*/
	.rt-site-footer .footer-top .footer-subscripbe-box .btn {
	        font-family: 'Poppins', sans-serifs;
	    font-size: 20px;
	    font-size: 14px;
	    font-weight: 600;
	    position: absolute;
	    top: 3px;
	    right: 3px;
	    height: 51px;
	    padding: 0 40px;
	    text-transform: uppercase;
	    border-radius: 999px;
	    /*position: relative;*/
	    z-index: 1;
	    color: #fff;
	    border: none;
	    background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
	    background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
	}
	/*back to top button*/
	#scrollUp {
	    font-size: 30px;
	    line-height: 48px;
	    position: fixed;
	    right: 30px;
	    bottom: 30px;
	    width: 45px;
	    height: 45px;
	    text-align: center;
	    color: #fff;
	    border-radius: 50%;
	    background: #E92C3C;
	}
	/*services hover bg color*/
	.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
	    padding-left: 23px;
	    color: #fff;
	    background: #E92C3C;
	}
	.sticky {
	  position: fixed;
	  top: 16%;
	  width: 100%;
	  z-index: 99;
	  animation-name: fadeInDown;
	}
	.rt{
		   background: linear-gradient(0deg, rgba(0, 0, 0, 0.20), rgba(0, 0, 0, 0.20)),url(<?php echo base_url()?>landing-assets/assets/images/backgrounds/home-finigoo1.jpg);
	    background-repeat: no-repeat;
	    background-size: 100% 100%;
	    height: 330px;
	    position: relative;
	    top: 93px;
	    margin-bottom: 95px;
	    background-attachment: fixed;
	}
	#rt_text{
		position: relative;
	    top: 45%;
	    color: white;
	    font-weight: 600;
	    width: 50%;
	    margin: auto;
	}
	#rt_text h3{
	line-height: 33px;
	    text-align: center;
	    font-weight: 900;
	    font-size: 30px;
	    font-family: sans-serif;
	    color: #fff;
	}
	.rt:after{
		content:"";
		display:table;
		clear:both;
	}
	.head{
		text-align:center;
	}
	.head h2{
		color:red!important;
		margin-bottom: 0px;
	}
	.head a{
		color:red!important;
	}
	.head p{
		margin-bottom: 0px;
	}
	.role ul li{
		font-size: 16px;
	    margin-bottom: 1.1em;
	}
	.role h3{
		font-weight:bold;
	}
	.role ul{
		padding-left: 20px;
	}
	.row{
		font-weight: 300;
	    letter-spacing: 0.02em;
	    color: #333333;
		text-align:justify;
	}
	.required{
		color:red;
	}
	#form .form-control{
	    font-size: 16px;
	}
	#form h2{
		text-align:center;
		color:red;
	}
	#resume1 input[type="file"]{
		opacity: 0;
	  right: 0;
	  top: 0;
	  position:absolute;
	}
	#linked p{
		font-size:14px;
		line-height:12px;
	}
	#linked button{
	    padding: 8px 15px;
	    font-size: 14px;
	}
	#linked{
		margin-bottom: 30px;
	    padding-left: 0px;	
	}
	form .btn-block{
		width: 50%;
	    margin: auto;
		color: #fff;
	    background-color: #ed1d24;
	    border-color: #ed1d24;
	    padding: 12px 12px;
	    width: 50%;
	    font-size: 16px;
	}
	.apply_job{
	    margin: 0 auto;
	    width: 70%;
	    box-shadow: 1px 1px 8px #63636326;
	    padding: 30px 10px;
	}
	@media(max-width:768px){
	    .apply_job{
	    margin: 0 auto;
	    width: 100%;
	    box-shadow: 1px 1px 8px #63636326;
	    padding: 30px 10px;
	}
	}
</style>
<!-- 
    ====== Services Start ==============
 -->
<div class="rt">
	<div id="rt_text">
		<h3>Current Job Openings at Finigoo</h3>
	</div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container">
	<div class="row justify-content-md-center">
		<div class="col-lg-8">
			<div class="head">
				<h2><b>Android Developer</b></h2>
				<p>At Finigoo<span><a href="<?php echo base_url();?>jobs">(View All Jobs)</a></span>
				</p>
				<p>Mumbai, Bengalurus</p>
			</div>
			<div class="section-title-spacer"></div>
			<div class="role">
				<p style="line-height:2em;">We, at FiniGoo, are looking to hire an Android Developer, who can own Android development throughout the planning, design and development life cycle and help us craft unique and seamless user experience. The pace of our growth is incredible – if you want to tackle hard and interesting problems at scale, and create an impact within an entrepreneurial environment, join us!</p>
				</br>
				<h3>About the Role / Responsibilities:</h3>
				</br>
				<ul>
					<li>Design, build and maintain high performance, reusable, and reliable Java & Kotlin code</li>
					<li>Writing unit-tests code for robustness, including edge cases, usability, and general reliability</li>
					<li>Take ownership of product/features: End to end development with testing and meet the deadline</li>
					<li>Work closely with our product and design teams to customize the FiniGoo experience for the Android platform.</li>
					<li>Prototype new and redesign features.</li>
					<li>A focus on UI design principles and making apps work intuitively.</li>
					<li>Contribute best-in-class programming skills to develop highly innovative, consumer-facing mobile products.</li>
					<li>Writing efficient android code in Java/Kotlin following MVP framework guidelines.</li>
					<li>Tracing and profiling android java code. Own and manage the finance technology control frameworks</li>
				</ul>
				</br>
				<h3>Desired Experience / Background</h3>
				</br>
				<ul>
					<li>2+ years of strong experience in building quality Android apps and strong application architectural experience.</li>
					<li>Good understanding of areas such as Algorithms, Data Structures, Object Oriented Design, Databases.</li>
					<li>Solid understanding of Android Architecture Components</li>
					<li>Understanding of large and complex code bases, including API design techniques to help keep them clean and maintainable.</li>
					<li>Programming experience in Java, kotlin and related frameworks</li>
				</ul>
			</div>
			</br>
		</div>
	</div>
</div>
<div class="section-title-spacer"></div>
<div class="container">
	<div class="row justify-content-md-center">
		<div class="col-12" id="form">
			<div class="apply_job">
				<h2><b>Apply for this Job</b></h2>
				</br>
				<div class="col-12">
					<div class="" id="linked">
						<p>We’ll share your profile. The job poster may use it for jobs with other companies. Learn More</p>
						<button type="submit" class="btn btn-primary">Apply With LinkedIn</button>
						</br>
					</div>
				</div>
				<form class="form-horizontal" action="">
					<div class="form-group form-group-md">
						<div class="col-12">
							<label for="name">First Name</label><span class="required">*</span>
							<input type="text" class="form-control" name="user_name" value="">
						</div>
						<div class="col-12">
							<label for="lname">Last Name</label><span class="required">*</span>
							<input type="text" class="form-control" name="last_name" value="">
						</div>
					</div>
					<div class="form-group form-group-md">
						<div class="col-12">
							<label for="email">Email</label><span class="required">*</span>
							<input type="text" class="form-control" name="email" value="">
						</div>
						<div class="col-12">
							<label for="num">Phone</label><span class="required">*</span>
							<div>
								<div id="code">
									<input type="tel" class="form-control " placeholder="+91">
								</div>
								<div id="num">
									<input type="tel" class="form-control col-md-12">
								</div>
							</div>
						</div>
					</div>
					</br>
					<div class="form-group form-group-md">
						<div class="col-12">
							<label for="location">Location(city)</label><span class="required">*</span>
							<input type="text" class="form-control" name="loaction" value="">	<span><a href="">Locate me</a></span>
						</div>
					</div>
					</br>
					<div class="form-group form-group-md">
						<div class="col-12">
							<label for="resume">Resume/CV</label><span class="required">*</span>
							</br>
							<input type="file" class="form-control" name="resume" value="" style="padding: 1px;">
						</div>
						<div class="col-12">
							<label for="letter">Cover Letter</label><span class="required">*</span>
							<input type="file" class="form-control" name="cover_letter" value="" style="padding: 1px;">
						</div>
					</div>
					</br>
					<div class="form-group form-group-md">
						<div class="col-12">
							<label for="school">School</label><span class="required">*</span>
							<select class="form-control" name="school">
								<option>Select a School</option>
								<option>University not listed (mention in notes)</option>
								<option>Abraham Baldwin Agricultural College</option>
								<option>Academy of Art University</option>
								<option>Acadia University</option>
							</select>
							</br>
						</div>
					</div>
					<div class="form-group form-group-md">
						<div class="col-12">
							<label for="degree">Degree</label><span class="required">*</span>
							<select class="form-control" name="degree">
								<option>High School</option>
								<option>Associate's Degree</option>
								<option>Bachelor's Degree</option>
								<option>Master's Degree</option>
								<option>Master of Business Administration (M.B.A.)</option>
							</select>
							</br>
						</div>
					</div>
					<div class="form-group form-group-md">
						<div class="col-12">
							<label for="discipline">Discipline</label><span class="required">*</span>
							<select class="form-control" name="discipline">
								<option>select a discipline</option>
								<option>African Studies</option>
								<option>Agriculture</option>
								<option>Anthropology</option>
								<option>Applied Health Services</option>
								<option>Architecture</option>
								<option>Art</option>
							</select>
							</br>
						</div>
					</div>
					<div class="form-group form-group-md">
						<div class="col-12">
							<label for="date">Start Date</label><span class="required">*</span>
							<input type="date" name="start_date" class="form-control" value="">
							</br>
						</div>
					</div>
					<div class="form-group form-group-md">
						<div class="col-12">
							<label for="date">End Date</label><span class="required">*</span>
							<input type="date" name="end_date" class="form-control" value="">
							</br>
						</div>
					</div>
					</br>
					<div class="form-group form-group-md">
						<div class="col-12">
							<label for="linkedin">LinkedIn Profile</label><span class="required">*</span>
							<input type="text" class="form-control" name="profile" value="">
						</div>
					</div>
					</br>
					<div class="form-group form-group-md">
						<div class="col-12">
							<label for="website">Website</label><span class="required">*</span>
							<input type="text" class="form-control" name="website" value="">
						</div>
					</div>
					</br>
					</br>
					<button type="submit" class="btn btn-default btn-block btn-lg"><b>Submit Application</b>
					</button>
				</form>
			</div>
		</div>
	</div>
</div>