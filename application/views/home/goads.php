<style>
    /*second nav */
    #myTab-2 li a {
        border: .8px solid #ad72056b;
        border-radius: 25px;
        color: black;
        padding: 6px 30px;
        font-size: 14px;
    }

    #myTab-2 li a.active {
        color: #fff;
        background-color: #E92C3C;
        font-size: 14px;
        display: inline-block;
        padding: 6px 30px;
        border-radius: 25px;
        /*border: .8px solid black;*/
    }

    /*footer subcribe*/
    .rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
        font-size: 20px;
        font-size: 14px;
        font-weight: 600;
        position: absolute;
        top: 3px;
        right: 3px;
        height: 51px;
        padding: 0 40px;
        text-transform: uppercase;
        border-radius: 999px;
        /*position: relative;*/
        z-index: 1;
        color: #fff;
        border: none;
        background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
        background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
    }

    /*back to top button*/
    #scrollUp {
        font-size: 30px;
        line-height: 48px;
        position: fixed;
        right: 30px;
        bottom: 30px;
        width: 45px;
        height: 45px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
        background: #E92C3C;
    }

    /*services hover bg color*/
    .main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
    .main-menu ul ul>li:hover>a {
        padding-left: 23px;
        color: #fff;
        background: #E92C3C;
    }

    .sticky {
        position: fixed;
        top: 16%;
        width: 100%;
        z-index: 99;
        animation-name: fadeInDown;
    }

    .rt {
        background: linear-gradient(0deg, rgba(0, 0, 0, 0.20), rgba(0, 0, 0, 0.20)),
            url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/Ads-banner.jpg);
        background-repeat: no-repeat;
        background-size: 100% 80%;
        height: 100%;
        position: relative;
        top: 93px;
        margin-bottom: 95px;
    }

    #rt_text {
        position: relative;
        top: 30%;
        color: white;
        font-weight: 600;
    }

    #rt_text h3 {
        line-height: 33px;
        text-align: initial;
        font-weight: bold;
        font-size: 22px;
    }

    #rt_text button {
        color: black;
        background-color: #fee685;
        border-color: transparent;
        padding: 10px 23px;
        font-size: 16px;
        font-weight: bold;
    }

    .rt:after {
        content: "";
        display: table;
        clear: both;
    }

    #billboards img {
        width: 100%;
        height: 190px;
    }

    #app img {
        height: 260px;
        padding: 20px;
        width: 60%;
        display: flex;
        margin: auto;
        border-radius: 30px;
    }

    .container h3,
    h4 {
        font-weight: bold;
    }

    #join_five .panel-primary>.panel-heading {
        color: black;
        background-color: transparent;
        border-color: transparent;
    }

    #join_five p {
        font-size: 22px;
        line-height: 35px;
    }

    #join_five a {
        font-size: 22px;
        line-height: 35px;
    }

    #expand .show {
        overflow: hidden;
    }
</style>

<!-- 
    ====== Services Start ==============
 -->

<div class="rt">
    <div class="container" id="rt_text">
        <div class="row">
            <div class="col-lg-4">
                <h1><b><b>Finigoo</b>Ads</b></h1>
                <h3>Our exposure. Your advantage.
                    Online and offline.</h3>
                <button type="btn" class="btn btn-default btn-lg">Get in Touch With Us</button>
            </div>
        </div>
    </div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container">
    <div class="row">

        <div class="col-lg-8" id="bnr1">
            <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/bike.png" class="img-responsive" alt="">
        </div>
        <div class="col-lg-4 align-items-center" id="txt1" style="padding-top:100px;padding-left:30px;">
            <h3>Go where your customers go.</h3>
            <p>People are always on the move, your brand should be too. With <b>Finigoo</b>Ads, you have the advantage to connect with millions of people across the region, both online and offline.</p>
        </div>
    </div>

</div><!-- /.row -->
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container">
    <div class="row">
        <div class="col-lg-8" id="bnr1">
            <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/car.jpg" class="img-responsive" alt="">
        </div>
        <div class="col-lg-4 align-items-center" id="txt1" style="padding-top:100px;padding-left:30px;">
            <h3>Online-to-Offline Integration</h3>
            <p>With our extensive on-ground fleet and rich digital presence, we can integrate your marketing communications across touchpoints to work seamlessly and more effectively.</p>
        </div>
    </div>
</div><!-- /.row -->

<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container">
    <div class="row">
        <div class="col-lg-4 align-items-center" id="txt1" style="padding-top:100px;padding-right:30px;">
            <h3>Connect with the right audience</h3>
            <p><b>Finigoo</b>Ads can help you identify and target specific audiences by making the right ad placements at the right place and time, both online and offline.</p>
        </div>
        <div class="col-lg-8" id="bnr1">
            <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/bus.jpg" class="img-responsive" alt="">
        </div>
    </div>
</div><!-- /.row -->
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container">
    <div class="row">
        <div class="col-lg-8" id="bnr1">
            <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/ads2.jpg" class="img-responsive" alt="">
        </div>
        <div class="col-lg-4 align-items-center" id="txt1" style="padding-top:100px;padding-left:30px;">
            <h3>Your trusted partner</h3>
            <p>At <b>Finigoo</b>, trust is one of our core values that extends to <b>Finigoo</b>Ads. We are committed to ensure that your brand's ads are appropriately shown only to relevant and verified audiences, while also respecting user privacy.</p>
        </div>
    </div>
</div><!-- /.row -->
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h3>Work with us</h3>
            <p>Let us help you meet your business objectives to build awareness, engage your audience and trigger action with <b>Finigoo</b>Ads.</p>
        </div>
    </div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container" id="billboards">
    <h3>Mobile Billboards</h3>
    <div class="row">

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">

            <div class="img-div">
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/Artboard 1.jpg">
                <h4>Car Wraps</h4>
                <p>Draw attention to your brand and message while</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">

            <div class="img-div">
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/car-6.png">
                <h4>Car Wraps</h4>
                <p>Draw attention to your brand and message while</p>
            </div>
        </div>
    </div>
</div>
<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>
<div class="container" id="billboards">
    <h3>In-Car Engagement</h3>
    <div class="row">

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">

            <div class="img-div">
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/ads1.1.jpg">
                <h4>In-Car Branding</h4>
                <p>Boost your brand awareness to millions of passengers every day.</p>
            </div>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">

            <div class="img-div">
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/ads1.3.jpg">
                <h4>In-Car Sampling</h4>
                <p>Display product testers for passengers to sample during their ride.</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">

            <div class="img-div">
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/ads1.4.jpg">
                <h4>In-Car Digital Display</h4>
                <p>Provide an interactive brand experience through digital displays.</p>
            </div>
        </div>
    </div>
</div>
<div class="section-title-spacer"></div>

<div class="section-title-spacer"></div>
<div class="container" id="">
    <h3>In-App Opportunities</h3>
    <div class="row">

        <div class="col-lg-4">

            <div style="text-align: center;margin: 40px 0px;">
                <img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/GoAds.png" style="width: 120px;margin-bottom: 10px;">
                <h4>Targeted <b>Finigoo</b>Gifts</h4>
                <p>Reward targeted audiences with exclusive promo codes for <b>Finigoo</b> rides.</p>
            </div>
        </div>

        <div class="col-lg-4">

            <div style="text-align: center;margin: 40px 0px;">
                <img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/GoAds.png" style="width: 120px;margin-bottom: 10px;">
                <h4>Sponsored Comms</h4>
                <p>Push your brand message to the inbox of targeted users and even enhance it with <b>Finigoo</b>Gifts.
                    </br>
                    Find the <b>Finigoo</b>Ads guideline <a href="">here</a></p>
            </div>
        </div>
        <div class="col-lg-4">

            <div style="text-align: center;margin: 40px 0px;">
                <img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/GoAds.png" style="width: 120px;margin-bottom: 10px;">
                <h4><b>Finigoo</b>Daily Widget</h4>
                <p>Drive engagement with customisable in-app widgets such as games, surveys and more.

                    Find the <b>Finigoo</b>Ads guideline <a href="">here</a></p>
            </div>
        </div>
    </div>
</div>

<div class="section-title-spacer"></div>
<div class="section-title-spacer"></div>

<div class="container">
    <div class="row">
        <div class="col-lg-12" id="join_five">
            <h1>Frequently Asked Questions</h1>

            <div id="accordion">
                <div class="card wow fade-in-bottom animated" style="visibility: visible; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                will I benefit as a Finigoo Points user?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            As a Finigoo Points user, you will receive rewards such as exciting Deal packages and Benefits. The more you use Finigoo, the greater the benefits you will earn.
                        </div>
                    </div>
                </div><!-- end single accrodain -->
                <div class="card wow fade-in-bottom animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1s; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                How can I earn points?


                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            You can earn points by taking a ride or ordering food on our platform. You will be awarded 1 point for every 10 BDT you spend on our services.


                        </div>
                    </div>
                </div><!-- end single accrodain -->
                <div class="card wow fade-in-bottom animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                How many points do I need to get to the next level?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            Finigoo Points has 4 levels - Bronze (0 - 199), Silver(200 - 999), Gold (1000 - 6999) and Platinum (7000+). You will be a Bronze user and have 0 points, to begin with. As soon as you earn enough points to get to the next level, your level status will change immediately.
                        </div>
                    </div>
                </div><!-- end single accrodian -->
                <div class="card wow fade-in-bottom animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
                    <div class="card-header card-primary" id="headingThree2">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree2" aria-expanded="false" aria-controls="collapseThree2"><span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i class="icofont-question"></i></span>
                                What is Finigoo Points?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree2" class="collapse" aria-labelledby="headingThree2" data-parent="#accordion">
                        <div class="card-body">
                            Finogoo Points allows you to earn points by taking our services and receive rewards based on your usage.
                        </div>
                    </div>
                </div><!-- end single accrodian -->

            </div>
        </div>
    </div>
</div>