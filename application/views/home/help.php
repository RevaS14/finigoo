<style>
    /*second nav */
    #myTab-2 li a {
        border: .8px solid #ad72056b;
        border-radius: 25px;
        color: black;
        padding: 6px 30px;
        font-size: 14px;
    }

    #myTab-2 li a.active {
        color: #fff;
        background-color: #01aef3;
        font-size: 14px;
        display: inline-block;
        padding: 6px 30px;
        border-radius: 25px;
        /*border: .8px solid black;*/
    }

    /*footer subcribe*/
    .rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
        font-size: 20px;
        font-size: 14px;
        font-weight: 600;
        position: absolute;
        top: 3px;
        right: 3px;
        height: 51px;
        padding: 0 40px;
        text-transform: uppercase;
        border-radius: 999px;
        /*position: relative;*/
        z-index: 999999;
        color: #fff;
        border: none;
        background: -webkit-linear-gradient(55deg, #01aef3 0%, #01aef3 53%, #01aef3 70%, #01aef3 100%);
        background: linear-gradient(35deg, #01aef3 0%, #01aef3 53%, #01aef3 70%, #01aef3 100%);
    }

    /*back to top button*/
    #scrollUp {
        font-size: 30px;
        line-height: 48px;
        position: fixed;
        right: 30px;
        bottom: 30px;
        width: 45px;
        height: 45px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
        background: #01aef3;
    }

    /*services hover bg color*/
    .main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
    .main-menu ul ul>li:hover>a {
        padding-left: 23px;
        color: #fff;
        background: #01aef3;
    }

    .rt-section-title span {
        font-family: 'Lato', sans-serif;
        font-size: 18px;
        display: block;
        margin: 0 0 0;
        text-transform: uppercase;
        color: #01aef3;
    }

    .icofont-check-circled:before {
        content: "\eed7";
        color: #01aef3;
    }

    .submenufix.show {
        background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.38)), url(<?php echo base_url(); ?>landing-assets/assets/images/extra/pay.jpg);
        background-size: cover;
    }

    @media (max-width:1200px) {
        .submenufix.show {
            background: white;
        }
    }

    .content-area {
        margin-top: 80px;
    }

    #scr img {
        width: auto;
        height: 350px;
        margin: auto;
        display: flex;
    }

    #scr h2 {
        font-size: 50px;
    }

    #scr1 .input-group-append {
        margin-left: 0px;
        position: absolute;
        right: 0;
        z-index: 22;
    }

    #scr1 input[type="text"] {
        width: 100%;
        padding: 10px 10px !important;
        height: unset !important;
    }

    #search {
        padding: 10px 24px;
        background-color: #ec1d23;
        color: aliceblue;
    }

    #tab_box a {
        margin-bottom: 15px;
    }

    #box {
        box-shadow: 0px 0px 8px 0px lightgrey;
        padding-bottom: 20px;
        padding-top: 20px;
        text-decoration: none;
        color: #000000b5;
        border-radius: 5px;
        font-weight: bold;
    }

    #box p {
        margin: 0 0 10px;
        font-size: 21px;
        letter-spacing: 0.9px;
        margin-bottom: 0px;
    }

    .para {
        width: calc(100% - 48px);
    }

    .para:after {
        content: "\f105";
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        position: absolute;
        right: 24px;
        font-size: 30px;
        top: 50%;
        transform: translateY(-50%);
    }

    #box .img p {
        display: inline;
        font-size: 17px;
        letter-spacing: 0px;
    }

    #box .img img {
        vertical-align: text-top;
    }

    #space {
        width: 80%;
        margin: auto;
        height: 4px;
        border-bottom: dashed 2px rgba(0, 0, 0, .16);
    }

    /*Carousel*/

    .ct-box-slider {
        margin-right: -15px;
    }

    @media (max-width: 991px) {
        .ct-box-slider {
            margin-left: -15px;
        }
    }

    .ct-box-slider__arrows .slick-arrow {
        padding: 0 1px 0 0;
        opacity: 0.3;
    }

    .ct-box-slider__arrows .slick-arrow:hover {
        opacity: 0.5;
    }

    .ct-box-slider__arrows .slick-arrow:hover:active {
        opacity: 0.55;
    }

    .ct-box {
        font-family: 'stenciletta-solid', sans-serif;
        font-size: 23px;
        line-height: 1.15;
        padding: 15px 0;
        display: block;
        color: #000;
    }

    .ct-box .hover {
        display: none;
    }

    .ct-box span {
        padding-top: 15px;
        display: block;
        max-width: 100%;
        font-size: 20px;
        font-weight: bold;
        font-variant: all-petite-caps;
    }

    .ct-box .inner:hover {
        transition: all .3s;
        z-index: 1;
        -webkit-box-shadow: 0 6px 18px 0 rgba(0, 0, 0, .2);
        box-shadow: 0 6px 18px 0 rgba(0, 0, 0, .2);
    }

    .ct-box .inner {
        border-radius: 16px;
        -webkit-box-shadow: 0 2px 6px 0 rgba(0, 0, 0, .2);
        box-shadow: 0 2px 6px 0 rgba(0, 0, 0, .2);
        -webkit-transition: all .3s;
        -moz-transition: all .3s;
        transition: all .3s;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        text-align: center;
        padding: 15px 25px;
        height: 200px;
        cursor: pointer;
    }

    .ct-box .inner .fa {
        font-size: 64px;
    }

    .ct-box:hover {
        color: #00719b;
    }

    .ct-box:hover .normal {
        display: none;
    }

    .ct-box:hover .hover {
        display: block;
    }

    @media (max-width: 479px) {
        .ct-box {
            font-size: 18px;
        }

        .ct-box img {
            max-width: 50%;
        }

        .ct-box .inner {
            height: 150px;
            padding: 10px;
        }
    }

    .slick-active {
        width: 1052px;
    }

    .ct-box-slider__arrows .slick-arrow {
        background-color: transparent;
        color: black;
        opacity: 1;
        border: none;
        padding: 5px;
        box-shadow: none;
        outline: 0;
    }

    .help .x {
        width: 50%;
        margin: auto;
        text-align: center;
    }

    .help:hover {
        transition: all .3s;
        z-index: 1;
        -webkit-box-shadow: 0 6px 18px 0 rgba(0, 0, 0, .2);
        box-shadow: 0 6px 18px 0 rgba(0, 0, 0, .2);
    }

    .help {
        padding-top: 30px;
        padding-bottom: 30px;
        border-radius: 16px;
        -webkit-box-shadow: 0 2px 6px 0 rgba(0, 0, 0, .2);
        box-shadow: 0 2px 6px 0 rgba(0, 0, 0, .2);
        -webkit-transition: all .3s;
        -moz-transition: all .3s;
        transition: all .3s;
    }

    /*End*/
</style>
<section class="content-area">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12" id="scr">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/Go Pay-01.png" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
                <h2 class="text-center"><b>Need Some Help?</b></h2>
                </br>
            </div><!-- /.col-lg-6 -->
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-lg-5 col-md-5 col-xs-12 col-sm-12" id="scr1">
                <div class="input-group input-group-lg">
                    <input type="text" class="form-control" placeholder="Enter your email address" aria-describedby="button-addon2">

                    <div class="input-group-append">
                        <button class="btn btn-lg" type="button" id="search"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                </div><!-- end input gorup -->
            </div>
        </div>
    </div>
</section>
<div class="section-title-spacer"></div><!-- /.section-title-spacer -->
<div class="section-title-spacer"></div><!-- /.section-title-spacer -->
<div class="container" id="tab_box">
    <h1><b>Most-Searched Issues</b></h1>
    <div class="row">
        <a href="" class="col-lg-6">
            <div class="col-lg-12" id="box">
                <div class="para">
                    <p>Driver didn't pick up but has completed my order</p>
                    </br>
                </div>
                <div class="img">
                    <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/8.png" width="15px;" height="15px;">
                    <p>GoRide</p>
                </div>
            </div>
        </a>
        <a href="" class="col-lg-6">
            <div class="col-lg-12" id="box">
                <div class="para">
                    <p>Driver has completed my order status without delivering it</p>
                    </br>
                </div>
                <div class="img">
                    <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/3.png" width="15px;" height="15px;">
                    <p>GoFood</p>
                </div>
            </div>
        </a>
        <a href="" class="col-lg-6">
            <div class="col-lg-12" id="box">
                <div class="para">
                    <p>Driver drove unsafely</p>
                    </br>
                </div>
                <div class="img">
                    <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/8.png" width="15px;" height="15px;">
                    <p>GoRide</p>
                </div>
            </div>
        </a>
        <a href="" class="col-lg-6">
            <div class="col-lg-12" id="box">
                <div class="para">
                    <p>I left my item</p>
                    </br>
                </div>
                <div class="img">
                    <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/1.png" width="15px;" height="15px;">
                    <p>GoAuto</p>
                </div>
            </div>
        </a>
        <a href="" class="col-lg-6">
            <div class="col-lg-12" id="box">
                <div class="para">
                    <p>Driver asked for more payment</p>
                    </br>
                </div>
                <div class="img">
                    <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/4.png" width="15px;" height="15px;">
                    <p>GoCar</p>
                </div>
            </div>
        </a>
        <a href="" class="col-lg-6">
            <div class="col-lg-12" id="box">
                <div class="para">
                    <p>I didn’t receive my item</p>
                    </br>
                </div>
                <div class="img">
                    <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/3.png" width="15px;" height="15px;">
                    <p>GoFood</p>
                </div>
            </div>
        </a>
    </div>

</div>
<div class="section-title-spacer"></div><!-- /.section-title-spacer -->
<div class="section-title-spacer"></div><!-- /.section-title-spacer -->
<div id="space"></div>

<div id="space"></div>

<div id="space"></div>
<div class="section-title-spacer"></div><!-- /.section-title-spacer -->

<div class="container" id="carousel">
    <h1><b>Browse Category</b></h1>
    </br>
    <h2><b>Service</b></h2>


    <div class="ct-box-slider ct-js-box-slider">

        <div class="item">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-6 col-sm-3">
                        <div onclick="location.href='GoAuto.html'" class="ct-box">
                            <div class="inner" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/1.png" width="60px;" height="60px;"><span>Go Auto</span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div onclick="location.href='GoPay.html'" class="ct-box">
                            <div class="inner" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/2.png" width="60px;" height="60px;"><span>Go Pay</span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div onclick="location.href='GoFood.html'" class="ct-box">
                            <div class="inner" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/3.png" width="60px;" height="60px;"><span>Go Food</span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div onclick="location.href='cabs.html'" class="ct-box">
                            <div class="inner" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/4.png" width="60px;" height="60px;"><span>Go Cab</span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div onclick="location.href='bike.html'" class="ct-box">
                            <div class="inner" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/8.png" width="60px;" height="60px;"><span>Go Bike</span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div onclick="location.href='GoPack.html'" class="ct-box">
                            <div class="inner" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/6.png" width="60px;" height="60px;"><span>Go Pack</span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div onclick="location.href='#'" class="ct-box">
                            <div class="inner" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/10.png" width="60px;" height="60px;"><span>Go Cargo</span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div onclick="location.href='grocery.html'" class="ct-box">
                            <div class="inner" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/15.png" width="60px;" height="60px;"><span>Go Grocery</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="item">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-6 col-sm-3">
                        <div onclick="location.href='GoBeauty.html'" class="ct-box">
                            <div class="inner" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/7.png" width="60px;" height="60px;"><span>Go Beauty</span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div onclick="location.href='bills.html'" class="ct-box">
                            <div class="inner" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/5.png" width="60px;" height="60px;"><span>Go Bills</span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div onclick="location.href='GoCashback.html'" class="ct-box">
                            <div class="inner" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/11.png" width="60px;" height="60px;"><span>Go Cashback</span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div onclick="location.href='GoUPI.html'" class="ct-box">
                            <div class="inner" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/9.png" width="60px;" height="60px;"><span>Go UPI</span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div onclick="location.href='GoDemand.html'" class="ct-box">
                            <div class="inner" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/14.png" width="60px;" height="60px;"><span>Go Demands</span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div onclick="location.href='money.html'" class="ct-box">
                            <div class="inner" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/5.png" width="60px;" height="60px;"><span>Go Money</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="section-title-spacer"></div><!-- /.section-title-spacer -->
<div class="section-title-spacer"></div><!-- /.section-title-spacer -->
<div class="container">
    <h2>Others</h2>
    </br>
    </br>
    <div class="row">
        <div class="col-xs-6 col-sm-3">
            <div onclick="location.href='GoDemand.html'" class="help">
                <div class="x" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/14.png" width="60px;" height="60px;">
                    <p><b>Any Demands</b></p>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div onclick="location.href='money.html'" class="help">
                <div class="x" role="presentation"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/5.png" width="60px;" height="60px;">
                    <p><b>Accounts</b></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 
    ======== call to action start====
 -->
<div class="spacer-top"></div><!-- /.spacer-bottom -->