<style >
    /*second nav */
    #myTab-2 li a{
         border: .8px solid #ad72056b;
         border-radius: 25px;
         color:black;
         padding: 6px 30px;
          font-size: 14px;
    }
    #myTab-2 li a.active {
    color: #fff;
    background-color: #E92C3C;
    font-size: 14px;
    display: inline-block;
    padding: 6px 30px;
    border-radius: 25px;
    /*border: .8px solid black;*/
}
/*footer subcribe*/
.rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
    font-size: 20px;
    font-size: 14px;
    font-weight: 600;
    position: absolute;
    top: 3px;
    right: 3px;
    height: 51px;
    padding: 0 40px;
    text-transform: uppercase;
    border-radius: 999px;
    /*position: relative;*/
    z-index: 999999;
    color: #fff;
    border: none;
    background: -webkit-linear-gradient(55deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
    background: linear-gradient(35deg, #E92C3C 0%, #E92C3C 53%, #E92C3C 70%, #E92C3C 100%);
}
/*back to top button*/
#scrollUp {
    font-size: 30px;
    line-height: 48px;
    position: fixed;
    right: 30px;
    bottom: 30px;
    width: 45px;
    height: 45px;
    text-align: center;
    color: #fff;
    border-radius: 50%;
    background: #0FA214;
}
/*services hover bg color*/
.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
    padding-left: 23px;
    color: #fff;
    background: #E92C3C;
}
.submenufix.show{
	background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.38)), url(<?php echo base_url();?>landing-assets/assets/images/extra/ridez.jpg);
    background-size: 100% 100%;
}
.rt-single-icon-box {
    border-top: 3px solid rgb(15, 162, 20) !important;
}
.rt-single-icon-box:hover {
    border-top: 3px solid #E82801 !important;
}

.rt-single-icon-box .iconbox-content h5 {
    color: #0fa214;
}

@media (max-width:1200px){
.submenufix.show{
		background:white;
	}
}
</style>
<!-- 
    ====== Services Start ==============
 -->
<div class="rt-breadcump rt-breadcump-height">
    <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/martbanner.jpg)">
    </div><!-- /.rt-page-bg -->
    <div class="container">
        <div class="row rt-breadcump-height">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h3>Go Mart</h3>
                    <p style="font-size: 30px;"> Go Mart Delivery at Your Doorstep</p>
                
                </div><!-- /.breadcrumbs-content -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.rt-bredcump -->
<!-- <div class="container" id="tabb" >
<div class="flight-list-box rt-mb-40">
                        <ul class="nav pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="#rt-itm_1" role="tab"
                                    aria-controls="rt-itm_1" aria-selected="true">GoMart</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">blog</a>
                            </li>
                            <li class="nav-item">
                               
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                               
                            </li>
                        </ul>
                    </div>
</div> -->

<section class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 mx-auto text-center">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title">Go Mart     
 <span>Your favourite grocery brand FiniGoo (Go Mart) is now online as Go Mark Ready. You can now buy your daily household need products that you find in a Go Mart app.</span></h2>
                    
                </div><!-- /.rt-section-title-wrapper -->
            </div><!-- /.col-lg-8 -->
        </div><!-- ./row -->
    </div>
</section>


   
<div class="spacer-bottom"></div><!-- /.spacer-bottom -->
<section class="caltoaction-4 rt-dim-light rt-pt-100 rt-pb-100">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h4 class="f-size-40 rt-semibold rt-mb-40 f-size-md-32 f-size-xs-26">Whats Go Mart?</h4>
                <p> Go Mart reflects our added value proposition of connecting the Indian shopper with global products, targeting premium customers and expats.</p>
                
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>


<div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row align-items-center">
            <div class="col-lg-6 mb-0 mb-md-4">

                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/gomart-similar.JPEG" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
            </div><!-- /.spacer-top -->
 <div class="col-lg-5 offset-lg-1">
                <!-- <div class="icon-thumb rt-mb-30 wow fadeInUp" data-wow-duration="1s">
                    <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/iconx-4.png" alt="icon-image" draggable="false">
                </div> --><!-- /.icon-thumb -->
                <h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInUp" data-wow-duration="1s">HOW WE ARE DIFFERENT:</h4>
                <div class="text-424 wow fadeInDown" data-wow-duration="1s">
                    <p class="f-size-14 text-424">
                       Unique marketing tactics These set of concepts are designed to sustain customer acquisition and retention for Go Mart. They have been designed keeping in mind the psychological principles of consumer behaviour. We plan to obtain IPRs for these innovative concepts in India

                    </p>
                </div></div></div>

<div class="spacer-top" style="margin-top: 50px;"></div><!-- /.spacer-bottom -->
 <div class="container">
       </div>
<section class="about-area2">
    
    <div class="rt-design-elmnts  rtbgprefix-contain"
        style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/all-img/faq-img.png)">
    </div><!-- /.rt-design-elmnts -->

    <div class="container">
       
        <div class="row">
            <div class="col-lg-7">
                <div id="accordion">
    <div class="card wow fade-in-bottom">
        <div class="card-header card-primary" id="headingOne">
            <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                    aria-controls="collapseOne">
                   Engaging 
                </button>
            </h5>
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
               A user shakes his phone after which their shake score is displayed on the screen. Users receive a coupon based on their score. This concept encourages repeat visits to the app by adding a ‘game’ feature to grocery shopping.
            </div>
        </div>
    </div><!-- end single accrodain -->
    <div class="card wow fade-in-bottom" data-wow-duration="1.0s">
        <div class="card-header card-primary" id="headingTwo">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="false" aria-controls="collapseTwo">
               Triggering 
                </button>
            </h5>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
            <div class="card-body">
                This is an exciting, time-based coupon. It is generated after a user spends more than 3 minutes on our platform and expires after 10 minutes. This incentivizes users to browse the website and eventually make a purchase
            </div>
        </div>
    </div><!-- end single accrodain -->
    <div class="card wow fade-in-bottom" data-wow-duration="1.5s">
        <div class="card-header card-primary" id="headingThree">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree"
                    aria-expanded="false" aria-controls="collapseThree">
               Surprising
                </button>
            </h5>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
            <div class="card-body">
                The blind shopping adds an element of surprise in grocery shopping. The customer purchases a package of certain value without knowing the contents and will know only when the same is delivered. This concept can be used by new brands to facilitate trial of products.
            </div>
        </div>
    </div><!-- end single accrodian -->
    <div class="card wow fade-in-bottom" data-wow-duration="1.5s">
        <div class="card-header card-primary" id="headingThree2">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree2"
                    aria-expanded="false" aria-controls="collapseThree2">
               Involving
                </button>
            </h5>
        </div>
        <div id="collapseThree2" class="collapse" aria-labelledby="headingThree2" data-parent="#accordion">
            <div class="card-body">
               Ultra Competition is an engaging feature on the platform, similar to a poll. Users will be asked to vote between two brands of the same product. All the users who voted for the winning brand will receive coupons to purchase from the same brand.
            </div>
        </div>
    </div></div><!-- end single accrodian -->
    <!-- <div class="card wow fade-in-bottom" data-wow-duration="1.5s">
        <div class="card-header card-primary" id="headingThree21">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree21"
                    aria-expanded="false" aria-controls="collapseThree21">
                <span class="d-inline-block f-size-18 rt-hw-40 bg-gradient-primary rt-circle rt-mr-10 text-center text-white"><i
                        class="icofont-question"></i></span>  Fair Pricing
                </button>
            </h5>
        </div>
        <div id="collapseThree21" class="collapse" aria-labelledby="headingThree21" data-parent="#accordion">
            <div class="card-body">
                All service prices are competitive and set by experienced providers.
            </div>
        </div>
    </div> --><!-- end single accrodian -->

</div><!-- end accrodian group -->
</div></div></div></section>

            </div><!-- /.col-lg-7 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>




<div class="spacer-top"></div>
<div class="spacer-top"></div><!-- /.spacer-top -->
<section class="book-area">
    <div class="rt-design-elmnts rtbgprefix-contain" style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/all-img/gomart-sm.jpg)">

    </div><!-- /.rt-design-elmnts -->
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="rt-section-title-wrapper">
                    <!-- /.rt-section-title -->
                    <h5>
                        Cultural and Language Diversity: Diversity in India makes it necessary to understand different

customer needs in each and every locality
                    </h5>
                </div><!-- /.rt-section-title-wrapper -->
                <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
                <div class="rt-single-icon-box wow fade-in-bottom">
    <!-- /.icon-thumb -->
    <div class="iconbox-content">
        <h5>Language Diversity</h5>
        <p>22 languages in India</p>
    </div><!-- /.iconbox-content -->
</div><!-- /.rt-single-icon-box wow fade-in-bottom -->
<div class="rt-single-icon-box wow fade-in-bottom" data-wow-duration="1.5s">
    <!-- /.icon-thumb -->
    <div class="iconbox-content">
        <h5>Cultural Diversity</h5>
        <p>7 major religions

 Different eating habits

Different product preferences</p>
    </div><!-- /.iconbox-content -->
</div><!-- /.rt-single-icon-box wow fade-in-bottom -->
<div class="rt-single-icon-box wow fade-in-bottom" data-wow-duration="2s">
    <!-- /.icon-thumb -->
    <div class="iconbox-content">
        <h5>
Women</h5>
        <p>Decision maker for F&G purchases

 Target customer segments</p>
    </div><!-- /.iconbox-content -->
</div><!-- /.rt-single-icon-box wow fade-in-bottom -->
<div class="rt-single-icon-box wow fade-in-bottom" data-wow-duration="2.5s">
   <!-- /.icon-thumb -->
    <div class="iconbox-content">
        <h5>Kirana Stores</h5>
        <p>Know customer preferences

Offer niche product segments (localized products)</p>
    </div><!-- /.iconbox-content -->
</div><!-- /.rt-single-icon-box wow fade-in-bottom -->
            </div><!-- /.col-lg-9 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>