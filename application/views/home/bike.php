<style>
	.works3-area .rt-inner-overlay {
	    height: 439px;
	    background-image: -webkit-linear-gradient(82deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
	    background-image: linear-gradient(8deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
	}
	    /*second nav */
	    #myTab-2 li a{
	         border: .8px solid #ad72056b;
	         border-radius: 25px;
	         color:black;
	         padding: 6px 30px;
	          font-size: 14px;
	    }
	    #myTab-2 li a.active {
	    color: #fff;
	    background-color: #01AC00;
	    font-size: 14px;
	    display: inline-block;
	    padding: 6px 30px;
	    border-radius: 25px;
	    /*border: .8px solid black;*/
	}
	/*footer subcribe*/
	.rt-site-footer .footer-top .footer-subscripbe-box .btn {
	        font-family: 'Poppins', sans-serifs;
	    font-size: 20px;
	    font-size: 14px;
	    font-weight: 600;
	    position: absolute;
	    top: 3px;
	    right: 3px;
	    height: 51px;
	    padding: 0 40px;
	    text-transform: uppercase;
	    border-radius: 999px;
	    /*position: relative;*/
	    z-index: 9999;
	    color: #fff;
	    border: none;
	    background: -webkit-linear-gradient(55deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
	    background: linear-gradient(35deg, #01AC00 0%, #01AC00 53%, #01AC00 70%, #01AC00 100%);
	}
	/*back to top button*/
	#scrollUp {
	    font-size: 30px;
	    line-height: 48px;
	    position: fixed;
	    right: 30px;
	    bottom: 30px;
	    width: 45px;
	    height: 45px;
	    text-align: center;
	    color: #fff;
	    border-radius: 50%;
	    background: #01AC00;
	}
	/*services hover bg color*/
	.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
	    padding-left: 23px;
	    color: #fff;
	    background: #01AC00;
	}
	
	.submenufix.show{
		background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.38)), url(<?php echo base_url()?>landing-assets/assets/images/extra/bike1.jpg);
	    background-size:100% 100%;
	}
</style>
<!-- 
    ====== Services Start ==============
 -->
<div class="rt-breadcump rt-breadcump-height">
	<div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url()?>landing-assets/assets/images/backgrounds/gobike.jpg)"></div>
	<!-- /.rt-page-bg -->
	<div class="container">
		<div class="row rt-breadcump-height">
			<div class="col-12">
				<div class="breadcrumbs-content">
					<h3>Go Bike</h3>
					<p style="font-size: 30px;">Beat the Traffic, Save Time</p>
					<!-- /.breadcrumbs -->
				</div>
				<!-- /.breadcrumbs-content -->
			</div>
			<!-- /.col-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</div>
<!-- /.rt-bredcump -->
<!-- 
    ====== Services Start ==============
 -->
<!-- <div class="container" id="tabb" >
<div class="flight-list-box rt-mb-40">
                        <ul class="nav  pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="#rt-itm_1" role="tab"
                                    aria-controls="rt-itm_1" aria-selected="true">Go Bike</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_3-tab" data-toggle="tab" href="#rt-itm_3" role="tab"
                                    aria-controls="rt-itm_3" aria-selected="false">Contact Us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_4-tab" data-toggle="tab" href="#rt-itm_4" role="tab"
                                    aria-controls="rt-itm_4" aria-selected="false">Driver Join</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_5-tab" data-toggle="tab" href="#rt-itm_5" role="tab"
                                    aria-controls="rt-itm_5" aria-selected="false">Policies</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_6-tab" data-toggle="tab" href="#rt-itm_6" role="tab"
                                    aria-controls="rt-itm_6" aria-selected="false">Reviews</a>
                            </li>
                        </ul>
                    </div>
</div> -->
<section class="svcPage-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 text-center mx-auto">
				<div class="rt-section-title-wrapper" style="margin-top: 40px; margin-bottom: 0px;">
					<h2 class="rt-section-title">
                        <span style="color:#01AC00">Go Bike</span>
                       Go Bike
                    </h2>
					<!-- /.rt-section-title -->
					<p>Moto: GET Anything, GO Anywhere</p>
				</div>
				<!-- /.rt-section-title-wrapper- -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<!-- /.section-title-spacer -->
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>
<!-- 
    ====== Works Start ==============
 -->
<div class="spacer-bottom"></div>
<!-- /.spacer-bottom -->
<section class="works3-area">
	<div class="rt-inner-overlay"></div>
	<!-- /.rt-overlay -->
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="rt-section-title-wrapper">
					<h3>Why GoBike?</h3>
				</div>
				<!-- /.rt-section-title-wrapper -->
				<div class="rt-box-style-1">
					<div class="row">
						<div class="col-lg-4 col-md-6">
							<div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="1.5s">
								<!-- /.icon-thumb -->
								<div class="iconbox-content">
									<img src="<?php echo base_url()?>landing-assets/assets/images/icons-image/8.png" style="width: 50px;" />
									<h5>Why GoBike?</h5>
									<p>Eliminating all worries when moving daily traffic such as traffic jams, parking spaces, unconnected streets.</p>
								</div>
								<!-- /.iconbox-content -->
							</div>
							<!-- /.rt-single-icon-box -->
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom">
								<!-- <div class="icon-thumb">
                                    <img src="assets/images/icons-image/box-icon-11.png" alt="box-icon" draggable="false">
                                </div> -->
								<!-- /.icon-thumb -->
								<div class="iconbox-content">
									<img src="<?php echo base_url()?>landing-assets/assets/images/icons-image/8.png" style="width: 50px;" />
									<h5>Ride Safe. Ride Smart.</h5>
									<p>Follow traffic laws and park responsibly. We recommend you always wear a helmet and be careful with your speed.</p>
								</div>
								<!-- /.iconbox-content -->
							</div>
							<!-- /.rt-single-icon-box -->
						</div>
						<!-- /.col-lg-4 -->
						<div class="col-lg-4 col-md-6">
							<div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom" data-wow-duration="1s">
								<!-- <div class="icon-thumb">
                                    <img src="assets/images/icons-image/box-icon-12.png" alt="box-icon" draggable="false">
                                </div> -->
								<!-- /.icon-thumb -->
								<div class="iconbox-content">
									<img src="<?php echo base_url()?>landing-assets/assets/images/icons-image/8.png" style="width: 50px;" />
									<h5>Lowest Prices </h5>
									<p>We make sure that Ryders pay less to get cost-effective and state of the art GoBike services.</p>
								</div>
								<!-- /.iconbox-content -->
							</div>
							<!-- /.rt-single-icon-box -->
						</div>
						<!-- /.col-lg-4 -->
						<!-- /.col-lg-4 -->
						<!-- /.col-12 -->
					</div>
					<!-- /.row -->
				</div>
				<!-- /.rt-box-style-1 -->
			</div>
			<!-- /.col-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>
<!-- 
    ======About Area Start ==============
 -->
<div class="spacer-top"></div>
<!-- /.spacer-top -->
<section class="about-area2">
	<div class="rt-design-elmnts  rtbgprefix-contain">
		<img src="<?php echo base_url()?>landing-assets/assets/images/all-img/Go ride-01.png">
	</div>
	<!-- /.rt-design-elmnts -->
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<div class="rt-section-title-wrapper">
					<h2 class="rt-section-title">
                        <!-- <span>Extraordinary experiences,</span> -->
                       We believe GoBike drivers to make happy customers!
                    </h2>
					<!-- /.rt-section-title -->
				</div>
			</div>
			<!-- /.col-lg-9 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>