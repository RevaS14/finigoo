<style >
    /*second nav */
    #myTab-2 li a{
         border: .8px solid #ad72056b;
         border-radius: 25px;
         color:black;
         padding: 6px 30px;
          font-size: 14px;
    }
    #myTab-2 li a.active {
    color: #fff;
    background-color: #01AEF3;
    font-size: 14px;
    display: inline-block;
    padding: 6px 30px;
    border-radius: 25px;
    /*border: .8px solid black;*/
}
/*footer subcribe*/
.rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
    font-size: 20px;
    font-size: 14px;
    font-weight: 600;
    position: absolute;
    top: 3px;
    right: 3px;
    height: 51px;
    padding: 0 40px;
    text-transform: uppercase;
    border-radius: 999px;
    /*position: relative;*/
    z-index: 99999;
    color: #fff;
    border: none;
    background: -webkit-linear-gradient(55deg, #01AEF3 0%, #01AEF3 53%, #01AEF3 70%, #01AEF3 100%);
    background: linear-gradient(35deg, #01AEF3 0%, #01AEF3 53%, #01AEF3 70%, #01AEF3 100%);
}
/*back to top button*/
#scrollUp {
    font-size: 30px;
    line-height: 48px;
    position: fixed;
    right: 30px;
    bottom: 30px;
    width: 45px;
    height: 45px;
    text-align: center;
    color: #fff;
    border-radius: 50%;
    background: #01AEF3;
}
/*services hover bg color*/
.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
    padding-left: 23px;
    color: #fff;
    background: #01AEF3;
}
.icofont-check-circled:before {
    content: "\eed7";
    color: #01aef3;
}
.submenufix.show{
	background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.38)), url(<?php echo base_url();?>landing-assets/assets/images/extra/money.jpg);
    background-size: cover;
}
@media (max-width:1200px){
.submenufix.show{
		background:white;
	}
}
</style>

<div class="rt-breadcump rt-breadcump-height">
    <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/gomoney.jpg)">
    </div><!-- /.rt-page-bg -->
    <div class="container">
        <div class="row rt-breadcump-height">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h3> Go Kiosk </h3>
                    <p style="font-size: 30px;">All-round Digital Shop</p>
                   <!--  <div class="breadcrumbs">
                        <span class="divider"><i class="icofont-home"></i></span>
                        <a href="#" title="Home">Home</a>
                        <span class="divider"><i class="icofont-simple-right"></i></span>
                        <a href="#">China</a>
                        <span class="divider"><i class="icofont-simple-right"></i></span>
                        <a href="#">Hotels</a>
                        <span class="divider"><i class="icofont-simple-right"></i></span>
                        <a href="#" title="Home">Hong Kong Hotels</a>
                        <span class="divider"><i class="icofont-simple-right"></i></span>
                        Empire Hotel Kowloon - Tsim Sha Tsui

                    </div> --><!-- /.breadcrumbs -->
                </div><!-- /.breadcrumbs-content -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.rt-bredcump -->

<!-- <div class="container" id="tabb" >
<div class="flight-list-box rt-mb-40">
                        <ul class="nav  pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="#rt-itm_1" role="tab"
                                    aria-controls="rt-itm_1" aria-selected="true">Go Money</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">Benefit & Convenience</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_3-tab" data-toggle="tab" href="#rt-itm_3" role="tab"
                                    aria-controls="rt-itm_3" aria-selected="false">How To Top Up GoMoney</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_4-tab" data-toggle="tab" href="#rt-itm_4" role="tab"
                                    aria-controls="rt-itm_4" aria-selected="false">Safety</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_5-tab" data-toggle="tab" href="#rt-itm_5" role="tab"
                                    aria-controls="rt-itm_5" aria-selected="false">GoMoney Everywhere</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_6-tab" data-toggle="tab" href="#rt-itm_6" role="tab"
                                    aria-controls="rt-itm_6" aria-selected="false">Terms & Conditions</a>
                            </li>
                        </ul>
                    </div>
</div> -->



<section class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 mx-auto text-center">
                <div class="rt-section-title-wrapper">
                    <h4 class="rt-section-title">Going Cashless Is Now Safer and More Rewarding Than Ever
                    </h4>
                    <p>Now accept UPI payments using PayTM, PhonePe,WhatsApp or any banking App</p>
                    <p>Accept payments via UPI and get paid instantly.</p>
            <p>What's interesting is, you get onboarded instantly by just downloading our App and creating your profile.</p>
                </div><!-- /.rt-section-title-wrapper -->
            </div><!-- /.col-lg-8 -->
        </div><!-- ./row -->
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row align-items-center">
            <div class="col-lg-5 mb-0 mb-md-4">
                <!-- <div class="icon-thumb rt-mb-30 wow fadeInDown" data-wow-duration="1s">
                    <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/iconx-3.png" alt="icon-image" draggable="false">
                </div> --><!-- /.icon-thumb -->
                <h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInDown" data-wow-duration="1s">Go Transfer</h4>
                <div class="text-424 wow fadeInUp" data-wow-duration="1s">
                    <ul class="rt-list f-size-14">
                       
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span> INSTANT money transfer service to any bank Account.</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Our payment solution that allows users to easily and safely send or receive money, or pay utility bills - using cash or a bank account or a FiniGoo mobile wallet.</li>
                       <!--  <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Variety of text links and banners.</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Reliable third-party tracking</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Accessible and dynamic reporting</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Monthly commission checks</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Exciting incentive programs</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Dedicated affiliate team</li>
 -->                    </ul>
                </div><!-- /.text-424 -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6 offset-lg-1">
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/pay.png" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row align-items-center">
            <div class="col-lg-6 mb-0 mb-md-4">

                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/Go Pay-01.png" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-5 offset-lg-1">
               <!--  <div class="icon-thumb rt-mb-30 wow fadeInUp" data-wow-duration="1s">
                    <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/iconx-4.png" alt="icon-image" draggable="false">
                </div> --><!-- /.icon-thumb -->
                <h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInUp" data-wow-duration="1s">Go MINI ATM</h4>
                <div class="text-424 wow fadeInDown" data-wow-duration="1s">
                    <p class="f-size-14 text-424"><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>
                        Use the GO MINI ATM Mobile Point of Sale Device to perform all our services. Enable your customers to withdraw cash using your FINIGOO (GO MINI ATM) Micro ATM or using their Adhaar Cards

                    </p>
                </div><!-- /.text-424 -->
               <!--  <a href="#" class="rt-btn rt-gradient rt-rounded text-uppercase rt-sm rt-mt-25 wow fadeInUp" data-wow-duration="1s">Read more</a> -->

            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        <div class="row align-items-center">
            <div class="col-lg-5 mb-0 mb-md-4">
                <!-- <div class="icon-thumb rt-mb-30 wow fadeInDown" data-wow-duration="1s">
                    <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/iconx-3.png" alt="icon-image" draggable="false">
                </div> --><!-- /.icon-thumb -->
                <h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInDown" data-wow-duration="1s">Go AEPS </h4>
                <div class="text-424 wow fadeInUp" data-wow-duration="1s">
                    <ul class="rt-list f-size-14">
                       
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span> AePS is a bank led model which allows online interoperable financial inclusion transaction at PoS (MicroATM) through the Business correspondent of any bank using the Aadhaar authentication.</li>
                        <!-- <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Our payment solution that allows users to easily and safely send or receive money, or pay utility bills - using cash or a bank account or a FiniGoo mobile wallet.</li> -->
                       <!--  <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Variety of text links and banners.</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Reliable third-party tracking</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Accessible and dynamic reporting</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Monthly commission checks</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Exciting incentive programs</li>
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Dedicated affiliate team</li>
 -->                    </ul>
                </div><!-- /.text-424 -->
            </div>
            <div class="col-lg-6 offset-lg-1">
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/iconx-4.png" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s" >
            </div>


    </div><!-- ./ copntainer -->

</section>
<!-- 
    ======== call to action start====
 -->
<!--  --><!-- /.container -->