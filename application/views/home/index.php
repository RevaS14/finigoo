<section>
    <div id="demo" class="carousel slide" data-ride="carousel" style="margin-bottom: 45px;margin-top: 90px;">
        <!-- Indicators -->
        <!-- <ul class="carousel-indicators">
          <li data-target="#demo" data-slide-to="0" class="active"></li>
          <li data-target="#demo" data-slide-to="1"></li>
          <li data-target="#demo" data-slide-to="2"></li>
        </ul> -->
        <!-- The slideshow -->
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/home-finigoo-banner.jpg" alt="Los Angeles">
            </div>
            <!-- <div class="carousel-item">
            <img src="chicago.jpg" alt="Chicago">
          </div>
          <div class="carousel-item">
            <img src="ny.jpg" alt="New York">
          </div> -->
        </div>
        <!-- Left and right controls -->
        <!-- <a class="carousel-control-prev" href="#demo" data-slide="prev">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
          <span class="carousel-control-next-icon"></span>
        </a> -->
    </div>
    <!-- <div class="single-rt-banner rt-banner-height" style="background-image: url(assets/images/all-img/home-finigoo-banner.jpg)">
        <div class="container">

            <div class="row  rt-banner-height align-items-center">
                <div class="col-lg-6 col-md-10 col-sm-9 col-xs-10">
                    <div class="rt-banner-content" id="hme_rht">
                       
                    
        <div class="rt-banner-searchbox standard-search wow fade-in-bottom" data-wow-duration="1s" data-wow-delay="1s">
            <div class="tab-content" id="myTabContent">
                 
				 <div class="tab-pane active text-center" id="rt-item_b_first" role="tabpanel" aria-labelledby="rt-item_a_second">
                    <form action="#">
                       Go Ride
                    </form>
                </div>
				  
                <div class="tab-pane  rtIncative text-center" id="rt-item_a_second" role="tabpanel" aria-labelledby="rt-item_a_second">
                    <form action="#">
                      Go Cars
                    </form>
                </div>
                <div class="tab-pane  rtIncative text-center" id="rt-item_a_third" role="tabpanel" aria-labelledby="rt-item_a_third">
                    <form action="#">
                    
					GO Food
					
                    </form>
				 <div class="tab-pane  rtIncative text-center" id="rt-item_b_four" role="tabpanel" aria-labelledby="rt-item_a_third">
                    <form action="#">
                    
					 Go Pay
					
                    </form>
                </div>
				 <div class="tab-pane  rtIncative text-center" id="rt-item_b_fifth" role="tabpanel" aria-labelledby="rt-item_a_third">
                    <form action="#">
                    
					 Go Beauty
					
                    </form>
                </div>
            </div>
        </div>

    
        <ul class="nav serachnavs wow fade-in-bottom" id="rtMultiTab" role="tablist" data-wow-duration="1.5s" data-wow-delay="1.5s">
            <li class="nav-item">
                <a class="nav-link active" id="first-tab" data-target="#rt-item_b_first" data-secondary="#rt-item_a_first"
                    data-toggle="tab" href="#rt-item_b_first" role="tab" aria-controls="first-tab" aria-selected="false">
                    <i class="icofont-bike"></i>
                    <span>Go Ride</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link inactive" id="second-tab" data-target="#rt-item_b_second" data-secondary="#rt-item_a_second"
                    data-toggle="tab" href="#rt-item_b_second" role="tab" aria-controls="second-tab" aria-selected="true">
        
                    <i ></i>
                    <span>Go Cars</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link inactive" id="third-tab" data-target="#rt-item_b_thrid" data-secondary="#rt-item_a_third"
                    data-toggle="tab" href="#rt-item_b_thrid" role="tab" aria-controls="third-tab" aria-selected="false">
                    <i ></i>
                    <span>GO Food</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link inactive" id="four-tab" data-target="#rt-item_b_four" data-secondary="#rt-item_a_four"
                    data-toggle="tab" href="#rt-item_b_four" role="tab" aria-controls="four-tab" aria-selected="false">
                    <i ></i>
                    <span>Go Pay</span>
                </a>
            </li>
			  <li class="nav-item">
                <a class="nav-link inactive" id="fifth-tab" data-target="#rt-item_b_fifth" data-secondary="#rt-item_a_fifth"
                    data-toggle="tab" href="#rt-item_b_fifth" role="tab" aria-controls="second-tab" aria-selected="true">
        
                    <i ></i>
                    <span>Go Beauty</span>
                </a>
            </li>
        </ul>
                    </div>
                </div>
            </div>




        </div>
    </div>
	<div class="spacer-bottom"></div> -->
</section>
<!-- 
    ========= Counter Start ========================
 -->
<div class="counter-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
                <div class="media counter-box-1 align-items-center wow fadeInUp">
                    <div class="media-body">
                        <h5>Trusted Users</h5>
                        <h6><span class="counter" style="color:red;">90,000</span><span>+</span></h6>
                    </div>
                </div>
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4 col-md-6 col-12">
                <div class="media counter-box-1 align-items-center wow fadeInUp" data-wow-duration="1.5s">
                    <div class="media-body">
                        <h5>Trusted partners</h5>
                        <h6><span class="counter" style="color:red;">2,000</span><span>+</span></h6>
                    </div>
                </div>
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4 col-md-6 col-12">
                <div class="media counter-box-1 align-items-center wow fadeInUp" data-wow-duration="2s">
                    <div class="media-body">
                        <h5>Trusted Members</h5>
                        <h6><span class="counter" style="color:red;">80,000</span><span>+</span></h6>
                    </div>
                </div>
            </div>
            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /.counter -->
<!-- 
    ===========Services Area start==========
 -->
<section class="emigr-services-area rtbgprefix-contain" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/backgrounds/dotbg.png)">
    <div class="spacer-bottom"></div>
    <!-- /.spacer-bottom -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8	 text-center mx-auto">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title">
                        <span>WHY Choose Our FiniGoo?</span>
                        Our Core Values
                    </h2>
                    <!-- /.rt-section-title -->
                    <p>FiniGoo providing a multi-service application with solutions for connecting four-wheel and two-wheeler transportation, ordering food, delivery,Mobile Payments and many other services to serve the needs.</p>
                </div>
                <!-- /.rt-section-title-wrapper- -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="section-title-spacer"></div>
        <!-- /.section-title-spacer -->
        <div class="row">
            <div class="col-lg-4 col-md-6 mx-auto text-center">
                <div class="services-box-1 wow fade-in-bottom">
                    <h4>Advanced technology</h4>
                    <p>FiniGoo is built and developed on the basis of world advanced technology has been improving the quality of life for millions of people in Southeast Asia.</p>
                </div>
                <!-- /.services-box-1 -->
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4 col-md-6 mx-auto text-center">
                <div class="services-box-1 wow fade-in-bottom" data-wow-duration="1.5s">
                    <div class="services-thumb"></div>
                    <!-- /.services-thumb -->
                    <h4>Anytime Anywhere </h4>
                    <p>FiniGoo with a team of dedicated partners in large numbers always gives you the best quality service regardless of weather or time.</p>
                </div>
                <!-- /.services-box-1 -->
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4 col-md-6 mx-auto text-center">
                <div class="services-box-1 wow fade-in-bottom" data-wow-duration="2s">
                    <div class="services-thumb"></div>
                    <!-- /.services-thumb -->
                    <h4>Excellent Support</h4>
                    <p>FiniGoo provides excellent customer support and service 24x7</p>
                </div>
                <!-- /.services-box-1 -->
            </div>
            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- 
    ============Deal Area Start===============
 -->
<!--
    ========works start==========
 -->
<!-- 
    ======== Flight deals start======
 -->
<!-- 
    ====== call to action======
 -->
<!-- 
    ======== call to action start====
 -->
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="slider_curve d-flex flex-column align-items-center justify-content-center bg-gradient-primary" style="background-color:#fff">
                <div class="row">
                    <div class="mx-auto">
                        <!-- <div class="rt-section-title-wrapper text-white text-center">
                            <h2 class="rt-section-title">
                                <span>Save Time, Save Money</span>
                                Let’s Get Started
                            </h2>
                            <p>We have the knowledge, experience, and expertise to take care of all your  needs.
                                Sign up and we'll send the best deals to you </p>
                                <div class="section-title-spacer"></div>
                            <a href="#" class="rt-btn rt-light pill rt-xl rt-Bshadow-1 text-uppercase">Sign UP</a>
                        </div> -->
                        <div id="demo" class="carousel slide" data-ride="carousel" style="padding-bottom: 40px; margin: 2px 15px;">
                            <!-- Indicators -->
                            <ul class="carousel-indicators circle_indicator">
                                <li data-target="#demo" data-slide-to="0" class="active"></li>
                                <li data-target="#demo" data-slide-to="1"></li>
                                <li data-target="#demo" data-slide-to="2"></li>
                            </ul>
                            <!-- The slideshow -->
                            <div class="carousel-inner" style="border-radius: 36px;">
                                <div class="carousel-item active">
                                    <img src="<?php echo base_url() ?>landing-assets/assets/images/backgrounds/gobike-sm-banner.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img src="<?php echo base_url() ?>landing-assets/assets/images/backgrounds/gofood-sm-banner.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img src="<?php echo base_url() ?>landing-assets/assets/images/backgrounds/gopack-sm-banner.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <!-- Left and right controls -->
                        <div class="left_arrow">
                            <a class="" href="#demo" data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="right_arrow">
                            <a class="" href="#demo" data-slide="next"> <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <!-- /.col-lg-7 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.inner-content -->
        </div>
        <!-- /.col-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->
<!-- 
    ====== Services Start ==============
 -->
<div class="spacer-top"></div>
<!-- /.spacer-bottom -->
<section class="svcPage-area demo-3">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 text-center mx-auto">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title">
                        <span>Our Exclusive Offers</span>
                        Our Services
                    </h2>
                    <!-- /.rt-section-title -->
                    <p>We have the knowledge, experience, and expertise to take care of all your needs.Our Friendly and Professional staff can assist you with</p>
                </div>
                <!-- /.rt-section-title-wrapper- -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="section-title-spacer"></div>
        <!-- /.section-title-spacer -->
        <div class="row">
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-red" style="border:5px solid  #ed1d24;">
                    <!-- <img src="assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/1.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="<?php echo base_url();?>bike">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-org" style="border:5px solid  #FD6000;">
                    <!-- <img src="<?php echo base_url() ?>landing-assets/assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/2.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="#">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-violet" style="border:5px solid #D51E92;">
                    <!-- <img src="assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/3.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="<?php echo base_url();?>gobeauty">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-blue" style="border:5px solid #771FEA ;">
                    <!-- <img src="assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/7.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="<?php echo base_url();?>godemand">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-green" style="border:5px solid #0FA214;">
                    <!-- <img src="assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/8.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="<?php echo base_url();?>gomart">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-red" style="border:5px solid #ED1D24;">
                    <!-- <img src="<?php echo base_url() ?>landing-assets/assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/5.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="<?php echo base_url();?>gofood">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-org" style="border:5px solid #FD6000;">
                    <!-- <img src="<?php echo base_url() ?>landing-assets/assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/9.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="<?php echo base_url();?>gopack">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-skyblue" style="border:5px solid #01ADD1;">
                    <!-- <img src="<?php echo base_url() ?>landing-assets/assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/10.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="<?php echo base_url();?>gopay">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-green" style="border:5px solid #0FA214;">
                    <!-- <img src="<?php echo base_url() ?>landing-assets/assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/11.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="<?php echo base_url();?>cabs">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-org" style="border:5px solid #FD6000;">
                    <!-- <img src="<?php echo base_url() ?>landing-assets/assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/12.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="#">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-red" style="border:5px solid #ED1D24;">
                    <!-- <img src="<?php echo base_url() ?>landing-assets/assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/4.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="<?php echo base_url();?>goupi">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-skyblue" style="border:5px solid #01ADD1;">
                    <!-- <img src="assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/6.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="<?php echo base_url();?>bills">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-green" style="border:5px solid #0FA214;">
                    <!-- <img src="assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/13.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="<?php echo base_url();?>send">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-skyblue" style="border:5px solid #01ADD1;">
                    <!-- <img src="assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/14.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="<?php echo base_url();?>gocashback">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-green" style="border:5px solid #0FA214;">
                    <!-- <img src="assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/15.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="<?php echo base_url();?>goauto">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-lg-4 col-md-6 margin_b_20">
                <figure class="effect-ruby effect-ruby-red" style="border:5px solid #ED1D24;">
                    <!-- <img src="assets/images/backgrounds/tab.jpg" alt="img13" class="backgroundcover"/> -->
                    <figcaption>
                        <img src="<?php echo base_url() ?>landing-assets/assets/images/Finigo_Icons/16.png" alt="box-icon" draggable="false" class="product_icon">
                        <p> <a href="<?php echo base_url();?>money">Read more <i class="icofont-long-arrow-right"></i></a>
                        </p>
                    </figcaption>
                </figure>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- 
    =======app area strat=========
 -->
<div class="spacer-top"></div>
<!-- /.spacer-top -->
<section class="app-area rtbgprefix-cover" style="background: #FBFBFB;padding-top: 50px;" data-scrollax-parent="true">
    <!-- <div class="rt-shape-elements-1 rtbgprefix-contain"
        style="background-image: url(assets/images/shape-elements/shape-3.png)"
        data-scrollax="properties: { translateY: '50px' }"></div> -->
    <div class="container">
        <div class="row">
            <div class="col-lg-5" style="position: relative;">
                <img class="download_app" src="<?php echo base_url() ?>landing-assets/assets/images/all-img/app-mbl2.png" alt="mockup image" draggable="false" class="wow fade-in-left" data-wow-duration="1s" data-wow-delay="0.2s">
            </div>
            <div class="col-lg-7">
                <div class="rt-section-title-wrapper content_view">
                    <div class="wow fade-in-bottom" data-wow-duration="1s" data-wow-delay="0.6s">
                        <h2 class="rt-section-title">
                            <span>All in one that is FINIGOO .</span>
                            Download App
                        </h2>
                        <p>Daily demand of Indian users, in world-leading technology platform. we wishes to improve the quality of life of users, creating many useful values for partners and the community. FiniGoo is a team of leaders with a vision to reach people from every walk of life by easy transaction solutions in banking and e-commerce, with the end goal of becoming a leading transaction service provider</p>
                        <br>
                        <p>Download theFiniGoo app and join our ever-growing community . Find lasting friendships, discover new adventures and create unforgettable memories.</p>
                    </div>
                    <div class="rt-button-group mt-5 wow fade-in-bottom" data-wow-duration="1s" data-wow-delay="0.9s">
                        <a href="#" class="rt-btn rt-app-parimary rt-rounded">
                            <div class="app-thumb"> <i class="fab fa-google-play"></i>
                            </div>
                            <div class="app-text"> <span>GET IT ON</span>
                                <span>Appstore</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- 
    ============= video area start==========
 -->
<div class="play-video rtbgprefix-full bg-hide-md" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/backgrounds/videobg.png)">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 mx-auto">
                <div class="play-video-box rtbgprefix-cover" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/backgrounds/play-video-bg.png)">
                    <a href="https://www.youtube.com/embed/RLXctHmSBI8" class="rt-btn rt-video-light playVideo"></a>
                </div>
                <!-- /.play-video-box -->
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- 
    ============= Testimonilas area start==========
 -->
<section class="testimonials-area" data-scrollax-parent="true">
    <div class="rt-shape-emenetns-1" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/shape-elements/shape-4.png)" data-scrollax="properties: { translateY: '-140px' }"></div>
    <!-- /.rt-shape-emenetns-1 -->
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto text-center">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title">
                        <span>one stop solution for all your needs</span>
                        What Our App Say
                    </h2>
                    <!-- /.rt-section-title -->
                    <p>We have many happy customers that have booked holidays with us.Some Impresions from our Customers! Please read some of the lovely things our Customers say about us.</p>
                </div>
                <!-- /.rt-section-title-wrapper -->
            </div>
            <!-- /.col-lg-9 -->
        </div>
        <!-- /.row -->
        <div class="section-title-spacer"></div>
        <!-- /.section-title-spacer -->
        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="testimoninal-active-1">
                    <div class="singleTbox-1 text-center active position-1" data-position="position-1">
                        <div class="testi-thumb">
                            <div class="inner-bg" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/testimonials/t_1.png)"></div>
                            <!-- /.inner-bg --> <span class="social-badge">Go</span>
                        </div>
                        <!-- /.testi-thumb -->
                        <div class="autor-bio">
                            <h5>FINIGOO</h5>
                        </div>
                        <!-- /.autor-bio -->
                        <div class="inner-content">
                            <p>FiniGoo providing a multi-service application with solutions for connecting four-wheel and two-wheeler transportation, ordering food, delivery,Mobile Payments and many other services to serve the needs. daily demand of Indian users, in world-leading technology platform. we wishes to improve the quality of life of users, creating many useful values for partners and the community.</p>
                        </div>
                        <!-- /.inner-content -->
                    </div>
                    <!-- /.singleTbox-1 -->
                    <div class="singleTbox-1 text-center inactive position-2" data-position="position-2">
                        <div class="testi-thumb">
                            <div class="inner-bg" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/icons-image/8.png)"></div>
                            <!-- /.inner-bg --> <span class="social-badge">Go</span>
                        </div>
                        <!-- /.testi-thumb -->
                        <div class="autor-bio">
                            <h5>GO RIDE</h5>
                        </div>
                        <!-- /.autor-bio -->
                        <div class="inner-content">
                            <p>Eliminating all worries when moving daily traffic such as traffic jams, parking spaces, unconnected streets, GO-BIKE with a team of dedicated drivers created to provide optimal service for all needs. Bridge on your mo</p>
                        </div>
                        <!-- /.inner-content -->
                    </div>
                    <!-- /.singleTbox-1 -->
                    <div class="singleTbox-1 text-center inactive position-3" data-position="position-3">
                        <div class="testi-thumb">
                            <div class="inner-bg" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/icons-image/2.png)"></div>
                            <!-- /.inner-bg --> <span class="social-badge">Go</span>
                        </div>
                        <!-- /.testi-thumb -->
                        <div class="autor-bio">
                            <h5>GO PAY</h5>
                        </div>
                        <!-- /.autor-bio -->
                        <div class="inner-content">
                            <p>India’s Most Generous Cashback. Join our millions of members worldwide and earn money back when you shop or pay online.</p>
                        </div>
                        <!-- /.inner-content -->
                    </div>
                    <!-- /.singleTbox-1 -->
                    <div class="singleTbox-1 text-center inactive position-4" data-position="position-4">
                        <div class="testi-thumb">
                            <div class="inner-bg" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/icons-image/3.png)"></div>
                            <!-- /.inner-bg --> <span class="social-badge">Go</span>
                        </div>
                        <!-- /.testi-thumb -->
                        <div class="autor-bio">
                            <h5>GO FOOD</h5>
                        </div>
                        <!-- /.autor-bio -->
                        <div class="inner-content">
                            <p>A marketplace that connects food hunters with restaurants and food vendors of their choice with a robust instant delivery. Go Food is now in the FIniGoo app, creating a more seamless experience with your daily needs in the everyday everything app.</p>
                        </div>
                        <!-- /.inner-content -->
                    </div>
                    <!-- /.singleTbox-1 -->
                    <div class="singleTbox-1 text-center inactive position-5" data-position="position-5">
                        <div class="testi-thumb">
                            <div class="inner-bg" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/icons-image/4.png)"></div>
                            <!-- /.inner-bg --> <span class="social-badge">Go</span>
                        </div>
                        <!-- /.testi-thumb -->
                        <div class="autor-bio">
                            <h5>GO CAB</h5>
                        </div>
                        <!-- /.autor-bio -->
                        <div class="inner-content">
                            <p>We are Go Cab, an online cab booking aggregator, providing customers with reliable and premium Intercity and Local car rental services. we are uniquely placed as the largest chauffeur driven car rental company in India in terms of geographical reach.</p>
                        </div>
                        <!-- /.inner-content -->
                    </div>
                    <!-- /.singleTbox-1 -->
                    <div class="singleTbox-1 text-center inactive position-6" data-position="position-6">
                        <div class="testi-thumb">
                            <div class="inner-bg" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/icons-image/14.png)"></div>
                            <!-- /.inner-bg --> <span class="social-badge">Go</span>
                        </div>
                        <!-- /.testi-thumb -->
                        <div class="autor-bio">
                            <h5>GO DEMAND</h5>
                        </div>
                        <!-- /.autor-bio -->
                        <div class="inner-content">
                            <p>Go Demand is on a mission to improve the service industry in Southeast Asia. We are dedicated to raising service standards and lifting income levels for thousands of service professionals and small business owners. We’ll still deliver the same great experience, whether you typed in Go Demand, We are on-demand mobile app and an evolution of the traditional directory service, yellow pages or local business listing. We take the best of mobile technology to make finding local businesses easier, more convenient and safer.</p>
                        </div>
                        <!-- /.inner-content -->
                    </div>
                    <!-- /.singleTbox-1 -->
                    <div class="singleTbox-1 text-center inactive position-7" data-position="position-7">
                        <div class="testi-thumb">
                            <div class="inner-bg" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/icons-image/6.png)"></div>
                            <!-- /.inner-bg --> <span class="social-badge">Go</span>
                        </div>
                        <!-- /.testi-thumb -->
                        <div class="autor-bio">
                            <h5>GO PACK</h5>
                        </div>
                        <!-- /.autor-bio -->
                        <div class="inner-content">
                            <p>FiniGoo Introduced GoPack is an intra-city on-demand last mile technology platform that connects customers (individuals/businesses) with trucks (logistics service providers) to efficiently move goods.</p>
                        </div>
                        <!-- /.inner-content -->
                    </div>
                    <!-- /.singleTbox-1 -->
                    <div class="singleTbox-1 text-center inactive position-8" data-position="position-8">
                        <div class="testi-thumb">
                            <div class="inner-bg" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/icons-image/7.png)"></div>
                            <!-- /.inner-bg --> <span class="social-badge">Go</span>
                        </div>
                        <!-- /.testi-thumb -->
                        <div class="autor-bio">
                            <h5>GO BEAUTY</h5>
                        </div>
                        <!-- /.autor-bio -->
                        <div class="inner-content">
                            <p>Go Beauty is now in the FIniGoo app, creating a more seamless experience with your daily needs in the everyday everything app.</p>
                        </div>
                        <!-- /.inner-content -->
                    </div>
                    <!-- /.singleTbox-1 -->
                    <div class="singleTbox-1 text-center inactive position-9" data-position="position-9">
                        <div class="testi-thumb">
                            <div class="inner-bg" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/icons-image/1.png)"></div>
                            <!-- /.inner-bg --> <span class="social-badge">Go</span>
                        </div>
                        <!-- /.testi-thumb -->
                        <div class="autor-bio">
                            <h5>GO AUTO</h5>
                        </div>
                        <!-- /.autor-bio -->
                        <div class="inner-content">
                            <p>We are Go AUTO, an online cab booking aggregator, providing customers with reliable and premium Intercity and Local Auto services.</p>
                        </div>
                        <!-- /.inner-content -->
                    </div>
                    <!-- /.singleTbox-1 -->
                    <div class="singleTbox-1 text-center inactive position-10" data-position="position-10">
                        <div class="testi-thumb">
                            <div class="inner-bg" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/icons-image/12.png)"></div>
                            <!-- /.inner-bg --> <span class="social-badge">Go</span>
                        </div>
                        <!-- /.testi-thumb -->
                        <div class="autor-bio">
                            <h5>GO MONEY</h5>
                        </div>
                        <!-- /.autor-bio -->
                        <div class="inner-content">
                            <p>Accept payments via UPI and get paid instantly. What's interesting is, you get onboarded instantly by just downloading our App and creating your profile.</p>
                        </div>
                        <!-- /.inner-content -->
                    </div>
                    <!-- /.singleTbox-1 -->
                </div>
                <!-- /.testimoninal-active-1 -->
            </div>
            <!-- /.col-lg-7 -->
        </div>
        <!-- /.row -->
        <div class="rt-divider style-one rt-margin-top"></div>
        <!-- /.divider -->
    </div>
    <!-- /.containe -->
</section>
<!-- 
    ============= Brands area start==========
 -->
<div class="spacer-top"></div>
<!-- /.spacer-top -->
<section class="about-area2">
    <div class="rt-design-elmnts  rtbgprefix-contain" style="background-image: url(<?php echo base_url() ?>landing-assets/assets/images/all-img/abt_vec_3.jpeg)"></div>
    <!-- /.rt-design-elmnts -->
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title">
                        <span>Extraordinary experiences,</span>
                        Designed Just for you
                    </h2>
                    <!-- /.rt-section-title -->
                    <p>FiniGoo providing a multi-service application with solutions for connecting four-wheel and two-wheeler transportation, ordering food, delivery,Mobile Payments and many other services to serve the needs. daily demand of Indian users, in world-leading technology platform. we wishes to improve the quality of life of users, creating many useful values for partners and the community. FiniGoo is a team of leaders with a vision to reach people from every walk of life by easy transaction solutions in banking and e-commerce, with the end goal of becoming a leading transaction service provider for all financial requirements across India & Southeast Asia. We, at FiniGoo, aspire to empower 10,00,000 stores across Tier I, II and rural towns in India. Using the power of Aadhaar & Mobility, we are motivated to transform the FiniGoo Franchise stores into Fintech & Hyper local Marts</p>
                </div>
                <!-- /.rt-section-title-wrapper -->
                <div class="section-title-spacer"></div>
                <!-- /.section-title-spacer --> <a href="#" class="rt-btn rt-primary rt-rounded">Get Matched to Travel Specialists</a>
            </div>
            <!-- /.col-lg-9 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>