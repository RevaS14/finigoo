<style>
        /*second nav */
    #myTab-2 li a{
         border: .8px solid #ad72056b;
         border-radius: 25px;
         color:black;
         padding: 6px 30px;
          font-size: 14px;
    }
    #myTab-2 li a.active {
    color: #fff;
    background-color: #EE2737;
    font-size: 14px;
    display: inline-block;
    padding: 6px 30px;
    border-radius: 25px;
    /*border: .8px solid black;*/
}
/*footer subcribe*/
.rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
    font-size: 20px;
    font-size: 14px;
    font-weight: 600;
    position: absolute;
    top: 3px;
    right: 3px;
    height: 51px;
    padding: 0 40px;
    text-transform: uppercase;
    border-radius: 999px;
    /*position: relative;*/
    z-index: 99999;
    color: #fff;
    border: none;
    background: -webkit-linear-gradient(55deg, #EE2737 0%, #EE2737 53%, #EE2737 70%, #EE2737 100%);
    background: linear-gradient(35deg, #EE2737 0%, #EE2737 53%, #EE2737 70%, #EE2737 100%);
}
/*back to top button*/
#scrollUp {
    font-size: 30px;
    line-height: 48px;
    position: fixed;
    right: 30px;
    bottom: 30px;
    width: 45px;
    height: 45px;
    text-align: center;
    color: #fff;
    border-radius: 50%;
    background: #EE2737;
}
/*services hover bg color*/
.main-menu > ul > li.menu-item-has-children > ul.sub-menu > li:hover > a, .main-menu ul ul > li:hover > a {
    padding-left: 23px;
    color: #fff;
    background: #EE2737;
}
.icofont-check-circled:before {
    content: "\eed7";
    color: #ee2737;
}
.submenufix.show{
	background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.38)), url(<?php echo base_url();?>landing-assets/assets/images/extra/grocery.jpg);
    background-size: 100% 100%;
}
@media (max-width:1200px){
.submenufix.show{
		background:white;
	}
}
</style>

<div class="rt-breadcump rt-breadcump-height">
    <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url();?>landing-assets/assets/images/backgrounds/home-finigoo1.jpg)">
    </div><!-- /.rt-page-bg -->
    <div class="container">
        <div class="row rt-breadcump-height">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h3>Go Mart </h3>
                    <p style="font-size: 30px;">Go Mart Delivery at Your Doorstep</p>
                    <!-- /.breadcrumbs -->
                </div><!-- /.breadcrumbs-content -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.rt-bredcump -->
<!-- <div class="container" id="tabb" >
<div class="flight-list-box rt-mb-40">
                        <ul class="nav pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="#rt-itm_1" role="tab"
                                    aria-controls="rt-itm_1" aria-selected="true">Go Mart</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">blog</a>
                            </li>
                            <li class="nav-item">
                               <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_3" aria-selected="false">GoGrocery Partners</a>
                            </li>
                           
                        </ul>
                    </div>
</div> -->

<section class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title"><span>Go Mart</span>
                    </h2>
                    <p>Go Gorcery  Instant Grocery Delivery Service,A marketplace that connects grocery/convenient stores to customers near them with a robust instant delivery.

</p>
                </div><!-- /.rt-section-title-wrapper -->
            </div><!-- /.col-lg-8 -->
        </div><!-- ./row -->
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row align-items-center">
            <div class="col-lg-6 mb-0 mb-md-4">
                <!-- <div class="rt-icon rt-hw-45 rt-circle icon-glow-1 icon-gradinet-1 f-size-20">
                    01
                </div> --><!-- /.rt-icon -->
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/work-img-1.png" alt="work image" draggable="false">
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-5 offset-lg-1 mb-0 mb-md-4">
                <!-- <div class="rt-icon rt-hw-65 f-size-45  rt-dotted-primary text-gradinet-primary rt-mb-30">
                        <i class="icofont-hand-drag1"></i>
                </div> --><!-- /.rt-icon -->
                 <h4 class="f-size-24 rt-semiblod rt-mb-20">Delivery</h4>
               <div class="text-424">
                    <p class="f-size-14 ">We deliver food and groceries fresh and fast from your favourite restaurants and stores, pick up, clean and return your laundry in two days and deliver tickets for the hottest attractions in town straight to your phone. We want to make your life easier so that you have time for the things that matter.</p>
                </div><!-- /.text-424 -->
               
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        <div class="row align-items-center">
            <div class="col-lg-5 mb-0 mb-md-4">
                <!-- <div class="rt-icon rt-hw-65 f-size-45  rt-dotted-primary text-gradinet-primary rt-mb-30">
                    <i class="icofont-hand-drag1"></i>
                </div> --><!-- /.rt-icon -->
                <h4 class="f-size-24 rt-semiblod rt-mb-20">Stress Free, Safe and Dependable</h4>
                <div class="text-424">
                    <p class="f-size-14 ">An easy platform for you to use on a daily and weekly basis for all of your shopping needs</p>
                </div><!-- /.text-424 -->
            
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6 offset-lg-1">
                <!-- <div class="rt-icon rt-hw-45 rt-circle icon-glow-1 icon-gradinet-1 f-size-20">
                    02
                </div> --><!-- /.rt-icon -->
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/work-img-2.png" alt="work image" draggable="false">
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        <div class="row align-items-center">
            <div class="col-lg-6 mb-0 mb-md-4">
                <!-- <div class="rt-icon rt-hw-45 rt-circle icon-glow-1 icon-gradinet-1 f-size-20">
                    03
                </div> --><!-- /.rt-icon -->
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/work-img-3.png" alt="work image" draggable="false">
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-5 offset-lg-1">
                <!-- <div class="rt-icon rt-hw-65 f-size-45  rt-dotted-primary text-gradinet-primary rt-mb-30">
                    <i class="icofont-runner-alt-1"></i>
                </div> --><!-- /.rt-icon -->
                <h4 class="f-size-24 rt-semiblod rt-mb-20">Affordable delivery fees</h4>
                <div class="text-424">
                    <p class="f-size-14 ">Don’t stress about the shipping cost, because Go Mart offers you with an affordable delivery fee anytime!</p>
                </div><!-- /.text-424 -->
        
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </div><!-- ./ copntainer -->
</section>

<!-- 
    ======== call to action start====
 -->
 

<div class="spacer-top"></div><!-- /.spacer-bottom -->

<section class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 mx-auto text-center">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title"><span>Single app for all your daily needs</span>
                    
        </h2>
                    <p>Order thousands of products at just a tap; milk, eggs, bread, cooking oil, ghee, atta, rice, fresh fruits & vegetables, spices, chocolates, chips, biscuits, Maggi, cold drinks, shampoos, soaps, body wash, pet food, diapers, electronics, other organic and gourmet products from your neighbourhood stores.</p>
                </div><!-- /.rt-section-title-wrapper -->
            </div><!-- /.col-lg-8 -->
        </div><!-- ./row -->
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row align-items-center">
            <div class="col-lg-5 mb-0 mb-md-4">
                <!-- <div class="icon-thumb rt-mb-30">
                    <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/iconx-1.png" alt="icon-image" draggable="false">
                </div> --><!-- /.icon-thumb --> 
                <h4 class="f-size-24 rt-semiblod rt-mb-20">Best Prices & Offers</h4>
                <div class="text-424">
                    <ul class="rt-list">
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span> Cheaper prices than your local supermarket, great cashback offers to top it off.</li>
                        
                    </ul>
                </div><!-- /.text-424 -->
                
        
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6 offset-lg-1">
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/work-img-4.png" alt="work image" draggable="false">
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row align-items-center">
            <div class="col-lg-6 mb-0 mb-md-4">
               
                <img src="<?php echo base_url();?>landing-assets/assets/images/all-img/work-img-5.png" alt="work image" draggable="false">
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-5 offset-lg-1">
                <!-- /.icon-thumb -->
                <h4 class="f-size-24 rt-semiblod rt-mb-20">Easy Returns </h4>
                <div class="text-424">
                    <ul class="rt-list">
                        <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span> Not satisfied with a product? Return it at the doorstep & get a refund within hours</li>
                       
                    </ul>
                </div><!-- /.text-424 -->
                

            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        
        
    </div><!-- ./ copntainer -->
</section>