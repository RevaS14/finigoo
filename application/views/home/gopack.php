<style>
    .works3-area .rt-inner-overlay {
        height: 439px;
        background-image: -webkit-linear-gradient(82deg, #F06400 0%, #F06400 53%, #F06400 70%, #F06400 100%);
        background-image: linear-gradient(8deg, #F06400 0%, #F06400 53%, #F06400 70%, #F06400 100%);
    }

    /*second nav */
    #myTab-2 li a {
        border: .8px solid #ad72056b;
        border-radius: 25px;
        color: black;
        padding: 6px 30px;
        font-size: 14px;
    }

    #myTab-2 li a.active {
        color: #fff;
        background-color: #F06400;
        font-size: 14px;
        display: inline-block;
        padding: 6px 30px;
        border-radius: 25px;
        /*border: .8px solid black;*/
    }

    /*footer subcribe*/
    .rt-site-footer .footer-top .footer-subscripbe-box .btn {
        font-family: 'Poppins', sans-serifs;
        font-size: 20px;
        font-size: 14px;
        font-weight: 600;
        position: absolute;
        top: 3px;
        right: 3px;
        height: 51px;
        padding: 0 40px;
        text-transform: uppercase;
        border-radius: 999px;
        /*position: relative;*/
        z-index: 99999;
        color: #fff;
        border: none;
        background: -webkit-linear-gradient(55deg, #F06400 0%, #F06400 53%, #F06400 70%, #F06400 100%);
        background: linear-gradient(35deg, #F06400 0%, #F06400 53%, #F06400 70%, #F06400 100%);
    }

    /*back to top button*/
    #scrollUp {
        font-size: 30px;
        line-height: 48px;
        position: fixed;
        right: 30px;
        bottom: 30px;
        width: 45px;
        height: 45px;
        text-align: center;
        color: #fff;
        border-radius: 50%;
        background: #F06400;
    }

    /*services hover bg color*/
    .main-menu>ul>li.menu-item-has-children>ul.sub-menu>li:hover>a,
    .main-menu ul ul>li:hover>a {
        padding-left: 23px;
        color: #fff;
        background: #F06400;
    }

    .submenufix.show {
        background: linear-gradient(to right, rgb(255, 255, 255), rgba(255, 255, 255, 0.38)), url(<?php echo base_url(); ?>landing-assets/assets/images/extra/box.jpg);
        background-size: cover;
    }
</style>

<div class="rt-breadcump rt-breadcump-height">
    <div class="rt-page-bg rtbgprefix-cover" style="background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/pack.jpg)">
    </div><!-- /.rt-page-bg -->
    <div class="container">
        <div class="row rt-breadcump-height">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h3 >Go pack</h3>
                    <p style="font-size: 30px;">On Demand Delivery at Your Doorstep</p>

                </div><!-- /.breadcrumbs-content -->
            </div><!-- /.col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.rt-bredcump -->
<!-- <div class="container" id="tabb" >
<div class="flight-list-box rt-mb-40">
                        <ul class="nav pill justify-content-lg-between pl-md-4 pr-md-4 justify-content-center" id="myTab-2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="rt-itm_1-tab" data-toggle="tab" href="#rt-itm_1" role="tab"
                                    aria-controls="rt-itm_1" aria-selected="true">Go pack</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">Driver Join</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="rt-itm_2-tab" data-toggle="tab" href="#rt-itm_2" role="tab"
                                    aria-controls="rt-itm_2" aria-selected="false">Terms & conditions</a>
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                                
                            </li>
                            <li class="nav-item">
                               
                            </li>
                        </ul>
                    </div>
</div> -->


<section class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 mx-auto text-center">
                <div class="rt-section-title-wrapper">
                    <h2 class="rt-section-title"><span style="color:#F06400">Go Pack</span>Deliver anything Instantly!
                        Affiliate Program</h2>
                    <p>Speedy delivery • Real Time Tracking • Convenient</p>
                </div><!-- /.rt-section-title-wrapper -->
            </div><!-- /.col-lg-8 -->
        </div><!-- ./row -->
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row align-items-center">
            <div class="col-lg-5 mb-0 mb-md-4">
                <!-- <div class="icon-thumb rt-mb-30 wow fadeInDown" data-wow-duration="1s">
                    <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/iconx-3.png" alt="icon-image" draggable="false">
                </div> -->
                <!-- /.icon-thumb -->
                <h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInDown" data-wow-duration="1s">WHAT IS GoPack?</h4>
                <div class="text-424 wow fadeInUp" data-wow-duration="1s">
                    <ul class="rt-list f-size-14">

                        <li><span class="rt-pr-4"></span> FiniGoo Introduced GoPack is an intra-city on-demand last mile technology platform that connects customers (individuals/businesses) with trucks (logistics service providers) to efficiently move goods.</li>

                    </ul>
                </div><!-- /.text-424 -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6 offset-lg-1">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/work-img-6.jpeg" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row align-items-center">
            <div class="col-lg-6 mb-0 mb-md-4">

                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/work-img-6bg1.jpg" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
                <!-- <?php echo base_url(); ?>landing-assets/assets/images/all-img/work-img-6bg1.jpg" -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-5 offset-lg-1">
                <!-- <div class="icon-thumb rt-mb-30 wow fadeInUp" data-wow-duration="1s">
                    <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/iconx-4.png" alt="icon-image" draggable="false">
                </div> -->
                <!-- /.icon-thumb -->
                <h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInUp" data-wow-duration="1s">HOW DOES GOPACK WORK?</h4>
                <div class="text-424 wow fadeInDown" data-wow-duration="1s">
                    <p class="f-size-14 text-424">
                        Download our app from Playstore or Appstore to book a Bike Or truck. We will match and send the nearest bike or truck driver available within 45 minutes prior to the pickup time. Track your goods while our trucks complete the trip and pay the suggested fare.

                    </p>
                </div><!-- /.text-424 -->


            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        <div class="row">

        </div><!-- ./row -->
        <div class="section-title-spacer"></div><!-- /.section-title-spacer -->
        <div class="row align-items-center">
            <div class="col-lg-5 mb-0 mb-md-4">
                <!-- <div class="icon-thumb rt-mb-30 wow fadeInDown" data-wow-duration="1s">
                    <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/iconx-3.png" alt="icon-image" draggable="false">
                </div> -->
                <!-- /.icon-thumb -->
                <h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInDown" data-wow-duration="1s">WHAT GOODS ARE PERMITTED ON GOPACK?</h4>
                <div class="text-424 wow fadeInUp" data-wow-duration="1s">
                    <ul class="rt-list f-size-14">

                        <li><span class="rt-pr-4"></span> We accept all goods that are not under our list of prohibited items and are suitable to be transported in our trucks based on payload and capacity requirements. To better match your requirements with the right truck, we classify goods under the following categories.</li>

                    </ul>
                </div><!-- /.text-424 -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6 offset-lg-1">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/all-img/gopack.png" alt="work image" draggable="false" class="wow zoomIn" data-wow-duration="2.5s">
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </div><!-- ./ copntainer -->
</section>
<!-- 
    ======== call to action start====
 -->
<div class="spacer-top"></div><!-- /.spacer-bottom -->
<div class="container" style="background-color:#F06400;border-radius:10px;padding:20px;">
    <div class="row" style="padding:50px;">
        <div class="col-lg-12">
            <h4>We categorize goods under the following to better match your requirement with the right truck.</h4></br>

            <div class="row">
                <div class="col-12 col-sm-4">
                    <ul class="cate_list">
                        <li>ELECTRICAL/ELECTRONICS</li>
                        <li>FURNITURE</li>
                        <li>FOOD & BEVERAGES</li>
                        <li>HOUSE SHIFTING</li>
                        <li>MACHINES / EQUIPMENT / SPARE PARTS WOOD/TIMBER/PLYWOOD</li>
                    </ul>
                </div>
                <div class="col-12 col-sm-4">
                    <ul class="cate_list">
                        <li>COURIER/MOVER AND PACKERS</li>
                        <li>VEHICLES/AUTOMOTIVE PARTS</li>
                        <li>CHEMICALS/PAINTS/OIL</li>
                        <li>TILES/CERAMICS / SANITARY-WARE</li>
                        <li>GLASSWARE</li>
                    </ul>
                </div>
                <div class="col-12 col-sm-4">
                    <ul class="cate_list">
                        <li>PIPES/METAL RODS (MORE THAN 7 FT)</li>
                        <li>PIPES/METAL RODS (LESS THAN 7 FT)</li>
                        <li>METAL SHEETS</li>
                        <li>GAS / COMMERCIAL CYLINDER</li>
                        <li>CONSTRUCTION MATERIALS</li>
                    </ul>
                </div>
            </div>


        </div>
    </div><!-- /.row -->
</div><!-- /.container -->