<!DOCTYPE html>
<html lang="en">

<head>
	<title>TrackOrder</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>landing-assets/login_css/main.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>landing-assets/assets/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<!--===============================================================================================-->
	<style>
		body {
			/* background: #bfbfbf; */
			background-image: url("<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/tracking-bg.jpg");
			/* background: #bfbfbf; */
			background-size: cover;
		}

		.flip-container {
			perspective: 1000;
		}

		.flipper {
			padding: 30px;
			/* height: 400px; */
			width: 450px;
			height: 525px;
			position: relative;
			background: #f5f5f5;
			-webkit-border-radius: 20px;
			-moz-border-radius: 20px;
			border-radius: 20px;

			transition: 0.6s;
			transform-style: preserve-3d;
			box-shadow: 0 10px 15px 0 rgba(0, 0, 0, .1) !important;
			background: #fff;
		}

		.front,
		.back {
			/*background-color: rgba(0,0,0,.3);*/
			position: absolute;
			padding: 40px 30px;
			top: 0;
			left: 0;
			right: 0;

			backface-visibility: hidden;
		}

		.front {
			z-index: 2;
			/* for firefox 31 */
			transform: rotateY(0deg);
		}

		.back {
			transform: rotateY(180deg);
		}

		.flip {
			transform: rotateY(180deg);
		}

		form {
			margin-top: 45px;
			/* width: 380px;
	margin: 4em auto;
	padding: 3em 2em 2em 2em;
	background: #fafafa;
	border: 1px solid #ebebeb;
	box-shadow: rgba(0,0,0,0.14902) 0px 1px 1px 0px,rgba(0,0,0,0.09804) 0px 1px 2px 0px; */
		}

		.group {
			position: relative;
			margin-bottom: 45px;
		}

		input {
			font-size: 18px;
			padding: 10px 10px 10px 5px;
			-webkit-appearance: none;
			display: block;
			background: #fff;
			color: #636363;
			width: 100%;
			border: none;
			border-radius: 0;
			border-bottom: 1px solid #757575;
		}

		input:focus {
			outline: none;
		}


		/* Label */

		.group label {
			color: #999;
			font-size: 16px;
			font-weight: normal;
			position: absolute;
			pointer-events: none;
			left: 5px;
			top: 10px;
			transition: all 0.2s ease;
			width: 100%;
		}


		/* active */

		input:focus~label,
		input.used~label {
			top: -20px;
			transform: scale(.75);
			left: -45px;
			/* font-size: 14px; */
			color: #4a89dc;
		}


		/* Underline */

		.bar {
			position: relative;
			display: block;
			width: 100%;
		}

		.bar:before,
		.bar:after {
			content: '';
			height: 2px;
			width: 0;
			bottom: 1px;
			position: absolute;
			background: #4a89dc;
			transition: all 0.2s ease;
		}

		.bar:before {
			left: 50%;
		}

		.bar:after {
			right: 50%;
		}


		/* active */

		input:focus~.bar:before,
		input:focus~.bar:after {
			width: 50%;
		}


		/* Highlight */

		.highlight {
			position: absolute;
			height: 60%;
			width: 100px;
			top: 25%;
			left: 0;
			pointer-events: none;
			opacity: 0.5;
		}


		/* active */

		input:focus~.highlight {
			animation: inputHighlighter 0.3s ease;
		}


		/* Animations */

		@keyframes inputHighlighter {
			from {
				background: #4a89dc;
			}

			to {
				width: 0;
				background: transparent;
			}
		}

		.title {
			font-weight: 400;
		}

		.forgot {
			cursor: pointer;
			width: 100%;
			text-align: right;
		}

		.forgot label {
			cursor: pointer;
		}

		.button {
			outline: none !important;
			border: none;
			padding: 20px 50px;
			border-radius: 50px;
			color: rgba(0, 0, 0, .26);
			background-color: rgba(0, 0, 0, .12);
			font-size: 18px;
		}

		.logn_btn {
			text-align: center;
			margin: 40px 0px;
		}

		.signin {
			font-size: 12px;
			margin-top: 10px;
		}

		.signin label a {
			font-size: 12px;
		}

		.signin label a:hover {
			color: #000;
		}

		.field-icon {
			position: absolute;
			right: 10px;
			top: 15px;
			cursor: pointer;
		}

		.f-logo {
			width: 200px;
			position: absolute;
			z-index: 2;
			padding: 15px;
		}
	</style>
</head>

<body>

	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<a href="<?php echo base_url(); ?>"> <img src="<?php echo base_url(); ?>landing-assets/assets/logo/finogoo-logo.png" class="f-logo"></a>
			</div>
			<div class="col-12 col-sm-6"></div>
			<div class="col-12 col-sm-6">
				<div class="">
				</div>
				<div class="limiter">
					<div class="container-login100">
						<div _ngcontent-bdw-c2="" class="backgroud-oval">
							<img _ngcontent-bdw-c2="" class="oval-back" src="<?php echo base_url(); ?>landing-assets/login_svg/back.svg">
							<img _ngcontent-bdw-c2="" class="oval-middle" src="<?php echo base_url(); ?>landing-assets/login_svg/middle.svg">
							<img _ngcontent-bdw-c2="" class="oval-front" src="<?php echo base_url(); ?>landing-assets/login_svg/front.svg">
						</div>




						<div class="flip-container">
							<div class="flipper" id="flipper" style="height: 400px;">
								<div class="front">
									<h1 class="title">TRACK YOUR ORDER</h1>
									<!-- <label style="font-size: 12px;">Enter your details below here</label> -->
									<form>
										<div class="group">
											<input type="text"><span class="highlight"></span><span class="bar"></span>
											<label>Client Order Id</label>
										</div>

										<div class="logn_btn">
											<button type="button" class="button buttonBlue" style="color: #ffffff;background-color: #ed1d24;">
												TRACK ORDER
											</button>
											<!-- <div class="signin">
									<label>New User? <a href="#">Sign Up</a></label>
								</div> -->
										</div>
									</form>
								</div>


							</div>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>






	<!-- <script>
		
		var loginButton = document.getElementById("loginButton");
var registerButton = document.getElementById("registerButton");

loginButton.onclick = function(){
	document.querySelector("#flipper").classList.toggle("flip");
}

registerButton.onclick = function(){
	document.querySelector("#flipper").classList.toggle("flip");
}

	</script> -->

	<script>
		$(window, document, undefined).ready(function() {

			$('input').blur(function() {
				var $this = $(this);
				if ($this.val())
					$this.addClass('used');
				else
					$this.removeClass('used');
			});

			var $ripples = $('.ripples');

			$ripples.on('click.Ripples', function(e) {

				var $this = $(this);
				var $offset = $this.parent().offset();
				var $circle = $this.find('.ripplesCircle');

				var x = e.pageX - $offset.left;
				var y = e.pageY - $offset.top;

				$circle.css({
					top: y + 'px',
					left: x + 'px'
				});

				$this.addClass('is-active');

			});

			$ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
				$(this).removeClass('is-active');
			});

		});
	</script>

	<script>
		$(".toggle-password").click(function() {

			$(this).toggleClass("fa-eye fa-eye-slash");
			var input = $($(this).attr("toggle"));
			if (input.attr("type") == "password") {
				input.attr("type", "text");
			} else {
				input.attr("type", "password");
			}
		});
	</script>
</body>

</html>