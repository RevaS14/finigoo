<!-- 
    !============= Footer Area Start ===========!
 -->
<section class="rt-site-footer" data-scrollax-parent="true">
    <div class="rt-shape-emenetns-1" style="background-image: url(<?php echo base_url(); ?>landing-assets/assets/images/shape-elements/shape-4.png)" data-scrollax="properties: { translateY: '340px' }"></div>
    <!-- /.rt-shape-emenetns-1 -->
    <div class="footer-top rtbgprefix-cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="rt-single-widget wow fade-in-bottom" data-wow-duration="1s">
                        <h3 class="rt-footer-title"><img src="<?php echo base_url(); ?>landing-assets/assets/logo/finogoo-logo.png"></h3>
                        <!-- /.rt-footer-title -->
                    </div>
                    <!-- /.rt-single-widge -->
                </div>
                <!-- /.col-lg-3-->
                <div class="col-lg-3 col-md-6">
                    <div class="rt-single-widget wow fade-in-bottom" data-wow-duration="1.5s">
                        <!-- <h3 class="rt-footer-title">Work With Us</h3> -->
                        <ul class="rt-usefulllinks">
                            <li><a href="<?php echo base_url(); ?>aboutus" style="list-style-type: none;">About Us</a>
                            </li>
                            <!-- <li><a href="#" style="list-style-type: none;">Services</a></li> -->
                            <li><a href="<?php echo base_url(); ?>career" style="list-style-type: none;">Careers</a>
                            </li>
                            <li><a href="<?php echo base_url(); ?>goads" style="list-style-type: none;">Go Ads</a>
                            </li>
                            <li><a href="<?php echo base_url(); ?>press-meet">Press Release</a>
                            </li>
                            <!-- <li><a href="#">Retirement Plan</a></li>
                            <li><a href="#">Travel APIs</a></li> -->
                        </ul>
                    </div>
                    <!-- /.rt-single-widget -->
                </div>
                <!-- /.col-lg-3-->
                <div class="col-lg-3 col-md-6">
                    <div class="rt-single-widget wow fade-in-bottom" data-wow-duration="2s">
                        <!-- <h3 class="rt-footer-title">
                            My Account
                        </h3> -->
                        <ul class="rt-usefulllinks">
                            <!-- <li><a href="#">Help Centre</a></li> -->
                            <li><a href="<?php echo base_url(); ?>contact-us">Contact Us</a>
                            </li>
                            <!-- <li><a href="#">Driver Help Centre</a></li> -->
                            <!-- <li><a href="#">Terms & Conditions</a></li> -->
                            <li><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a>
                            </li>
                            <li><a href="<?php echo base_url(); ?>safety">Safety    </a>
                            </li>
                        </ul>
                        <!-- /.rt-usefulllinks -->
                    </div>
                    <!-- end single widget -->
                </div>
                <!-- /.col-lg-3-->
                <div class="col-lg-3 col-md-6" id="social_icon">
                    <div class="rt-single-widget wow fade-in-bottom" data-wow-duration="2.5s">
                        <h3 class="rt-footer-title" id="follow_us">Follow Us On</h3>
                        <a href="#"><img src="<?php echo base_url(); ?>landing-assets/assets/logo/fb.jpg" /></a>
                        <a href="#"><img src="<?php echo base_url(); ?>landing-assets/assets/logo/twitter.jpg" src="#" /></a>
                        <a href="#"><img src="<?php echo base_url(); ?>landing-assets/assets/logo/youtube.jpg" src="#" /></a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="col-sm-12" id="fin_info">
                <p>© 2020 Finigoo</p>
            </div>
        </div>
        <div class="container-fluid " id="ftr_1">
            <div class="col-md-12" id="ftr_2">
                <img src="<?php echo base_url(); ?>landing-assets/assets/images/flights/brush_banner.png">
            </div>
        </div>
</section>
<div class="submenufix" id="tr">
    <div class="row container" id="main-menu">
        <ul class="dropdown_bg" style="margin-bottom: 0px !important;padding: 0;background-image: linear-gradient(to right, rgba(246,248,250,1) 40%, rgba(246,248,250,0.5)), url(<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/services_banner.jpg);">
            <div class="grabnav-tab-container">
                <div class="grabnav-tab-menu">
                    <div class="list-group"> <a href="#" class="list-group-item text-center active" data-bg="<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/services_banner.jpg">Services</a>
                        <a href="" class="list-group-item text-center " data-bg="<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/aboutus_banner.jpg">About Us</a>
                        <a href="" class="list-group-item text-center " data-bg="<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/helpcentre_banner.jpg">Help Centre</a>
                        <a href="" class="list-group-item text-center " data-bg="<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/joinus_banner.jpg">Join Us</a>
                        <a href="" class="list-group-item text-center " data-bg="<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/career_banner.jpg">Career</a>
                        <a href="" class="list-group-item text-center " data-bg="<?php echo base_url(); ?>landing-assets/assets/images/backgrounds/FranchiseLogin_banner.jpg" style="background: #01AB00;
                            color: #fff;">Franchise Login <i class="fa fa-arrow-right arrow-animate" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="grabnav-tab">
                    <div class="grabnav-tab-content active">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <ul class="sub-menu">
                                        <li class="menu-item"><a href="#" class="title_submenu clickmenuitem">Transport &amp; Logistics</a>
                                            <ul class="sub-menu">
                                                <li> <a href="<?php echo base_url(); ?>bike">
                                                        <img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/8.png" alt="box-icon" draggable="false">
                                                        <label>Go Bike<br />
                                                            <span>Beat the Traffic, Save Time</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                <li> <a href="<?php echo base_url(); ?>cabs"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/4.png" alt="box-icon" draggable="false">
                                                        <label>Go Cabs<br />
                                                            <span>Travel in Comfort, at Your Convenience</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                <li> <a href="<?php echo base_url(); ?>goauto"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/1.png" alt="box-icon" draggable="false">
                                                        <label>Go Auto<br />
                                                            <span>Local.. Reliable.. Safe !</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                <li> <a href="<?php echo base_url(); ?>gopack"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/6.png" alt="box-icon" draggable="false">
                                                        <label> Go Pack<br />
                                                            <span>On Demand Delivery at Your Doorstep</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                <li> <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/10.png" alt="box-icon" draggable="false">
                                                        <label> Go Cargo<br />
                                                            <span>...</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                <li> <a href="<?php echo base_url(); ?>send"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/gosend.png" alt="box-icon" draggable="false">
                                                        <label> Go Send<br />
                                                            <span>Deliveries for Your Business</span>
                                                        </label>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item"><a href="#" class="title_submenu clickmenuitem">Food &amp; FMCG</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?php echo base_url(); ?>gofood"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/3.png" alt="box-icon" draggable="false">
                                                        <label> Go Food<br />
                                                            <span>Get Your Food in Less Than an Hour</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                <li><a href="<?php echo base_url(); ?>gomart"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/15.png" alt="box-icon" draggable="false">
                                                        <label> Go Mart<br />
                                                            <span>Go Mart Delivery at Your Doorstep</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                <li><a href="<?php echo base_url(); ?>gobeauty"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/7.png" alt="box-icon" draggable="false">
                                                        <label> Go Beauty<br />
                                                            <span>Smart Salon at Home & Wellness</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                <li><a href="<?php echo base_url();?>gomedical"><img src="<?php echo base_url();?>landing-assets/assets/images/icons-image/gomedicine.png" alt="box-icon" draggable="false" >
                                                    <label> Go Medicine<br/>
                                                        <span>...</span>
                                                    </label>
                                                </a>
                                            </li>
                                            </ul>
                                        </li>
                                        <!-- <li class="menu-item"><a href="#" class="title_submenu clickmenuitem">Go Ads</a>
                                                <ul class="sub-menu">
                                                    <li><a href="Goads.html"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/GoAds.png" alt="box-icon" draggable="false" >
                                                       <label>  Go Ads<br/>
                                                        <span>Your Service Experts</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                </ul>
                                            </li> -->
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <ul class="sub-menu">
                                        <li class="menu-item"><a href="#" class="title_submenu clickmenuitem">Payments</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?php echo base_url(); ?>gokiosk"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/gokiosk.png" alt="box-icon" draggable="false">
                                                        <label> Go Kiosk <br />
                                                            <span>All-round Digital Shop</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                <li><a href="<?php echo base_url(); ?>bills"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/11.png" alt="box-icon" draggable="false">
                                                        <label> Go Bills<br />
                                                            <span>Fastest & Simplest Way for all you Bills.</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                <li><a href="<?php echo base_url(); ?>gopoints"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/12.png" alt="box-icon" draggable="false">
                                                        <label> Go Point<br />
                                                            <span>Earn Points for Every Order</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                <li><a href="<?php echo base_url(); ?>gopay"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/2.png" alt="box-icon" draggable="false">
                                                        <label> Go Pay<br />
                                                            <span>Go Mart Delivery at Your Doorstep</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                <li><a href="<?php echo base_url(); ?>goupi "><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/9.png" alt="box-icon" draggable="false">
                                                        <label> Go UPI<br />
                                                            <span>India's Payment App</span>
                                                        </label>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item"><a href="#" class="title_submenu clickmenuitem">Business</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?php echo base_url(); ?>godemand"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/14.png" alt="box-icon" draggable="false">
                                                        <label> Go Demand<br />
                                                            <span>Your Service Experts</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                <!-- <li><a href="GoMart.html">&nbsp;&nbsp;Go Mart</a></li> -->
                                            </ul>
                                        </li>
                                        <li class="menu-item"><a href="#" class="title_submenu clickmenuitem">Loan</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?php echo base_url(); ?>goloan"><img src="<?php echo base_url(); ?>landing-assets/assets/images/icons-image/goloan.png" alt="box-icon" draggable="false">
                                                        <label> Go Loan<br />
                                                            <span>...</span>
                                                        </label>
                                                    </a>
                                                </li>
                                                <!-- <li><a href="GoMart.html">&nbsp;&nbsp;Go Mart</a></li> -->
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grabnav-tab-content ">
                        <div class="item-link">
                            <h4><a href="<?php echo base_url(); ?>aboutus">About Us</a></h4>
                            <p>OUR TEAM OF LEADERS WITH A VISION TO REACH PEOPLE FROM EVERY WALK OF LIFE</p>
                        </div>
                    </div>
                    <div class="grabnav-tab-content ">
                        <div class="item-link">
                            <h4><a href="<?php echo base_url(); ?>help">Help Centre</a></h4>
                            <p>No one is useless in this world who lightens the burdens of anothe</p>
                        </div>
                    </div>
                    <div class="grabnav-tab-content ">
                        <div class="item-link">
                            <h4><a href="<?php echo base_url(); ?>godriver-tab">Join Us</a></h4>
                            <p>I hope someday you'll join us, and the world will be as one</p>
                        </div>
                    </div>
                    <div class="grabnav-tab-content ">
                        <div class="item-link">
                            <h4><a href="<?php echo base_url(); ?>career">Career</a></h4>
                            <p>We are driven by our mission to simplify and improve the lives of people and build an awesome organisation that inspire</p>
                        </div>
                    </div>
                    <div class="grabnav-tab-content ">
                        <div class="item-link">
                            <h4><a href="<?php echo base_url(); ?>franchise">Franchise Login</a></h4>
                            <p>Do not wait to strike till the iron is hot, but make it hot by striking.</p>
                        </div>
                        <div class="item-link">
                            <h4><a href="<?php echo base_url(); ?>pricing">Pricing</a></h4>
                            <p>Do not wait to strike till the iron is hot, but make it hot by striking.</p>
                        </div>
                    </div>
                </div>
            </div>
        </ul>
    </div>
    <!-- ==================Start Js Link===================== -->
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/jquery-2.2.4.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/instafeed.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/jquery.scrollUp.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/TweenMax.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/scrollax.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3&amp;key=AIzaSyCy7becgYuLwns3uumNm6WdBYkBpLfy44k"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/wow.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/jquery.overlayScrollbars.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/jquery-ui.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/jquery.appear.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/slick.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/slider-range.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/vivus.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/tippy.all.min.js"></script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/app.js"></script>
    <!-- ==================End Js Link===================== -->
    <style>
        .closedicon {
            display: block;
            position: absolute;
            right: 25px;
            color: red;
            font-weight: bold;
            font-size: 20px;
            border: 1px solid red;
            width: 28px;
            height: 28px;
            top: 30px;
            z-index: 999999999;
            text-align: center;
        }

        .show {
            display: block !important;
        }

        .hide {
            display: none !important;
        }

        div.grabnav-tab {
            float: left;
            width: calc(85% - 338px);
        }

        #main-menu {
            margin: auto;
        }

        .submenufix .sub-menu li {
            list-style: none;
            color: black;
            line-height: 2.5;
        }

        .submenufix .sub-menu li a {
            color: black;
        }

        .title_submenu {
            font-size: 20px;
            color: #e82801 !important;

        }
    </style>
    <script>
        $(document).ready(function(e) {
            var count = 1;

            function transition() {
                count++;
                $('.position-' + count.toString()).trigger('click');
                if (count == 10) {
                    count = 1;
                }
            }
            //setInterval(transition, 3000);



            $('.serviceclick').click(function() {
                if ($('.submenufix').hasClass('show')) {
                    $('.submenufix').removeClass('show');
                    $('.main-header').removeClass('rt-sticky-active');

                } else {
                    $('.submenufix').addClass('show');
                    $('.main-header').addClass('rt-sticky-active');
                }
                $('.clickmenuitem').trigger('click');
                if ($(window).width() < 768) {
                    $('.closedicon').show();
                    $('.mobileloadopen').show();
                    $('.clickmenuitem').next().addClass('hide');
                }
            });


            $('.clickmenuitem').click(function() {
                if ($(window).width() < 768) {
                    $('.closedicon').show();
                    $('.mobileloadopen').show();
                    $('.clickmenuitem').next().addClass('hide');
                    $(this).next().removeClass('hide');
                }
            });

            $(window).resize(function() {
                if ($(window).width() < 768) {
                    $('.clickmenuitem').next().addClass('hide');
                } else {
                    $('.closedicon').hide();

                    $('.mobileloadopen').hide();
                    $('.clickmenuitem').next().removeClass('hide');
                }
            });

            $('.closedicon').click(function() {

                if ($('.submenufix').hasClass('show')) {
                    $('.submenufix').removeClass('show');
                    $('.main-header').removeClass('rt-sticky-active');

                } else {
                    $('.submenufix').addClass('show');
                    $('.main-header').addClass('rt-sticky-active');
                }
            });

            $('.closedicon').hide();
            $('.mobileloadopen').hide();
        });
    </script>
    <script>
        var a = document.getElementById("trs");
        a.addEventListener("focus", myFocusFunction, true);

        function myFocusFunction() {
            document.getElementById("tr").style.backgroundColor = "pink";
            document.getElementsByClassName("sub-menu").style.color = "white";

        }
    </script>
    <script>
        var a = document.getElementById("trg");
        a.addEventListener("focus", myFocusFunction, true);

        function myFocusFunction() {
            document.getElementById("tr").style.backgroundColor = "#fee685";

        }
    </script>
    <script>
        var z = document.getElementById("trf");
        z.addEventListener("focus", myFocusFunction, true);

        function myFocusFunction() {
            document.getElementById("tr").style.backgroundColor = "orange";
        }
    </script>
    <script>
        var y = document.getElementById("trd");
        y.addEventListener("focus", myFocusFunction, true);

        function myFocusFunction() {
            document.getElementById("tr").style.backgroundColor = "#01AEF3";
        }
    </script>
    <script>
        $(document).ready(function() {

            $(".menu-item-has-children").click(function() {
                $(".main-header").css({
                    "position": "fixed",
                    "width": "100%",
                    "background-color": "white",
                    "animation-name": "fade-out"
                });
            });

        });
    </script>
    <script>
        $('.dropdown-menu').on({
            "click": function(e) {
                e.stopPropagation();
            }
        });
        $(document).ready(function() {
            $("div.grabnav-tab-menu>div.list-group>a").click(function(e) {
                e.preventDefault();
                var bg = $(this).attr('data-bg');
                $('.dropdown_bg').css('background-image', 'linear-gradient(to right, rgba(246,248,250,1) 40%, rgba(246,248,250,0.5)), url(' + bg + ')');

                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.grabnav-tab>div.grabnav-tab-content").removeClass("active");
                $("div.grabnav-tab>div.grabnav-tab-content").eq(index).addClass("active");
            });
        });
    </script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wp_ulike_params = {
            "ajax_url": "https:\/\/www.grab.com\/sg\/wp-admin\/admin-ajax.php",
            "notifications": "0"
        };
        /* ]]> */
    </script>
    <script>
        $(document).ready(function() {
            $('.menu_icon').click(function() {
                //  $('#display_advance').toggle('1000');
                $("i", this).toggleClass("fa-times fa-bars");
                $(".bars").toggleClass("display_none")
            });
        });
    </script>
    <script type="text/javascript">
        function DropDown(el) {
            this.dd = el;
            this.placeholder = this.dd.children('span');
            this.opts = this.dd.find('ul.dropdown > li');
            this.val = '';
            this.index = -1;
            this.initEvents();
            // this.addClass = this.dd.find('ul.dropdown > li');
        }
        DropDown.prototype = {
            initEvents: function() {
                var obj = this;

                obj.dd.on('click', function(event) {
                    $(this).toggleClass('active');

                    return false;
                });

                obj.opts.on('click', function() {
                    debugger;
                    var opt = $(this);
                    obj.val = opt.text();
                    obj.index = opt.index();
                    obj.placeholder.text(obj.val);

                    // $(this).addClass('dropdd');
                    $(this).addClass('dropdown_active').siblings().removeClass('dropdown_active');

                });
            },
            getValue: function() {
                return this.val;
            },
            getIndex: function() {
                return this.index;
            }
        }

        $(function() {

            var dd = new DropDown($('#dd'));

            $(document).click(function() {
                // all dropdowns
                $('.wrapper-dropdown-3').removeClass('active');
            });

        });
    </script>
    <script src="<?php echo base_url(); ?>landing-assets/assets/js/jquery.flagstrap.js"></script>
    <script>
        $('#options').flagStrap({
            countries: {
                "IN": "India",
                "TH": "Thailand",
                "SG": "Singapore",
                "ID": "Indonesia",
            },
            buttonSize: "btn-sm",
            buttonType: "btn-info",
            labelMargin: "10px",
            scrollable: false,
            scrollableHeight: "350px"
        });
    </script>
    <script>
        $(document).ready(function() {
            // $('.mfp-iframe').attr('src', "jksdafjksa");
            $(".playVideo").click(function(e) {
                e.preventDefault();
                $(".mfp-iframe").attr("src", $(this).attr("href"));
            })

        });
    </script>
    <script>
        var $button = $('#menu-btn');

        $button.on('click', function(e) {
            e.preventDefault();
            if ($button.hasClass('open')) {
                $button.removeClass('open');
                $button.addClass('close-i');
            } else {
                $button.removeClass('close-i');
                $button.addClass('open');
            }
        });
    </script>
    </body>

    </html>