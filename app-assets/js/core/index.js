$(function() {
	var $wrapper = $('#wrapper');

	// theme switcher
	var theme_match = String(window.location).match(/[?&]theme=([a-z0-9]+)/);
	var theme = (theme_match && theme_match[1]) || 'default';
	var themes = ['default','legacy','bootstrap2','bootstrap3'];
	$('head').append('<link rel="stylesheet" href="../app-assets/css/selectize.' + theme + '.css">');

	var $themes = $('<div>').addClass('theme-selector').insertAfter('h1');
	for (var i = 0; i < themes.length; i++) {
		//$themes.append('<a href="?theme=' + themes[i] + '"' + (themes[i] === theme ? ' class="active"' : '') + '>' + themes[i] + '</a>');
	}

	// display scripts on the page
	$('script', $wrapper).each(function() {
		var code = this.text;
		if (code && code.length) {
			var lines = code.split('\n');
			var indent = null;

			for (var i = 0; i < lines.length; i++) {
				if (/^[	 ]*$/.test(lines[i])) continue;
				if (!indent) {
					var lineindent = lines[i].match(/^([ 	]+)/);
					if (!lineindent) break;
					indent = lineindent[1];
				}
				lines[i] = lines[i].replace(new RegExp('^' + indent), '');
			}

			var code = $.trim(lines.join('\n')).replace(/	/g, '    ');
			var $pre = $('<pre>').addClass('js').text(code);
			$pre.insertAfter(this);
		}
	});

	// show current input values
	$('select.selectized,input.selectized', $wrapper).each(function() {
		//var $container = $('<div>').addClass('value').html('Current Value: ');
		//var $value = $('<span>').appendTo($container);
						
		var $input = $(this);

		var update = function(e) { 
			//$value.text(JSON.stringify($input.val())); 
			id = $input.val();
			url = '/index.php/api/nearby-drivers-zone';
			// url = '/quicklar/index.php/api/nearby-drivers-zone';
			var token = $("#token").val();

			var mapOptions = {
				center: new google.maps.LatLng(11.0168,76.9558),
				zoom: 10,
				zoomControl: true,
				disableDefaultUI: true,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};

			var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);  
			var image = 'http://quicklar.com/assets/images/img_car_micro.png';
			var locations ='';
			$.ajax({
				type: 'post',
				headers: {"X-Authorization": token },
				dataType: 'json', 
				url: url,
				data: {id: id},
				success: function (drivers) {
					//alert(drivers);
					var driversList  = ''
			
					$.each( drivers.drivers, function( driverIndex, driver ) {
						
						if(driver!=''){
							driversList += `<tr>
								<td>`+(driverIndex+1)+`</td>
								<td><img width="30px" height="30px" class="rounded-circle" src="http://quicklar.com/`+driver['DisplayPicture']+`"></td>
								<td> <h5 style="margin-bottom: 0">`+driver['Name']+`</h5></td>
								<td> <a href="http://quicklar.com/admin/listdrivers/details?view=`+driver['UserId']+`&job=`+driver['JobCategory_JobCategoryId']+`"> <i class="fa fa-eye" aria-hidden="true"></i> </a> 
									<a href="http://quicklar.com/admin/view-driver/`+driver['UserId']+`" style="margin-left:15px">
									<i class="fa fa-superpowers" aria-hidden="true"></i>
								</a>

								</td>
								<td><a href=""><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a></td>
							</tr>`;
						}
						
						if(driver['location']!='') {
							new google.maps.Marker({
								position: new google.maps.LatLng(driver['location'][0]['coordinates'][0], driver['location'][0]['coordinates'][1]),
								map: map,
								icon: image
							});
						}
					});
		
					
					//console.log(driversList)
					$("#near-drivers").html(driversList);
					//alert(result);
				}
			 });

		}

		$(this).on('change', update);
		update();

		//$container.insertAfter($input);
	});
});
