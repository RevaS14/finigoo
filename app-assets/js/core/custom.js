$(document).ready(function ($) {

            $(document).on('click', '.collect', function (e) {
               e.preventDefault();
               var id = $(this).attr('id');
               swal({
                     title: "Are you sure? Collect Money?",
                     text: "if press yes then money will colleted!",
                     type: "warning",
                     showCancelButton: true,
                     confirmButtonClass: "btn-danger",
                     confirmButtonText: "Yes",
                     cancelButtonText: "No",
                     closeOnConfirm: false,
                     closeOnCancel: true
                  },
                  function (isConfirm) {
                     if (isConfirm) {
                           $.ajax({
                              type: 'post',
                              url: '/finigoo-web/admin/collectamount',
                              data: {id: id},
                              success: function (result) {
                                    swal("Success", "Amount collected successfully..", "success");
                                    location.reload();
                                    }
                           });
                     } 
                  });
         });

         $(document).on('click', '.request-accept', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            var status = 'A';
            swal({
                  title: "Are you sure? Accept Request?",
                  text: "if press yes then request will accepted!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: true
               },
               function (isConfirm) {
                  if (isConfirm) {
                        $.ajax({
                           type: 'post',
                           url: '/finigoo-web/admin/change-withdraw-status',
                           data: {id: id, status: status},
                           success: function (result) {
                                 swal("Success", "Request accepted successfully..", "success");
                                 location.reload();
                                 }
                        });
                  } 
               });
         });

         $(document).on('click', '.request-cancel', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            var status = 'C';
            swal({
                  title: "Are you sure? Cancel Request?",
                  text: "if press yes then request will be cancelled!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: true
               },
               function (isConfirm) {
                  if (isConfirm) {
                        $.ajax({
                           type: 'post',
                           url: '/finigoo-web/admin/change-withdraw-status',
                           data: {id: id, status: status},
                           success: function (result) {
                                 swal("Success", "Request cancelled successfully..", "success");
                                 location.reload();
                                 }
                        });
                  } 
               });
         });

         $(document).on('click', '.category-delete', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            swal({
                  title: "Are you sure? Delete Category?",
                  text: "if press yes then category will be deleted!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: true
               },
               function (isConfirm) {
                  if (isConfirm) {
                        $.ajax({
                           type: 'post',
                           url: '/finigoo-web/admin/delete-provider-category',
                           data: {id: id},
                           success: function (result) {
                                 swal("Success", "Category deleted successfully..", "success");
                                 location.reload();
                                 }
                        });
                  } 
               });
         });

         $(document).on('click', '.sub-category-delete', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            swal({
                  title: "Are you sure? Delete Category?",
                  text: "if press yes then category will be deleted!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: true
               },
               function (isConfirm) {
                  if (isConfirm) {
                        $.ajax({
                           type: 'post',
                           url: '/finigoo-web/partner/delete-category',
                           data: {id: id},
                           success: function (result) {
                                 swal("Success", "Category deleted successfully..", "success");
                                 location.reload();
                                 }
                        });
                  } 
               });
         });


         $(document).on('click', '.item-delete', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            swal({
                  title: "Are you sure? Delete Item?",
                  text: "if press yes then item will be deleted!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: true
               },
               function (isConfirm) {
                  if (isConfirm) {
                        $.ajax({
                           type: 'post',
                           url: '/finigoo-web/partner/delete-item',
                           data: {id: id},
                           success: function (result) {
                                 swal("Success", "Item deleted successfully..", "success");
                                 location.reload();
                                 }
                        });
                  } 
               });
         });

         $(document).on('click', '.delete-action', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            url = $(this).attr('href');
            swal({
                  title: "Are you sure? Delete?",
                  text: "if press yes then it will be deleted!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: true
               },
               function (isConfirm) {
                  if (isConfirm) {
                        $.ajax({
                           type: 'post',
                           url: url,
                           data: {id: id},
                           success: function (result) {
                                 swal("Success", "Deleted successfully..", "success");
                                 location.reload();
                                 }
                        });
                  } 
               });
         });

         $(document).on('click', '.approve-status', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            url = $(this).attr('href');
            status = $(this).attr('name');

            if(status=='A') {
               title = "Are you sure? Accept Request?";
               text = "if press yes then request will be accepted!";
               message = "Request accepted successfully..";
            } else {
               title = "Are you sure? Want to Cancel Request?";
               text = "if press yes then request will be cancelled!";
               message = "Request cancelled successfully..";
            }
            // var status = 'A';
            swal({
                  title: title,
                  text: text,
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: true
               },
               function (isConfirm) {
                  if (isConfirm) {
                        $.ajax({
                           type: 'post',
                           url: url,
                           data: {id: id, status: status},
                           success: function (result) {

                                 swal("Success", message, "success");
                                 location.reload();
                                 }
                        });
                  } 
               });
         });

      
   });